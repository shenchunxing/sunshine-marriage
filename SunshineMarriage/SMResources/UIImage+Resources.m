//
//  UIImage+Resources.m
//  Pods-YCResources_Example
//
//  Created by haima on 2019/4/3.
//

#import "UIImage+Resources.h"

@implementation UIImage (Resources)

+ (UIImage *)yc_imageWithNamed:(NSString *)name {
    
    return [self yc_imageWithNamed:name type:@"png"];
}
    
+ (UIImage *)yc_imageWithNamed:(NSString *)name type:(NSString *)type {
  
    NSString *mainBundlePath = [NSBundle mainBundle].bundlePath;
    NSString *bundlePath = [NSString stringWithFormat:@"%@/%@",mainBundlePath,@"YCResources.bundle"];
    NSBundle *bundle = [NSBundle bundleWithPath:bundlePath];
    if (bundle == nil) {
        bundlePath = [NSString stringWithFormat:@"%@/%@",mainBundlePath,@"Frameworks/YCResources.framework/YCResources.bundle"];
        bundle = [NSBundle bundleWithPath:bundlePath];
    }
    if ([UIImage respondsToSelector:@selector(imageNamed:inBundle:compatibleWithTraitCollection:)]) {
        return [UIImage imageNamed:name inBundle:bundle compatibleWithTraitCollection:nil];
    } else {
        return [UIImage imageWithContentsOfFile:[bundle pathForResource:name ofType:type]];
    }
    return nil;
}

@end
