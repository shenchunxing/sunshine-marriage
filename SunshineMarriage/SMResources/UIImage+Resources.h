//
//  UIImage+Resources.h
//  Pods-YCResources_Example
//
//  Created by haima on 2019/4/3.
//

#import <UIKit/UIKit.h>

#define ResourceImage(named) [UIImage yc_imageWithNamed:named];

@interface UIImage (Resources)

/**
 从YCResources.bundle中获取图片，默认png类型

 @param name 图片名
 @return image
 */
+ (UIImage *)yc_imageWithNamed:(NSString *)name;

/**
 从YCResources.bundle中获取图片

 @param name 图片名
 @param type 图片类型，png、jpg等
 @return 图片
 */
+ (UIImage *)yc_imageWithNamed:(NSString *)name type:(NSString *)type;


    
@end

