//
//  SMTooL.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/24.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMTooL.h"
#import "SMAlertView.h"
#import "NSObject+Helper.h"
#import "UIViewController+CustomToast.h"

#define cachePath [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject]

@implementation SMTooL

+ (void)clearCacheTitle:(NSString *)title content:(NSString *)content {
    SMAlertView *alert = [[SMAlertView alloc] initWithTitle:title content:content confirmHandle:^{
        [self clearCaches];
        [[self yc_getCurrentViewController] showCustomMessage_SM:@"缓存已清除"];
    }];
    [alert show];
}

+ (void)findNewVerisonWithTitle:(NSString *)title content:(NSString *)content jumpBlock:(void(^)(void))jumpBlock {
    SMAlertView *alert = [[SMAlertView alloc] initWithTitle:title content:content confirmHandle:^{
        jumpBlock();
    }];
    [alert show];
}

+ (void)loginOutWithTitle:(NSString *)title content:(NSString *)content loginOutBlock:(void(^)(void))loginOutBlock {
    SMAlertView *alert = [[SMAlertView alloc] initWithTitle:title content:content confirmHandle:^{
        loginOutBlock();
    }];
    [alert show];
}

+ (void)toolWithTitle:(NSString *)title content:(NSString *)content confirmHandle:(void(^)(void))confirmHandle {
    SMAlertView *alert = [[SMAlertView alloc] initWithTitle:title content:content confirmHandle:^{
        confirmHandle();
    }];
    [alert show];
}

+ (void)clearCaches{
     NSArray *subpathArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:cachePath error:nil];
       NSError *error = nil;
       NSString *filePath = nil;
       BOOL flag = NO;
       for (NSString *subpath in subpathArray) {
           filePath = [cachePath stringByAppendingPathComponent:subpath];
           if ([[NSFileManager defaultManager] fileExistsAtPath:cachePath]) {
               BOOL isRemoveSuccessed = [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
               if (isRemoveSuccessed) { // 删除成功
                   flag = YES;
               }
           }
       }
}

@end
