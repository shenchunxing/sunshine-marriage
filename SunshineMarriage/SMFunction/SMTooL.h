//
//  SMTooL.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/24.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SMTooL : NSObject

+ (void)clearCacheTitle:(NSString *)title content:(NSString *)content ;
+ (void)findNewVerisonWithTitle:(NSString *)title content:(NSString *)content jumpBlock:(void(^)(void))jumpBlock;
+ (void)loginOutWithTitle:(NSString *)title content:(NSString *)content loginOutBlock:(void(^)(void))loginOutBlock ;
+ (void)toolWithTitle:(NSString *)title content:(NSString *)content confirmHandle:(void(^)(void))confirmHandle ;
@end

NS_ASSUME_NONNULL_END
