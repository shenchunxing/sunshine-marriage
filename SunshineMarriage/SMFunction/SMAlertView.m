//
//  YCAlertView.m
//  YCBaseUI
//
//  Created by 刘成 on 2019/4/17.
//

#import "SMAlertView.h"
#import "YCCategoryModule.h"
#import "SMDefine.h"
#import "UIFont+Style.h"
#import "UIColor+Style.h"

@interface SMAlertView ()

@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *content;

@property (copy, nonatomic) YCAlertHandle leftHanlde;
@property (copy, nonatomic) YCAlertHandle rightHanlde;

@property (strong, nonatomic) UIView *popView;
@property (strong, nonatomic) UIView *backView;
@property (nonatomic, strong) UIButton *leftBtn;
@property (nonatomic, strong) UIButton *rightBtn;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *contentLabel;

@property (nonatomic, strong) UIFont *contentFont;

@end

@implementation SMAlertView

+ (instancetype)bs_initWithTitle:(NSString * _Nullable)title confirmHandle:(YCAlertHandle _Nullable)confirmHandle {
    SMAlertView *alertView = [[SMAlertView alloc] initWithTitle:title confirmHandle:confirmHandle];
    [alertView configureColor:[UIColor yc_hex_9B9EA8] rightColor:[UIColor yc_hex_F8513B] leftFont:nil rightFont:[UIFont yc_18_bold] titleColor:[UIColor yc_hex_333440] titleFont:nil];
    return alertView;
}

+ (void)showBS_ErrorWithTitle:(NSString * _Nullable)title {
    SMAlertView *alertView = [[SMAlertView alloc] initWithTitle:title content:nil leftTitle:nil rightTitle:@"确定" leftHandle:nil rightHandle:nil];
    [alertView configureColor:[UIColor yc_hex_9B9EA8] rightColor:[UIColor yc_hex_F8513B] leftFont:nil rightFont:[UIFont yc_18] titleColor:[UIColor yc_hex_333440] titleFont:nil];
    [alertView show];
}

+ (void)showWithTitle:(NSString * _Nullable)title rightTitle:(NSString *)rightTitle rightHandle:(YCAlertHandle)rightHandle {
    SMAlertView *alertView = [[SMAlertView alloc] initWithTitle:title content:nil leftTitle:nil rightTitle:rightTitle leftHandle:nil rightHandle:rightHandle];
    [alertView configureColor:[UIColor yc_hex_9B9EA8] rightColor:[UIColor yc_hex_F8513B] leftFont:nil rightFont:[UIFont yc_18] titleColor:[UIColor yc_hex_333440] titleFont:nil];
    [alertView show];
}

- (instancetype)initWithTitle:(NSString *)title confirmHandle:(YCAlertHandle)confirmHandle{
    return [self initWithTitle:title content:nil leftTitle:@"取消" rightTitle:@"确定" leftHandle:nil rightHandle:confirmHandle];
}


- (instancetype)initWithTitle:(NSString *)title content:(NSString *)content confirmHandle:(YCAlertHandle)confirmHandle{
    return [self initWithTitle:title content:content leftTitle:@"取消" rightTitle:@"确定" leftHandle:nil rightHandle:confirmHandle];
}

- (instancetype)initWithTitle:(NSString *)title content:(NSString *)content leftTitle:(NSString *)leftTitle rightTitle:(NSString *)rightTitle leftHandle:(YCAlertHandle)leftHandle rightHandle:(YCAlertHandle)rightHandle{
    
    if (self = [super initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)]) {
        _title = title;
        _content = content;
        _leftTitle = leftTitle;
        _rightTitle = rightTitle;
        _leftHanlde = leftHandle;
        _rightHanlde = rightHandle;
        
        [self createUI];
    }
    
    return self;
}

- (void)createUI{
    self.backgroundColor = [UIColor clearColor];
    
    UIView *backView = [[UIView alloc] initWithFrame:self.bounds];
    backView.backgroundColor = [UIColor blackColor];
    backView.alpha = 0.6;
    [self addSubview:backView];
    _backView = backView;
    
    UIView *popView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kWIDTH-100, 135)];
    popView.backgroundColor = [UIColor whiteColor];
    popView.layer.cornerRadius = 5;
    popView.layer.masksToBounds = YES;
    popView.center = self.center;
    [self addSubview:popView];
    self.popView = popView;
    
    UILabel *titleLabel = [UILabel labelWithFrame:CGRectMake(0, 20, VW(popView), 16) text:_title textFont:[UIFont yc_16] textColor:[UIColor yc_hex_333333] textAlignment:NSTextAlignmentCenter];
    CGRect frame = [titleLabel textRectForBounds:CGRectMake(0, 20, VW(popView), 400) limitedToNumberOfLines:0];
    titleLabel.frame = frame;
    [popView addSubview:titleLabel];
    self.titleLabel = titleLabel;
    
    UILabel *contentLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, FH(titleLabel)+14.5, VW(popView), 0)];
    if (_content.length>0) {
        contentLabel = [UILabel labelWithFrame:CGRectMake(10, FH(titleLabel)+4, VW(popView)-20, 24) text:_content textFont:[UIFont yc_15] textColor:[UIColor yc_hex_666666] textAlignment:NSTextAlignmentCenter numberOfLines:0];
        frame = [contentLabel textRectForBounds:CGRectMake(10, FH(titleLabel)+4, VW(popView)-20, 9999) limitedToNumberOfLines:0];
        contentLabel.frame = frame;
        [popView addSubview:contentLabel];
    }
    self.contentLabel = contentLabel;
    
    UIView *hLine = [[UIView alloc] initWithFrame:CGRectMake(0, FH(contentLabel)+25, VW(popView), kLineHeight)];
    hLine.backgroundColor = [UIColor yc_hex_EEEEEE];
    [popView addSubview:hLine];
    
    
    UIView *vLine = [[UIView alloc] initWithFrame:CGRectMake(VW(popView)/2.0, FH(hLine), kLineHeight, 45)];
    vLine.backgroundColor = [UIColor yc_hex_EEEEEE];
    if (_leftTitle && _rightTitle) {
         [popView addSubview:vLine];
    }
    
    if (_leftTitle) {
         UIButton *leftBtn = [UIButton customWithFrame:CGRectMake(0, FH(hLine), VW(popView)/2.0, 45) title:_leftTitle titleColor:[UIColor yc_hex_666666] fontSize:[UIFont yc_16] target:self action:@selector(leftClick)];
           [leftBtn setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
           [leftBtn setBackgroundImage:[UIImage imageWithColor:[UIColor yc_hex_EEEEEE]] forState:UIControlStateHighlighted];

           [popView addSubview:leftBtn];
           self.leftBtn = leftBtn;
    }
   
    
    if (_rightTitle) {
        UIButton *rightBtn = [UIButton customWithFrame:CGRectMake(_leftTitle ? FW(vLine):0, FH(hLine), _leftTitle ? (VW(popView)/2.0-kLineHeight):VW(popView), 45) title:_rightTitle titleColor:[UIColor yc_hex_5360FF] fontSize:[UIFont yc_16] target:self action:@selector(rightClick)];
        [rightBtn setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [rightBtn setBackgroundImage:[UIImage imageWithColor:[UIColor yc_hex_EEEEEE]] forState:UIControlStateHighlighted];
        [popView addSubview:rightBtn];
        self.rightBtn = rightBtn;
    }
    
    
    frame = popView.frame;
    frame.size.height = FH(self.rightBtn);
    popView.frame = frame;
    
}

- (void)updateContentAutoLayoutByFont:(UIFont *)font {
    self.contentLabel.font = font;
    CGFloat height = [self.contentLabel textRectForBounds:CGRectMake(24, FH(self.titleLabel)+4, self.popView.width - 48, 9999) limitedToNumberOfLines:0].size.height;
    self.contentLabel.frame = CGRectMake(24, self.titleLabel.bottom+4, self.popView.width - 48, height);
}

- (void)setRightTitle:(NSString *)rightTitle {
    _rightTitle = rightTitle;
    [self.rightBtn setTitle:rightTitle forState:UIControlStateNormal];
}

- (void)setLeftTitle:(NSString *)leftTitle {
    _leftTitle = leftTitle;
    [self.leftBtn setTitle:leftTitle forState:UIControlStateNormal];
}

- (void)configureColor:(UIColor *)leftColor rightColor:(UIColor *)rightColor leftFont:(UIFont *)leftFont rightFont:(UIFont *)rightFont titleColor:(UIColor *)titleColor titleFont:(UIFont *)titleFont{
    if (leftColor) { [self.leftBtn setTitleColor:leftColor forState:UIControlStateNormal]; }
    if (rightColor) { [self.rightBtn setTitleColor:rightColor forState:UIControlStateNormal] ;}
    if (leftFont) { self.leftBtn.titleLabel.font = leftFont ; }
    if (rightFont) { self.rightBtn.titleLabel.font = rightFont ; }
    if (titleColor) { self.titleLabel.textColor = titleColor;}
    if (titleFont) {self.titleLabel.font = titleFont;}
}

- (void)leftClick{
    
    if (_leftHanlde) {
        _leftHanlde();
    }
    [self hide];
}

- (void)rightClick{
    if (_rightHanlde) {
        _rightHanlde();
    }
    [self hide];
}

- (void)show{
    
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [window addSubview:self];
    
    _popView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.05, 1.05);
    _backView.alpha = 0;
    [UIView animateWithDuration:0.3 animations:^{
        self.popView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
        self.backView.alpha = 0.6;

    }];
    
}

- (void)hide{
    
    [UIView animateWithDuration:0.3 animations:^{
        self.backView.alpha = 0.0;
        self.popView.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end
