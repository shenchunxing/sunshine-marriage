//
//  YCGCDCountDown.h
//  YCLoginModule
//
//  Created by 沈春兴 on 2019/5/23.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YCGCDTimer : NSObject

@property(nonatomic,strong,nullable)dispatch_source_t timer;

+ (instancetype)timerManager;
- (void)resume;//重置
- (void)pause;//暂停
- (void)loadTimerWithCountDownHandle:(void(^)(NSInteger countDown))countDownHandle finishHandle:(void(^)(void))finishHandle;
@property (nonatomic,assign) NSUInteger countDown;//默认60秒

@end

NS_ASSUME_NONNULL_END
