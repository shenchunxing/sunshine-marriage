//
//  YCGCDCountDown.m
//  YCLoginModule
//
//  Created by 沈春兴 on 2019/5/23.
//

#import "YCGCDTimer.h"

@implementation YCGCDTimer

+ (instancetype)timerManager{
    static YCGCDTimer *timer = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (timer == nil) {
            timer = [[YCGCDTimer alloc]init];
        }
    });
    return timer;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        self.countDown = 59;
    }
    return self;
}

- (void)loadTimerWithCountDownHandle:(void(^)(NSInteger countDown))countDownHandle finishHandle:(void(^)(void))finishHandle{
    __block NSInteger time = self.countDown;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(timer,DISPATCH_TIME_NOW,1.0*NSEC_PER_SEC, 0);
    dispatch_source_set_event_handler(timer, ^{
        if(time <= 0){
            dispatch_source_cancel(timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                finishHandle();
            });
        }else{
            int second = time % 60;
            dispatch_async(dispatch_get_main_queue(), ^{
                countDownHandle(second);
            });
            time--;
        }
    });
    dispatch_resume(timer);
}

- (void)resume{
    [self loadTimerWithCountDownHandle:^(NSInteger countDown) {
        
    } finishHandle:^{
        
    }];
}

- (void)pause{
    if (self.timer) {
        dispatch_cancel(self.timer);
        self.timer = nil;
    }
    
}

@end
