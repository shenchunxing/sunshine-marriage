//
//  YCOperationResultViewController.m
//  YCBaseUI
//
//  Created by haima on 2019/7/3.
//

#import "YCOperationResultViewController.h"
#import "UIColor+Style.h"
#import "UIFont+Style.h"
#import "SMDefine.h"
#import "YCDataCenter.h"
#import "YCCategoryModule.h"

@interface YCOperationResultViewController ()
@property (nonatomic,strong) UIView *containerView;
@property (nonatomic,strong) UIImageView *imgView;
@property (nonatomic,strong) UILabel *detailLabel;

/* 标题 */
@property (nonatomic, copy) NSString *viewTitle;
/* 提示文字 */
@property (nonatomic, copy) NSString *message;
/* 类型 */
@property (nonatomic, assign) YCResultType resultType;
/* 按钮 */
@property (nonatomic, strong) NSMutableArray<YCOperationAction *> *actions;

@end

@implementation YCOperationResultViewController

+ (instancetype)resultControllerWithTitle:(NSString *)title message:(NSString *)message tyle:(YCResultType)type {
    
    YCOperationResultViewController *vc = [[YCOperationResultViewController alloc] init];
    vc.viewTitle = title;
    vc.message = message;
    vc.resultType = type;
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    YCNavigationBarType type = [YCNavgationBarAdapter shareInstance].isCustom ? YCNavigationBarTypeNormal : YCNavigationBarTypeCustom;
    [self yc_setupNavigationBarType:type];
    [self yc_setupTitleViewWithTitle:self.viewTitle];
}

- (void)initView {
    
    [self.view addSubview:self.containerView];
    [self.containerView addSubview:self.imgView];
    [self.containerView addSubview:self.detailLabel];
    
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kNavigationHeight);
        make.left.right.equalTo(self.view);
        make.height.mas_equalTo(256);
    }];
    [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(54);
        make.size.mas_equalTo(CGSizeMake(126, 126));
        make.centerX.mas_equalTo(self.containerView);
    }];
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.containerView);
        make.top.equalTo(self.imgView.mas_bottom).offset(19);
    }];
    self.detailLabel.text = self.message;
    self.imgView.image = [UIImage yc_resourceModuleImgWithName:[self _imageNameWithResultType:self.resultType]];
    [self setUpActionViews];
}

- (void)setUpActionViews {
    
    NSMutableArray *buttons = [NSMutableArray array];
    [self.actions enumerateObjectsUsingBlock:^(YCOperationAction * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIButton *button = [self _buttonWithAtion:obj atIndex:idx];
        [self.view addSubview:button];
        [buttons addObject:button];
    }];
    [buttons mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.containerView.mas_bottom).offset(40);
        make.height.mas_equalTo(48);
    }];
    if (buttons.count > 1) {
        [buttons mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:15 leadSpacing:14 tailSpacing:14];
    }else {
        [buttons.lastObject mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.offset(14);
            make.trailing.offset(-14);
        }];
    }
}

#pragma mark - public
- (void)addOperationAction:(YCOperationAction *)action {
    [self.actions addObject:action];
}
- (void)addOperationActions:(NSArray *)actions {
    [self.actions addObjectsFromArray:actions];
}

#pragma mark - private
- (UIButton *)_buttonWithAtion:(YCOperationAction *)action atIndex:(NSInteger)index{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:action.title forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont yc_18];
    button.layer.cornerRadius = 2.0;
    if (action.style == YCActionStyleFilled) {
        UIColor *backgroundColor = [YCDataCenter isCarDealer] ? [UIColor yc_hex_397AE6] : [UIColor yc_hex_108EE9];
        [button setBackgroundColor:backgroundColor];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    if (action.style == YCActionStyleBorder) {
        UIColor *color = [YCDataCenter isCarDealer] ? [UIColor yc_hex_397AE6] : [UIColor yc_hex_108EE9];
        [button setTitleColor:color forState:UIControlStateNormal];
        button.layer.borderColor = color.CGColor;
        button.layer.borderWidth = kLineHeight;
    }
    [button addTarget:self action:@selector(onButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    button.tag = index;
    return button;
}

- (void)onButtonClick:(UIButton *)button {
    
    YCOperationAction *action = self.actions[button.tag];
    if (action.handler) {
        action.handler();
    }
}

- (NSString *)_imageNameWithResultType:(YCResultType)type {
    
    switch (type) {
        case YCResultTypeSuccess:
            return @"img_succes";
            break;
        case YCResultTypeFailure:
            return @"img_fail";
            break;
    }
    return @"";
}

#pragma mark - getter
- (UIView *)containerView {
    
    if (_containerView == nil) {
        _containerView = [[UIView alloc] init];
        _containerView.backgroundColor = [UIColor whiteColor];
    }
    return _containerView;
}
- (UIImageView *)imgView {
    
    if (_imgView == nil) {
        _imgView = [[UIImageView alloc] init];
        
    }
    return _imgView;
}
- (UILabel *)detailLabel {
    
    if (_detailLabel == nil) {
        _detailLabel = [[UILabel alloc] init];
        _detailLabel.textColor = [UIColor yc_hex_5B6071];
        _detailLabel.font = [UIFont yc_18];
        _detailLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _detailLabel;
}

- (NSMutableArray<YCOperationAction *> *)actions {
    
    if (_actions == nil) {
        _actions = [[NSMutableArray alloc] init];
        
    }
    return _actions;
}

@end

@implementation YCOperationAction

+ (instancetype)actionWithTitle:(NSString *)title style:(YCActionStyle)style block:(ActionHandler)block {
    
    YCOperationAction *action = [[YCOperationAction alloc] init];
    action.title = title;
    action.style = style;
    action.handler = block;
    return action;
}

@end
