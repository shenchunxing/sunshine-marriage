//
//  YCOperationResultViewController.h
//  YCBaseUI
//
//  Created by haima on 2019/7/3.
//

#import "YCBaseViewController.h"

typedef NS_ENUM(NSUInteger, YCActionStyle) {
    YCActionStyleFilled,    //填充
    YCActionStyleBorder,    //描边
};

typedef NS_ENUM(NSUInteger, YCResultType) {
    YCResultTypeSuccess,    //提示成功
    YCResultTypeFailure    //提示失败
};

typedef void(^ActionHandler)(void);

@interface YCOperationAction : NSObject
/* 标题 */
@property (nonatomic, copy) NSString *title;
/* 风格 */
@property (nonatomic, assign) YCActionStyle style;
/* 回调 */
@property (nonatomic, copy) ActionHandler handler;

+ (instancetype)actionWithTitle:(NSString *)title style:(YCActionStyle)style block:(ActionHandler)block;
@end

@interface YCOperationResultViewController : YCBaseViewController

+ (instancetype)resultControllerWithTitle:(NSString *)title message:(NSString *)message tyle:(YCResultType)type;

- (void)addOperationAction:(YCOperationAction *)action;
- (void)addOperationActions:(NSArray *)actions;

@end


