//
//  YCSearchWMPageViewController.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/10/10.
//

#import "YCWMPageViewController.h"
#import "SMDefine.h"
#import "UIFont+Style.h"
#import "UIColor+Style.h"

@interface YCWMPageViewController ()

@end

@implementation YCWMPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navLineHidden = YES;
    self.progressWidth = 30;
    self.menuView.backgroundColor = [UIColor whiteColor];
}

//- (instancetype)initWithWMPageDelegate:(id<WMPageProtocol>)wmPageDelegate {
//    if (self = [super init]) {
//        self.wmPageDelegate = wmPageDelegate;
//    }
//    return self;
//}

- (instancetype)init {
    if (self = [super init]) {
        self.menuItemWidth = kWIDTH*50/375.0;
        self.menuViewStyle = WMMenuItemStateNormal;
        self.titleColorNormal = [UIColor yc_hex_666666];
        self.titleColorSelected  = [UIColor yc_hex_333333];
        self.progressHeight = 3.0;
        self.titleSizeNormal = [UIFont yc_13].pointSize;
        self.titleSizeSelected = [UIFont yc_15_bold].pointSize;
        self.progressColor = [UIColor yc_hex_6666FF];
        self.pageAnimatable = NO;
        self.progressViewBottomSpace = 0;
        self.titles = @[@"婚前报告",@"婚内报告",@"婚后报告"];
//        self.scrollEnable = YES;
    }
    return self;
}

#pragma mark - WMPageControllerDataSource

- (void)pageController:(WMPageController *)pageController willEnterViewController:(__kindof UIViewController *)viewController withInfo:(NSDictionary *)info {
    DELog(@"%@", info);
}

- (void)pageController:(WMPageController *)pageController didEnterViewController:(__kindof UIViewController *)viewController withInfo:(NSDictionary *)info {
    NSInteger index = [info[@"index"] integerValue];
    [self.menuView updateBadgeViewAtIndex:index];
}

- (void)menuView:(WMMenuView *)menu didLayoutItemFrame:(WMMenuItem *)menuItem atIndex:(NSInteger)index {
    DELog(@"%@", NSStringFromCGRect(menuItem.frame));
}

- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.titles.count;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    return self.titles[index];
}

- (CGFloat)menuView:(WMMenuView *)menu widthForItemAtIndex:(NSInteger)index {
    CGFloat width = [super menuView:menu widthForItemAtIndex:index];
    return width + 20;
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView {
    
    return CGRectMake(0, kNavigationHeight, kWIDTH, 50);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    
    return CGRectMake(0, kNavigationHeight, kWIDTH, kHEIGHT-kNavigationHeight-50);
}

@end
