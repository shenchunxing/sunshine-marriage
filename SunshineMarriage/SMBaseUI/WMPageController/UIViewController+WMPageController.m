//
//  UIViewController+WMPageController.m
//  WMPageController
//
//  Created by Mark on 15/6/11.
//  Copyright (c) 2015年 yq. All rights reserved.
//

#import "UIViewController+WMPageController.h"
#import "WMPageController.h"

@implementation UIViewController (WMPageController)

- (WMPageController *)wm_pageController {
    UIViewController *parentViewController = self.parentViewController;
    while (parentViewController) {
        if ([parentViewController isKindOfClass:[WMPageController class]]) {
            return (WMPageController *)parentViewController;
        }
        parentViewController = parentViewController.parentViewController;
    }
    return nil;
}

- (void)popToWMPageControllerWithIndex:(NSInteger)index{
    [self.navigationController.viewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[WMPageController class]]) {
            [obj setValue:@(index) forKey:@"selectIndex"];
            [self.navigationController popToViewController:obj animated:YES];
        }
    }];
}


@end
