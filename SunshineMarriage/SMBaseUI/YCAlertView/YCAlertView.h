//
//  YCAlertView.h
//  YCBaseUI
//
//  Created by 刘成 on 2019/4/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^YCAlertHandle)(void);

@interface YCAlertView : UIView
@property (nonatomic, copy) NSString *rightTitle;
@property (nonatomic, copy) NSString *leftTitle;
+ (void)showBS_ErrorWithTitle:(NSString * _Nullable)title;
+ (void)showWithTitle:(NSString * _Nullable)title rightTitle:(NSString *)rightTitle rightHandle:(YCAlertHandle)rightHandle;
- (instancetype)initWithTitle:(NSString * _Nullable)title confirmHandle:(YCAlertHandle _Nullable)confirmHandle;

- (instancetype)initWithTitle:(NSString * _Nullable)title content:(NSString * _Nullable)content confirmHandle:(YCAlertHandle _Nullable)confirmHandle;

- (instancetype)initWithTitle:(NSString * _Nullable)title content:(NSString * _Nullable)content leftTitle:(NSString * _Nullable)leftTitle rightTitle:(NSString * _Nullable)rightTitle leftHandle:(YCAlertHandle _Nullable)leftHandle rightHandle:(YCAlertHandle _Nullable)rightHandle;

- (void)show;

+ (instancetype)bs_initWithTitle:(NSString * _Nullable)title confirmHandle:(YCAlertHandle _Nullable)confirmHandle ;
- (void)configureColor:(UIColor *)leftColor rightColor:(UIColor *)rightColor leftFont:(UIFont *)leftFont rightFont:(UIFont *)rightFont titleColor:(UIColor *)titleColor titleFont:(UIFont *)titleFont;

+ (instancetype)bs_initWithTitle:(NSString * _Nullable)title content:(NSString *)content confirmHandle:(YCAlertHandle _Nullable)confirmHandle;

@end


NS_ASSUME_NONNULL_END
