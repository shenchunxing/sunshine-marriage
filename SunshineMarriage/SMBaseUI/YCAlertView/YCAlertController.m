//
//  YCAlertController.m
//  YCBaseUI
//
//  Created by haima on 2019/6/4.
//

#import "YCAlertController.h"
#import <Masonry/Masonry.h>
#import "UIFont+Style.h"
#import "UIColor+Style.h"

@implementation YCAlertAction

+ (instancetype)alertActionWithTitle:(NSString *)title
                           textColor:(UIColor *)textColor
                               block:(ActionHandler)block {
    
    YCAlertAction *action = [[YCAlertAction alloc] init];
    action.title = title;
    if (textColor) {
        action.textColor = textColor;
    }else{
        action.textColor = [UIColor yc_hex_121D32];
    }
    action.textColor = textColor;

    action.font = [UIFont yc_17];
    action.action = block;
    return action;
}

@end

@interface YCAlertController ()

/* 标题 */
@property (nonatomic, copy) NSString *alertTitle;
/* 消息 */
@property (nonatomic, copy) NSString *message;
/* actions */
@property (nonatomic, strong) NSMutableArray<YCAlertAction *> *alerActions;

/* 背景图层 */
@property (nonatomic, strong) UIView *bgView;
/* 主视图 */
@property (nonatomic, strong) UIView *containerView;
/* 标题 */
@property (nonatomic, strong) UILabel *titleLbl;
/* 消息 */
@property (nonatomic, strong) UILabel *desLbl;
/* 取消 */
@property (nonatomic, strong) UIButton *cancelBtn;
/* 确定 */
@property (nonatomic, strong) UIButton *confirmBtn;

@property (nonatomic, strong) UIView *hLine;

@property (nonatomic, strong) UIView *vLine;
@end

@implementation YCAlertController

#pragma mark - Init
+ (instancetype)alertControllerWithTitle:(NSString *)title message:(NSString *)message {
    
    YCAlertController *vc = [[YCAlertController alloc] init];
    vc.alertTitle = title;
    vc.message = message;
    vc.alerActions = [NSMutableArray array];
    return vc;
}


#pragma mark - LifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
}

- (void)setupUI {
    
    [self.view addSubview:self.bgView];
    [self.view addSubview:self.containerView];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.view);
        make.leading.offset(33);
        make.trailing.offset(-32);
    }];
    BOOL showTitle = (self.alertTitle && self.alertTitle.length > 0);
    BOOL showMessage = self.message && self.message.length > 0;
    //标题
    if (showTitle) {
        self.titleLbl.text = self.alertTitle;
        [self.containerView addSubview:self.titleLbl];
        [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.offset(8);
            make.trailing.offset(-8);
            make.height.mas_equalTo(25);
            make.top.mas_equalTo(24);
        }];
    }
    
    //消息
    if (showMessage) {
        self.desLbl.text = self.message;
        [self.containerView addSubview:self.desLbl];
        [self.desLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            if (showTitle) {
                make.top.equalTo(self.titleLbl.mas_bottom).offset(24);
            }else{
                make.top.mas_equalTo(24);
            }
            make.leading.offset(14);
            make.trailing.offset(-14);
        }];
    }
    
    //分割线
    UIView *preView = self.containerView.subviews.lastObject;
    [self.containerView addSubview:self.hLine];
    [self.hLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(preView.mas_bottom).offset(24);
        make.left.right.equalTo(self.containerView);
        make.height.mas_equalTo(1/[UIScreen mainScreen].scale);
        make.bottom.equalTo(self.containerView.mas_bottom).offset(-50);
    }];
    
    if (self.alerActions && self.alerActions.count > 0) {
        //布局按钮，最多两个按钮
        CGFloat ratio = 1.0 / self.alerActions.count;
        [self.alerActions enumerateObjectsUsingBlock:^(YCAlertAction * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            UIButton *button = [self actionButtonWithTitle:obj.title color:obj.textColor font:obj.font];
            button.tag = 20000 + idx;
            UIView *preView = self.containerView.subviews.lastObject;
            [self.containerView addSubview:button];
            if (idx == 0) {
                [button mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.bottom.equalTo(self.containerView);
                    make.height.mas_equalTo(50);
                    make.width.equalTo(self.containerView.mas_width).multipliedBy(ratio);
                }];
            }else{
                [button mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(preView.mas_right);
                    make.bottom.equalTo(self.containerView);
                    make.height.mas_equalTo(50);
                    make.width.equalTo(self.containerView.mas_width).multipliedBy(ratio);
                }];
            }
        }];
    }
    
    if (self.alerActions && self.alerActions.count > 1) {
        //两个按钮的时候，添加分割线
        [self.containerView addSubview:self.vLine];
        [self.vLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(1/[UIScreen mainScreen].scale);
            make.height.mas_equalTo(50);
            make.centerX.equalTo(self.containerView);
            make.bottom.equalTo(self.containerView);
        }];
    }
}

- (void)setAlignment:(NSTextAlignment)alignment {
    _alignment = alignment;
    self.desLbl.textAlignment = alignment;
}

- (void)onActionButtonClick:(UIButton *)button {
    
    NSInteger index = button.tag - 20000;
    
    [self dismissViewControllerAnimated:YES completion:^{
        YCAlertAction *action = self.alerActions[index];
        if (action.action) {
            action.action();
        }
    }];
}

- (void)addAlertAction:(YCAlertAction *)action {
    [self.alerActions addObject:action];
}

- (void)addAlertActions:(NSArray<YCAlertAction *> *)actions {
    
    [self.alerActions addObjectsFromArray:actions];
}


- (UIButton *)actionButtonWithTitle:(NSString *)title color:(UIColor *)color font:(UIFont *)font {
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.titleLabel.font = font;
    [btn setTitleColor:color forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(onActionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    return btn;
}

#pragma mark - getter
- (UIView *)bgView {
    
    if (_bgView == nil) {
        _bgView = [[UIView alloc] init];
        _bgView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
    }
    return _bgView;
}
- (UIView *)containerView {
    
    if (_containerView == nil) {
        _containerView = [[UIView alloc] init];
        _containerView.backgroundColor = [UIColor whiteColor];
        _containerView.layer.masksToBounds = YES;
        _containerView.layer.cornerRadius = 2.0;
    }
    return _containerView;
}

- (UILabel *)titleLbl {
    
    if (_titleLbl == nil) {
        _titleLbl = [[UILabel alloc] init];
        _titleLbl.textColor = [UIColor yc_hex_121D32];
        _titleLbl.font = [UIFont yc_18_bold];
        _titleLbl.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLbl;
}
- (UILabel *)desLbl {
    
    if (_desLbl == nil) {
        _desLbl = [[UILabel alloc] init];
        _desLbl.textColor = [UIColor yc_hex_5B6071];
        _desLbl.font = [UIFont yc_17];
        _desLbl.textAlignment = NSTextAlignmentCenter;
        _desLbl.numberOfLines = 0;
    }
    return _desLbl;
}
- (UIView *)hLine {
    
    if (_hLine == nil) {
        _hLine = [[UIView alloc] init];
        _hLine.backgroundColor = [UIColor yc_hex_EEEEEE];
    }
    return _hLine;
}
- (UIView *)vLine {
    
    if (_vLine == nil) {
        _vLine = [[UIView alloc] init];
        _vLine.backgroundColor = [UIColor yc_hex_EEEEEE];
    }
    return _vLine;
}



@end
