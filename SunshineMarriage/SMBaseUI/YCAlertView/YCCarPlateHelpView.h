//
//  YCCarPlateHelpView.h
//  YCBaseUI
//
//  Created by haima on 2019/4/24.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YCCarPlateHelpView : UIView

+ (void)showCarPlateHelpView;

@end

NS_ASSUME_NONNULL_END
