//
//  YCCarPlateHelpView.m
//  YCBaseUI
//
//  Created by haima on 2019/4/24.
//

#import "YCCarPlateHelpView.h"
#import <Masonry/Masonry.h>
#import "UIColor+Style.h"

@interface YCCarPlateHelpView ()

/* 背景按钮 */
@property (nonatomic, strong) UIButton *bgBtn;
@property (nonatomic, strong) UIView *containerView;
/* 私牌 */
@property (nonatomic, strong) UILabel *personLbl;
/* 公牌 */
@property (nonatomic, strong) UILabel *companyLbl;

@end

@implementation YCCarPlateHelpView

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
        [self initViews];
    }
    return self;
}


- (void)initViews {
    [self addSubview:self.bgBtn];
    [self addSubview:self.containerView];
    [self.containerView addSubview:self.personLbl];
    [self.containerView addSubview:self.companyLbl];
    [self.bgBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
        make.height.equalTo(@100);
        make.left.mas_equalTo(32);
        make.right.mas_equalTo(-32);
    }];
    [self.personLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(24);
        make.top.mas_equalTo(24);
        make.right.lessThanOrEqualTo(@(-24));
        make.height.equalTo(@22);
    }];
    [self.companyLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.personLbl);
        make.top.equalTo(self.personLbl.mas_bottom).offset(8);
        make.height.equalTo(@22);
        make.right.lessThanOrEqualTo(@(-24));
    }];
    
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:@"私牌：私人牌照"];
    [attr addAttributes:@{NSForegroundColorAttributeName:[UIColor yc_hex_5B6071]} range:NSMakeRange(0, 3)];
    self.personLbl.attributedText = attr;
    attr = [[NSMutableAttributedString alloc] initWithString:@"公牌：公司牌照"];
    [attr addAttributes:@{NSForegroundColorAttributeName:[UIColor yc_hex_5B6071]} range:NSMakeRange(0, 3)];
    self.companyLbl.attributedText = attr;
    attr = nil;
}

+ (void)showCarPlateHelpView {
    
    YCCarPlateHelpView *helpView = [[YCCarPlateHelpView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [window addSubview:helpView];
    
    helpView.bgBtn.alpha = 0.0;
    helpView.containerView.alpha = 0.0;
    [UIView animateWithDuration:0.25 animations:^{
        helpView.bgBtn.alpha = 0.6;
        helpView.containerView.alpha = 1.0;
    } completion:nil];
}

- (void)onBgButtonClick:(UIButton *)button {
    
    
    [UIView animateWithDuration:0.2 animations:^{
        self.bgBtn.alpha = 0.0;
        self.containerView.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - getter
- (UIView *)containerView {
    
    if (_containerView == nil) {
        _containerView = [[UIView alloc] init];
        _containerView.backgroundColor = [UIColor whiteColor];
        _containerView.layer.masksToBounds = YES;
        _containerView.layer.cornerRadius = 2.0;
    }
    return _containerView;
}

- (UILabel *)personLbl {
    
    if (_personLbl == nil) {
        _personLbl = [[UILabel alloc] init];
        _personLbl.textAlignment = NSTextAlignmentLeft;
        _personLbl.textColor = [UIColor yc_hex_3C3D49];
    }
    return _personLbl;
}

- (UILabel *)companyLbl {
    
    if (_companyLbl == nil) {
        _companyLbl = [[UILabel alloc] init];
        _companyLbl.textAlignment = NSTextAlignmentLeft;
        _companyLbl.textColor = [UIColor yc_hex_3C3D49];
    }
    return _companyLbl;
}

- (UIButton *)bgBtn {
    
    if (_bgBtn == nil) {
        _bgBtn = [[UIButton alloc] init];
        _bgBtn.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
        [_bgBtn addTarget:self action:@selector(onBgButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _bgBtn;
}

@end
