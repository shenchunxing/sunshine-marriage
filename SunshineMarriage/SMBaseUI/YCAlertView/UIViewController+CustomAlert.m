//
//  UIViewController+CustomAlert.m
//  YCBaseUI
//
//  Created by haima on 2019/6/4.
//

#import "UIViewController+CustomAlert.h"
#import <objc/runtime.h>
#import "UIFont+Style.h"
#import "UIColor+Style.h"
#import "YCNavgationBarAdapter.h"
#import "YCAlertController.h"
#import "YCAlertView.h"
static NSString *kAlertWindowKey;
static NSString *kOriginKeyWindowKey;

@interface YCAlertController (Window)

- (void)show;
- (void)show:(BOOL)animated;

@end

@interface YCCustomAlertViewController (Window)
- (void)show;
- (void)show:(BOOL)animated;
@end

@interface UIViewController (Private)
@property (nonatomic, strong) UIWindow *alertWindow;
/* 原key window */
@property (nonatomic, strong) UIWindow *originKeyWindow;
@end

@implementation UIViewController (Private)
@dynamic alertWindow;
@dynamic originKeyWindow;

- (void)setAlertWindow:(UIWindow *)alertWindow {
    objc_setAssociatedObject(self, &kAlertWindowKey, alertWindow, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIWindow *)alertWindow {
    return objc_getAssociatedObject(self, &kAlertWindowKey);
}

- (void)setOriginKeyWindow:(UIWindow *)originKeyWindow {
    objc_setAssociatedObject(self, &kOriginKeyWindowKey, originKeyWindow, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIWindow *)originKeyWindow {
    return objc_getAssociatedObject(self, &kOriginKeyWindowKey);
}
@end

@implementation YCAlertController (Window)

- (void)show {
    [self show:YES];
}

- (void)show:(BOOL)animated {
    self.alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.alertWindow.rootViewController = [[UIViewController alloc] init];
    
    id<UIApplicationDelegate> delegate = [UIApplication sharedApplication].delegate;
    // Applications that does not load with UIMainStoryboardFile might not have a window property:
    if ([delegate respondsToSelector:@selector(window)]) {
        // we inherit the main window's tintColor
        // 设置->通用->辅助功能->增强对比度->加深颜色  会造成按钮文字无法显示
        //        self.alertWindow.tintColor = delegate.window.tintColor;
    }
    
    // window level is above the top window (this makes the alert, if it's a sheet, show over the keyboard)
    UIWindow *topWindow = [UIApplication sharedApplication].windows.lastObject;
    self.originKeyWindow = [UIApplication sharedApplication].keyWindow;
    self.alertWindow.windowLevel = topWindow.windowLevel + 1;
    
    [self.alertWindow makeKeyAndVisible];
    [self setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    //dispatch_after fix Unbalanced calls to begin/end appearance transitions issue
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.alertWindow.rootViewController presentViewController:self animated:animated completion:nil];
    });
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.originKeyWindow makeKeyAndVisible];
    self.alertWindow.hidden = YES;
    self.alertWindow = nil;
}

@end

@implementation YCCustomAlertViewController (Window)

- (void)show {
    [self show:YES];
}

- (void)show:(BOOL)animated {
    self.alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.alertWindow.rootViewController = [[UIViewController alloc] init];
    
    id<UIApplicationDelegate> delegate = [UIApplication sharedApplication].delegate;
    // Applications that does not load with UIMainStoryboardFile might not have a window property:
    if ([delegate respondsToSelector:@selector(window)]) {
        // we inherit the main window's tintColor
        // 设置->通用->辅助功能->增强对比度->加深颜色  会造成按钮文字无法显示
        //        self.alertWindow.tintColor = delegate.window.tintColor;
    }
    
    // window level is above the top window (this makes the alert, if it's a sheet, show over the keyboard)
    UIWindow *topWindow = [UIApplication sharedApplication].windows.lastObject;
    self.originKeyWindow = [UIApplication sharedApplication].keyWindow;
    self.alertWindow.windowLevel = topWindow.windowLevel + 1;
    
    [self.alertWindow makeKeyAndVisible];
    [self setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    //dispatch_after fix Unbalanced calls to begin/end appearance transitions issue
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.alertWindow.rootViewController presentViewController:self animated:animated completion:nil];
    });
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.originKeyWindow makeKeyAndVisible];
    self.alertWindow.hidden = YES;
    self.alertWindow = nil;
}

@end

@implementation UIViewController (CustomAlert)

- (void)showBS_Error:(NSString *)title {
    [YCAlertView showBS_ErrorWithTitle:title];
}

- (void)yc_showAlertWithMessage:(NSString *)message
                        confirm:(ConfirmHandler)block {

    [self yc_showAlertWithTitle:nil message:message confirm:^{
        if (block) {
            block();
        }
    }];
}

- (void)yc_showAlertWithTitle:(NSString *)title
                      message:(NSString *)message
                      confirm:(ConfirmHandler)block {
    

    [self yc_showAlertWithTitle:title message:message messageAlignment:NSTextAlignmentCenter confirm:^{
        if (block) {
            block();
        }
    }];
}

- (void)yc_showAlertWithTitle:(NSString *)title
                      message:(NSString *)message
             messageAlignment:(NSTextAlignment)alignment
                      confirm:(ConfirmHandler)block {
    
    [self yc_showAlertWithTitle:title
                        message:message
               messageAlignment:alignment
                   confirmTitle:nil
                   confirmColor:[[YCNavgationBarAdapter shareInstance] adapterThemeColor]
                        confirm:^{
                            if (block) {
                                block();
                            }
                        } cancelTitle:nil cancelColor:nil cancel:nil];
}

- (void)yc_showAlertWithTitle:(NSString *)title
                      message:(NSString *)message
                      confirm:(ConfirmHandler)confirmBlock
                       cancel:(CancelHandler)cancelBlock {
    
    [self yc_showAlertWithTitle:title
                        message:message
               messageAlignment:NSTextAlignmentCenter
                   confirmTitle:nil
                   confirmColor:[UIColor yc_hex_F65448]
                        confirm:^{
                            if (confirmBlock) {
                                confirmBlock();
                            }
                        }cancelTitle:nil
                    cancelColor:[UIColor yc_hex_121D32]
                         cancel:^{
                             if (cancelBlock) {
                                 cancelBlock();
                             }
                         }];
}


- (void)yc_showAlertWithTitle:(NSString *)title
                      message:(NSString *)message
             messageAlignment:(NSTextAlignment)alignment
                 confirmTitle:(NSString *)confirmTitle
                 confirmColor:(UIColor *)confirmColor
                      confirm:(ConfirmHandler)confirmBlock
                  cancelTitle:(NSString *)cancelTitle
                  cancelColor:(UIColor *)cancelColor
                       cancel:(CancelHandler)cancelBlock {

    YCAlertController *alerVc = [YCAlertController alertControllerWithTitle:title message:message];
    alerVc.alignment = alignment;
    if (cancelBlock) {
        NSString *title = (cancelTitle && cancelTitle.length > 0)?confirmTitle:@"取消";
        YCAlertAction *action = [YCAlertAction alertActionWithTitle:title textColor:cancelColor block:cancelBlock];
        [alerVc addAlertAction:action];
    }
    if (confirmBlock) {
        NSString *title = (confirmTitle && confirmTitle.length > 0)?confirmTitle:@"确定";
        YCAlertAction *action = [YCAlertAction alertActionWithTitle:title textColor:confirmColor block:confirmBlock];
        [alerVc addAlertAction:action];
    }
    [alerVc show];
}

#pragma mark - YCCustomAlertViewController
- (void)yc_showCustomAlertWithTitle:(NSString *)title type:(YCAlertType)type confirm:(ConfirmHandler)confirm {
    [self yc_showCustomAlertWithTitle:title message:nil type:type cancelTitle:nil cancel:nil confirmTitle:nil confirm:confirm];
}
- (void)yc_showCustomAlertWithTitle:(NSString *)title type:(YCAlertType)type message:(NSString *)message confirm:(ConfirmHandler)confirm {
    [self yc_showCustomAlertWithTitle:title message:message type:type cancelTitle:nil cancel:nil confirmTitle:nil confirm:confirm];
}
- (void)yc_showCustomAlertWithTitle:(NSString *)title type:(YCAlertType)type cancel:(CancelHandler)cancel confirm:(ConfirmHandler)confirm {
    [self yc_showCustomAlertWithTitle:title message:nil type:type cancelTitle:nil cancel:cancel confirmTitle:nil confirm:confirm];
}
- (void)yc_showCustomAlertWithTitle:(NSString *)title type:(YCAlertType)type message:(NSString *)message cancel:(CancelHandler)cancel confirm:(ConfirmHandler)confirm {
    [self yc_showCustomAlertWithTitle:title message:message type:type cancelTitle:nil cancel:cancel confirmTitle:nil confirm:confirm];
}
- (void)yc_showCustomAlertWithTitle:(NSString *)title
                            message:(NSString *)message
                               type:(YCAlertType)type
                        cancelTitle:(NSString *)cancelTitle
                             cancel:(CancelHandler)cancel
                       confirmTitle:(NSString *)confirmTitle
                            confirm:(ConfirmHandler)confirm {
    YCCustomAlertViewController *vc = [YCCustomAlertViewController alertControllerWithTitle:title message:message type:type];
    if (cancel) {
        NSString *title = (cancelTitle && cancelTitle.length > 0)?cancelTitle:@"取消";
        YCCustomAlertAction *action = [YCCustomAlertAction actionWithTitle:title type:YCActionTypeCancel action:cancel];
        [vc addAlertAction:action];
    }
    if (confirm) {
        NSString *title = (confirmTitle && confirmTitle.length > 0)?confirmTitle:@"确定";
        YCCustomAlertAction *action = [YCCustomAlertAction actionWithTitle:title type:YCActionTypeConfirm action:confirm];
        [vc addAlertAction:action];
    }
    [vc show];
}
@end
