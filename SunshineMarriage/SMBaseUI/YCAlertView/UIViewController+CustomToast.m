//
//  UIViewController+AlertMessage.m
//  YCBaseUI
//
//  Created by haima on 2019/4/19.
//

#import "UIViewController+CustomToast.h"
#import "UIImage+YCBundle.h"
#import "UIFont+Style.h"
#import "UIColor+Style.h"
#import "YCHudDefine.h"


@implementation UIViewController (CustomToast)

- (void)showMessage:(NSString *)message afterDelay:(NSTimeInterval)delay {
    
//    CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
//    style.horizontalPadding = 10;
//    style.verticalPadding = 7.0;
//    style.cornerRadius = 15.0;
//    [self.view makeToast:message duration:delay position:CSToastPositionCenter style:style];
    [self showMessage:message afterDelay:delay completion:nil];
}

- (void)showMessage:(NSString *)message afterDelay:(NSTimeInterval)delay completion:(void(^)(void))completion {
    
    CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.horizontalPadding = 10;
    style.verticalPadding = 7.0;
    style.cornerRadius = 15.0;
    [self.view makeToast:message duration:delay position:CSToastPositionCenter title:nil image:nil style:style completion:^(BOOL didTap) {
        !completion?:completion();
    }];
}

- (void)showCustomMessage_SM:(NSString *)message {
    [self showCustomMessage:message horizontalPadding:25 verticalPadding:15 cornerRadius:4 afterDelay:1.5 completion:nil];
}

- (void)showCustomMessage:(NSString *)message horizontalPadding:(CGFloat)horizontalPadding verticalPadding:(CGFloat)verticalPadding cornerRadius:(CGFloat)cornerRadius afterDelay:(NSTimeInterval)delay completion:(void(^)(void))completion {
    CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.horizontalPadding = horizontalPadding;
    style.verticalPadding = verticalPadding;
    style.cornerRadius = cornerRadius;
    style.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha: 0.5];
    [self.view makeToast:message duration:delay position:CSToastPositionCenter title:nil image:nil style:style completion:^(BOOL didTap) {
        !completion?:completion();
    }];
}

- (void)showCustomMessage_newAddModule:(NSString *)message {
    [self showCustomMessage:message horizontalPadding:20 verticalPadding:10 cornerRadius:2.0 afterDelay:1.5 completion:nil];
}

- (void)show_clearCacheSuccess:(NSString *)message {
    CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.horizontalPadding = 20;
    style.verticalPadding = 10;
    style.cornerRadius = 2;
    style.imageSize = CGSizeMake(18, 18);
    style.imageMessageInterval = 8;
    UIView *view = [self.view toastViewForMessage:message title:nil image:[UIImage yc_BaseImgWithName:@"icon_tjcg"] style:style];
    
    [self.view showToast:view duration:1.5 position:CSToastPositionCenter completion:nil];

}

- (void)showToast:(NSString *)message{
    [self showMessage:message afterDelay:1.5];
}

- (void)showToastInKeyWindow:(NSString *)message{
    showCenterToast([UIApplication sharedApplication].keyWindow, message);
}

- (void)showToastOnWindow:(NSString *)message{
    showCenterToast([UIApplication sharedApplication].delegate.window, message);
}

- (void)showSuccessMessage:(NSString *)message afterDelay:(NSTimeInterval)delay {
    
    [self showSuccessMessage:message afterDelay:delay completion:nil];
}

- (void)showSuccessMessage:(NSString *)message afterDelay:(NSTimeInterval)delay completion:(void(^)(void))completion {
    
    CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.horizontalPadding = 10;
    style.cornerRadius = 21.0;
    style.backgroundColor = [UIColor blackColor];
    style.messageFont = [UIFont yc_16];
    style.messageColor = [UIColor whiteColor];
    style.imageSize = CGSizeMake(22, 22);
    UIView *view = [self.view toastViewForMessage:message title:nil image:[UIImage yc_BaseImgWithName:@"icon_tjcg"] style:style];
    
    [self.view showToast:view duration:1.5 position:CSToastPositionCenter completion:^(BOOL didTap) {
        if (completion) {
            completion();
        }
    }];
}

- (void)showFailureMessage:(NSString *)message afterDelay:(NSTimeInterval)delay {

    [self showFailureMessage:message afterDelay:delay completion:nil];
}

- (void)showFailureMessage:(NSString *)message afterDelay:(NSTimeInterval)delay completion:(void(^)(void))completion {
    
    CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.horizontalPadding = 10;
    style.cornerRadius = 21.0;
    style.backgroundColor = [UIColor blackColor];
    style.messageFont = [UIFont yc_16];
    style.messageColor = [UIColor whiteColor];
    style.imageSize = CGSizeMake(22, 22);
    UIView *view = [self.view toastViewForMessage:message title:nil image:[UIImage yc_BaseImgWithName:@"icon_shibai"] style:style];
    
    [self.view showToast:view duration:delay position:CSToastPositionCenter completion:^(BOOL didTap) {
        if (completion) {
            completion();
        }
    }];
}
@end
