//
//  YCAlertController.h
//  YCBaseUI
//
//  Created by haima on 2019/6/4.
//

#import <UIKit/UIKit.h>

typedef void(^ActionHandler)(void);
@interface YCAlertAction : NSObject

/* 标题 */
@property (nonatomic, copy) NSString *title;
/* 颜色 */
@property (nonatomic, strong) UIColor *textColor;
/* 字体 */
@property (nonatomic, strong) UIFont *font;
/* 回调 */
@property (nonatomic, copy) ActionHandler action;

+ (instancetype)alertActionWithTitle:(NSString *)title
                           textColor:(UIColor *)textColor
                               block:(ActionHandler)block;

@end

@interface YCAlertController : UIViewController
/* <#mark#> */
@property (nonatomic, assign) NSTextAlignment alignment;

+ (instancetype)alertControllerWithTitle:(NSString *)title message:(NSString *)message;
- (void)addAlertAction:(YCAlertAction *)action;
- (void)addAlertActions:(NSArray<YCAlertAction *> *)actions;
@end

