//
//  YCCustomAlertViewController.m
//  YCBaseUI
//
//  Created by shenweihang on 2019/12/2.
//

#import "YCCustomAlertViewController.h"
#import <Masonry/Masonry.h>
#import "YCCategoryModule.h"
#import "SMDefine.h"


@interface YCCustomAlertViewController ()
/* 标题 */
@property (nonatomic, copy) NSString *alertTitle;
/* 消息 */
@property (nonatomic, copy) NSString *message;
/* 弹框类型 */
@property (nonatomic, assign) YCAlertType alertType;
/* actions */
@property (nonatomic, strong) NSMutableArray<YCCustomAlertAction *> *alerActions;

/* 背景图层 */
@property (nonatomic, strong) UIView *bgView;
/* 主视图 */
@property (nonatomic, strong) UIView *containerView;
/* 图标 */
@property (nonatomic, strong) UILabel *iconLbl;
/* 标题 */
@property (nonatomic, strong) UILabel *titleLbl;
/* 消息 */
@property (nonatomic, strong) UILabel *msgLbl;

@end

@implementation YCCustomAlertViewController

+ (YCCustomAlertViewController *)alertControllerWithTitle:(NSString *)title message:(NSString *)message type:(YCAlertType)alertType {
    YCCustomAlertViewController *vc = [[YCCustomAlertViewController alloc] init];
    vc.alertTitle = title;
    vc.message = message;
    vc.alertType = alertType;
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeUI];
}

- (void)initializeUI {
    
    [self.view addSubview:self.bgView];
    CGFloat width = FitScale(300);
    CGFloat height = 24.0;
    BOOL showIcon = (self.alertType == YCAlertTypeIcon);
    BOOL showTitle = (self.alertTitle && self.alertTitle.length > 0);
    BOOL showMsg = (self.message && self.message.length > 0);
    if (showIcon) {
        [self.containerView addSubview:self.iconLbl];
        self.iconLbl.frame = CGRectMake((width-36)/2, height, 36, 36);
        height += 36.0;
    }
    if (showTitle) {
        if (showIcon) {
            height += 12.0;
        }
        [self.containerView addSubview:self.titleLbl];
        self.titleLbl.text = self.alertTitle;
        CGFloat h = [self.alertTitle yc_boundingRectWithSize:CGSizeMake(width-28, HUGE) font:self.titleLbl.font].height;
        self.titleLbl.frame = CGRectMake(14, height, width-28, ceilf(h));
        height += ceilf(h);
        if (showMsg) {
            height += 8;
        }else {
            height += 24;
        }
    }
    if (showMsg) {
        [self.containerView addSubview:self.msgLbl];
        self.msgLbl.text = self.message;
        CGFloat h = [self.message yc_boundingRectWithSize:CGSizeMake(width-28, HUGE) font:self.msgLbl.font].height;
        self.msgLbl.frame = CGRectMake(14, height, width-28, ceilf(h));
        height += ceilf(h);
        height += 12;
    }
    UIView *hLine = [[UIView alloc] initWithFrame:CGRectMake(0, height, width, kLineHeight)];
    hLine.backgroundColor = [UIColor yc_hex_EFEFEF];
    [self.containerView addSubview:hLine];
    height += kLineHeight;
    if (self.alerActions && self.alerActions.count > 0) {
        CGFloat buttonWidth = width / self.alerActions.count;
        __block CGFloat x = 0;
        [self.alerActions enumerateObjectsUsingBlock:^(YCCustomAlertAction * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            UIButton *button = [self actionButtonWithTitle:obj.title color:obj.fontColor font:obj.font];
            button.tag = 20000 + idx;
            [self.containerView addSubview:button];
            button.frame = CGRectMake(x, height, buttonWidth, 50);
            if (idx < self.alerActions.count-1) {
                UIView *line = [self generateLineView];
                [self.containerView addSubview:line];
                line.frame = CGRectMake(x+buttonWidth, height, kLineHeight, 50);
                x+=(buttonWidth+kLineHeight);
            }
        }];
        height += 50;
    }
    self.containerView.frame = CGRectMake(0, 0, width, height);
    [self.view addSubview:self.containerView];
    self.containerView.center = self.view.center;
}

- (UIView *)generateLineView {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor yc_hex_EFEFEF];
    return view;
}

- (UIButton *)actionButtonWithTitle:(NSString *)title color:(UIColor *)color font:(UIFont *)font {
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.titleLabel.font = font;
    [btn setTitleColor:color forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(onActionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    return btn;
}

- (void)onActionButtonClick:(UIButton *)button {
    
    NSInteger index = button.tag - 20000;
    [self dismissViewControllerAnimated:YES completion:^{
        YCCustomAlertAction *action = self.alerActions[index];
        if (action.action) {
            action.action();
        }
    }];
}

- (void)addAlertAction:(YCCustomAlertAction *)action {
    [self.alerActions addObject:action];
}
- (void)addAlertActions:(NSArray<YCCustomAlertAction *> *)actions {
    [self.alerActions addObjectsFromArray:actions];
}

#pragma mark - getter
- (UIView *)bgView {
    
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kWIDTH, kScreenHEIGHT)];
        _bgView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
    }
    return _bgView;
}
- (UIView *)containerView {
    
    if (!_containerView) {
        _containerView = [[UIView alloc] init];
        _containerView.backgroundColor = [UIColor whiteColor];
        _containerView.layer.cornerRadius = 2.0;
        _containerView.layer.masksToBounds = YES;
    }
    return _containerView;
}
- (UILabel *)iconLbl {
    
    if (!_iconLbl) {
        _iconLbl = [[UILabel alloc] init];
        _iconLbl.font = [UIFont yc_13];
        _iconLbl.textColor = [UIColor yc_hex_F8513B];
        _iconLbl.text = @"";
        _iconLbl.textAlignment = NSTextAlignmentLeft;
    }
    return _iconLbl;
}
- (UILabel *)titleLbl {
    
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc] init];
        _titleLbl.font = [UIFont yc_18_bold];
        _titleLbl.textColor = [UIColor yc_hex_333440];
        _titleLbl.textAlignment = NSTextAlignmentCenter;
        _titleLbl.numberOfLines = 0;
    }
    return _titleLbl;
}
- (UILabel *)msgLbl {
    
    if (!_msgLbl) {
        _msgLbl = [[UILabel alloc] init];
        _msgLbl.font = [UIFont yc_14];
        _msgLbl.textColor = [UIColor yc_hex_5B6071];
        _msgLbl.textAlignment = NSTextAlignmentCenter;
        _msgLbl.numberOfLines = 0;
    }
    return _msgLbl;
}

- (NSMutableArray<YCCustomAlertAction *> *)alerActions {
    
    if (!_alerActions) {
        _alerActions = [[NSMutableArray alloc] init];
    }
    return _alerActions;
}
@end

@implementation YCCustomAlertAction

+ (YCCustomAlertAction *)actionWithTitle:(NSString *)title type:(YCActionType)type action:(CustomAction)action {
    
    YCCustomAlertAction *alertAction = [[YCCustomAlertAction alloc] init];
    alertAction.title = title;
    alertAction.action = action;
    alertAction.actionType = type;
    alertAction.font = [UIFont yc_18];
    if (type == YCAlertTypeDefault) {
        alertAction.fontColor = [UIColor yc_hex_F8513B];
    }
    if (type == YCActionTypeCancel) {
        alertAction.fontColor = [UIColor yc_hex_9B9EA8];
    }
    if (type == YCActionTypeConfirm) {
        alertAction.fontColor = [UIColor yc_hex_F8513B];
    }
    return alertAction;
}

@end
