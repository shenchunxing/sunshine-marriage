//
//  UIViewController+CustomAlert.h
//  YCBaseUI
//
//  Created by haima on 2019/6/4.
//

#import <UIKit/UIKit.h>
#import "YCCustomAlertViewController.h"

typedef void(^ConfirmHandler)(void);
typedef void(^CancelHandler)(void);
@interface UIViewController (CustomAlert)

- (void)showBS_Error:(NSString *)title;
/**
 消息+确定
 
 @param message 消息
 @param block 回调
 */
- (void)yc_showAlertWithMessage:(NSString *)message
                        confirm:(ConfirmHandler)block;

/**
 标题+消息+确定

 @param title 标题
 @param message 消息
 @param block 确定回调
 */
- (void)yc_showAlertWithTitle:(NSString *)title
                      message:(NSString *)message
                      confirm:(ConfirmHandler)block;

/**
 标题+消息+确定 增加消息对齐方式

 @param title 标题
 @param message 消息
 @param alignment 对齐方式
 @param block 回调
 */
- (void)yc_showAlertWithTitle:(NSString *)title
                      message:(NSString *)message
             messageAlignment:(NSTextAlignment)alignment
                      confirm:(ConfirmHandler)block;

/**
 标题+消息+（取消+确定）

 @param title 标题
 @param message 消息
 @param confirmBlock 确定
 @param cancelBlock 取消
 */
- (void)yc_showAlertWithTitle:(NSString *)title
                      message:(NSString *)message
                      confirm:(ConfirmHandler)confirmBlock
                       cancel:(CancelHandler)cancelBlock;


- (void)yc_showAlertWithTitle:(NSString *)title
                      message:(NSString *)message
             messageAlignment:(NSTextAlignment)alignment
                 confirmTitle:(NSString *)confirmTitle
                 confirmColor:(UIColor *)confirmColor
                      confirm:(ConfirmHandler)confirmBlock
                  cancelTitle:(NSString *)cancelTitle
                  cancelColor:(UIColor *)cancelColor
                       cancel:(CancelHandler)cancelBlock;

//YCCustomAlertViewController

/**
 自定义弹框

 @param title 标题
 @param type 类型，默认YCAlertTypeDefault
 @param confirm 确定回调
 */
- (void)yc_showCustomAlertWithTitle:(NSString *)title type:(YCAlertType)type confirm:(ConfirmHandler)confirm;
- (void)yc_showCustomAlertWithTitle:(NSString *)title type:(YCAlertType)type message:(NSString *)message confirm:(ConfirmHandler)confirm;
- (void)yc_showCustomAlertWithTitle:(NSString *)title type:(YCAlertType)type cancel:(CancelHandler)cancel confirm:(ConfirmHandler)confirm;
- (void)yc_showCustomAlertWithTitle:(NSString *)title type:(YCAlertType)type message:(NSString *)message cancel:(CancelHandler)cancel confirm:(ConfirmHandler)confirm;

- (void)yc_showCustomAlertWithTitle:(NSString *)title
                            message:(NSString *)message
                               type:(YCAlertType)type
                        cancelTitle:(NSString *)cancelTitle
                             cancel:(CancelHandler)cancel
                       confirmTitle:(NSString *)confirmTitle
                            confirm:(ConfirmHandler)confirm;

@end
