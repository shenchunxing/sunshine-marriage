//
//  YCCustomAlertViewController.h
//  YCBaseUI
//
//  Created by shenweihang on 2019/12/2.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, YCAlertType) {
    YCAlertTypeDefault, //不带图标
    YCAlertTypeIcon     //带图标
};
typedef NS_ENUM(NSUInteger, YCActionType) {
    YCActionTypeDefault,
    YCActionTypeCancel,
    YCActionTypeConfirm
};

typedef void(^CustomAction)(void);
@interface YCCustomAlertAction : NSObject

/* 标题 */
@property (nonatomic, copy) NSString *title;
/* 字体 */
@property (nonatomic, strong) UIFont *font;
/* 字体颜色 */
@property (nonatomic, strong) UIColor *fontColor;
/* 回调 */
@property (nonatomic, copy) CustomAction action;
/* 类型，默认YCActionTypeDefault */
@property (nonatomic, assign) YCActionType actionType;

+ (YCCustomAlertAction *)actionWithTitle:(NSString *)title type:(YCActionType)type action:(CustomAction)action;

@end

@interface YCCustomAlertViewController : UIViewController

+ (YCCustomAlertViewController *)alertControllerWithTitle:(NSString *)title message:(NSString *)message type:(YCAlertType)alertType;
- (void)addAlertAction:(YCCustomAlertAction *)action;
- (void)addAlertActions:(NSArray<YCCustomAlertAction *> *)actions;
@end

NS_ASSUME_NONNULL_END
