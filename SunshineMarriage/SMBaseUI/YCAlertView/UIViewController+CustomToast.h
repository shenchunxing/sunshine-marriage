//
//  UIViewController+AlertMessage.h
//  YCBaseUI
//
//  Created by haima on 2019/4/19.
//

#import <UIKit/UIKit.h>

/**
 该分类统一处理toast
 */
@interface UIViewController (CustomToast)


#pragma mark - toast提示
- (void)showMessage:(NSString *)message afterDelay:(NSTimeInterval)delay;
- (void)showMessage:(NSString *)message afterDelay:(NSTimeInterval)delay completion:(void(^)(void))completion;
- (void)showSuccessMessage:(NSString *)message afterDelay:(NSTimeInterval)delay;
- (void)showSuccessMessage:(NSString *)message afterDelay:(NSTimeInterval)delay completion:(void(^)(void))completion;
- (void)showFailureMessage:(NSString *)message afterDelay:(NSTimeInterval)delay;
- (void)showFailureMessage:(NSString *)message afterDelay:(NSTimeInterval)delay completion:(void(^)(void))completion;
- (void)showToast:(NSString *)message;
- (void)showToastInKeyWindow:(NSString *)message;
- (void)showToastOnWindow:(NSString *)message;

- (void)showCustomMessage:(NSString *)message horizontalPadding:(CGFloat)horizontalPadding verticalPadding:(CGFloat)verticalPadding cornerRadius:(CGFloat)cornerRadius afterDelay:(NSTimeInterval)delay completion:(void(^)(void))completion ;

- (void)show_clearCacheSuccess:(NSString *)message ;

/// 银行端-新增模块
/// @param message message
- (void)showCustomMessage_newAddModule:(NSString *)message;

- (void)showCustomMessage_SM:(NSString *)message;


@end
