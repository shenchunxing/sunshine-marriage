//
//  YCNavigationController.h
//  Pods-YCHomeModule_Example
//
//  Created by 沈春兴 on 2019/4/2.
//

#import <UIKit/UIKit.h>

@interface YCNavigationController : UINavigationController<UIGestureRecognizerDelegate,UINavigationControllerDelegate>

- (void)pushViewController:(UIViewController *)viewController;

+ (YCNavigationController *)currentNavigationController;

@end
