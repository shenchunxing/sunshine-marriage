//
//  YCSearchView.h
//  YCBaseUI
//
//  Created by 刘成 on 2019/4/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^YCSearchChangeHandle)(NSString *searchText);
typedef void(^YCSearchLeftClickHandle)(void);

typedef NS_ENUM(NSInteger , YCSearchType) {
    YCSearchTypeCommon = 0,
    YCSearchTypeVideoFace,
    YCSearchTypeCarDealer, //车商
    YCSearchTypeCancel      //右侧带‘取消’按钮
};

@protocol YCSearchViewDelegate <NSObject>

- (void)interruptCancleButtonClick ;

@end

@interface YCSearchView : UIView

@property (copy, nonatomic) NSString *placeHolderStr;
@property (assign, nonatomic) YCSearchType searchType;
@property (nonatomic, copy) NSString *buttonTitle;
@property (nonatomic, assign) CGRect whiteFrame;
@property (nonatomic, assign) CGRect rightBtnFrame;
@property (nonatomic, weak) id<YCSearchViewDelegate> delegate;
/**
 通用
 */
- (instancetype)initWithFrame:(CGRect)frame placeHolderStr:(NSString *)placeHolderStr searchHandle:(YCSearchChangeHandle)searchHandle;

/**
 视频面签时使用
 */
- (instancetype)initWithFrame:(CGRect)frame leftClickHandle:(YCSearchLeftClickHandle)leftClickHandle searchHandle:(YCSearchChangeHandle)searchHandle;

/**
 车商订单
 */
- (instancetype)initWithFrame:(CGRect)frame searchType:(YCSearchType)searchType placeHolderStr:(NSString *)placeHolderStr searchHandle:(YCSearchChangeHandle)searchHandle;

- (void)setLeftBtnTitle:(NSString *)str;

/* 取消回调 */
@property (nonatomic, strong) void(^cancelHandler)(void);

- (void)yc_beconmeFirstResponser;
@end

NS_ASSUME_NONNULL_END
