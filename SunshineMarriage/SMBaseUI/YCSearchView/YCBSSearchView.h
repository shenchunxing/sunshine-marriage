//
//  YCBSSearchView.h
//  YCBaseUI
//
//  Created by 刘成 on 2019/11/25.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^YCBSSearchChangeHandle)(NSString *searchText);

@interface YCBSSearchView : UIView

@property (copy, nonatomic) NSString *placeHolderStr;

- (instancetype)initWithFrame:(CGRect)frame placeHolderStr:(NSString *)placeHolderStr searchHandle:(YCBSSearchChangeHandle)searchHandle;

@end

NS_ASSUME_NONNULL_END
