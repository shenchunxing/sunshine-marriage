//
//  YCBSSearchView.m
//  YCBaseUI
//
//  Created by 刘成 on 2019/11/25.
//

#import "YCBSSearchView.h"
#import "YCBaseUI.h"
#import "SMDefine.h"
#import "YCCategoryModule.h"

@interface YCBSSearchView ()

@property (copy, nonatomic) YCBSSearchChangeHandle searchHandle;

@property (copy, nonatomic) NSString *inputStr;

@property (nonatomic,strong) UITextField *textField;

@end

@implementation YCBSSearchView

- (instancetype)initWithFrame:(CGRect)frame placeHolderStr:(NSString *)placeHolderStr searchHandle:(YCBSSearchChangeHandle)searchHandle{
    
    if (self = [super initWithFrame:frame]) {
        _searchHandle = searchHandle;
        _placeHolderStr = placeHolderStr;
        _inputStr = @"";
        [self createUI];
    }
    return self;
}

- (void)createUI{
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 2;
    
    UIImageView *searhIcon = [UIImageView imageViewWithFrame:CGRectMake(8, 0, 14, 14) image:@"icon_search" bundle:@"YCBaseUI"];
    searhIcon.centerY = VH(self)/2.0;
    [self addSubview:searhIcon];
    
    UITextField *textField = [[UITextField alloc]initWithFrame:CGRectMake(FW(searhIcon)+8, 0, VW(self)-34, VH(self))];
    
    textField.tag = 100;
    textField.borderStyle = UITextBorderStyleNone;
    textField.font = [UIFont yc_14];
    textField.textColor = [UIColor yc_hex_3C3D49];
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [textField setPlaceHolder:(_placeHolderStr?:@"搜索订单编号/客户姓名") color:[UIColor yc_hex_9B9EA8] font:[UIFont yc_14]];

    [textField addTarget:self action:@selector(textChanged:) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:textField];
    
    self.textField = textField;
    
}
- (void)textChanged:(UITextField *)sender {
    NSString *text = sender.text;
    DELog(@"textChanged---%@",text);
    
    if (_searchHandle && ![_inputStr isEqualToString:text]) {
        _inputStr = text;
        
        _searchHandle(text);
    }
}

- (void)rightClick{
    if (_searchHandle && _inputStr.length>0) {
        _searchHandle(_inputStr);
    }
}


- (void)yc_beconmeFirstResponser {
    [self.textField becomeFirstResponder];
}


- (void)setPlaceHolderStr:(NSString *)placeHolderStr
{
    self.textField.placeholder = placeHolderStr;
}

@end
