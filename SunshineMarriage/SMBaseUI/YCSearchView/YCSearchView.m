//
//  YCSearchView.m
//  YCBaseUI
//
//  Created by 刘成 on 2019/4/8.
//

#import "YCSearchView.h"
#import "YCBaseUI.h"
#import "SMDefine.h"
#import "YCCategoryModule.h"

@interface YCSearchView ()

@property (copy, nonatomic) YCSearchLeftClickHandle leftClickHandle;

@property (copy, nonatomic) YCSearchChangeHandle searchHandle;

@property (copy, nonatomic) NSString *inputStr;
@property (nonatomic, strong) UIButton *rightBtn;
@property (nonatomic, strong) UIView *whiteView;


@property (nonatomic,strong) UITextField *textField;

@end

@implementation YCSearchView

- (instancetype)initWithFrame:(CGRect)frame placeHolderStr:(NSString *)placeHolderStr searchHandle:(YCSearchChangeHandle)searchHandle{
    
    return [self initWithFrame:frame placeHolderStr:placeHolderStr searchType:YCSearchTypeCommon leftClickHandle:nil searchHandle:searchHandle];
}

/**
 车商订单
 */
- (instancetype)initWithFrame:(CGRect)frame searchType:(YCSearchType)searchType placeHolderStr:(NSString *)placeHolderStr searchHandle:(YCSearchChangeHandle)searchHandle{
    
    return [self initWithFrame:frame placeHolderStr:placeHolderStr searchType:searchType leftClickHandle:nil searchHandle:searchHandle];
}

- (instancetype)initWithFrame:(CGRect)frame leftClickHandle:(YCSearchLeftClickHandle)leftClickHandle searchHandle:(YCSearchChangeHandle)searchHandle{
    
    return [self initWithFrame:frame placeHolderStr:@"请输入搜索内容" searchType:YCSearchTypeVideoFace leftClickHandle:leftClickHandle searchHandle:searchHandle];
}

- (instancetype)initWithFrame:(CGRect)frame placeHolderStr:(NSString *)placeHolderStr searchType:(YCSearchType)searchType leftClickHandle:(YCSearchLeftClickHandle)leftClickHandle searchHandle:(YCSearchChangeHandle)searchHandle{
    
    if (self = [super initWithFrame:frame]) {
        _searchType = searchType;
        _leftClickHandle = leftClickHandle;
        _searchHandle = searchHandle;
        _placeHolderStr = placeHolderStr;
        _inputStr = @"";
        
        [self createUI];
    }
    return self;
}


- (void)createUI{
    self.backgroundColor = _searchType==YCSearchTypeCarDealer?[UIColor whiteColor]:[UIColor yc_hex_108EE9];
    
    UIView *whiteView = [[UIView alloc]initWithFrame:CGRectMake(14, 12, kWIDTH-28, 36)];
    whiteView.backgroundColor = _searchType==YCSearchTypeCarDealer?[UIColor yc_hex_F5F6FA]:[UIColor whiteColor];
    whiteView.layer.cornerRadius = 2;
    [self addSubview:whiteView];
    self.whiteView = whiteView;
    
    UIButton *leftBtn;
    UIButton *rightBtn;
    if (_searchType == YCSearchTypeVideoFace) {
        leftBtn = [UIButton customWithFrame:CGRectMake(14, 0, 40, 40) title:@"姓名" titleColor:[UIColor whiteColor] fontSize:[UIFont yc_14] image:@"" target:self action:@selector(leftClick)];
        leftBtn.centerY = whiteView.centerY;
        [leftBtn setImage:[UIImage yc_imgWithName:@"icon_sanjiao_chaoxia" bundle:@"YCBaseUI"] forState:UIControlStateNormal];
        leftBtn.imageEdgeInsets = UIEdgeInsetsMake(16, 32, 16, 0);
        leftBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 8);
        leftBtn.tag = 10086;
        [self addSubview:leftBtn];
        
        whiteView.frame = CGRectMake(FW(leftBtn)+16, 12, kWIDTH-28-56-44, 36);
        
        rightBtn = [UIButton systemWithFrame:CGRectMake(FW(whiteView)+12, 0, 32, 40) title:@"查询" titleColor:[UIColor whiteColor] fontSize:[UIFont yc_16] target:self action:@selector(rightClick)];
        rightBtn.centerY = whiteView.centerY;
        [self addSubview:rightBtn];
        self.rightBtn = rightBtn;

    }
    if (_searchType == YCSearchTypeCancel) {
        whiteView.frame = CGRectMake(14, 12, kWIDTH-14-58, 36);
        rightBtn = [UIButton customWithFrame:CGRectMake(FW(whiteView)+12, 0, 32, 40) title:@"取消" titleColor:[UIColor whiteColor] fontSize:[UIFont yc_16] target:self action:@selector(cancelClick)];
        rightBtn.centerY = whiteView.centerY;
        [self addSubview:rightBtn];
        self.rightBtn = rightBtn;
    }
    
    UIImageView *searhIcon = [UIImageView imageViewWithFrame:CGRectMake(12, 0, 14, 14) image:@"icon_search" bundle:@"YCBaseUI"];
    searhIcon.centerY = VH(whiteView)/2.0;
    [whiteView addSubview:searhIcon];
    
    UITextField *textField = [[UITextField alloc]initWithFrame:CGRectMake(FW(searhIcon)+3, 0, VW(whiteView)-34, VH(whiteView))];
//    textField.placeholder = _placeHolderStr?:@"搜索客户姓名";
    
    textField.tag = 100;
    textField.borderStyle = UITextBorderStyleNone;
    textField.font = [UIFont yc_14];
    textField.textColor = [UIColor yc_hex_3C3D49];
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [textField setPlaceHolder:(_placeHolderStr?:@"搜索客户姓名") color:[UIColor yc_hex_9B9EA8] font:[UIFont yc_14]];
//    [textField setPlaceHolderLabel:[UIColor yc_hex_9B9EA8] font:[UIFont yc_14]];
//    [textField setValue:[UIColor yc_hex_9B9EA8] forKeyPath:@"_placeholderLabel.textColor"];
//    [textField setValue:[UIFont yc_14] forKeyPath:@"_placeholderLabel.font"];
    [textField addTarget:self action:@selector(textChanged:) forControlEvents:UIControlEventEditingChanged];
    [whiteView addSubview:textField];
    
    self.textField = textField;
    
}

- (void)setButtonTitle:(NSString *)buttonTitle {
    _buttonTitle = buttonTitle;
    [self.rightBtn setTitle:buttonTitle forState:UIControlStateNormal];
}

- (void)setWhiteFrame:(CGRect)whiteFrame {
    _whiteFrame = whiteFrame;
    self.whiteView.frame = whiteFrame;
    self.textField.width = self.whiteView.width - 34;
}

- (void)setRightBtnFrame:(CGRect)rightBtnFrame {
    _rightBtnFrame = rightBtnFrame;
    self.rightBtn.frame = rightBtnFrame;
}

- (void)textChanged:(UITextField *)sender {
    NSString *text = sender.text;
    DELog(@"textChanged---%@",text);
    
    if (_searchHandle && ![_inputStr isEqualToString:text]) {
        _inputStr = text;
        
        _searchHandle(text);
//        if (_searchType != YCSearchTypeVideoFace) {
//            _searchHandle(text);
//        }
    }
}

- (void)leftClick{
    if (_leftClickHandle) {
        _leftClickHandle();
    }
}

- (void)rightClick{
    
    if (_searchHandle && _inputStr.length>0) {
        _searchHandle(_inputStr);
    }
}

- (void)cancelClick {
    
    if ([self.delegate respondsToSelector:@selector(interruptCancleButtonClick)]) {
        [self.delegate interruptCancleButtonClick];
        return;
    }
    
    _inputStr = nil;
    self.textField.text = nil;
    if (self.cancelHandler) {
        self.cancelHandler();
    }
}

- (void)yc_beconmeFirstResponser {
    [self.textField becomeFirstResponder];
}

- (void)setLeftBtnTitle:(NSString *)str{
    UIButton *btn = [self viewWithTag:10086];
    [btn setTitle:[str substringToIndex:2] forState:UIControlStateNormal];
}

- (void)setPlaceHolderStr:(NSString *)placeHolderStr
{
    self.textField.placeholder = placeHolderStr;
}

- (void)setSearchType:(YCSearchType)searchType{
    _searchType = searchType;
    switch (searchType) {
        case YCSearchTypeCommon:
            self.backgroundColor = [UIColor yc_hex_108EE9];
            break;
        case YCSearchTypeCarDealer:
            self.backgroundColor = [UIColor yc_hex_397AE6];
            break;
        default:
            break;
    }
}


@end
