//
//  YCNavgationBarAdapter.h
//  YCBaseUI
//
//  Created by haima on 2019/5/27.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

//导航栏风格
typedef NS_ENUM(NSUInteger, YCNavigationBarStyle) {
    YCNavigationBarStyleWhite,     //白色
    YCNavigationBarStyleBlue        //蓝色
};

/**
 导航栏风格适配器
 */
@interface YCNavgationBarAdapter : NSObject

//单例
+ (instancetype)shareInstance;


@property (nonatomic, assign, readonly) BOOL isCustom;

/**
 设置导航栏风

 @param style 风格
 */
- (void)adapterNavigationBarStyle:(YCNavigationBarStyle)style;

/**
 导航栏背景色

 @param color 默认色
 @return 背景色
 */
- (UIColor *)adapterNavigationBarColorWithDefaultColor:(UIColor *)color;

/**
 导航栏标题色

 @param color 默认色
 @return 标题色
 */
- (UIColor *)adapterNavigationBarTitleColorWithDefaultColor:(UIColor *)color;

/**
 导航栏按钮色

 @param color 默认
 @return 按钮颜色
 */
- (UIColor *)adapterNavigationBarButtonColorWithDefaultColor:(UIColor *)color;

/**
 导航栏返回按钮图标

 @param image 默认
 @return 图标
 */
- (UIImage *)adapterNavigationBarBackImageWithDefaultImage:(UIImage *)image;

/**
 app主题颜色

 @return 颜色
 */
- (UIColor *)adapterThemeColor;

@end

@interface UIColor (Hex)
+(UIColor *)colorWithHeX:(long)hexColor;
+(UIColor *)colorWithHeX:(long)hexColor alpha:(float)alpha;
+ (UIColor *)colorWithHexString:(NSString *)color;
+ (UIColor *)colorWithHexString:(NSString *)color alpha:(CGFloat)alpha;
@end

NS_ASSUME_NONNULL_END
