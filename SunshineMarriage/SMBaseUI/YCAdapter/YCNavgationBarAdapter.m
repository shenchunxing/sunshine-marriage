//
//  YCNavgationBarAdapter.m
//  YCBaseUI
//
//  Created by haima on 2019/5/27.
//

#import "YCNavgationBarAdapter.h"
#import "UIColor+Style.h"
#import "UIImage+Resources.h"
#import "YCSystemConfigure.h"

static NSString *kNavigationBarColorKey = @"barColor";
static NSString *kNavigationBarTitleColorKey = @"titleColor";
static NSString *kNavigationBarButtonColorKey = @"buttonColor";
static NSString *kNavigationBarBackImageKey = @"backImageName";
static NSString *kNavigationBarBackFont = @"backFont";

@interface YCNavgationBarAdapter ()

/* 无配置表时，使用业务团队app样式 */
@property (nonatomic, assign) BOOL isCustom;

/* 导航栏样式配置表 */
@property (nonatomic, strong) NSDictionary *styleMap;

/* 自定义样式 */
@property (nonatomic, strong) NSDictionary *customDict;

@end

@implementation YCNavgationBarAdapter

+ (instancetype)shareInstance {

    static YCNavgationBarAdapter *_instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[YCNavgationBarAdapter alloc] init];
        [_instance configure];
    });
    return _instance;
}

//MARK:跨平台适配样式，需要各平台自定义导航栏样式配置表
- (void)configure{
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"YCCustomNavBar" ofType:@"plist"];
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
    
    if (dict) {
        self.isCustom = YES;
        self.styleMap = dict;
    } else {
        self.isCustom = NO;
    }
}

- (void)adapterNavigationBarStyle:(YCNavigationBarStyle)style {
    
    if (!self.isCustom) {
        return;
    }
    switch (style) {
        case YCNavigationBarStyleWhite:
            self.customDict = self.styleMap[@"WhiteStyle"];
            break;
        case YCNavigationBarStyleBlue:
            self.customDict = self.styleMap[@"CustomStyle"];
            break;
    }
}

- (UIColor *)adapterNavigationBarColorWithDefaultColor:(UIColor *)color {
    
    NSString *hexColor = self.customDict[kNavigationBarColorKey];
    return !self.isCustom ? color : [UIColor colorWithHexString:hexColor];
}

- (UIColor *)adapterNavigationBarTitleColorWithDefaultColor:(UIColor *)color {
    
    NSString *hexColor = self.customDict[kNavigationBarTitleColorKey];
    return !self.isCustom ? color : [UIColor colorWithHexString:hexColor];
}

- (UIColor *)adapterNavigationBarButtonColorWithDefaultColor:(UIColor *)color {
    
    NSString *hexColor = self.customDict[kNavigationBarButtonColorKey];
    return !self.isCustom ? color : [UIColor colorWithHexString:hexColor];
}

- (UIImage *)adapterNavigationBarBackImageWithDefaultImage:(UIImage *)image {

    return !self.isCustom ? image : [UIImage yc_imageWithNamed:self.customDict[kNavigationBarBackImageKey]];
}

- (UIColor *)adapterThemeColor {
    
    return self.isCustom ? [UIColor colorWithHeX:0x397AE6] : [UIColor colorWithHeX:0x108EE9];
}

@end


@implementation UIColor (Hex)
+(UIColor *)colorWithHeX:(long)hexColor{
    return [UIColor colorWithHeX:hexColor alpha:1.0f];
}

+(UIColor *)colorWithHeX:(long)hexColor alpha:(float)alpha{
    float red = ((float)((hexColor &0xFF0000) >> 16))/255.0;
    float green = ((float)((hexColor &0xFF00) >> 8))/255.0;
    float blue = ((float)(hexColor &0xFF))/255.0;
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

+ (UIColor *)colorWithHexString:(NSString *)color {
    return [UIColor colorWithHexString:color alpha:1];
}

+ (UIColor *)colorWithHexString:(NSString *)color alpha:(CGFloat)alpha{
    NSString *cString = [[color stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    if ([cString length] < 6) {
        return [UIColor clearColor];
    }
    if ([cString hasPrefix:@"0X"])
        cString = [cString substringFromIndex:2];
    if ([cString hasPrefix:@"#"])
        cString = [cString substringFromIndex:1];
    if ([cString length] != 6)
        return [UIColor clearColor];
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    return [UIColor colorWithRed:((float) r / 255.0f) green:((float) g / 255.0f) blue:((float) b / 255.0f) alpha:alpha];
    
}
@end
