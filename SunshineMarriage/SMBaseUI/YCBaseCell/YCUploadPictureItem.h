//
//  YCUploadPictureItem.h
//  YCBaseUI
//
//  Created by haima on 2019/4/29.
//

#import "YCTableViewItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCUploadPictureItem : YCTableViewItem
/* 标题 */
@property (nonatomic, copy) NSString *title;
/* 是否可编辑 */
@property (nonatomic, assign) BOOL editable;
/* <#mark#> */
@property (nonatomic, assign) BOOL showBottomLine;

@end

NS_ASSUME_NONNULL_END
