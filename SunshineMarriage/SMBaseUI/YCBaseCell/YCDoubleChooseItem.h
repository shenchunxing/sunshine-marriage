//
//  YCDoubleChooseItem.h
//  YCBaseUI
//
//  Created by haima on 2019/3/30.
//

#import "YCBaseItem.h"
#import "YCOptionProtocol.h"

@interface YCDoubleChooseItem : YCBaseItem

/* 所选项code */
@property (nonatomic, copy) NSString *code;

/* 选项组 */
@property (nonatomic, copy) NSArray<id<YCOptionProtocol>> *optionModels;

/* 选中回调 */
@property (nonatomic, strong) void(^chooseHandler)(NSString *code);

@property (nonatomic, strong) void(^chooseKeyHandler)(NSString *code, NSString *key);



+ (instancetype)doubleChooseItemWithTitle:(NSString *)title;

- (NSString *)selectedOptionNameByCode:(NSString *)code;

@end

