//
//  YCSingleButtonItem.m
//  YCBaseUI
//
//  Created by haima on 2019/5/24.
//

#import "YCSingleButtonItem.h"
#import "UIColor+Style.h"
@implementation YCSingleButtonItem

+ (instancetype)singleButtonItemWithTitle:(NSString *)title {

    YCSingleButtonItem *item = [[YCSingleButtonItem alloc] init];
    item.title = title;
    item.isBorder = NO;
    item.disable = YES;
    item.backgroundColor = [UIColor yc_hex_108EE9];
    item.disableColor = [UIColor yc_hex_CFD6E1];
    return item;
}
@end
