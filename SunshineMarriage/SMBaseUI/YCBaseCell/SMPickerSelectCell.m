//
//  SMPickerSelectCell.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/2/6.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMPickerSelectCell.h"
#import <Masonry/Masonry.h>
#import "YCCategoryModule.h"
#import "SMDefine.h"
#import "YCOptionModel.h"
#import "SMPickerSelectItem.h"

@interface SMPickerSelectCell ()
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UIImageView *selectedImageView;
@end

@implementation SMPickerSelectCell

- (void)cellDidLoad {
    self.selectionStyle = UITableViewCellSelectionStyleNone ;
    [self.contentView addSubview:self.selectedImageView];
    [self.selectedImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-35);
        make.width.height.mas_equalTo(15);
        make.centerY.mas_equalTo(self.contentView);
    }];
    
   [self.contentView addSubview:self.nameLabel];
   [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       make.left.mas_equalTo(29);
       make.centerY.mas_equalTo(self.contentView);
       make.right.lessThanOrEqualTo(self.selectedImageView.mas_left).mas_offset(-20);
   }];
}

+ (CGFloat)heightForCellWithItem:(SMPickerSelectItem *)item {
    return item.cellHeight;
}

- (void)configCellWithItem:(SMPickerSelectItem *)item {
    self.nameLabel.text = item.model.obtainName;
    self.nameLabel.textColor = item.isChoosed ? [UIColor yc_hex_5360FF]:[UIColor yc_hex_333333];
    self.selectedImageView.image = [UIImage imageNamed:item.isChoosed ? @"icon_selected":@"icon_unselected"];
    self.contentView.backgroundColor = item.isChoosed ? [UIColor yc_hex_F3F4F9]:[UIColor clearColor];
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel yc_labelWithText:@"哈哈哈" textFont:[UIFont yc_17] textColor:[UIColor yc_hex_333333]];
        _nameLabel.numberOfLines = 0;
    }
    return _nameLabel;
}

- (UIImageView *)selectedImageView {
    if (!_selectedImageView) {
        _selectedImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_unselected"]];
        _selectedImageView.highlightedImage = [UIImage imageNamed:@"icon_selected"];
    }
    return _selectedImageView;
}

@end
