//
//  YCBaseCell.h
//  YCBaseUI
//
//  Created by haima on 2019/3/29.
//

#import <UIKit/UIKit.h>
#import "YCTableViewKit.h"
#import "YCBaseItem.h"

@interface YCBaseCell : UITableViewCell<YCTableViewCellProtocol>
/* titleView */
@property (nonatomic, strong) UIView *titleView;
/* 必填标识 */
@property (nonatomic, strong) UILabel *tipLabel;
/* icon */
@property (nonatomic, strong) UIImageView *iconImageView;
/* 标题 */
@property (nonatomic, strong) UILabel *titleLabel;
/* 帮助按钮 */
@property (nonatomic, strong) UIButton *helpBtn;
/* 分割线 */
@property (nonatomic, strong) UIView *lineView;


@end

