//
//  SMOptionPickerStrategy.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/28.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMPickerStrategy.h"
#import "YCOptionModel.h"

typedef void(^SMOptionPickerStrategyBlock)(id _Nullable item);
NS_ASSUME_NONNULL_BEGIN

@interface SMOptionPickerStrategy : SMPickerStrategy

@property (nonatomic, copy) NSArray<YCOptionModel *> *options;
@property (nonatomic, strong) YCOptionModel *selectedModel;
@property (nonatomic, copy) SMOptionPickerStrategyBlock block;

+ (instancetype)stragetyWithBlock:(SMOptionPickerStrategyBlock)block ;

@end

NS_ASSUME_NONNULL_END
