//
//  YCTipsCell.m
//  YCBusinessManagementModule
//
//  Created by 沈春兴 on 2019/4/15.
//

#import "YCTipsCell.h"
#import "YCBaseUI.h"
#import <Masonry/Masonry.h>
#import "YCCategoryModule.h"

@interface YCTipsCell ()
@property (nonatomic,strong) UILabel *parterNameLabel;
@end

@implementation YCTipsCell

- (void)cellDidLoad
{
    [self.contentView addSubview:self.parterNameLabel];
    [self.parterNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).mas_offset(12);
        make.top.mas_equalTo(16);
    }];
}

+ (CGFloat)heightForCellWithItem:(YCTipsItem *)item
{
    return 45.f;
}

- (void)configCellWithItem:(YCTableViewItem *)item {
    
}

- (UILabel *)parterNameLabel{
    if (_parterNameLabel == nil) {
        _parterNameLabel = [UILabel yc_labelWithText:@"请确认客户信息，若有误请点击修改" textFont:[UIFont yc_14] textColor:[UIColor yc_hex_9B9EA8]];
    }
    return _parterNameLabel;
}


@end
