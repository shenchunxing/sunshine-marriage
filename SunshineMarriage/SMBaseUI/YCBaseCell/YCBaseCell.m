//
//  YCBaseCell.m
//  YCBaseUI
//
//  Created by haima on 2019/3/29.
//

#import "YCBaseCell.h"
#import <Masonry/Masonry.h>
#import "UIImage+Resources.h"
#import "YCCategoryModule.h"

@interface YCBaseCell ()
/* 基础item */
@property (nonatomic, strong) YCBaseItem *baseItem;
@end

@implementation YCBaseCell

#pragma mark - YCTableViewCellProtocol
- (void)cellDidLoad {

    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.titleView];
    [self.titleView addSubview:self.tipLabel];
    [self.titleView addSubview:self.iconImageView];
    [self.titleView addSubview:self.titleLabel];
    [self.titleView addSubview:self.helpBtn];
    [self.contentView addSubview:self.lineView];
    
    [self.titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self.contentView);
        make.width.equalTo(self.contentView.mas_width).multipliedBy(0.4);
    }];
    [self.tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleView).offset(cellMargin);
        make.leading.offset((cellMargin-tipWidth)/2);
        make.width.equalTo(@(tipWidth));
    }];
    
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.offset(cellMargin);
        make.size.mas_equalTo(CGSizeMake(iconWidth, iconWidth));
        make.centerY.equalTo(self.tipLabel);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleView).offset(cellMargin);
        make.leading.offset(cellMargin+iconWidth+5);
        make.bottom.equalTo(self.titleView.mas_bottom);
    }];
    
    [self.helpBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLabel.mas_right).offset(8);
        make.size.mas_equalTo(CGSizeMake(16, 16));
        make.centerY.equalTo(self.iconImageView);
        make.right.lessThanOrEqualTo(self.titleView.mas_right);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.contentView);
        make.height.equalTo(@((1 / [UIScreen mainScreen].scale)));
    }];
    
}

+ (CGFloat)heightForCellWithItem:(YCBaseItem *)item {
    
    if (item.shouldShow) {
        return item.cellHeight;
    }
    return 0;
}

- (void)configCellWithItem:(YCBaseItem *)item {
    
    
    
    self.baseItem = item;
    self.titleLabel.text = item.title;

    self.helpBtn.hidden = !item.showHelp;
    
    if (item.titleColor) {
        self.titleLabel.textColor = item.titleColor;
    }
    
    if (item.titleFont) {
        self.titleLabel.font = item.titleFont;
    }
    
    //MARK:更新约束
    //*
    self.tipLabel.hidden = YES;
    
    CGFloat leftMargin = cellMargin;
    //图标
    self.iconImageView.hidden = !item.hasIcon;
    if (item.hasIcon) {
        leftMargin += (iconWidth+5);
        self.iconImageView.image = [UIImage yc_imageWithNamed:item.iconName];
    }
    //更新标题
    [self.titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.leading.offset(leftMargin);
    }];
    
    CGFloat leftPadding = 0;
    CGFloat rightPadding = 0;
    if (item.separatorStyle == YCTableViewCellSeparatorStyleIndent || item.separatorStyle == YCTableViewCellSeparatorStyleIndentLeft) {
        leftPadding = cellMargin;
    }
    if (item.separatorStyle == YCTableViewCellSeparatorStyleIndent || item.separatorStyle == YCTableViewCellSeparatorStyleIndentRight) {
        rightPadding = -cellMargin;
    }
    [self.lineView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(leftPadding);
        make.right.equalTo(self.contentView).offset(rightPadding);
    }];
    
    if (item.separatorStyle == YCTableViewCellSeparatorStyleIndentNone) {
        self.lineView.hidden = YES;
    }
        
     self.hidden = !item.shouldShow;

}

- (void)setFrame:(CGRect)frame
{
    if (self.baseItem.hasSpaceing) {
        frame.origin.x = 14;
        frame.size.width -= 2 * frame.origin.x;
    }
    [super setFrame:frame];
}

- (void)onHelpButtonClick:(UIButton *)btn {
    
    if (self.baseItem.showHelp && self.baseItem.helpHandler) {
        self.baseItem.helpHandler(self.baseItem.indexPath);
    }
}

#pragma mark - getter
- (UIView *)titleView {
    
    if (_titleView == nil) {
        _titleView = [[UIView alloc] init];
        
    }
    return _titleView;
}
- (UILabel *)tipLabel {
    
    if (_tipLabel == nil) {
        _tipLabel = [[UILabel alloc] init];
        _tipLabel.text = @"*";
        _tipLabel.textColor = [UIColor yc_hex_FF3B30];
        _tipLabel.font = [UIFont yc_minor_font];
        _tipLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _tipLabel;
}

- (UIImageView *)iconImageView {
    
    if (_iconImageView == nil) {
        _iconImageView = [[UIImageView alloc] init];
    }
    return _iconImageView;
}

- (UILabel *)titleLabel {
    
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        _titleLabel.font = [UIFont yc_major_font];
        _titleLabel.numberOfLines = 0;
        _titleLabel.textColor = [UIColor yc_hex_5B6071];
    }
    return _titleLabel;
}

- (UIView *)lineView {
    
    if (_lineView == nil) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = [[UIColor yc_hex_EEEEEE] colorWithAlphaComponent:0.7];
    }
    return _lineView;
}

- (UIButton *)helpBtn {
    
    if (_helpBtn == nil) {
        _helpBtn = [[UIButton alloc] init];
        _helpBtn.hidden = YES;
        [_helpBtn setImage:[UIImage yc_imageWithNamed:@"icon_help"] forState:UIControlStateNormal];
        [_helpBtn addTarget:self action:@selector(onHelpButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _helpBtn;
}



@end
