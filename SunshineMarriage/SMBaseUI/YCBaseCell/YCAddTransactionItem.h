//
//  YCAddTransactionItem.h
//  YCBaseUI
//
//  Created by haima on 2019/4/29.
//

#import "YCTableViewItem.h"

typedef NS_ENUM(NSUInteger, YCAddTransactionType) {
    YCAddTransactionTypeFill,       //填充，蓝色填充，白色标题
    YCAddTransactionTypeBorder,     //描边, 蓝色描边，标题蓝色
};

NS_ASSUME_NONNULL_BEGIN

@interface YCAddTransactionItem : YCTableViewItem

/* 标题 */
@property (nonatomic, copy) NSString *title;
/* 类型 */
@property (nonatomic, assign) YCAddTransactionType transactionType;
/* 是否可点击 */
@property (nonatomic, assign) BOOL enable;

/* 左右缩进，默认0 */
@property (nonatomic, assign) CGFloat margin;
/* 高度 默认48*/
@property (nonatomic, assign) CGFloat cellH;

/* 回调 */
@property (nonatomic, copy) void(^buttonClickHandler)(void);

+ (instancetype)addItemWithTitle:(NSString *)title;

@end

NS_ASSUME_NONNULL_END
