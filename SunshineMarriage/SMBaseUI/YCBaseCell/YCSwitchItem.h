//
//  YCSwitchItem.h
//  YCMineModule
//
//  Created by 沈春兴 on 2019/4/29.
//

#import "YCTableViewItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCSwitchItem : YCTableViewItem

@property (nonatomic,copy) NSString *title;
@property (nonatomic,assign) BOOL open;
@property (nonatomic,assign) CGFloat cellHeight;
@property (nonatomic,copy) void (^switchChangeHandle)(UISwitch *sw);
@property (nonatomic, assign) BOOL custom; //自定义大小颜色

@end

NS_ASSUME_NONNULL_END
