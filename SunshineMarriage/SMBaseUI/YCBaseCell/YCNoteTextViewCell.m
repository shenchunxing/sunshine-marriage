//
//  YCNoteTextView.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/4/15.
//

#import "YCNoteTextViewCell.h"
#import <Masonry/Masonry.h>
#import "YCNoteTextViewItem.h"

@implementation YCNoteTextViewCell

- (void)cellDidLoad {
    
    [super cellDidLoad];
    
    self.textView.placeHolder = @"请输入";
    self.textView.clearMode =  YCTextViewClearModeNever;
    [self.textView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(cellMargin);
        make.left.equalTo(self.contentView.mas_left).offset(5);
        make.right.equalTo(self.contentView.mas_right).offset(-5);
        make.bottom.mas_equalTo(self.contentView.mas_bottomMargin);
    }];
}

+ (CGFloat)heightForCellWithItem:(YCNoteTextViewItem *)item
{
    return item.cellHeight;
}

//- (void)configCellWithItem:(YCNoteTextViewItem *)item
//{
//    self.textView.textView.text = item.
//}

@end
