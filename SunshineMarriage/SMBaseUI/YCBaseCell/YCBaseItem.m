//
//  YCBaseItem.m
//  YCBaseUI
//
//  Created by haima on 2019/3/29.
//

#import "YCBaseItem.h"
#import "YCCategoryModule.h"
#import "NSString+Size.h"

@interface YCBaseItem ()

@end

@implementation YCBaseItem

- (instancetype)init {
    
    if (self = [super init]) {
        self.titleColor = [UIColor yc_hex_5B6071];
        self.textColor = [UIColor yc_hex_3C3D49];
        self.placeHolderColor = [UIColor yc_hex_B3B7C2];
        self.shouldShow = YES;
        self.cellHeight = kDefaultCellHeight;
        self.isRequired = YES;
        self.hasIcon = NO;
        self.iconName = nil;
        self.showHelp = NO;
        self.editStyle  = YCEditStyleEditable;
        self.separatorStyle = YCTableViewCellSeparatorStyleIndent;
    }
    return self;
}

+ (instancetype)normalItemWithTitle:(NSString *)title
{
    YCBaseItem *item = [[YCBaseItem alloc] init];
    item.placeHolder = @"请输入";
    item.isRequired = NO;
    item.hasIcon = NO;
    item.title = title;
    return item;
}

+ (instancetype)itemWithArray:(NSArray *)itemArr{
    YCBaseItem *item = [[NSClassFromString(itemArr[2]) alloc] init];
    item.isRequired = NO;
    item.hasIcon = NO;
    item.placeHolder = itemArr.count > 4 ? itemArr[4]:@"请输入";
    item.title = itemArr[0];
    item.key = itemArr[1];
    item.text = itemArr[3];
    return item;
}

- (void)setEditStyle:(YCEditStyle)editStyle{
    _editStyle = editStyle;
}

- (void)setTextColor:(UIColor *)textColor
{
    _textColor = textColor;
}

- (void)setMaxWidth:(CGFloat)maxWidth {
    if (maxWidth == 0) {
        return;
    }
    [super setMaxWidth:maxWidth];
    [self changerTitleHeight];
    if (!self.noChangeContent) {
        [self needChangeContentHeight];
    }
}

- (void)changerTitleHeight {

    CGSize size = [self.title boundingRectWithSize:CGSizeMake(self.maxWidth * 0.4 - cellMargin - kTitlePadding, HUGE_VAL) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont yc_major_font]} context:nil].size;
    self.cellHeight = MAX((ceil(size.height) + 2 * cellMargin), self.cellHeight);
    self.titleHeight = ceil(size.height) + 2 * cellMargin;
}

- (BOOL)needChangeContentHeight {
    
    //宽度=屏幕宽度-标题宽度-左右缩进-距离-内间距
    CGFloat maxWidth = self.maxWidth * 0.6 - cellMargin - 5.0;
    CGSize size = [self.text yc_boundingRectWithSize:CGSizeMake(maxWidth, HUGE_VAL) font:[UIFont yc_13]];
    CGFloat height = ceil(size.height) + 2 * cellMargin;
    //textview高度增加时，cell高度也增加
    if (height > self.cellHeight) {
        self.cellHeight = height;
        return YES;
    }
    //textview高度减小时，cell也响应减小高度，最小不能小于标题文本高度
    if (height < self.cellHeight && height >= self.titleHeight) {
        self.cellHeight = height;
        return YES;
    }
    return NO;
}

- (NSString *)getRequireNilTipStr{
    
    NSString *classStr = NSStringFromClass([self class]);

    NSString *str = @"请输入";
    
    if ([classStr containsString:@"Picker"]) {
        str = @"请选择";
    }
    
    return [NSString stringWithFormat:@"%@%@",str,self.title];
}

#pragma mark - TDFValidatableProtocol
- (BOOL)valid {
    if (self.isRequired) {
        return self.text.yc_isNotEmpty;
    }
    return YES;
}

- (NSString *)validatedTitle {
    return [self getRequireNilTipStr];
}

@end
