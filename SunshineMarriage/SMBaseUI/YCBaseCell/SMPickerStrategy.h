//
//  SMPickerStrategy.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/28.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SMPickerStrategy : NSObject

//需要子类实现
- (void)invoke;

+ (instancetype)stragety;


@end

NS_ASSUME_NONNULL_END
