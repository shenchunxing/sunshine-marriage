//
//  AccountItem.h
//  YCBSMixingModule
//
//  Created by 沈春兴 on 2019/11/25.
//

#import "YCTableViewItem.h"
NS_ASSUME_NONNULL_BEGIN
@interface AccountItem : YCTableViewItem
@property (nonatomic, copy) NSArray *items;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) CGFloat itemsHeight;

- (instancetype)initWithName:(NSString *)name relations:(NSArray *)relations;
@end

NS_ASSUME_NONNULL_END
