//
//  YCSidebarMenuCell.h
//  YCMineModule
//
//  Created by haima on 2019/4/28.
//

#import <UIKit/UIKit.h>
#import "YCTableViewCellProtocol.h"

@interface YCSidebarMenuCell : UITableViewCell<YCTableViewCellProtocol>

@end

