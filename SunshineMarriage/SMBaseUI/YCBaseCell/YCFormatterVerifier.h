//
//  YCFormatterVerifier.h
//  YCBaseUI
//
//  Created by haima on 2019/7/29.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

//格式验证器
@interface YCFormatterVerifier : NSObject

/**
 数字验证，最多2位小数
 */
+ (BOOL(^)(id value))twoDecimalPlacesValidator;

+ (BOOL(^)(NSString *value))phoneNumberValidator;

@end

NS_ASSUME_NONNULL_END
