//
//  YCOrderCardCell.h
//  YCBSOrderModule
//
//  Created by shenweihang on 2019/11/26.
//

#import <UIKit/UIKit.h>
#import "YCTableViewKit.h"
NS_ASSUME_NONNULL_BEGIN

@interface YCOrderCardCell : UITableViewCell<YCTableViewCellProtocol>

@end

NS_ASSUME_NONNULL_END
