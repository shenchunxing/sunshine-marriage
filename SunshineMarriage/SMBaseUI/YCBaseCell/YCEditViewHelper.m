//
//  YCEditViewHelper.m
//  Pods
//
//  Created by haima on 2019/4/19.
//

#import "YCEditViewHelper.h"
#import "YCBaseItem.h"
#import "YCCategoryModule.h"
#import "UIViewController+CustomToast.h"
#import "UIViewController+CustomAlert.h"
#import "YCValidatableProtocol.h"

@implementation YCEditViewHelper

+ (BOOL)isAnyItemValiableInSections:(NSArray<YCTableViewSection *> *)sections showMessage:(BOOL)isShow {
    for (YCTableViewSection *section in sections) {
        for (YCTableViewItem *item in section.items) {
            if ([item conformsToProtocol:@protocol(YCCellShowProtocol)] &&
                ![(id <YCCellShowProtocol>)item shouldShow]) {
                continue;
            }
            if ([item conformsToProtocol:@protocol(YCValidatableProtocol)]) {
                id <YCValidatableProtocol> fItem = (id <YCValidatableProtocol>)item;
                NSString *validatedTitle = [[fItem validatedTitle] stringByTrimmingCharactersInSet:[NSCharacterSet symbolCharacterSet]];
                
                if ([fItem valid]) {
                    if ([item conformsToProtocol:@protocol(YCFormatValidatableProtocol)]) {
                        id <YCFormatValidatableProtocol> fItem = (id <YCFormatValidatableProtocol>)item;
                        YCFormatValidBlock formatValidBlock = [fItem formatValidBlock];
                        if (formatValidBlock) {
                            id validatedObject = [(id <YCFormatValidatableProtocol>)item validatedObject];
                            if (!formatValidBlock(validatedObject)) {
                                NSString *validateMessage = [validatedTitle yc_suffix:@"格式不正确!"];
                                if ([fItem respondsToSelector:@selector(validatedMessage)] && [fItem validatedMessage].length > 0) {
                                    validateMessage = [fItem validatedMessage];
                                }
                                if (isShow) {
                                    [[UIApplication sharedApplication].keyWindow.rootViewController showToast:validateMessage];
                                }
                                return NO;
                            }
                        }
                    }
                } else {
                    if (isShow) {
                        [[UIApplication sharedApplication].keyWindow.rootViewController showToast:[NSString stringWithFormat:@"%@",validatedTitle]];
                    }
                    return NO;
                }
            }
        }
    }
    return YES;
}

+ (BOOL)isAnyItemValiableInSections:(NSArray<YCTableViewSection *> *)sections showMessage:(BOOL)isShow completion:(void(^)(NSIndexPath *indexPath))block {
    
    for (YCTableViewSection *section in sections) {
        for (YCTableViewItem *item in section.items) {
            if ([item conformsToProtocol:@protocol(YCCellShowProtocol)] &&
                ![(id <YCCellShowProtocol>)item shouldShow]) {
                continue;
            }
            if ([item conformsToProtocol:@protocol(YCValidatableProtocol)]) {
                id <YCValidatableProtocol> fItem = (id <YCValidatableProtocol>)item;
                NSString *validatedTitle = [[fItem validatedTitle] stringByTrimmingCharactersInSet:[NSCharacterSet symbolCharacterSet]];
                
                if ([fItem valid]) {
                    if ([item conformsToProtocol:@protocol(YCFormatValidatableProtocol)]) {
                        id <YCFormatValidatableProtocol> fItem = (id <YCFormatValidatableProtocol>)item;
                        YCFormatValidBlock formatValidBlock = [fItem formatValidBlock];
                        if (formatValidBlock) {
                            id validatedObject = [(id <YCFormatValidatableProtocol>)item validatedObject];
                            if (!formatValidBlock(validatedObject)) {
                                NSString *validateMessage = [validatedTitle yc_suffix:@"格式不正确!"];
                                if ([fItem respondsToSelector:@selector(validatedMessage)] && [fItem validatedMessage].length > 0) {
                                    validateMessage = [fItem validatedMessage];
                                }
                                if (isShow) {
                                    [[UIApplication sharedApplication].keyWindow.rootViewController showToast:validateMessage];
                                }
                                if (block) {
                                    block(item.indexPath);
                                }
                                return NO;
                            }
                        }
                    }
                } else {
                    if (isShow) {
                        [[UIApplication sharedApplication].keyWindow.rootViewController showToast:[NSString stringWithFormat:@"%@",validatedTitle]];
                    }
                    if (block) {
                        block(item.indexPath);
                    }
                    return NO;
                }
            }
        }
    }
    return YES;
}


+ (NSMutableDictionary *)formatSectionData:(NSArray<YCTableViewSection *> *)sections toDictionary:(NSMutableDictionary *)dic {
    
    if (!dic) {
        dic = [NSMutableDictionary dictionary];
    }
    for (YCTableViewSection *section in sections) {
        for (YCTableViewItem *item in section.items) {
            if (item.shouldShow && item.requestKey) {
                if (item.mapRequestBlock) {
                    [dic setValue:item.mapRequestBlock(item.requestValue) forKey:item.requestKey];
                }else{
                    [dic setValue:item.requestValue forKey:item.requestKey];
                }
            }
        }
    }
    return dic;
}

+ (BOOL)isAnyChangedItemInSections:(NSArray<YCTableViewSection *> *)sections {
    
    BOOL changeSign = NO;
    for (YCTableViewSection *section in sections) {
        for (YCTableViewItem *item in section.items) {
            if (item.shouldShow && (item.requestValue || item.preValue)) {
                if (item.mapRequestBlock) {
                    if (![item.mapRequestBlock(item.requestValue) isEqualToString:item.preValue]) {
                        changeSign = YES;
                        break;
                    }
                }else{
                    if (![item.requestValue isEqualToString:item.preValue]) {
                        changeSign = YES;
                        break;
                    }
                }
            }
        }
    }
    return changeSign;
}


+ (BOOL)sm_isAnyItemValiableInSections:(NSArray<YCTableViewSection *> *)sections showMessage:(BOOL)isShow {
    for (YCTableViewSection *section in sections) {
        for (YCBaseItem *item in section.items) {
            if (item.text.length <=0 && item.isRequired) {
                return NO;
            }
        }
    }
    return YES;
}

@end
