//
//  YCCreditOnlyContentCell.m
//  YCCreditModule
//
//  Created by 刘成 on 2019/4/8.
//

#import "YCOnlyContentCell.h"
#import "YCCategoryModule.h"
#import "YCBaseUI.h"
#import "SMDefine.h"
#import "YCOnlyContentItem.h"
#import "NSString+Size.h"

@interface YCOnlyContentCell ()

@property (strong, nonatomic) UILabel *contentLabel;

@property (strong, nonatomic) UIView *lineView;

@end

@implementation YCOnlyContentCell

- (void)cellDidLoad{
    self.backgroundColor = [UIColor whiteColor];
    
    [self.contentView addSubview:self.contentLabel];
    [self.contentView addSubview:self.lineView];

    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@14);
        make.top.equalTo(@-6);
        make.bottom.equalTo(@6);
        make.right.equalTo(@-14);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.bottom.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(14);
        make.height.equalTo(@(kLineHeight));
    }];
}

+ (CGFloat)heightForCellWithItem:(YCTableViewItem *)item{
    
    YCOnlyContentItem *creditItem = (YCOnlyContentItem *)item;
    
    if (!creditItem.shouldShow) {
        return 0;
    }
    return creditItem.cellHeight;
}

- (void)configCellWithItem:(YCTableViewItem *)item {
    YCOnlyContentItem *creditItem = (YCOnlyContentItem *)item;

    self.contentLabel.text = creditItem.text;
    
    self.contentLabel.textColor = creditItem.textColor;
    self.contentLabel.font = creditItem.textFont;
    
    //设置背景色
    if (creditItem.backgroundColor){
        self.contentView.backgroundColor = creditItem.backgroundColor;
    }
    
 
    self.hidden = !creditItem.shouldShow;

}

- (UILabel *)contentLabel{
    if (_contentLabel == nil) {
        _contentLabel = [UILabel labelWithFrame:CGRectZero text:@"该客户的征信不错，可可以进行待贷款申请，贷款 金额不得超过10万" textFont:[UIFont yc_14] textColor:[UIColor yc_hex_3C3D49] textAlignment:NSTextAlignmentLeft numberOfLines:0];
    }
    return _contentLabel;
}

- (UIView *)lineView {
    
    if (_lineView == nil) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = [[UIColor yc_hex_EEEEEE] colorWithAlphaComponent:0.7];
    }
    return _lineView;
}
@end
