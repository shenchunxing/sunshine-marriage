//
//  YCTableViewCell.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/4/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YCCustomCell : UITableViewCell

/**
 *  设置重用id
 */
+ (NSString *)cellIdentifier;
/**
 *  重用cell
 */
+ (instancetype)dequeueReusableCellForTableView:(UITableView *)tableView;
/**
 *  detailTextLabel
 *
 *  @param text      文本
 *  @param textColor 文本颜色
 *  @param font      文本字体
 */
- (UILabel *)detailTextLabelWithText:(NSString *)text textColor:(UIColor *)textColor font:(UIFont *)font;

/**
 *  textLabel
 *
 *  @param text      文本
 *  @param textColor 文本颜色
 *  @param font      文本字体
 */
- (UILabel *)textLabelWithText:(NSString *)text textColor:(UIColor *)textColor font:(UIFont *)font;


@end

NS_ASSUME_NONNULL_END
