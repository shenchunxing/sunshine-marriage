//
//  YCSingleButtonItem.h
//  YCBaseUI
//
//  Created by haima on 2019/5/24.
//

#import "YCTableViewItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCSingleButtonItem : YCTableViewItem

/* 标题 */
@property (nonatomic, copy) NSString *title;
/* 是否描边 */
@property (nonatomic, assign) BOOL isBorder;
/* 是否可点击 */
@property (nonatomic, assign) BOOL disable;
/* 背景色 */
@property (nonatomic, strong) UIColor *backgroundColor;
/* 不开点击时背景色 */
@property (nonatomic, strong) UIColor *disableColor;
/* 描边色 */
@property (nonatomic, strong) UIColor *borderColor;

/* 回调 */
@property (nonatomic, strong) void(^btnClickHandler)(void);

+ (instancetype)singleButtonItemWithTitle:(NSString *)title;
@end

NS_ASSUME_NONNULL_END
