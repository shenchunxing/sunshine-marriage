//
//  YCUploadPictureCell.m
//  YCBaseUI
//
//  Created by haima on 2019/4/29.
//

#import "YCUploadPictureCell.h"
#import <Masonry/Masonry.h>
#import "UIImage+YCBundle.h"
#import "UIImage+Resources.h"
#import "UIFont+Style.h"
#import "UIColor+Style.h"
#import "YCUploadPictureItem.h"
#import "SMDefine.h"

@interface YCUploadPictureCell ()

/* 标题 */
@property (nonatomic, strong) UILabel *titleLbl;
/* 图标 */
@property (nonatomic, strong) UIImageView *iconImageView;
/* 查看 */
@property (nonatomic, strong) UILabel *lookOverLbl;
/* > */
@property (nonatomic, strong) UIImageView *nextImageView;
/* <#mark#> */
@property (nonatomic, strong) UIView *bottomLine;
@end

@implementation YCUploadPictureCell

- (void)cellDidLoad {
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.contentView addSubview:self.titleLbl];
    [self.contentView addSubview:self.iconImageView];
    [self.contentView addSubview:self.lookOverLbl];
    [self.contentView addSubview:self.nextImageView];
    [self.contentView addSubview:self.bottomLine];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.offset(14);
        make.centerY.equalTo(self.contentView);
        make.right.lessThanOrEqualTo(self.iconImageView.mas_left);
    }];
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.trailing.offset(-14);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    [self.lookOverLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.nextImageView.mas_left).offset(-2);
        make.centerY.equalTo(self.contentView);
    }];
    [self.nextImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.trailing.offset(-14);
        make.size.mas_equalTo(CGSizeMake(10, 16));
    }];
    [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.offset(14);
        make.trailing.offset(-14);
        make.bottom.equalTo(self.contentView);
        make.height.mas_equalTo(1/[UIScreen mainScreen].scale);
    }];
}

+ (CGFloat)heightForCellWithItem:(YCTableViewItem *)item {
    
    if (item.shouldShow) {
        return 78.0;
    }
    return 0.0;
}

- (void)configCellWithItem:(YCUploadPictureItem *)item {
    self.titleLbl.text = item.title;
    self.iconImageView.hidden = !item.editable;
    self.lookOverLbl.hidden = item.editable;
    self.nextImageView.hidden = item.editable;
    self.bottomLine.hidden = !item.showBottomLine;
}


- (UILabel *)titleLbl {
    
    if (_titleLbl == nil) {
        _titleLbl = [[UILabel alloc] init];
        _titleLbl.textColor = [UIColor yc_hex_5B6071];
        _titleLbl.font = [UIFont yc_15];
        _titleLbl.textAlignment = NSTextAlignmentLeft;
        _titleLbl.text = @"上传图片资料";
    }
    return _titleLbl;
}

- (UIImageView *)iconImageView {
    
    if (_iconImageView == nil) {
        _iconImageView = [[UIImageView alloc] init];
        _iconImageView.image = [UIImage yc_imageWithNamed:@"img_add"];
    }
    return _iconImageView;
}

- (UILabel *)lookOverLbl {
    
    if (_lookOverLbl == nil) {
        _lookOverLbl = [[UILabel alloc] init];
        _lookOverLbl.textColor = [UIColor yc_hex_108EE9];
        _lookOverLbl.font = [UIFont yc_15];
        _lookOverLbl.textAlignment = NSTextAlignmentRight;
        _lookOverLbl.text = @"查看";
    }
    return _lookOverLbl;
}

- (UIImageView *)nextImageView {
    
    if (_nextImageView == nil) {
        _nextImageView = [[UIImageView alloc] init];
        _nextImageView.image = [UIImage yc_imgWithName:@"icon_arrow_right_blue" bundle:@"YCBaseUI"];
    }
    return _nextImageView;
}

- (UIView *)bottomLine {
    
    if (_bottomLine == nil) {
        _bottomLine = [[UIView alloc] init];
        _bottomLine.backgroundColor = [UIColor yc_hex_EEEEEE];
    }
    return _bottomLine;
}
@end
