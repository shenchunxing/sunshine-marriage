//
//  YCTextFieldCell.h
//  YCBaseUI
//
//  Created by haima on 2019/3/29.
//

#import "YCBaseCell.h"
#import "YCTextInputView.h"
#import "YCTextViewItem.h"
@interface YCTextViewCell : YCBaseCell

/* YCTextInputView */
@property (nonatomic, strong) YCTextInputView *textView;

/* item */
@property (nonatomic, strong) YCTextViewItem *textViewItem;




@end
