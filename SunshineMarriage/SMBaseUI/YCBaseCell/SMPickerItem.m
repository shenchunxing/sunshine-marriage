//
//  SMPickerItem.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/28.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMPickerItem.h"
#import "NSString+Size.h"
#import "YCCategoryModule.h"

@implementation SMPickerItem

- (void)setOptionModel:(YCOptionModel *)optionModel {
    _optionModel = optionModel;
    self.text = optionModel.name;
    self.strategy.selectedModel = optionModel ;
}

+ (instancetype)pickerItem {
    
    SMPickerItem *item = [[SMPickerItem alloc] init];
    item.placeHolder = @"请选择";
    return item;
}

+ (instancetype)pickerItemWithTitle:(NSString *)title {
    
    SMPickerItem *item = [SMPickerItem pickerItem];
    item.isRequired = NO;
    item.hasIcon = NO;
    item.title = title;
    return item;
}

- (void)setText:(NSString *)text {
    
    [super setText:text];
}

- (void)setSelectItemHandler:(void (^)(NSIndexPath * _Nonnull))selectItemHandler{
   
    [super setSelectItemHandler:selectItemHandler];
    
    if (selectItemHandler) {
        
    }
}

- (void)setMaxWidth:(CGFloat)maxWidth {

    [super setMaxWidth:maxWidth];
    [self changerTitleHeight];
    if (!self.noChangeContent) {
        [self changeContentHeight];
    }
}

- (void)changeContentHeight {
    
    //宽度=cell宽度-标题宽度-右缩进-图标宽读-间距
    CGFloat offset = self.editStyle == YCEditStyleEditable ? 25 : 0;
    CGFloat maxWidth = self.maxWidth * 0.6 - cellMargin - 5.0 - offset;
    CGSize size = [self.text yc_boundingRectWithSize:CGSizeMake(maxWidth, HUGE_VAL) font:[UIFont yc_13]];
    CGFloat height = ceil(size.height) + 2 * cellMargin;
    if (height > self.titleHeight) {
        self.cellHeight = height;
    }
}
@end
