//
//  YCTipsCell.h
//  YCBusinessManagementModule
//
//  Created by 沈春兴 on 2019/4/15.
//

#import <UIKit/UIKit.h>
#import "YCTableViewKit.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCTipsCell : UITableViewCell<YCTableViewCellProtocol>

@end

NS_ASSUME_NONNULL_END
