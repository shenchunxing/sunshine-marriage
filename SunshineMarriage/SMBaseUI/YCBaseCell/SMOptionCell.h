//
//  SMOptionCell.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/28.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YCOptionModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface SMOptionCell : UITableViewCell

@property (nonatomic, strong) YCOptionModel  *model;



@end

NS_ASSUME_NONNULL_END
