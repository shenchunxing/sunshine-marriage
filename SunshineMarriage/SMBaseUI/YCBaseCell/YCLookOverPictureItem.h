//
//  YCLookOverPictureItem.h
//  YCBaseUI
//
//  Created by haima on 2019/5/20.
//

#import "YCTableViewItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCLookOverPictureItem : YCTableViewItem

/* 标题 */
@property (nonatomic, copy) NSString *title;
/* 已上传图片数量 */
@property (nonatomic, copy) NSString *value;
/* 占位提示文案 默认 必传， value为空时显示 */
@property (nonatomic, copy) NSString *placeholder;
/* 附件code */
@property (nonatomic, copy) NSString *code;
/* 是否必填 */
@property (nonatomic, assign) BOOL isRequired;

+ (instancetype)itemWithTitle:(NSString *)title value:(NSString *)value;
@end

NS_ASSUME_NONNULL_END
