//
//  YCEditViewHelper.h
//  Pods
//
//  Created by haima on 2019/4/19.
//

#import <Foundation/Foundation.h>
#import "YCTableViewKit.h"

@interface YCEditViewHelper : NSObject

/**
 验证所有items
 itme非空验证的需要实现YCValidatableProtocol，格式验证的需要实现YCFormatValidatableProtocol

 @param sections YCTableViewSection
 @param isShow 是否提示信息
 @return 验证是否通过
 */
+ (BOOL)isAnyItemValiableInSections:(NSArray<YCTableViewSection *> *)sections showMessage:(BOOL)isShow;

+ (BOOL)isAnyItemValiableInSections:(NSArray<YCTableViewSection *> *)sections showMessage:(BOOL)isShow completion:(void(^)(NSIndexPath *indexPath))block;

/**
 取出YCBaseItem子类中的requestKey\requestValue，用于上传给服务端

 @param sections YCTableViewSection
 @param dic 可以时模型对象字典
 @return 用于上传的json
 */
+ (NSMutableDictionary *)formatSectionData:(NSArray<YCTableViewSection *> *)sections toDictionary:(NSMutableDictionary *)dic;

/**
 检查所有items是否有编辑过

 @param sections YCTableViewSection
 @return 是否编辑
 */
+ (BOOL)isAnyChangedItemInSections:(NSArray<YCTableViewSection *> *)sections;

+ (BOOL)sm_isAnyItemValiableInSections:(NSArray<YCTableViewSection *> *)sections showMessage:(BOOL)isShow;

@end

