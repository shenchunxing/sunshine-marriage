//
//  YCMutiTimerCell.m
//  YCBaseUI
//
//  Created by haima on 2019/3/29.
//

#import "YCMutiTimerCell.h"
#import <Masonry/Masonry.h>
#import "YCMutiTimerItem.h"

@interface YCMutiTimerCell ()

/* 开始时间文本 */
@property (nonatomic, strong) UITextField *startTimeText;
/* 开始按钮 */
@property (nonatomic, strong) UIButton *startButton;
/* —— */
@property (nonatomic, strong) UILabel *symoblLabel;

/* 结束时间文本 */
@property (nonatomic, strong) UITextField *endTimeText;
/* 结束按钮 */
@property (nonatomic, strong) UIButton *endButton;
/* item */
@property (nonatomic, strong) YCMutiTimerItem *timerItem;

@end


@implementation YCMutiTimerCell

- (void)cellDidLoad {
    
    [super cellDidLoad];
    [self.contentView addSubview:self.startTimeText];
    [self.contentView addSubview:self.startButton];
    [self.contentView addSubview:self.symoblLabel];
    [self.contentView addSubview:self.endTimeText];
    [self.contentView addSubview:self.endButton];
    
    [self.startTimeText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(cellMargin);
        make.left.equalTo(self.titleView.mas_right).offset(5);
        make.centerY.equalTo(self.titleLabel);
    }];
    [self.startButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.startTimeText.mas_left).offset(-5);
        make.top.equalTo(self.startTimeText.mas_top).offset(-5);
        make.right.equalTo(self.startTimeText.mas_right).offset(5);
        make.bottom.equalTo(self.startTimeText.mas_bottom).offset(5);
    }];
    [self.symoblLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.startTimeText.mas_right).offset(2);
        make.centerY.equalTo(self.startTimeText);
        make.height.equalTo(@10);
    }];
    [self.endTimeText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.startTimeText);
        make.left.equalTo(self.symoblLabel.mas_right).offset(2);
        make.right.lessThanOrEqualTo(self.contentView).offset(-cellMargin);
    }];
    [self.endButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.endTimeText.mas_left).offset(-5);
        make.top.equalTo(self.endTimeText.mas_top).offset(-5);
        make.right.equalTo(self.endTimeText.mas_right).offset(5);
        make.bottom.equalTo(self.endTimeText.mas_bottom).offset(5);
    }];
    //标题抗拉升
    [self.titleLabel setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisVertical];
    [self.startTimeText setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
    [self.endTimeText setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
}

- (void)configCellWithItem:(YCMutiTimerItem *)item {

    [super configCellWithItem:item];
    self.timerItem = item;
    self.startTimeText.text = item.startTime;
    self.startTimeText.placeholder = item.startPlaceholder;
    
    self.endTimeText.text = item.endTime;
    self.endTimeText.placeholder = item.endPlaceholder;
    
    self.startTimeText.textColor = item.editStyle == YCEditStyleEditable ? self.timerItem.textColor : [UIColor yc_hex_5B6071];
    self.endTimeText.textColor = item.editStyle == YCEditStyleEditable ? self.timerItem.textColor : [UIColor yc_hex_5B6071];
    
    self.startButton.hidden = !(item.editStyle == YCEditStyleEditable);
    self.endButton.hidden = !(item.editStyle == YCEditStyleEditable);
}


- (void)onStartTimeClick {
    
    if (self.timerItem.startStrategy) {
        [self.timerItem.startStrategy invoke];
    }
    if (self.timerItem.clickStartHandler) {
        self.timerItem.clickStartHandler();
    }
}

- (void)onEndTimeClick {
    
    if (self.timerItem.endStrategy) {
        [self.timerItem.endStrategy invoke];
    }
    if (self.timerItem.clickEndHandler) {
        self.timerItem.clickEndHandler();
    }
}


#pragma mark - getter
- (UITextField *)startTimeText {
    
    if (_startTimeText == nil) {
        _startTimeText = [[UITextField alloc] init];
        _startTimeText.textAlignment = NSTextAlignmentLeft;
        _startTimeText.userInteractionEnabled = NO;
        _startTimeText.font = [UIFont yc_major_font];
        _startTimeText.textColor = [UIColor yc_hex_3C3D49];
    }
    return _startTimeText;
}

- (UIButton *)startButton {
    
    if (_startButton == nil) {
        _startButton = [[UIButton alloc] init];
        [_startButton addTarget:self action:@selector(onStartTimeClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _startButton;
}

- (UILabel *)symoblLabel {
    
    if (_symoblLabel == nil) {
        _symoblLabel = [[UILabel alloc] init];
        _symoblLabel.text = @"—";
        _symoblLabel.textAlignment = NSTextAlignmentCenter;
        _symoblLabel.textColor = [UIColor yc_hex_9B9EA8];
    }
    return _symoblLabel;
}


- (UITextField *)endTimeText {
    
    if (_endTimeText == nil) {
        _endTimeText = [[UITextField alloc] init];
        _endTimeText.textAlignment = NSTextAlignmentLeft;
        _endTimeText.userInteractionEnabled = NO;
        _endTimeText.font = [UIFont yc_major_font];
        _endTimeText.textColor = [UIColor yc_hex_3C3D49];
    }
    return _endTimeText;
}

- (UIButton *)endButton {
    
    if (_endButton == nil) {
        _endButton = [[UIButton alloc] init];
        [_endButton addTarget:self action:@selector(onEndTimeClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _endButton;
}

@end
