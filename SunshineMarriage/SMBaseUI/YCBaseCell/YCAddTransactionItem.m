//
//  YCAddTransactionItem.m
//  YCBaseUI
//
//  Created by haima on 2019/4/29.
//

#import "YCAddTransactionItem.h"

@implementation YCAddTransactionItem

+ (instancetype)addItemWithTitle:(NSString *)title {
    YCAddTransactionItem *item = [YCAddTransactionItem item];
    item.title = title;
    return item;
}

- (instancetype)init {
    
    if (self = [super init]) {
        self.cellH = 48;
        self.margin = 0;
    }
    return self;
}

@end
