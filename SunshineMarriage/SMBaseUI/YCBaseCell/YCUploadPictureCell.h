//
//  YCUploadPictureCell.h
//  YCBaseUI
//
//  Created by haima on 2019/4/29.
//

#import <UIKit/UIKit.h>
#import "YCTableViewKit.h"
NS_ASSUME_NONNULL_BEGIN

@interface YCUploadPictureCell : UITableViewCell<YCTableViewCellProtocol>

@end

NS_ASSUME_NONNULL_END
