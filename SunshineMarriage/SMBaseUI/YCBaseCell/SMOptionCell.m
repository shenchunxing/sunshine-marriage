//
//  SMOptionCell.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/28.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMOptionCell.h"
#import <Masonry/Masonry.h>
#import "YCCategoryModule.h"
#import "SMDefine.h"


@interface SMOptionCell ()
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *selectedImageView;
@end

@implementation SMOptionCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {

        [self.contentView addSubview:self.selectedImageView];
//        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.selectedBackgroundView = [[UIView alloc] initWithFrame:self.frame];
        self.selectedBackgroundView.backgroundColor = [UIColor yc_hex_F3F4F9];
        [self.selectedImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-35);
            make.width.height.mas_equalTo(15);
            make.centerY.mas_equalTo(self.contentView);
        }];
        
        [self.contentView addSubview:self.titleLabel];
       [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
           make.left.mas_equalTo(29);
           make.centerY.mas_equalTo(self.contentView);
           make.right.lessThanOrEqualTo(self.selectedImageView.mas_left).mas_offset(-20);
       }];
    }
    return self;
}

- (void)setModel:(YCOptionModel *)model  {
    _model = model;
    self.titleLabel.text = model.obtainName;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel yc_labelWithText:@"哈哈哈" textFont:[UIFont yc_17] textColor:[UIColor yc_hex_333333]];
    }
    return _titleLabel;
}

- (UIImageView *)selectedImageView {
    if (!_selectedImageView) {
        _selectedImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_unselected"]];
        _selectedImageView.highlightedImage = [UIImage imageNamed:@"icon_selected"];
    }
    return _selectedImageView;
}

@end
