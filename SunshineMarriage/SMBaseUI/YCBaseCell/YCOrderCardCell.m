//
//  YCOrderCardCell.m
//  YCBSOrderModule
//
//  Created by shenweihang on 2019/11/26.
//

#import "YCOrderCardCell.h"
#import <Masonry/Masonry.h>
#import "YCCategoryModule.h"
#import "SMDefine.h"
#import "YCOrderCardItem.h"

@interface YCOrderCardView : UIView

@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UILabel *textLbl;

@end


@interface YCOrderCardCell ()
@property (nonatomic, strong) UIView *containerView;
@end
@implementation YCOrderCardCell

- (void)cellDidLoad {
    self.selectionStyle  = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.containerView];
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 12, 0, 12));
    }];
}

- (void)configCellWithItem:(YCOrderCardItem *)item {
    
    for (NSDictionary *dict in item.infoList) {
        YCOrderCardView *cardView = [[YCOrderCardView alloc] init];
        cardView.titleLbl.text = dict[@"title"];
        cardView.textLbl.text = dict[@"text"];
        if (dict[@"textColor"]) {
            cardView.textLbl.textColor = dict[@"textColor"];
        }else {
            cardView.textLbl.textColor = [UIColor yc_hex_5B6071];
        }
        [self.containerView addSubview:cardView];
        [cardView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.offset(12);
            make.trailing.offset(-12);
        }];
    }
    [self distributeViewsAlongVertical:self.containerView.subviews];
}

- (void)distributeViewsAlongVertical:(NSArray *)views {
    
    NSInteger count = views.count;
    MAS_VIEW *prev;
    for (int i = 0; i < count; i++) {
        MAS_VIEW *v = views[i];
        [v mas_makeConstraints:^(MASConstraintMaker *make) {
            if (prev) {
                make.top.equalTo(prev.mas_bottom).offset(4).priority(UILayoutPriorityDefaultLow);
                if (i == count - 1) {//last one
                    make.bottom.equalTo(self.containerView).offset(-16);
                }
            }
            else {//first one
                make.top.equalTo(self.containerView).offset(16);
            }
            
        }];
        prev = v;
    }
}


+ (CGFloat)heightForCellWithItem:(YCOrderCardItem *)item {
    //cell高度自适应
    return UITableViewAutomaticDimension;
}

#pragma mark - getter
- (UIView *)containerView {
    
    if (!_containerView) {
        _containerView = [[UIView alloc] init];
        _containerView.backgroundColor = [UIColor whiteColor];
        _containerView.layer.cornerRadius = 4.0;
        // 阴影颜色
        _containerView.layer.shadowColor = [UIColor yc_hex_818181].CGColor;
        // 阴影偏移，默认(0, -3)
        _containerView.layer.shadowOffset = CGSizeMake(0,0);
        // 阴影透明度，默认0
        _containerView.layer.shadowOpacity = 0.18;
        // 阴影半径，默认3
        _containerView.layer.shadowRadius = 5;
    }
    return _containerView;
}

@end

@implementation YCOrderCardView

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.titleLbl];
        [self addSubview:self.textLbl];
        [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.equalTo(self);
            make.width.mas_equalTo(FitScale(70));
            make.bottom.lessThanOrEqualTo(self);
        }];
        [self.textLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.right.equalTo(self);
            make.left.equalTo(self.titleLbl.mas_right);
            make.bottom.lessThanOrEqualTo(self);
        }];
    }
    return self;
}

- (UILabel *)titleLbl {
    
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc] init];
        _titleLbl.font = [UIFont yc_14];
        _titleLbl.textColor = [UIColor yc_hex_9B9EA8];
        _titleLbl.textAlignment = NSTextAlignmentLeft;
        _titleLbl.numberOfLines = 0;
    }
    return _titleLbl;
}
- (UILabel *)textLbl {
    
    if (!_textLbl) {
        _textLbl = [[UILabel alloc] init];
        _textLbl.font = [UIFont yc_14];
        _textLbl.textColor = [UIColor yc_hex_5B6071];
        _textLbl.textAlignment = NSTextAlignmentLeft;
        _textLbl.numberOfLines = 0;
    }
    return _textLbl;
}
@end
