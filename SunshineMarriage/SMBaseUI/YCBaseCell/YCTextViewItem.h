//
//  YCTextFieldItem.h
//  YCBaseUI
//
//  Created by haima on 2019/3/29.
//

#import "YCBaseItem.h"

typedef void(^YCTextUpdateHandle)(NSString *key, NSString *text);

@interface YCTextViewItem : YCBaseItem<YCFormatValidatableProtocol>

/* 是否第一响应者 ,默认NO */
@property (nonatomic, assign) BOOL becomeFirstResponder;

@property (copy, nonatomic) YCTextUpdateHandle textUpdateHandle;

@property (nonatomic,assign) BOOL hideClearBtn;//是否隐藏清除按钮

/* 输入键盘类型 */
@property (nonatomic, assign) UIKeyboardType keyBoardType;

//限制长度
@property (nonatomic,assign) NSInteger limitLength;

+ (instancetype)textViewItem;

+ (instancetype)textViewItemWithTitle:(NSString *)title;

+ (instancetype)personalInfo:(NSString *)title;


@end

