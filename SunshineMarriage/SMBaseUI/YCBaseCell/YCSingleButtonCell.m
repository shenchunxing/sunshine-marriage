//
//  YCSingleButtonCell.m
//  YCBaseUI
//
//  Created by haima on 2019/5/24.
//

#import "YCSingleButtonCell.h"
#import <Masonry/Masonry.h>
#import "YCTableViewKit.h"
#import "UIFont+Style.h"
#import "UIColor+Style.h"
#import "YCSingleButtonItem.h"

@interface YCSingleButtonCell ()<YCTableViewCellProtocol>

@property (nonatomic, strong) UIButton *singleBtn;
/* <#mark#> */
@property (nonatomic, strong) YCSingleButtonItem *item;
@end

@implementation YCSingleButtonCell

- (void)cellDidLoad {
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    
    [self.contentView addSubview:self.singleBtn];
    [self.singleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.offset(14);
        make.trailing.offset(-14);
        make.centerY.equalTo(self.contentView);
        make.height.mas_equalTo(48);
    }];
}

+ (CGFloat)heightForCellWithItem:(YCTableViewItem *)item {
    return 48.0;
}

- (void)configCellWithItem:(YCSingleButtonItem *)item {
    
    self.item = item;
    [self.singleBtn setTitle:item.title forState:UIControlStateNormal];
    self.singleBtn.userInteractionEnabled = !item.disable;
    if (item.disable) {
        [self.singleBtn setBackgroundColor:item.disableColor];
    }else{
        [self.singleBtn setBackgroundColor:item.backgroundColor];
    }
    if (item.isBorder) {
        self.singleBtn.layer.borderColor = item.borderColor.CGColor;
        self.singleBtn.layer.borderWidth = 1/[UIScreen mainScreen].scale;
    }
}

- (void)onSingleButtonClick:(UIButton *)button {
    
    if (!self.item.disable) {
        !self.item.btnClickHandler?:self.item.btnClickHandler();
    }
}

- (UIButton *)singleBtn {
    
    if (_singleBtn == nil) {
        _singleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _singleBtn.titleLabel.font = [UIFont yc_18];
        _singleBtn.layer.cornerRadius = 2.0;
        [_singleBtn addTarget:self action:@selector(onSingleButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _singleBtn;
}

@end
