//
//  YCTextFieldItem.m
//  YCBaseUI
//
//  Created by haima on 2019/3/29.
//

#import "YCTextViewItem.h"
#import "NSString+Size.h"
#import <objc/runtime.h>

@interface YCTextViewItem ()

@end

@implementation YCTextViewItem

+ (instancetype)textViewItem {
    
    YCTextViewItem *item = [[YCTextViewItem alloc] init];
    item.placeHolder = @"请输入";
    item.becomeFirstResponder = NO;
    return item;
}

+ (instancetype)textViewItemWithTitle:(NSString *)title
{
    YCTextViewItem *item = [YCTextViewItem textViewItem];
    item.isRequired = NO;
    item.hasIcon = NO;
    item.title = title;
    item.keyBoardType = UIKeyboardTypeDefault;
    return item;
}


+ (instancetype)personalInfo:(NSString *)title
{
    YCTextViewItem *item = [YCTextViewItem textViewItemWithTitle:title];
    item.editStyle = YCEditStyleUnEditable;
    item.alignedRight = YES;
    item.titleFont = [UIFont yc_17];
    item.titleColor = [UIColor yc_hex_121D32];
    item.textFont = [UIFont yc_16];
    item.textColor = [UIColor yc_hex_5B6071];
    item.cellHeight = 56.f;
    item.hideClearBtn = YES;
    return item;
}

- (void)setText:(NSString *)text {
    
    [super setText:text];
}

- (void)setMaxWidth:(CGFloat)maxWidth {

    [super setMaxWidth:maxWidth];
    [self changerTitleHeight];
    if (!self.noChangeContent) {
         [self needChangeContentHeight];
    }
}

- (BOOL)needChangeContentHeight {
    
    CGFloat rightOffset = self.editStyle == YCEditStyleEditable ? 26 : 0;
    //宽度=屏幕宽度-标题宽度-左右缩进-距离-内间距 - 清空按钮
    CGFloat maxWidth = self.maxWidth * 0.6 - cellMargin - 5.0 - 4.0 - rightOffset;
    CGSize size = [self.text yc_boundingRectWithSize:CGSizeMake(maxWidth, HUGE_VAL) font:[UIFont yc_major_font]];
    CGFloat height = ceil(size.height) + 2 * cellMargin;
    //textview高度增加时，cell高度也增加
    if (height > self.cellHeight) {
        self.cellHeight = height;
        return YES;
    }
    //textview高度减小时，cell也响应减小高度，最小不能小于标题文本高度
    if (height < self.cellHeight && height >= self.titleHeight) {
        self.cellHeight = height;
        return YES;
    }
    return NO;
}


/** 校验对象 */
- (id)validatedObject {
    return self.text;
}

/** 校验block */
- (void)setFormatValidBlock:(YCFormatValidBlock)formatValidBlock {
    objc_setAssociatedObject(self, @selector(formatValidBlock), formatValidBlock, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (YCFormatValidBlock)formatValidBlock {
    return objc_getAssociatedObject(self, @selector(formatValidBlock));
}

- (NSString *)validatedMessage {
    return [NSString stringWithFormat:@"%@格式不正确",self.title];
}

- (void)setFilterBlock:(YCFilterBlock)filerBlock {
    objc_setAssociatedObject(self, @selector(filterBlock), filerBlock, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (YCFilterBlock)filterBlock {
  return  objc_getAssociatedObject(self, @selector(filterBlock));
}

@end
