//
//  YCSidebarMenuCell.m
//  YCMineModule
//
//  Created by haima on 2019/4/28.
//

#import "YCSidebarMenuCell.h"
#import <Masonry/Masonry.h>
#import "UIColor+Style.h"
#import "UIFont+Style.h"
#import "SMDefine.h"
#import "UIImage+Resources.h"
#import "UIImage+YCBundle.h"
#import "YCSidebarMenuItem.h"

@interface YCSidebarMenuCell ()

/* icon */
@property (nonatomic, strong) UIImageView *iconImageView;
/* 标题 */
@property (nonatomic, strong) UILabel *titleLbl;
/* > */
@property (nonatomic, strong) UIImageView *nextImageView;
/* 分割线 */
@property (nonatomic, strong) UIView *lineView;

@end

@implementation YCSidebarMenuCell

- (void)cellDidLoad {
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.contentView addSubview:self.iconImageView];
    [self.contentView addSubview:self.titleLbl];
    [self.contentView addSubview:self.nextImageView];
    [self.contentView addSubview:self.lineView];
    
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.leading.offset(14);
        make.size.mas_equalTo(CGSizeMake(24, 24));
    }];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.iconImageView);
        make.left.equalTo(self.iconImageView.mas_right).offset(12);
    }];
    [self.nextImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.trailing.offset(-14);
        make.size.mas_equalTo(CGSizeMake(10, 16));
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLbl.mas_left);
        make.right.bottom.equalTo(self.contentView);
        make.height.equalTo(@(kLineHeight));
    }];
}

+ (CGFloat)heightForCellWithItem:(YCTableViewItem *)item {
    return 57.0;
}

- (void)configCellWithItem:(YCSidebarMenuItem *)item {
    
    self.iconImageView.image = item.iconImage;
    self.titleLbl.text = item.title;
}

#pragma mark - getter
- (UIImageView *)iconImageView {
    
    if (_iconImageView == nil) {
        _iconImageView = [[UIImageView alloc] init];
        
    }
    return _iconImageView;
}
- (UILabel *)titleLbl {
    
    if (_titleLbl == nil) {
        _titleLbl = [[UILabel alloc] init];
        _titleLbl.textColor = [UIColor yc_hex_121D32];
        _titleLbl.font = [UIFont yc_17];
        _titleLbl.textAlignment = NSTextAlignmentLeft;
    }
    return _titleLbl;
}
- (UIImageView *)nextImageView {
    
    if (_nextImageView == nil) {
        _nextImageView = [[UIImageView alloc] init];
        _nextImageView.image = [UIImage yc_imageWithNamed:@"icon_arrow_right"];
    }
    return _nextImageView;
}
- (UIView *)lineView {
    
    if (_lineView == nil) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = [UIColor yc_hex_EEEEEE];
    }
    return _lineView;
}
@end
