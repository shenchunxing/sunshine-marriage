//
//  SMNamePickerView.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/28.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMOptionPickerView.h"
#import "SMDefine.h"
#import <Masonry/Masonry.h>
#import "YCCategoryModule.h"
#import "SMOptionCell.h"
#import "YCTableViewKit.h"
#import "SMPickerSelectItem.h"

@interface SMOptionPickerView ()
@property (nonatomic, strong) UIButton *cancleButton;
@property (nonatomic, strong) UIButton *sureButton;
@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, strong) UIView *vLine;
@property (nonatomic, strong) UIView *hLine;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *options;
@property (nonatomic, strong) id model;
@property (nonatomic, strong) YCTableViewManager *manager;
@property (nonatomic, strong) YCTableViewSection *section;

@end

@implementation SMOptionPickerView

- (instancetype)initWithOptions:(NSArray *)options {
    self = [super init];
    if (self) {
        self.options = options;
        [self initViews];
    }
    return self;
}

- (void)addItems {
    __block int height = 0;
    __block SMPickerSelectItem *lastItem = nil;
    [self.options enumerateObjectsUsingBlock:^(YCOptionModel *model, NSUInteger idx, BOOL * _Nonnull stop) {
        SMPickerSelectItem *item = [SMPickerSelectItem item];
        item.model = model;
        [self.section addItem:item];
        height+=item.cellHeight;
        @weakify(item);
        //手动选中
        item.selectItemHandler = ^(NSIndexPath *indexPath) {
            @strongify(item);
            lastItem.isChoosed = NO;
            item.isChoosed = YES;
            lastItem = item;
            self.model = item.model;
            [self.manager reloadData];
        };
        
        /// 默认选中
        if ([self.selectedModel.code isEqualToString:model.code]) {
            lastItem.isChoosed = NO;
            self.model = self.selectedModel ;
            item.isChoosed = YES;
            lastItem = item;
            [self.manager reloadData];
        }
    }];
    
    if (height > 360) height = 360;
    [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(height+18+18);
    }];
    
    self.containerView.frame = CGRectMake(52.5, (kScreenHEIGHT - height- 36 -45) /2, kWIDTH - 105, height+36+45) ;
    
    [self.manager reloadData];
}

- (CGFloat)tableViewMaxHeight {
    CGFloat height = self.options.count > 6 ? 6*41:self.options.count * 41;
    self.tableView.scrollEnabled = self.options.count > 6 ;
    return height;
}

- (CGFloat)totalHeight {
    return [self tableViewMaxHeight] + 18+45;
}

- (void)initViews {
    [self addSubview:self.backgroundView];
    [self addSubview:self.containerView];
    
    [self.containerView addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@0);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo([self tableViewMaxHeight]);
    }];

    
    [self.containerView addSubview:self.hLine];
    [self.hLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tableView.mas_bottom);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.equalTo(@0.5);
    }];
    
    [self.containerView addSubview:self.cancleButton];
    [self.cancleButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.hLine.mas_bottom);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(self.containerView).multipliedBy(0.5);
        make.height.equalTo(@44.5);
    }];
    
    [self.containerView addSubview:self.vLine];
    [self.vLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.hLine.mas_bottom);
        make.left.mas_equalTo(self.containerView.width/2);
        make.width.mas_equalTo(@0.5);
        make.height.equalTo(@44.5);
    }];
    
    [self.containerView addSubview:self.sureButton];
    [self.sureButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.hLine.mas_bottom);
        make.left.mas_equalTo(self.containerView.width/2);
        make.right.mas_equalTo(self.containerView);
        make.height.equalTo(@44.5);
    }];
    
    self.frame = [UIScreen mainScreen].bounds;
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    [keyWindow addSubview:self];
}

#pragma mark - 弹出视图方法
- (void)showWithAnimation:(BOOL)animation {
    self.containerView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.05, 1.05);
    self.backgroundView.alpha = 0;
    [UIView animateWithDuration:0.3 animations:^{
        self.containerView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
        self.backgroundView.alpha = 0.6;

    }];
}

#pragma mark - 关闭视图方法
- (void)dismissWithAnimation:(BOOL)animation {
    [UIView animateWithDuration:0.3 animations:^{
        self.backgroundView.alpha = 0.0;
        self.containerView.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - overwrite
//取消
- (void)cancelPickerView {
    [self dismissWithAnimation:YES];
}

//确定
- (void)confirmPickerView {
    if (self.block) {
        self.block(self.model);
    }
    [self dismissWithAnimation:YES];
}

//点击背景图层
- (void)didTapBackgroundView:(UIGestureRecognizer *)gesture {
    [self dismissWithAnimation:YES];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] init];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone ;
    }
    return _tableView;
}

- (YCTableViewManager *)manager {
    if (!_manager) {
        _manager = [[YCTableViewManager alloc] initWithTableView:self.tableView];
        [_manager addSection:self.section];
        [_manager registerItems:@[@"SMPickerSelectItem"]];
    }
    return _manager;
}

- (YCTableViewSection *)section {
    if (!_section) {
        _section = [YCTableViewSection section];
        _section.headerView = [UIView new];
        _section.headerHeight = 18;
        _section.footerView = [UIView new];
        _section.footerHeight = 18;
    }
    return _section;
}

- (UIButton *)cancleButton {
    if (!_cancleButton) {
        _cancleButton = [UIButton yc_customTextWithTitle:@"取消" titleColor:[UIColor yc_hex_333333] fontSize:[UIFont yc_16] target:self action:@selector(cancelPickerView)];
    }
    return _cancleButton;
}

- (UIButton *)sureButton {
    if (!_sureButton) {
        _sureButton = [UIButton yc_customTextWithTitle:@"确定" titleColor:[UIColor yc_hex_5360FF] fontSize:[UIFont yc_16] target:self action:@selector(confirmPickerView)];
    }
    return _sureButton;
}

#pragma mark - getter
- (UIView *)backgroundView {
    if (_backgroundView == nil) {
        _backgroundView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _backgroundView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5f];
        _backgroundView.userInteractionEnabled = YES;
        UITapGestureRecognizer *myTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didTapBackgroundView:)];
        [_backgroundView addGestureRecognizer:myTap];
    }
    return _backgroundView;
}

- (UIView *)containerView {
    if (_containerView == nil) {
        _containerView = [[UIView alloc] initWithFrame:CGRectMake(52.5, (kScreenHEIGHT - [self totalHeight]) /2, kWIDTH - 105, [self totalHeight])];
        _containerView.backgroundColor = [UIColor whiteColor];
        [_containerView yc_radiusWithRadius:5];
    }
    return _containerView;
}

- (UIView *)vLine {
    if (!_vLine) {
        _vLine = [[UIView alloc] init];
        _vLine.backgroundColor = [UIColor yc_hex_CCCCCC];
    }
    return _vLine;
}

- (UIView *)hLine {
    if (!_hLine) {
        _hLine = [[UIView alloc] init];
        _hLine.backgroundColor = [UIColor yc_hex_CCCCCC];
    }
    return _hLine;
}

@end
