//
//  YCPayCell.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/6/21.
//

#import <UIKit/UIKit.h>
#import "YCTableViewKit.h"
NS_ASSUME_NONNULL_BEGIN

@interface YCPayCell : UITableViewCell<YCTableViewCellProtocol>

@end

NS_ASSUME_NONNULL_END
