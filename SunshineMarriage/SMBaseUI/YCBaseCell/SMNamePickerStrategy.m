//
//  SMNamePickerStrategy.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/28.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMNamePickerStrategy.h"
#import "SMNamePickerView.h"

@interface SMNamePickerStrategy ()

@end

@implementation SMNamePickerStrategy

+ (instancetype)stragetyWithNamePickerStrategyBlock:(NamePickerStrategyBlock)namePickerStrategyBlock {
    SMNamePickerStrategy *stragety = [[SMNamePickerStrategy alloc] init];
    stragety.namePickerStrategyBlock = namePickerStrategyBlock;
    return stragety;
}

- (void)invoke {
    SMNamePickerView *dateView = [[SMNamePickerView alloc] init];
    dateView.enterBlock = self.namePickerStrategyBlock ;
    [dateView showWithAnimation:YES];
}

@end
