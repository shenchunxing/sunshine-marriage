//
//  YCCreditOnlyContentCell.h
//  YCCreditModule
//
//  Created by 刘成 on 2019/4/8.
//

#import <UIKit/UIKit.h>
#import "YCTableViewKit.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCOnlyContentCell : UITableViewCell<YCTableViewCellProtocol>

@end

NS_ASSUME_NONNULL_END
