//
//  YCPayCell.m
//  YCBusinessManagementModule
//
//  Created by 沈春兴 on 2019/6/20.
//

#import "YCPayCell.h"
#import "YCCategoryModule.h"
#import <Masonry/Masonry.h>
#import "YCPayItem.h"
#import "SMDefine.h"

@interface YCPayCell ()

@property (nonatomic,strong) UIImageView *iconImageView;
@property (nonatomic,strong) UILabel *payLabel;
@property (nonatomic,strong) UIImageView *selectedImageView;
@property (nonatomic,strong) UIView *line;

@end

@implementation YCPayCell

- (void)cellDidLoad{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.contentView addSubview:self.iconImageView];
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(12);
        make.left.mas_equalTo(14);
        make.width.height.mas_equalTo(22);
    }];
    
    [self.contentView addSubview:self.payLabel];
    [self.payLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.iconImageView.mas_right).offset(12);
        make.top.mas_equalTo(12);
    }];
    
    [self.contentView addSubview:self.selectedImageView];
    [self.selectedImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(14);
        make.right.mas_equalTo(self.contentView).offset(-14);
        make.width.height.mas_equalTo(20);
    }];
    
    [self.contentView addSubview:self.line];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(14);
        make.right.mas_equalTo(self.contentView).offset(-14);
        make.height.mas_equalTo(kLineHeight);
        make.bottom.mas_equalTo(self.contentView);
    }];
}

+ (CGFloat)heightForCellWithItem:(YCPayItem *)item
{
    return 53.f;
}

- (void)configCellWithItem:(YCPayItem *)item
{
    self.iconImageView.image = [UIImage yc_resourceModuleImgWithName:item.icon];
    self.payLabel.text = item.name;
    self.selectedImageView.image =  [UIImage yc_resourceModuleImgWithName: item.selected ? @"icon_xuanzhong_default":@"icon_xuanzhong_default(1)"];
    self.line.hidden = item.isLastItemInSection;
}

- (UIImageView *)iconImageView{
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] init];
        _iconImageView.image = [UIImage yc_resourceModuleImgWithName:@"icon_wechat_pay"];
    }
    return _iconImageView;
}

- (UILabel *)payLabel{
    if (!_payLabel) {
        _payLabel = [UILabel yc_labelWithText:@"微信支付" textFont:[UIFont yc_15] textColor:[UIColor yc_hex_3C3D49]];
    }
    return _payLabel;
}

- (UIImageView *)selectedImageView{
    if (!_selectedImageView) {
        _selectedImageView = [[UIImageView alloc] init];
        _selectedImageView.image = [UIImage yc_resourceModuleImgWithName:@"icon_xuanzhong_default"];
    }
    return _selectedImageView;
}

- (UIView *)line{
    if (!_line) {
        _line = [[UIView alloc] init];
        _line.backgroundColor = [UIColor yc_hex_EEEEEE];
    }
    return _line;
}

@end

