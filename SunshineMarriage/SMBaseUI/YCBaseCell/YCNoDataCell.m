//
//  YCNoDataCell.m
//  YCBaseUI
//
//  Created by shenweihang on 2019/9/4.
//

#import "YCNoDataCell.h"
#import <Masonry/Masonry.h>
#import "YCTableViewKit.h"
#import "YCNoDataItem.h"
#import "YCCategoryModule.h"
#import "UIColor+Style.h"
#import "UIFont+Style.h"

@interface YCNoDataCell ()<YCTableViewCellProtocol>

/* <#备注#> */
@property (nonatomic, strong) UIImageView *noDataImgView;
/* <#备注#> */
@property (nonatomic, strong) UILabel *desLbl;

@end

@implementation YCNoDataCell

- (void)cellDidLoad {
    self.selectionStyle  = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    
    [self.contentView addSubview:self.noDataImgView];
    [self.contentView addSubview:self.desLbl];
    [self.noDataImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(150, 150));
        make.top.equalTo(self.contentView);
    }];
    [self.desLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.top.equalTo(self.noDataImgView.mas_bottom).offset(8);
    }];
}

- (void)configCellWithItem:(YCNoDataItem *)item {
    
    self.desLbl.text = item.des;
}

+ (CGFloat)heightForCellWithItem:(YCNoDataItem *)item {
    return 180;
}

- (UIImageView *)noDataImgView {
    
    if (!_noDataImgView) {
        _noDataImgView = [[UIImageView alloc] init];
        _noDataImgView.image = [UIImage yc_resourceModuleImgWithName:@"img_nodata"];
    }
    return _noDataImgView;
}

- (UILabel *)desLbl {
    
    if (!_desLbl) {
        _desLbl = [[UILabel alloc] init];
        _desLbl.font = [UIFont yc_16];
        _desLbl.textColor = [UIColor yc_hex_9B9EA8];
        _desLbl.textAlignment = NSTextAlignmentCenter;
        _desLbl.text = @"暂无图片";
    }
    return _desLbl;
}

@end
