//
//  YCOrderCardItem.m
//  YCBSOrderModule
//
//  Created by shenweihang on 2019/11/26.
//

#import "YCOrderCardItem.h"

@implementation YCOrderCardItem


- (NSMutableArray *)infoList {
    
    if (!_infoList) {
        _infoList = [[NSMutableArray alloc] init];
    }
    return _infoList;
}
@end
