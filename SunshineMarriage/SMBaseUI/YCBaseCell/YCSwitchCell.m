//
//  YCSwitchCell.m
//  YCMineModule
//
//  Created by 沈春兴 on 2019/4/29.
//

#import "YCSwitchCell.h"
#import <Masonry/Masonry.h>
#import "UIImage+Resources.h"
#import "YCCategoryModule.h"
#import "YCSwitchItem.h"
#import "YCBaseUI.h"
#import "SMDefine.h"
#import "YCDataCenter.h"

@interface YCSwitchCell ()
/* 基础item */
@property (nonatomic, strong) YCSwitchItem *baseItem;
@property (nonatomic,strong) UISwitch *sw;
@end

@implementation YCSwitchCell

#pragma mark - YCTableViewCellProtocol
- (void)cellDidLoad {
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.backgroundColor = [UIColor whiteColor];

    [self.contentView addSubview:self.titleLabel];

    [self.contentView addSubview:self.lineView];
    
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(14);
        make.centerY.equalTo(self.contentView);
    }];

    [self.contentView addSubview:self.sw];
    [self.sw mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-14);
        make.centerY.equalTo(self.contentView);
    }];
    
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.contentView);
        make.height.equalTo(@((1 / [UIScreen mainScreen].scale)));
    }];
    
}

+ (CGFloat)heightForCellWithItem:(YCSwitchItem *)item {
    
    return item.cellHeight;
}

- (void)configCellWithItem:(YCSwitchItem *)item {
    
    self.baseItem = item;
    self.titleLabel.text = item.title;
    
    //更新标题
    [self.titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.leading.offset(14);
    }];
    
    [self.lineView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(14);
        make.right.equalTo(self.contentView).offset(-14);
    }];
    
    //设置是否打开状态
    [self.sw setOn:item.open];
    
    if(item.custom) {
        self.sw.transform = CGAffineTransformMakeScale(44.0/51.0, 24.0/31.0);
        self.sw.onTintColor = [UIColor yc_hex_10B711];
    }
    
}

- (void)switchAction:(UISwitch *)sw{
    if (self.baseItem.switchChangeHandle) {
        self. baseItem.switchChangeHandle(sw);
    }
}


#pragma mark - getter

- (UISwitch *)sw
{
    if (!_sw) {
        _sw = [[UISwitch alloc] init];
        _sw.tintColor = [UIColor yc_hex_9B9EA8];
        _sw.onTintColor = [UIColor yc_hex_59B50A];
        _sw.backgroundColor = [UIColor yc_hex_9B9EA8];
        _sw.layer.cornerRadius = _sw.height/2.0;
        _sw.layer.masksToBounds = YES;
        [_sw addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _sw;
}


- (UILabel *)titleLabel {
    
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        _titleLabel.font = [UIFont yc_major_font];
        _titleLabel.numberOfLines = 0;
        _titleLabel.textColor = [UIColor yc_hex_5B6071];
    }
    return _titleLabel;
}

- (UIView *)lineView {
    
    if (_lineView == nil) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = [[UIColor yc_hex_EEEEEE] colorWithAlphaComponent:0.7];
    }
    return _lineView;
}



@end
