//
//  YCDoubleChooseCell.m
//  YCBaseUI
//
//  Created by haima on 2019/3/30.
//

#import "YCDoubleChooseCell.h"
#import <Masonry/Masonry.h>
#import "YCDoubleChooseItem.h"
#import "YCSingleChooseView.h"

@interface YCDoubleChooseCell ()<YCSingleViewDelegate>

/* 第一个view */
@property (nonatomic, strong) YCSingleChooseView *firstView;

/* 第二个view */
@property (nonatomic, strong) YCSingleChooseView *secondView;

/* item */
@property (nonatomic, strong) YCDoubleChooseItem *chooseItem;
/* 不可编辑时，显示文本 */
@property (nonatomic, strong) UILabel *contentLbl;
@end


@implementation YCDoubleChooseCell

- (void)cellDidLoad {
    
    [super cellDidLoad];
    
    [self.contentView addSubview:self.firstView];
    [self.contentView addSubview:self.secondView];
    [self.contentView addSubview:self.contentLbl];
    [self.contentLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(cellMargin);
        make.left.equalTo(self.titleView.mas_right).offset(5);
        make.right.equalTo(self.contentView.mas_right).offset(-cellMargin);
    }];
    [self.firstView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(cellMargin);
        make.left.equalTo(self.titleView.mas_right).offset(5);
        make.width.equalTo(@90);
    }];

    [self.secondView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.firstView);
        make.left.equalTo(self.firstView.mas_right).offset(12);
        make.right.lessThanOrEqualTo(self.contentView).offset(-cellMargin);
    }];
}

- (void)configCellWithItem:(YCDoubleChooseItem *)item {
    
    [super configCellWithItem:item];
    self.chooseItem = item;
    
    if (item.editStyle == YCEditStyleEditable) {
        
        id<YCOptionProtocol> firstOption = item.optionModels.firstObject;
        id<YCOptionProtocol> lastOption = item.optionModels.lastObject;
        
        [self.firstView setContent:[firstOption obtainName] chooseStatus:[item.code isEqualToString:[firstOption obtainCode]]];
        [self.secondView setContent:[lastOption obtainName] chooseStatus:[item.code isEqualToString:[lastOption obtainCode]]];
    
        
    }else{
        self.contentLbl.text = [item selectedOptionNameByCode:item.code];
    }
    self.contentLbl.hidden = item.editStyle == YCEditStyleEditable;
    self.firstView.hidden = item.editStyle == YCEditStyleUnEditable;
    self.secondView.hidden = item.editStyle == YCEditStyleUnEditable;
}

#pragma mark - YCSingleViewDelegate
- (void)chooseSingleViewAtIndex:(NSInteger)index {
    
    id<YCOptionProtocol> option = self.chooseItem.optionModels[index];
    switch (index) {
            case 0:
                [self.secondView cancelSelectedState];
            break;
            case 1:
                [self.firstView cancelSelectedState];
            break;
    }
    self.chooseItem.code = [option obtainCode];
    self.chooseItem.requestValue = [option obtainCode];
    if (self.chooseItem.chooseHandler) {
        self.chooseItem.chooseHandler([option obtainCode]);
    }
    
    if (self.chooseItem.chooseKeyHandler) {
        self.chooseItem.chooseKeyHandler([option obtainCode],self.chooseItem.key);
    }
}

#pragma mark - getter
- (YCSingleChooseView *)firstView {
    
    if (_firstView == nil) {
        _firstView = [[YCSingleChooseView alloc] init];
        _firstView.delegate = self;
        _firstView.index = 0;
    }
    return _firstView;
}

- (YCSingleChooseView *)secondView {
    
    if (_secondView == nil) {
        _secondView = [[YCSingleChooseView alloc] init];
        _secondView.delegate = self;
        _secondView.index = 1;
    }
    return _secondView;
}
- (UILabel *)contentLbl {
    
    if (_contentLbl == nil) {
        _contentLbl = [[UILabel alloc] init];
        _contentLbl.textColor = [UIColor yc_hex_3C3D49];
        _contentLbl.font = [UIFont yc_15];
        _contentLbl.textAlignment = NSTextAlignmentLeft;
    }
    return _contentLbl;
}
@end
