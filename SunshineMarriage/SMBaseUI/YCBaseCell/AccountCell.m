//
//  AccountCell.m
//  YCBSMixingModule
//
//  Created by 沈春兴 on 2019/11/25.
//

#import "AccountCell.h"
#import "YCCategoryModule.h"
#import "SMDefine.h"
#import <Masonry/Masonry.h>
#import "AccountItem.h"
#import "AccountConjfigureView.h"
@interface AccountCell ()
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UIView *line;
@property (nonatomic, strong) AccountConjfigureView *configureView;
@end

@implementation AccountCell

- (void)cellDidLoad {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(14);
        make.top.mas_equalTo(13);
    }];
    [self.contentView addSubview:self.line];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
       make.left.mas_equalTo(14);
        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(12);
        make.right.mas_equalTo(self).mas_offset(-14);
        make.height.mas_equalTo(kLineHeight);
    }];
}

+ (CGFloat)heightForCellWithItem:(AccountItem *)item {
    return item.itemsHeight == 0 ?: item.itemsHeight + 46;
}

- (void)configCellWithItem:(AccountItem *)item {
    self.nameLabel.text = item.name;
    if (!self.configureView.superview && item.itemsHeight >0 ) {
        [self.contentView addSubview:self.configureView];
        [self.configureView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.line.mas_bottom);
            make.left.right.bottom.mas_equalTo(self.contentView);
        }];
        self.configureView.items = item.items;
    }
    self.hidden = item.itemsHeight == 0;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel yc_labelWithText:@"所属角色" textFont:[UIFont yc_15] textColor:[UIColor yc_hex_5B6071]];
    }
    return _nameLabel;
}

- (UIView *)line {
    if (!_line) {
        _line = [[UIView alloc] init];
        _line.backgroundColor = [UIColor yc_hex_EEEEEE];
    }
    return _line;
}

- (AccountConjfigureView *)configureView {
    if (!_configureView) {
        _configureView = [[AccountConjfigureView alloc] init];
    }
    return _configureView;
}

@end
