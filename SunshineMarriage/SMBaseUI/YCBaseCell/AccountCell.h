//
//  AccountCell.h
//  YCBSMixingModule
//
//  Created by 沈春兴 on 2019/11/25.
//

#import <UIKit/UIKit.h>
#import "YCTableViewKit.h"
NS_ASSUME_NONNULL_BEGIN

@interface AccountCell : UITableViewCell<YCTableViewCellProtocol>

@end

NS_ASSUME_NONNULL_END
