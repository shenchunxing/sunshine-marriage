//
//  YCTradingCell.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/7/12.
//

#import "YCTradingCell.h"
#import <Masonry/Masonry.h>
#import "UIImage+Resources.h"
#import "YCCategoryModule.h"
#import "YCTradingItem.h"

@interface YCTradingCell ()
//交易金额
@property (nonatomic,strong) UILabel *moneyLabel;
//交易编号
@property (nonatomic,strong) UILabel *numberLabel;
//交易类型
@property (nonatomic,strong) UILabel *typeLabel;

@property (nonatomic,copy) NSArray *nums;

@end

@implementation YCTradingCell

- (void)cellDidLoad {
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.backgroundColor = [UIColor whiteColor];

    for (int i = 0; i<self.nums.count; i++) {
        UILabel *titleLabel = [UILabel yc_labelWithText:self.nums[i] textFont:[UIFont yc_15] textColor:[UIColor yc_hex_9B9EA8]];
        [self.contentView addSubview:titleLabel];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(12+29*i);
            make.left.mas_equalTo(14);
        }];
    }
    
    [self.contentView addSubview:self.moneyLabel];
    [self.moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(12);
        make.right.offset(-14);
    }];
    
    [self.contentView addSubview:self.numberLabel];
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(41);
        make.right.offset(-14);
    }];
    
    [self.contentView addSubview:self.typeLabel];
    [self.typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(70);
        make.right.offset(-14);
    }];
    
}

- (NSArray *)nums{
    return @[@"交易金额",@"交易编号",@"交易类型"];
}

- (void)configCellWithItem:(YCTradingItem *)item {


}

- (UILabel *)moneyLabel{
    if (!_moneyLabel) {
        _moneyLabel = [UILabel yc_labelWithText:@"￥44567.57" textFont:[UIFont yc_15] textColor:[UIColor yc_hex_F5A623] textAlignment:NSTextAlignmentRight];
    }
    return _moneyLabel;
}

- (UILabel *)numberLabel{
    if (!_numberLabel) {
        _numberLabel = [UILabel yc_labelWithText:@"C615456900656599" textFont:[UIFont yc_15] textColor:[UIColor yc_hex_3C3D49] textAlignment:NSTextAlignmentRight];
    }
    return _numberLabel;
}

- (UILabel *)typeLabel{
    if (!_typeLabel) {
        _typeLabel = [UILabel yc_labelWithText:@"维保查询" textFont:[UIFont yc_15] textColor:[UIColor yc_hex_3C3D49] textAlignment:NSTextAlignmentRight];
    }
    return _typeLabel;
}


@end
