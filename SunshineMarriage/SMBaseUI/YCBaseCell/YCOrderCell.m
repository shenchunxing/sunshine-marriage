//
//  YCOrderCell.m
//  YCBaseUI
//
//  Created by shenweihang on 2019/9/5.
//

#import "YCOrderCell.h"
#import <Masonry/Masonry.h>
#import "YCTableViewKit.h"
#import "YCCategoryModule.h"
#import "SMDefine.h"
#import "UIFont+Style.h"
#import "UIColor+Style.h"
#import "YCOrderItem.h"

@interface YCOrderCell ()<YCTableViewCellProtocol>

/* 背景图 */
@property (nonatomic, strong) UIView *containerView;
/* 团队名 */
@property (nonatomic, strong) UILabel *groupLabel;
/* 业务员 */
@property (nonatomic, strong) UILabel *salesmanLabel;
/* 分割线 */
@property (nonatomic, strong) UIView *lineView;
/* 客户名 */
@property (nonatomic, strong) UILabel *nameLabel;
/* 性别图标 */
@property (nonatomic, strong) UIImageView *sexImgView;
/* 状态 */
@property (nonatomic, strong) UILabel *statusLbl;
/* 身份证 */
@property (nonatomic, strong) UILabel *idCardLabel;
/* 业务编号 */
@property (nonatomic, strong) UILabel *titleLbl1;
/* 编号 */
@property (nonatomic, strong) UILabel *valueLbl1;
/* 贷款银行 */
@property (nonatomic, strong) UILabel *titleLbl2;
/* 银行名称 */
@property (nonatomic, strong) UILabel *valueLbl2;
/* 贷款银行 */
@property (nonatomic, strong) UILabel *titleLbl3;
/* 银行名称 */
@property (nonatomic, strong) UILabel *valueLbl3;
@end

@implementation YCOrderCell

- (void)cellDidLoad {
    self.selectionStyle  = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    
    [self.contentView addSubview:self.containerView];
    [self.containerView addSubview:self.groupLabel];
    [self.containerView addSubview:self.salesmanLabel];
    [self.containerView addSubview:self.lineView];
    [self.containerView addSubview:self.nameLabel];
    [self.containerView addSubview:self.sexImgView];
    [self.containerView addSubview:self.statusLbl];
    [self.containerView addSubview:self.idCardLabel];
    [self.containerView addSubview:self.titleLbl1];
    [self.containerView addSubview:self.valueLbl1];
    [self.containerView addSubview:self.titleLbl2];
    [self.containerView addSubview:self.valueLbl2];
    [self.containerView addSubview:self.titleLbl3];
    [self.containerView addSubview:self.valueLbl3];
    
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@12);
        make.leading.offset(14);
        make.trailing.offset(-14);
        make.bottom.equalTo(self.contentView.mas_bottom);
    }];
    [self.groupLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@12);
        make.leading.offset(14);
        make.right.lessThanOrEqualTo(self.salesmanLabel.mas_left);
    }];
    [self.salesmanLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.groupLabel);
        make.trailing.offset(-14);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@45);
        make.leading.offset(14);
        make.trailing.offset(-14);
        make.height.equalTo(@(kLineHeight));
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineView.mas_bottom).offset(16);
        make.leading.offset(14);
    }];
    [self.sexImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.nameLabel);
        make.left.equalTo(self.nameLabel.mas_right).offset(12);
    }];
    [self.statusLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.nameLabel);
        make.trailing.offset(-14);
        make.left.greaterThanOrEqualTo(self.sexImgView.mas_right);
    }];
    [self.idCardLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.nameLabel.mas_bottom).offset(4);
        make.left.equalTo(self.nameLabel);
        make.right.lessThanOrEqualTo(self.containerView);
    }];
    [self.titleLbl1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.idCardLabel.mas_bottom).offset(12);
        make.left.equalTo(self.idCardLabel);
        make.right.lessThanOrEqualTo(self.valueLbl1.mas_left);
        make.height.equalTo(@20);
    }];
    [self.valueLbl1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleLbl1);
        make.trailing.offset(-14);
        make.height.equalTo(@20);
    }];
    [self.titleLbl2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLbl1.mas_bottom).offset(4);
        make.left.equalTo(self.titleLbl1);
        make.right.lessThanOrEqualTo(self.valueLbl2.mas_left);
        make.height.equalTo(@20);
    }];
    [self.valueLbl2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleLbl2);
        make.trailing.offset(-14);
        make.height.equalTo(@20);
    }];
    [self.titleLbl3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLbl2.mas_bottom).offset(4);
        make.left.equalTo(self.titleLbl2);
        make.right.lessThanOrEqualTo(self.valueLbl3.mas_left);
        make.height.equalTo(@20);
    }];
    [self.valueLbl3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleLbl3);
        make.trailing.offset(-14);
        make.height.equalTo(@20);
    }];
}

- (void)configCellWithItem:(YCOrderItem *)item {
    
    self.groupLabel.text = item.groupName;
    self.salesmanLabel.text = item.salesman;
    self.nameLabel.text = item.name;
    self.sexImgView.image = item.sex == 1 ? [UIImage yc_resourceModuleImgWithName:@"icon_man"]:[UIImage yc_resourceModuleImgWithName:@"icon_woman"];
    self.statusLbl.text = item.status;
    self.statusLbl.textColor = item.statusColor;
    self.idCardLabel.text = item.idCardNo;
    self.titleLbl1.text = item.title1;
    self.valueLbl1.text = item.value1;
    self.titleLbl2.text = item.title2;
    self.valueLbl2.text = item.value2;
    self.titleLbl3.text = item.title3;
    self.valueLbl3.text = item.value3;
}

+ (CGFloat)heightForCellWithItem:(YCOrderItem *)item {
    
    return item.showTitle3 ? 210.0 : 186.0;
}


#pragma mark - getter
- (UIView *)containerView {
    
    if (_containerView == nil) {
        _containerView = [[UIView alloc] init];
        _containerView.backgroundColor = [UIColor whiteColor];
        _containerView.layer.cornerRadius = 2.0;
    }
    return _containerView;
}

- (UILabel *)groupLabel {
    
    if (_groupLabel == nil) {
        _groupLabel = [[UILabel alloc] init];
        _groupLabel.textColor = [UIColor yc_hex_5B6071];
        _groupLabel.font = [UIFont yc_15];
        _groupLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _groupLabel;
}

- (UILabel *)salesmanLabel {
    
    if (_salesmanLabel == nil) {
        _salesmanLabel = [[UILabel alloc] init];
        _salesmanLabel.textColor = [UIColor yc_hex_5B6071];
        _salesmanLabel.font = [UIFont yc_15];
        _salesmanLabel.textAlignment = NSTextAlignmentRight;
    }
    return _salesmanLabel;
}

- (UIView *)lineView {
    
    if (_lineView == nil) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = [UIColor yc_hex_EEEEEE];
    }
    return _lineView;
}

- (UILabel *)nameLabel {
    
    if (_nameLabel == nil) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.textColor = [UIColor yc_hex_121D32];
        _nameLabel.font = [UIFont yc_16];
        _nameLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _nameLabel;
}

- (UIImageView *)sexImgView {
    
    if (_sexImgView == nil) {
        _sexImgView = [[UIImageView alloc] init];
    }
    return _sexImgView;
}

- (UILabel *)statusLbl {
    
    if (_statusLbl == nil) {
        _statusLbl = [[UILabel alloc] init];
        _statusLbl.textColor = [UIColor yc_hex_108EE9];
        _statusLbl.font = [UIFont yc_15];
        _statusLbl.textAlignment = NSTextAlignmentRight;
    }
    return _statusLbl;
}

- (UILabel *)idCardLabel {
    
    if (_idCardLabel == nil) {
        _idCardLabel = [[UILabel alloc] init];
        _idCardLabel.textColor = [UIColor yc_hex_9B9EA8];
        _idCardLabel.font = [UIFont yc_13];
        _idCardLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _idCardLabel;
}

- (UILabel *)titleLbl1 {
    
    if (_titleLbl1 == nil) {
        _titleLbl1 = [[UILabel alloc] init];
        _titleLbl1.textColor = [UIColor yc_hex_9B9EA8];
        _titleLbl1.font = [UIFont yc_14];
        _titleLbl1.textAlignment = NSTextAlignmentLeft;
        _titleLbl1.text = @"订单编号";
    }
    return _titleLbl1;
}

- (UILabel *)valueLbl1 {
    
    if (_valueLbl1 == nil) {
        _valueLbl1 = [[UILabel alloc] init];
        _valueLbl1.textColor = [UIColor yc_hex_5B6071];
        _valueLbl1.font = [UIFont yc_14];
        _valueLbl1.textAlignment = NSTextAlignmentRight;
    }
    return _valueLbl1;
}

- (UILabel *)titleLbl2 {
    
    if (_titleLbl2 == nil) {
        _titleLbl2 = [[UILabel alloc] init];
        _titleLbl2.textColor = [UIColor yc_hex_9B9EA8];
        _titleLbl2.font = [UIFont yc_14];
        _titleLbl2.textAlignment = NSTextAlignmentLeft;
        _titleLbl2.text = @"贷款银行";
    }
    return _titleLbl2;
}

- (UILabel *)valueLbl2 {
    
    if (_valueLbl2 == nil) {
        _valueLbl2 = [[UILabel alloc] init];
        _valueLbl2.textColor = [UIColor yc_hex_5B6071];
        _valueLbl2.font = [UIFont yc_14];
        _valueLbl2.textAlignment = NSTextAlignmentRight;
    }
    return _valueLbl2;
}

- (UILabel *)titleLbl3 {
    
    if (_titleLbl3 == nil) {
        _titleLbl3 = [[UILabel alloc] init];
        _titleLbl3.textColor = [UIColor yc_hex_9B9EA8];
        _titleLbl3.font = [UIFont yc_14];
        _titleLbl3.textAlignment = NSTextAlignmentLeft;
    }
    return _titleLbl3;
}

- (UILabel *)valueLbl3 {
    
    if (_valueLbl3 == nil) {
        _valueLbl3 = [[UILabel alloc] init];
        _valueLbl3.textColor = [UIColor yc_hex_5B6071];
        _valueLbl3.font = [UIFont yc_14];
        _valueLbl3.textAlignment = NSTextAlignmentRight;
    }
    return _valueLbl3;
}

@end
