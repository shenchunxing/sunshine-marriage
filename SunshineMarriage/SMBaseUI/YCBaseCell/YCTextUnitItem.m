//
//  YCTextUnitItem.m
//  YCBaseUI
//
//  Created by haima on 2019/4/19.
//

#import "YCTextUnitItem.h"
#import <objc/runtime.h>

@implementation YCTextUnitItem

+ (instancetype)textUnitItemWithTitle:(NSString *)title {
    YCTextUnitItem *item = [[YCTextUnitItem alloc] init];
    item.placeHolder = @"请输入";
    item.title = title;
    item.isRequired  = NO;
    item.hasIcon = NO;
    item.textType = YCTextUnitTypeUnit;
    item.keyboardType = UIKeyboardTypeDefault;
    return item;
}

- (id)validatedObject {
    return self.text;
}

- (void)setFormatValidBlock:(YCFormatValidBlock)formatValidBlock {
    objc_setAssociatedObject(self, _cmd, formatValidBlock, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (YCFormatValidBlock)formatValidBlock {
    
    return objc_getAssociatedObject(self, _cmd);
}
@end
