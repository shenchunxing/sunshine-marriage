//
//  YCLoanCustomerCreditCell.m
//  YCAuditManagementModule
//
//  Created by shenweihang on 2019/8/30.
//

#import "YCLoanCustomerCreditCell.h"
#import <Masonry/Masonry.h>
#import "YCBaseUI.h"
#import "SMDefine.h"
#import "YCCategoryModule.h"
#import "YCLoanCustomerCreditItem.h"
@interface YCLoanCustomerCreditCell ()<YCTableViewCellProtocol>

/* 容器 */
@property (nonatomic, strong) UIView *containerView;
/* 图标 */
@property (nonatomic, strong) UIImageView *iconView;
/* 标题 */
@property (nonatomic, strong) UILabel *titleLbl;
/* 状态 */
@property (nonatomic, strong) UILabel *statusLbl;
/* 分割线 */
@property (nonatomic, strong) UIView *lineView;
/* 备注背景色 */
@property (nonatomic, strong) UIView *bgView;
/* 备注信息 */
@property (nonatomic, strong) UILabel *remarksLbl;

@end

@implementation YCLoanCustomerCreditCell

- (void)cellDidLoad {
    
    self.selectionStyle  = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor whiteColor];
    
    [self.contentView addSubview:self.containerView];
    [self.containerView addSubview:self.iconView];
    [self.containerView addSubview:self.titleLbl];
    [self.containerView addSubview:self.statusLbl];
    [self.containerView addSubview:self.lineView];
    [self.containerView addSubview:self.bgView];
    [self.containerView addSubview:self.remarksLbl];
    
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [self.iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.offset(14);
        make.top.mas_equalTo(18);
        make.size.mas_equalTo(CGSizeMake(18, 18));
    }];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.iconView);
        make.left.equalTo(self.iconView.mas_right).offset(12);
    }];
    [self.statusLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.iconView);
        make.trailing.offset(-14);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.iconView.mas_bottom).offset(14);
        make.leading.offset(14);
        make.trailing.offset(-14);
        make.height.mas_equalTo(kLineHeight);
    }];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineView.mas_bottom);
        make.leading.offset(14);
        make.trailing.offset(-14);
        make.bottom.equalTo(self.remarksLbl.mas_bottom).offset(8);
    }];
    [self.remarksLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineView.mas_bottom).offset(8);
        make.leading.offset(22);
        make.trailing.offset(-22);
    }];
}

- (void)configCellWithItem:(YCLoanCustomerCreditItem *)item {
    
    self.iconView.image = item.creditType == YCLoanCustomerCreditTypeBank ? [UIImage yc_auditImgWithName:@"icon_bank"] : [UIImage yc_auditImgWithName:@"icon_social"];
    self.titleLbl.text = item.title;
    self.statusLbl.text = item.status;
    self.statusLbl.textColor = [item statusColor];
    self.remarksLbl.text = item.remarks;

    self.bgView.hidden  = item.isFolded;
    self.remarksLbl.hidden  = item.isFolded;
    self.lineView.hidden = (!item.showBottomLine && item.isFolded);
}

+ (CGFloat)heightForCellWithItem:(YCLoanCustomerCreditItem *)item {
    return item.isFolded ? 48.0 : item.height+item.offset;
}

#pragma mark - getter
- (UIView *)containerView {
    
    if (!_containerView) {
        _containerView = [[UIView alloc] init];
        _containerView.backgroundColor = [UIColor whiteColor];
    }
    return _containerView;
}
- (UIImageView *)iconView {
    if (!_iconView) {
        _iconView = [[UIImageView alloc] init];
    }
    return _iconView;
}

- (UILabel *)titleLbl {
    
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc] init];
        _titleLbl.font = [UIFont yc_16];
        _titleLbl.textColor = [UIColor yc_hex_3C3D49];
        _titleLbl.textAlignment = NSTextAlignmentLeft;
    }
    return _titleLbl;
}

- (UILabel *)statusLbl {
    
    if (!_statusLbl) {
        _statusLbl = [[UILabel alloc] init];
        _statusLbl.font = [UIFont yc_16];
        _statusLbl.textColor = [UIColor yc_hex_59B50A];
        _statusLbl.textAlignment = NSTextAlignmentRight;
    }
    return _statusLbl;
}

- (UIView *)lineView {
    
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = [UIColor yc_hex_EEEEEE];
    }
    return _lineView;
}

- (UIView *)bgView {
    
    if (!_bgView) {
        _bgView = [[UIView alloc] init];
        _bgView.backgroundColor = [UIColor yc_hex_F5F6FA];
    }
    return _bgView;
}

- (UILabel *)remarksLbl {
    
    if (!_remarksLbl) {
        _remarksLbl = [[UILabel alloc] init];
        _remarksLbl.font = [UIFont yc_14];
        _remarksLbl.textColor = [UIColor yc_hex_9B9EA8];
        _remarksLbl.textAlignment = NSTextAlignmentLeft;
        _remarksLbl.numberOfLines = 0;
    }
    return _remarksLbl;
}

@end
