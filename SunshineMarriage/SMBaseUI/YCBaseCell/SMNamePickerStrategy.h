//
//  SMNamePickerStrategy.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/28.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMPickerStrategy.h"

typedef void(^NamePickerStrategyBlock)(NSString *text);

NS_ASSUME_NONNULL_BEGIN

@interface SMNamePickerStrategy : SMPickerStrategy

@property (nonatomic, copy) NamePickerStrategyBlock namePickerStrategyBlock;
+ (instancetype)stragetyWithNamePickerStrategyBlock:(NamePickerStrategyBlock)namePickerStrategyBlock ;

@end

NS_ASSUME_NONNULL_END
