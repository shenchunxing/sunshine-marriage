//
//  YCSidebarMenuItem.h
//  YCMineModule
//
//  Created by haima on 2019/4/28.
//

#import "YCTableViewItem.h"


@interface YCSidebarMenuItem : YCTableViewItem

/* 图标 */
@property (nonatomic, strong) UIImage *iconImage;

/* 标题 */
@property (nonatomic, copy) NSString *title;


@end

