//
//  YCTextFieldCell.m
//  YCBaseUI
//
//  Created by haima on 2019/3/29.
//

#import "YCTextViewCell.h"
#import <Masonry/Masonry.h>
#import "YCCategoryModule.h"
#import "SMDefine.h"

@interface YCTextViewCell ()<YCTextInputViewDelegate>


@end

@implementation YCTextViewCell

- (void)cellDidLoad {
    
    [super cellDidLoad];
    
    [self.contentView addSubview:self.textView];
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(cellMargin);
        make.left.equalTo(self.titleView.mas_right).offset(5);
        make.right.equalTo(self.contentView.mas_right).offset(-25);
        make.height.greaterThanOrEqualTo(@25);
    }];
    
}

- (void)configCellWithItem:(YCTextViewItem *)item {
    
    [super configCellWithItem:item];
    self.textViewItem = item;
    self.textView.textView.keyboardType = item.keyBoardType;
    
    UIColor *color = self.textViewItem.textColor?:(item.editStyle == YCEditStyleEditable) ? [UIColor yc_hex_3F7FAC] : [UIColor yc_hex_3C3D49];
    self.textView.textView.userInteractionEnabled = item.editStyle == YCEditStyleEditable;
     [self.textView changeContentWithText:item.text
                                textColor:color
                              placeholder:item.editStyle == YCEditStyleUnEditable ? @"":item.placeHolder
                         placeholderColor:item.placeHolderColor];
    //靠右对齐
    if (item.alignedRight) {
        self.textView.textView.rightAlignment = item.alignedRight;
    }
    //设置背景色
    if (item.backgroundColor){
        self.contentView.backgroundColor = item.backgroundColor;
        self.textView.backgroundColor = item.backgroundColor;
        self.textView.textView.backgroundColor = item.backgroundColor;
    }
    //选中无点击效果
    if(item.selectStyleIsNone){
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (item.hideClearBtn) {
        self.textView.clearMode = YCTextViewClearModeNever;
    }
    if (item.becomeFirstResponder) {
        [self.textView.textView becomeFirstResponder];
    }
    
    if (item.keyBoardType) {
        self.textView.textView.keyboardType = item.keyBoardType;
    }

}

#pragma mark - UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView *)textView {
    self.textViewItem.becomeFirstResponder = YES;
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    
    self.textViewItem.becomeFirstResponder = NO;
    if (self.textViewItem.key && self.textViewItem.textUpdateHandle) {
        self.textViewItem.textUpdateHandle(self.textViewItem.key, textView.text);
    }
}

- (void)textViewDidChange:(UITextView *)textView {
    
    if (self.textViewItem.filterBlock && !self.textViewItem.filterBlock(textView.text)) {
        textView.text = self.textViewItem.text;
        return;
    }
    self.textViewItem.text = textView.text;
    self.textViewItem.requestValue = textView.text;
    
    //限制长度
    if (self.textViewItem.limitLength) {
        [self fliterWithTextView:textView length:self.textViewItem.limitLength];
        self.textViewItem.text = textView.text;
    }
    
    if (![self.textViewItem needChangeContentHeight]) {
        //不需要刷新cell，直接return
        return;
    }
    [self reloadCells];
}

- (void)fliterWithTextView:(UITextView *)textView length:(NSInteger)length{
    NSString *toBeString = textView.text;
    UITextRange *selectedRange = [textView markedTextRange];
    UITextPosition *position = [textView positionFromPosition:selectedRange.start offset:0];
    if (!position){
        if (toBeString.length > length){
            NSRange rangeIndex = [toBeString rangeOfComposedCharacterSequenceAtIndex:length];
            if (rangeIndex.length == 1){
                textView.text = [toBeString substringToIndex:length];
            }
            else{
                NSRange rangeRange = [toBeString rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, length)];
                textView.text = [toBeString substringWithRange:rangeRange];
            }
        }
    }
}

#pragma mark - private
//MARK:刷新当前cell以下的cell，防止刷新当前cell引起键盘收缩
- (void)reloadCells {
    
    UITableView *tableView = (UITableView *)[self valueForKey:@"tableView"];
    CGRect frame = self.frame;
    CGRect left = CGRectMake(0.0f, ceil(frame.origin.y) + frame.size.height, tableView.frame.size.width, tableView.contentSize.height - ceil(frame.origin.y) - frame.size.height);
    NSArray *indexPaths = [tableView indexPathsForRowsInRect:left];
    if (indexPaths.count > 0) {
        [tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    }
    frame.size.height = self.textViewItem.cellHeight;
    self.frame = frame;
    if (self.textViewItem.tableViewItemDelegate && [self.textViewItem.tableViewItemDelegate respondsToSelector:@selector(filletedCornersForCell:atIndexPath:)]) {
        [self.textViewItem.tableViewItemDelegate filletedCornersForCell:self atIndexPath:self.textViewItem.indexPath];
    }
}


#pragma mark - getter
- (YCTextInputView *)textView {
    
    if (_textView == nil) {
        _textView = [[YCTextInputView alloc] initWitDelegate:self];
        _textView.clearMode = YCTextViewClearModeWhileEditing;
    }
    return _textView;
}

@end
