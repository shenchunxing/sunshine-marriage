//
//  YCButtonViewItem.h
//  AFNetworking
//
//  Created by 沈春兴 on 2019/4/12.
//

#import "YCBaseItem.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,YCButtonViewItemType){
    YCButtonViewItemWhiteEndBlueWord,
    YCButtonViewItemBlueEndWhiteWord
};

@interface YCButtonViewItem : YCBaseItem

@property (nonatomic,copy) NSArray *buttonItemTypes;
@property (nonatomic,copy) NSArray *buttonTitles;
- (instancetype)initWithTitles:(NSArray *)titles buttonItemTypes:(NSArray *)buttonItemTypes;

@end

NS_ASSUME_NONNULL_END
