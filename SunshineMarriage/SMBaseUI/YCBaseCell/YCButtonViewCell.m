//
//  YCBottomButtonView.m
//  YCBaseUI
//
//  Created by haima on 2019/4/8.
//

#import "YCButtonViewCell.h"
#import <Masonry/Masonry.h>
#import "UIColor+Style.h"
#import "UIFont+Style.h"
#import "SMDefine.h"
#import "UIView+Extension.h"
#import "YCButtonViewItem.h"

CGFloat const leftSpace = 12;
CGFloat const middleSpace = 15;

@interface YCButtonViewCell ()

@property (nonatomic, strong, readwrite) NSArray<YCButtonViewItem *> *buttonItems;
@property (nonatomic, strong) NSMutableArray *footerButtons;

@end

@implementation YCButtonViewCell

- (void)configCellWithItem:(YCButtonViewItem *)item {
    __block CGFloat width = (kWIDTH - 2*12 - 2*leftSpace - (item.buttonTitles.count - 1)*middleSpace) / item.buttonTitles.count;
    
    [item.buttonTitles enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.titleLabel.font = [UIFont yc_18];
        [button addTarget:self action:@selector(onButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        button.layer.cornerRadius = 25;
        button.layer.borderWidth = 1;
        button.layer.masksToBounds = YES;
        button.tag = 10000 + idx;
        [self.contentView addSubview:button];

        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@25);
            make.height.equalTo(@50);
            make.width.mas_equalTo(width);
            make.left.mas_equalTo(leftSpace+idx*(middleSpace+width));
        }];
        
        UIColor *backgroundColor = nil;
        UIColor *titleColor = nil;
        switch ([item.buttonItemTypes[idx] intValue]) {
            case YCButtonViewItemBlueEndWhiteWord:
                backgroundColor = [UIColor yc_hex_108EE9];
                titleColor = [UIColor whiteColor];
                break;
            case YCButtonViewItemWhiteEndBlueWord:
                backgroundColor = [UIColor whiteColor];
                titleColor = [UIColor yc_hex_108EE9];
                break;
        }
        [button setTitle:item.buttonTitles[idx] forState:UIControlStateNormal];
        [button setBackgroundColor:backgroundColor];
        [button setTitleColor:titleColor forState:UIControlStateNormal];
        button.layer.borderColor = titleColor.CGColor;
    }];
}

- (void)onButtonClick:(UIButton *)button {
    
//    NSInteger index = button.tag - 10000;
//    YCButtonViewItem *transaction = self.buttonItems[index];
//    !transaction.bottomButtonClick?:transaction.bottomButtonClick();
}

@end
