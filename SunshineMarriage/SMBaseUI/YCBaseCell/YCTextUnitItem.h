//
//  YCTextUnitItem.h
//  YCBaseUI
//
//  Created by haima on 2019/4/19.
//

#import "YCBaseItem.h"

typedef NS_ENUM(NSUInteger, YCTextUnitType) {
    YCTextUnitTypeNone,         //普通文本输入
    YCTextUnitTypeUnit,         //普通文本输入,带单位显示
    YCTextUnitTypePhone,        //手机号码输入，需要格式化
    YCTextUnitTypeCamera,       //车架号码输入，需要格式化
    YCTextUnitTypeZipCode,       //邮编,控制6位数字
    YCTextUnitTypeAreaCode,       //单位区号,控制3-4位数字
    YCTextUnitTypeMoney,       //金额、公里数等带小数点
    YCTextUnitTypeRatio,       //比例,最大100%
    YCTextUnitTypeNumber,       //纯数字0-9，不带小数点
    
    //车商
    YCTextUnitTypeCDPhone,        //手机号码输入，需要格式化
    YCTextUnitTypeCDCamera        //车架号码输入，需要格式化
};

//typedef NS_ENUM(NSUInteger, YCTextUnitEnterType) {
//    YCTextUnitEnterNoneLimit,//不限制
//    YCTextUnitEnterTwoNumTwoPoint,//2位数字，2位小数
//};

@interface YCTextUnitItem : YCBaseItem

/* 单位 */
@property (nonatomic, copy) NSString *unit;
/* 输入文本类型 默认YCTextUnitTypeNone*/
@property (nonatomic, assign) YCTextUnitType textType;
/* UIKeyboardTypeDefault */
@property (nonatomic, assign) UIKeyboardType keyboardType;
/* 电话回调 */
@property (nonatomic, copy) void(^phoneHandler)(NSString *phoneNumber);

//@property (nonatomic,assign) YCTextUnitEnterType enterType;

+ (instancetype)textUnitItemWithTitle:(NSString *)title;

//长度限制
@property (nonatomic,assign) NSInteger limitLength;


@property (nonatomic, copy) void(^textHandle)(NSString *text);
@property (nonatomic, copy) void(^textUpdateHandle)(NSString *requestKey,NSString *text);


@end


