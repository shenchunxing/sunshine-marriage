//
//  AccountItem.m
//  YCBSMixingModule
//
//  Created by 沈春兴 on 2019/11/25.
//

#import "AccountItem.h"
#import "UIFont+Style.h"
@implementation AccountItem

- (instancetype)initWithName:(NSString *)name relations:(NSArray *)relations {
    if (self = [super init]) {
        self.name = name;
        self.items = relations;
    }
    return self;
}

- (void)setItems:(NSArray *)items {
    _items = items;
    if (items.count <= 0) {
        self.itemsHeight = 0;
        return;
    }
    __block CGFloat totalHeight = 44;
    __block CGFloat totalWidth = 0;
    [items enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        CGRect rect = [obj boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width -28, 16) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont yc_12]} context:nil];
         CGFloat W = rect.size.width + 20+8;
         totalWidth += W;
         CGFloat yuWidth = [UIScreen mainScreen].bounds.size.width - 28 - totalWidth;
         if (yuWidth < -8) {
             totalHeight += 32;
             totalWidth = W;
         }
    }];
    self.itemsHeight =  totalHeight+4;
}

@end

