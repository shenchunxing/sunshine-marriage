//
//  YCTableViewCell.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/4/17.
//

#import "YCCustomCell.h"

@implementation YCCustomCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.clipsToBounds = YES;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

+ (NSString *)cellIdentifier {
    return NSStringFromClass([self class]);
}

+ (instancetype)dequeueReusableCellForTableView:(UITableView *)tableView{
    id cell = [tableView dequeueReusableCellWithIdentifier:[self cellIdentifier]];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:[self cellIdentifier]];
    }
    return cell;
}

- (UILabel *)textLabelWithText:(NSString *)text textColor:(UIColor *)textColor font:(UIFont *)font{
    self.textLabel.text = text;
    self.textLabel.textColor = textColor;
    self.textLabel.font = font;
    return self.textLabel;
}

- (UILabel *)detailTextLabelWithText:(NSString *)text textColor:(UIColor *)textColor font:(UIFont *)font{
    self.detailTextLabel.text = text;
    self.detailTextLabel.textColor = textColor;
    if(!font){
        return self.detailTextLabel;
    }
    self.detailTextLabel.font = font;
    return self.detailTextLabel;
}

@end
