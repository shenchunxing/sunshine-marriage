//
//  SMPickerItem.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/28.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "YCBaseItem.h"
#import "SMPickerStrategy.h"
#import "YCOptionModel.h"
#import "SMOptionPickerStrategy.h"
NS_ASSUME_NONNULL_BEGIN

@interface SMPickerItem : YCBaseItem
/* 选择策略 */
@property (nonatomic, strong) SMOptionPickerStrategy *strategy;
@property (nonatomic, copy) NSString *code;
@property (nonatomic, strong) YCOptionModel *optionModel;
@property (nonatomic, assign) CGFloat cellDefaultHeight;

+ (instancetype)pickerItem;
+ (instancetype)pickerItemWithTitle:(NSString *)title ;

@end

NS_ASSUME_NONNULL_END
