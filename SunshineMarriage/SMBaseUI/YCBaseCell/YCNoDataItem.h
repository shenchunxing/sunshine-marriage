//
//  YCNoDataItem.h
//  YCBaseUI
//
//  Created by shenweihang on 2019/9/4.
//

#import "YCTableViewItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCNoDataItem : YCTableViewItem

/* <#备注#> */
@property (nonatomic, copy) NSString *des;

+ (instancetype)noDataItem;
@end

NS_ASSUME_NONNULL_END
