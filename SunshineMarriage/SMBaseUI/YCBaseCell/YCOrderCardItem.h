//
//  YCOrderCardItem.h
//  YCBSOrderModule
//
//  Created by shenweihang on 2019/11/26.
//

#import "YCTableViewItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCOrderCardItem : YCTableViewItem

@property (nonatomic, strong) NSMutableArray *infoList;

@end

NS_ASSUME_NONNULL_END
