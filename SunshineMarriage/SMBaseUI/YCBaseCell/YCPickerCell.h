//
//  YCPickerCell.h
//  YCBaseUI
//
//  Created by haima on 2019/3/29.
//

#import "YCBaseCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCPickerCell : YCBaseCell

/* > */
@property (nonatomic, strong) UIImageView *nextImageView;

/* 展示文字 */
@property (nonatomic, strong) UILabel *contentLabel;
/* 占位文字 */
@property (nonatomic, strong) UILabel *placeholderLbl;

@end

NS_ASSUME_NONNULL_END
