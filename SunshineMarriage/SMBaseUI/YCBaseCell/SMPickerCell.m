//
//  YCPickerCell.m
//  YCBaseUI
//
//  Created by haima on 2019/3/29.
//

#import "SMPickerCell.h"
#import <Masonry/Masonry.h>
#import "SMPickerItem.h"
#import "UIImage+Resources.h"
#import "NSObject+Helper.h"
#import "UIViewController+CustomToast.h"

@interface SMPickerCell ()


/* item */
@property (nonatomic, strong) SMPickerItem *pickerItem;

@end

@implementation SMPickerCell


- (void)cellDidLoad {
    
    [super cellDidLoad];
    [self.contentView addSubview:self.contentLabel];
    [self.contentView addSubview:self.nextImageView];
    [self.contentView addSubview:self.placeholderLbl];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleView.mas_right).offset(5);
        make.right.equalTo(self.nextImageView.mas_left).offset(-15);
        make.centerY.mas_equalTo(self.contentView);
//        make.height.mas_equalTo(13);
    }];
    [self.placeholderLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(cellMargin);
        make.left.equalTo(self.titleView.mas_right).offset(5);
        make.right.equalTo(self.nextImageView.mas_left).offset(-2);
    }];
    [self.nextImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(6.5, 12));
        make.right.equalTo(self.contentView.mas_right).offset(-25);
    }];
    
    [self.nextImageView setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
    
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView.mas_centerY);
        make.leading.offset(0);
    }];
}

+ (CGFloat)heightForCellWithItem:(SMPickerItem *)item {
    if (item.cellDefaultHeight) {
        return item.cellDefaultHeight;
    }
    return [super heightForCellWithItem:item];
}

- (void)configCellWithItem:(SMPickerItem *)item {

    [super configCellWithItem:item];
    self.pickerItem = item;
    
    self.contentLabel.text = item.optionModel.name;
    if (!item.optionModel) {
        self.contentLabel.text = item.text ;
    }
    
    if (item.editStyle == YCEditStyleEditable) {
        self.placeholderLbl.hidden = (item.text && item.text.length > 0);
        self.placeholderLbl.text = item.placeHolder;
        self.placeholderLbl.textColor = item.placeHolderColor;
    }else{
        self.placeholderLbl.hidden = YES;
    }

    self.nextImageView.hidden = item.editStyle == YCEditStyleUnEditable;
    
    if (item.editStyle == YCEditStyleUnEditable) {
         [self.contentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
              make.right.equalTo(@-25);
              make.centerY.mas_equalTo(self.contentView);
           }];
    }else{
        [self.contentLabel mas_updateConstraints:^(MASConstraintMaker *make) {
              make.right.equalTo(self.nextImageView.mas_left).offset(-15);
            make.centerY.mas_equalTo(self.contentView);
        }];
    }
   
    if (item.editStyle == YCEditStyleEditable && !item.selectItemHandler) {
        
        //MARK:外部未实现回调时，使用策略模式
        __weak typeof(UIView *) weakView = [self yc_getCurrentViewController].view;
        __weak typeof(SMPickerItem *) weakItem = item;
        item.selectItemHandler = ^(NSIndexPath * _Nonnull indexPath) {
            
            //不同picker执行不同类型
            [weakItem.strategy invoke];
            
            //键盘回收，防止被覆盖,使用weak防止循环引用
            [weakView endEditing:YES];
        };
    }

}

#pragma mark - getter
- (UIImageView *)nextImageView {
    
    if (_nextImageView == nil) {
        _nextImageView = [[UIImageView alloc] init];
        UIImage *image = [UIImage imageNamed:@"jiantou"];
        _nextImageView.image = image;
    }
    return _nextImageView;
}

- (UILabel *)contentLabel {
    
    if (_contentLabel == nil) {
        _contentLabel = [[UILabel alloc] init];
        _contentLabel.font = [UIFont yc_13];
        _contentLabel.textColor = [UIColor yc_hex_999999];
        _contentLabel.textAlignment = NSTextAlignmentRight;
        _contentLabel.numberOfLines = 0;
    }
    return _contentLabel;
}
- (UILabel *)placeholderLbl {
    
    if (_placeholderLbl == nil) {
        _placeholderLbl = [[UILabel alloc] init];
        _placeholderLbl.textColor = [UIColor yc_hex_B3B7C2];
        _placeholderLbl.font = [UIFont yc_15];
        _placeholderLbl.textAlignment = NSTextAlignmentRight;
        _placeholderLbl.numberOfLines = 0;
    }
    return _placeholderLbl;
}
@end
