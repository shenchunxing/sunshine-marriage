//
//  YCPickerCell.m
//  YCBaseUI
//
//  Created by haima on 2019/3/29.
//

#import "YCPickerCell.h"
#import <Masonry/Masonry.h>
#import "YCPickerItem.h"
#import "UIImage+Resources.h"
#import "NSObject+Helper.h"
#import "UIViewController+CustomToast.h"

@interface YCPickerCell ()


/* item */
@property (nonatomic, strong) YCPickerItem *pickerItem;

@end

@implementation YCPickerCell


- (void)cellDidLoad {
    
    [super cellDidLoad];
    [self.contentView addSubview:self.contentLabel];
    [self.contentView addSubview:self.nextImageView];
    [self.contentView addSubview:self.placeholderLbl];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(cellMargin);
        make.left.equalTo(self.titleView.mas_right).offset(5);
        make.right.equalTo(self.nextImageView.mas_left).offset(-2);
    }];
    [self.placeholderLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(cellMargin);
        make.left.equalTo(self.titleView.mas_right).offset(5);
        make.right.equalTo(self.nextImageView.mas_left).offset(-2);
    }];
    [self.nextImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentLabel).offset(4);
        make.size.mas_equalTo(CGSizeMake(10, 16));
        make.right.equalTo(self.contentView.mas_right).offset(-cellMargin);
    }];
    [self.nextImageView setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
}

- (void)configCellWithItem:(YCPickerItem *)item {

    [super configCellWithItem:item];
    self.pickerItem = item;
    self.contentLabel.text = item.text;
    if (item.editStyle == YCEditStyleEditable) {
        self.placeholderLbl.hidden = (item.text && item.text.length > 0);
        self.placeholderLbl.text = item.placeHolder;
        self.placeholderLbl.textColor = item.placeHolderColor;
    }else{
        self.placeholderLbl.hidden = YES;
    }
    
    if (item.alignedRight) {
        self.placeholderLbl.textAlignment = NSTextAlignmentRight;
        self.contentLabel.textAlignment = NSTextAlignmentRight;
    }

    self.contentLabel.textColor = item.editStyle == YCEditStyleEditable ? self.pickerItem.textColor : [UIColor yc_hex_3C3D49];
    self.nextImageView.hidden = item.editStyle == YCEditStyleUnEditable;
    if (item.editStyle == YCEditStyleEditable) {
        [self.contentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(cellMargin);
            make.left.equalTo(self.titleView.mas_right).offset(5);
            make.right.equalTo(self.nextImageView.mas_left).offset(-2);
        }];
    }else{
        [self.contentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(cellMargin);
            make.left.equalTo(self.titleView.mas_right).offset(5);
            make.right.equalTo(self.contentView.mas_right).offset(-cellMargin);
        }];
    }
    if (item.editStyle == YCEditStyleEditable && !item.selectItemHandler) {
        
        //MARK:外部未实现回调时，使用策略模式
        __weak typeof(UIView *) weakView = [self yc_getCurrentViewController].view;
        __weak typeof(YCPickerItem *) weakItem = item;
        item.selectItemHandler = ^(NSIndexPath * _Nonnull indexPath) {
            
            //不同picker执行不同类型
            [weakItem.strategy invoke];
            
            //键盘回收，防止被覆盖,使用weak防止循环引用
            [weakView endEditing:YES];
        };
    }

}

#pragma mark - getter
- (UIImageView *)nextImageView {
    
    if (_nextImageView == nil) {
        _nextImageView = [[UIImageView alloc] init];
        UIImage *image = [UIImage yc_imageWithNamed:@"icon_arrow_right"];
        _nextImageView.image = image;
    }
    return _nextImageView;
}

- (UILabel *)contentLabel {
    
    if (_contentLabel == nil) {
        _contentLabel = [[UILabel alloc] init];
        _contentLabel.font = [UIFont yc_major_font];
        _contentLabel.textAlignment = NSTextAlignmentLeft;
        _contentLabel.numberOfLines = 0;
    }
    return _contentLabel;
}
- (UILabel *)placeholderLbl {
    
    if (_placeholderLbl == nil) {
        _placeholderLbl = [[UILabel alloc] init];
        _placeholderLbl.textColor = [UIColor yc_hex_B3B7C2];
        _placeholderLbl.font = [UIFont yc_15];
        _placeholderLbl.textAlignment = NSTextAlignmentLeft;
        _placeholderLbl.numberOfLines = 0;
    }
    return _placeholderLbl;
}
@end
