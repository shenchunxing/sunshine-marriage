//
//  SMPickerSelectItem.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/2/6.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "YCBaseItem.h"
#import "YCOptionModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface SMPickerSelectItem : YCBaseItem
@property (nonatomic, strong) YCOptionModel *model;
@property (nonatomic, assign) BOOL isChoosed;
@end

NS_ASSUME_NONNULL_END
