//
//  YCPayItem.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/6/21.
//

#import "YCTableViewItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCPayItem : YCTableViewItem

@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *icon;
@property (nonatomic,assign) BOOL selected;
@end

NS_ASSUME_NONNULL_END
