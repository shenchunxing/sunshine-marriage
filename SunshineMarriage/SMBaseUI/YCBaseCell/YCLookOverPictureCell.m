//
//  YCLookOverPictureCell.m
//  YCBaseUI
//
//  Created by haima on 2019/5/20.
//

#import "YCLookOverPictureCell.h"
#import <Masonry/Masonry.h>
#import "YCTableViewKit.h"
#import "UIColor+Style.h"
#import "UIFont+Style.h"
#import "YCLookOverPictureItem.h"

@interface YCLookOverPictureCell ()<YCTableViewCellProtocol>
/* 标题 */
@property (nonatomic, strong) UILabel *titleLbl;
/* 数值 */
@property (nonatomic, strong) UILabel *valueLbl;
/* 查看 */
@property (nonatomic, strong) UILabel *lookoverLbl;
/* <#mark#> */
@property (nonatomic, strong) UIView *lineView;
/* <#mark#> */
@property (nonatomic, strong) UILabel *placeholderLbl;
@end

@implementation YCLookOverPictureCell

- (void)cellDidLoad {
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.contentView addSubview:self.titleLbl];
    [self.contentView addSubview:self.valueLbl];
    [self.contentView addSubview:self.lookoverLbl];
    [self.contentView addSubview:self.lineView];
    [self.contentView addSubview:self.placeholderLbl];
    
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.leading.offset(14);
    }];
    
    [self.lookoverLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.trailing.offset(-14);
    }];
    [self.valueLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.lookoverLbl.mas_left).offset(-20);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.offset(14);
        make.trailing.offset(-14);
        make.bottom.equalTo(self.contentView);
        make.height.mas_equalTo(1/[UIScreen mainScreen].scale);
    }];
    [self.placeholderLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.trailing.offset(-14);
    }];
}

+ (CGFloat)heightForCellWithItem:(YCTableViewItem *)item {
    if (item.shouldShow) {
        
        return 45.0;
    }
    return 0.0;
}

- (void)configCellWithItem:(YCLookOverPictureItem *)item {
    
    self.titleLbl.text = item.title;
    self.valueLbl.text = item.value;
    self.placeholderLbl.text = item.placeholder;
    
    BOOL isShow = (item.value && item.value.length > 0);
    self.valueLbl.hidden = !isShow;
    self.lookoverLbl.hidden = !isShow;
    self.placeholderLbl.hidden = isShow;
}


#pragma mark - getter
- (UILabel *)titleLbl {
    
    if (_titleLbl == nil) {
        _titleLbl = [[UILabel alloc] init];
        _titleLbl.textColor = [UIColor yc_hex_5B6071];
        _titleLbl.font = [UIFont yc_15];
        _titleLbl.textAlignment = NSTextAlignmentLeft;
    }
    return _titleLbl;
}
- (UILabel *)valueLbl {
    
    if (_valueLbl == nil) {
        _valueLbl = [[UILabel alloc] init];
        _valueLbl.textColor = [UIColor yc_hex_9B9EA8];
        _valueLbl.font = [UIFont yc_13];
        _valueLbl.textAlignment = NSTextAlignmentRight;
    }
    return _valueLbl;
}
- (UILabel *)lookoverLbl {
    
    if (_lookoverLbl == nil) {
        _lookoverLbl = [[UILabel alloc] init];
        _lookoverLbl.textColor = [UIColor yc_hex_108EE9];
        _lookoverLbl.font = [UIFont yc_13];
        _lookoverLbl.textAlignment = NSTextAlignmentRight;
        _lookoverLbl.text = @"查看";
    }
    return _lookoverLbl;
}

- (UIView *)lineView {
    
    if (_lineView == nil) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = [UIColor yc_hex_EEEEEE];
    }
    return _lineView;
}
- (UILabel *)placeholderLbl {
    
    if (_placeholderLbl == nil) {
        _placeholderLbl = [[UILabel alloc] init];
        _placeholderLbl.textColor = [UIColor yc_hex_B3B7C2];
        _placeholderLbl.font = [UIFont yc_15];
        _placeholderLbl.textAlignment = NSTextAlignmentRight;
    }
    return _placeholderLbl;
}
@end
