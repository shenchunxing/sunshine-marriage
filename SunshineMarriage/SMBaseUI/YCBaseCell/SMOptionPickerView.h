//
//  SMNamePickerView.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/28.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YCOptionModel.h"
NS_ASSUME_NONNULL_BEGIN

typedef void(^SMOptionPickerViewBlock)(YCOptionModel *model);

@protocol SMOptionPickerViewDelegate <NSObject>

- (NSString *)showText ;

@end

@interface SMOptionPickerView : UIView

@property (nonatomic, strong) YCOptionModel *selectedModel;
@property (nonatomic, copy) SMOptionPickerViewBlock block;
- (void)showWithAnimation:(BOOL)animation;

- (instancetype)initWithOptions:(NSArray *)options ;
@property (nonatomic, weak) id<SMOptionPickerViewDelegate> delegate;

- (void)addItems ;

@end

NS_ASSUME_NONNULL_END
