//
//  YCPickerItem.h
//  YCBaseUI
//
//  Created by haima on 2019/3/29.
//

#import "YCBaseItem.h"
#import "YCPickerStrategy.h"
NS_ASSUME_NONNULL_BEGIN

@interface YCPickerItem : YCBaseItem

/* 选择策略 */
@property (nonatomic, strong) YCPickerStrategy *strategy;

+ (instancetype)pickerItem;
+ (instancetype)pickerItemWithTitle:(NSString *)title ;


@end

NS_ASSUME_NONNULL_END
