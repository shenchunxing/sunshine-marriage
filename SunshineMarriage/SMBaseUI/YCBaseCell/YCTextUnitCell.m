//
//  YCTextUnitCell.m
//  YCBaseUI
//
//  Created by haima on 2019/4/19.
//

#import "YCTextUnitCell.h"
#import <Masonry/Masonry.h>
#import "YCTextUnitItem.h"
#import "UIImage+Resources.h"
#import "YCCategoryModule.h"

@interface YCTextUnitCell ()<YCTableViewCellProtocol,UITextFieldDelegate>

/* 输入文本 */
@property (nonatomic, strong) UITextField *textField;
/* 单位 */
@property (nonatomic, strong) UILabel *unitLbl;
/* 电话按钮 */
@property (nonatomic, strong) UIButton *phoneBtn;
/* item */
@property (nonatomic, strong) YCTextUnitItem *textUnitItem;
@end

@implementation YCTextUnitCell

- (void)cellDidLoad {
    [super cellDidLoad];
   
    
    
    [self.contentView addSubview:self.textField];
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(cellMargin);
        make.left.equalTo(self.titleView.mas_right).offset(5);
        make.height.mas_equalTo(21);
    }];
    
    
    [self.contentView addSubview:self.unitLbl];
    [self.unitLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel).offset(4);
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView.mas_right).offset(-cellMargin);
        make.left.equalTo(self.textField.mas_right);
//        make.width.greaterThanOrEqualTo(@44);
    }];
    
    [self.contentView addSubview:self.phoneBtn];
    [self.phoneBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(44, self.contentView.height));
        make.right.equalTo(self.contentView);
    }];
    
    [self.textField addObserver:self forKeyPath:@"text" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)configCellWithItem:(YCTextUnitItem *)item {
    
    [super configCellWithItem:item];
    self.textUnitItem = item;
    self.textField.text = item.text;
//    self.textField.placeholder = item.placeHolder;
//    [self.textField setValue: forKeyPath:@"_placeholderLabel.textColor"];
    [self.textField setPlaceHolder:item.placeHolder color:item.placeHolderColor];
//    [self.textField setPlaceHolderColor:item.placeHolderColor];
    self.unitLbl.text = item.unit;
    
//    CGFloat width = [item.unit boundingRectWithSize:CGSizeMake(HUGE_VAL , self.contentView.height) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont yc_15]} context:nil].size.width;
    
    self.textField.keyboardType = item.textType == YCTextUnitTypePhone ? UIKeyboardTypePhonePad :(item.textType == YCTextUnitTypeAreaCode || item.textType == YCTextUnitTypeZipCode) ? UIKeyboardTypeNumberPad: (item.keyboardType >0)?item.keyboardType : UIKeyboardTypeDefault;
    
    self.phoneBtn.hidden = !(item.textType == YCTextUnitTypePhone || item.textType == YCTextUnitTypeCDPhone || item.textType == YCTextUnitTypeCamera || item.textType == YCTextUnitTypeCDCamera);

    self.unitLbl.hidden = (item.textType != YCTextUnitTypeUnit);
    switch (item.textType) {
        case YCTextUnitTypeNone:{
            [self.textField mas_updateConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(self.contentView.mas_right).offset(-cellMargin);
            }];
        }
            break;
        case YCTextUnitTypeUnit:{

            [self.unitLbl mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.textField.mas_right).offset(4).priorityHigh();
                
            }];
        }
            break;
        case YCTextUnitTypePhone: {
            [self.textField mas_updateConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(self.contentView.mas_right).offset(-cellMargin);
            }];
            self.phoneBtn.hidden = YES;
        }
            break;
        case YCTextUnitTypeZipCode: {
            
        }
            break;
        case YCTextUnitTypeAreaCode: {
            
        }
            break;
            
        case YCTextUnitTypeMoney:
        case YCTextUnitTypeRatio: {
            self.textField.keyboardType = UIKeyboardTypeDecimalPad;
            self.unitLbl.hidden = NO;
        }
            break;
         case YCTextUnitTypeNumber: {
             self.textField.keyboardType = UIKeyboardTypeNumberPad;
         }
         break;
            
        case YCTextUnitTypeCamera:{
            [self.phoneBtn setImage:[UIImage yc_businessImgWithName:@"icon_shibie"] forState:UIControlStateNormal];
        }
            break;
        case YCTextUnitTypeCDPhone: {
            
            [self.textField mas_updateConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(self.phoneBtn.mas_left).offset(-2);
            }];
             [self.phoneBtn setImage:[UIImage yc_cdhomeImgWithName:@"icon_phone"] forState:UIControlStateNormal];
        }
            break;
        case YCTextUnitTypeCDCamera:{
            [self.phoneBtn setImage:[UIImage yc_cdhomeImgWithName:@"icon_shibie"] forState:UIControlStateNormal];
        }
    }
    self.textField.userInteractionEnabled = item.editStyle == YCEditStyleEditable;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    if ([keyPath isEqualToString:@"text"]) {
        NSString *text = change[NSKeyValueChangeNewKey];
        if (self.textUnitItem.textType == YCTextUnitTypePhone) {
            text = [self noneSymbolString:text];
        }
        self.textUnitItem.text = text;
        self.textUnitItem.requestValue = text;
        
    }
}

- (void)dealloc {
    [self.textField removeObserver:self forKeyPath:@"text"];
}

- (void)onPhoneButtonClick:(UIButton *)btn {

    NSString *phone = [self noneSymbolString:self.textField.text];
    !self.textUnitItem.phoneHandler?:self.textUnitItem.phoneHandler(phone);
}

- (void)fliterWithTextField:(UITextField *)textField length:(NSInteger)length{
    NSString *toBeString = textField.text;
    UITextRange *selectedRange = [textField markedTextRange];
    UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
    if (!position){
        if (toBeString.length > length){
            NSRange rangeIndex = [toBeString rangeOfComposedCharacterSequenceAtIndex:length];
            if (rangeIndex.length == 1){
                textField.text = [toBeString substringToIndex:length];
            }
            else{
                NSRange rangeRange = [toBeString rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, length)];
                textField.text = [toBeString substringWithRange:rangeRange];
            }
        }
    }
}

#pragma mark - UITextFieldDelegate

- (void)textFieldChanged:(UITextField*)textField
{
    if (self.textUnitItem.textHandle) {
        
        //长度限制
        if (self.textUnitItem.limitLength) {
            [self fliterWithTextField:textField length:self.textUnitItem.limitLength];
        }
        
        self.textUnitItem.textHandle(textField.text);
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (self.textUnitItem.key && self.textUnitItem.textUpdateHandle) {
        self.textUnitItem.textUpdateHandle(self.textUnitItem.key, textField.text);
    }
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField && self.textUnitItem.textType == YCTextUnitTypePhone) {
        //输入手机号进行格式化
        NSString* text = textField.text;
        //删除
        if([string isEqualToString:@"-"]){
            //删除一位
            if(range.length == 1){
                //最后一位,遇到空格则多删除一次
                if (range.location == text.length-1 ) {
                    if ([text characterAtIndex:text.length-1] == '-') {
                        [textField deleteBackward];
                    }
                    return YES;
                }
                else{
                    //从中间删除
                    NSInteger offset = range.location;
                    
                    if (range.location < text.length && [text characterAtIndex:range.location] == '-' && [textField.selectedTextRange isEmpty]) {
                        [textField deleteBackward];
                        offset --;
                    }
                    [textField deleteBackward];
                    textField.text = [self parseString:textField.text];
                    UITextPosition *newPos = [textField positionFromPosition:textField.beginningOfDocument offset:offset];
                    textField.selectedTextRange = [textField textRangeFromPosition:newPos toPosition:newPos];
                    return NO;
                }
            }
            else if (range.length > 1) {
                BOOL isLast = NO;
                //如果是从最后一位开始
                if(range.location + range.length == textField.text.length ){
                    isLast = YES;
                }
                [textField deleteBackward];
                textField.text = [self parseString:textField.text];
                
                NSInteger offset = range.location;
                if (range.location == 3 || range.location  == 8) {
                    offset ++;
                }
                if (isLast) {
                    //光标直接在最后一位了
                }else{
                    UITextPosition *newPos = [textField positionFromPosition:textField.beginningOfDocument offset:offset];
                    textField.selectedTextRange = [textField textRangeFromPosition:newPos toPosition:newPos];
                }
                
                return NO;
            }
            else{
                return YES;
            }
        }
        
        else if(string.length >0){
            
            //限制输入字符个数
            if (([self noneSymbolString:textField.text].length + string.length - range.length > 11) ) {
                return NO;
            }
            //判断是否是纯数字(千杀的搜狗，百度输入法，数字键盘居然可以输入其他字符)
            if(![string isPureNumber]){
                return NO;
            }
            [textField insertText:string];
            textField.text = [self parseString:textField.text];
            
            NSInteger offset = range.location + string.length;
            if (range.location == 3 || range.location  == 8) {
                offset ++;
            }
            UITextPosition *newPos = [textField positionFromPosition:textField.beginningOfDocument offset:offset];
            textField.selectedTextRange = [textField textRangeFromPosition:newPos toPosition:newPos];
            return NO;
        }else{
            return YES;
        }
        
    }
    
    //区号
    if (textField && self.textUnitItem.textType == YCTextUnitTypeAreaCode) {
        
        if(![string isPureNumber]){
            return NO;
        }
        if(string.length>0 && range.location>3){
            return NO;
        }
        
    }
    
    //邮编
    if (textField && self.textUnitItem.textType == YCTextUnitTypeZipCode) {
        
        if(![string isPureNumber]){
            return NO;
        }
        if(string.length>0 && range.location>5){
            return NO;
        }
    }
    
    //金额
    if (textField && (self.textUnitItem.textType == YCTextUnitTypeRatio || self.textUnitItem.textType == YCTextUnitTypeMoney)) {
        
        //0后面只能输小数点
        if ([textField.text isEqualToString:@"0"] && !([string isEqualToString:@"."] || range.length>0)) {
            return NO;
        }
        
        //允许输一个小数点
        if ([string isEqualToString:@"."] && [textField.text containsString:@"."]) {
            return NO;
        }
        
        //第一位不能为小数点
        if ([string isEqualToString:@"."] && textField.text.length==0) {
            return NO;
        }
        
        //小数点后最多2位
        if ([textField.text containsString:@"."] && [[textField.text componentsSeparatedByString:@"."] lastObject].length>1 && range.length==0) {
            return NO;
        }

        //允许输入指定字符和回退
        NSString *allowStr = @"1234567890.";
        if ([allowStr containsString:string] || range.length>0) {
            
            //如果是比例,限制最大100%
            if (range.length==0 && self.textUnitItem.textType == YCTextUnitTypeRatio) {
                
                NSString *newText = [textField.text stringByAppendingString:string];
                if (newText.floatValue>100.00) {
                    textField.text = @"100";
                    return NO;
                }
            }
            return YES;
        }
        
        return NO;
    }
   
    //纯数字
    if (textField && self.textUnitItem.textType == YCTextUnitTypeNumber) {
        
        //允许输入指定字符和回退
        NSString *allowStr = @"1234567890";
        if ([allowStr containsString:string] || range.length>0) {
            
            return YES;
        }
        
        return NO;
    }
    
    return YES;
    
}


-(NSString*)noneSymbolString:(NSString*)string {
    
    return [string stringByReplacingOccurrencesOfString:@"-" withString:@""];
}

- (NSString*)parseString:(NSString*)string {
    
    if (!string) {
        return nil;
    }
    NSMutableString* mStr = [NSMutableString stringWithString:[string stringByReplacingOccurrencesOfString:@"-" withString:@""]];
    if (mStr.length >3) {
        [mStr insertString:@"-" atIndex:3];
    }if (mStr.length > 8) {
        [mStr insertString:@"-" atIndex:8];
        
    }
    return  mStr;
}


#pragma mark - getter
- (UITextField *)textField {
    
    if (_textField == nil) {
        _textField = [[UITextField alloc] init];
        _textField.textColor = [UIColor yc_hex_3C3D49];
        _textField.font = [UIFont yc_15];
        _textField.textAlignment = NSTextAlignmentLeft;
        _textField.delegate = self;
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        [_textField addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return _textField;
}

- (UILabel *)unitLbl {
    
    if (_unitLbl == nil) {
        _unitLbl = [[UILabel alloc] init];
        _unitLbl.textColor = [UIColor yc_hex_3C3D49];
        _unitLbl.font = [UIFont yc_15];
        _unitLbl.textAlignment = NSTextAlignmentRight;
    }
    return _unitLbl;
}

- (UIButton *)phoneBtn {
    
    if (_phoneBtn == nil) {
        _phoneBtn = [[UIButton alloc] init];
        [_phoneBtn setImage:[UIImage yc_imageWithNamed:@"icon_phone"] forState:UIControlStateNormal];
        [_phoneBtn addTarget:self action:@selector(onPhoneButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        _phoneBtn.imageEdgeInsets = UIEdgeInsetsMake(13.5, 14, 13.5, 14);
    }
    return _phoneBtn;
}
@end
