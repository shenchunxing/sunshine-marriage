//
//  YCLoanCustomerCreditItem.h
//  YCAuditManagementModule
//
//  Created by shenweihang on 2019/8/30.
//

#import "YCTableViewItem.h"

typedef NS_ENUM(NSUInteger, YCLoanCustomerCreditType) {
    YCLoanCustomerCreditTypeBank,   //银行征信
    YCLoanCustomerCreditTypeSocial  //综合评估
};

NS_ASSUME_NONNULL_BEGIN

@interface YCLoanCustomerCreditItem : YCTableViewItem

/* 征信类型 */
@property (nonatomic, assign) YCLoanCustomerCreditType creditType;
/* 标题 */
@property (nonatomic, copy) NSString *title;
/* 状态 1 通过 2 关注 3 不通过  */
@property (nonatomic, copy) NSString *status;
/* 备注 */
@property (nonatomic, copy) NSString *remarks;
/* 默认折叠 */
@property (nonatomic, assign) BOOL isFolded;
@property (nonatomic, assign) BOOL showBottomLine;
/* 高度 */
@property (nonatomic, assign) CGFloat height;
/* 底部偏移量 */
@property (nonatomic, assign) CGFloat offset;

- (UIColor *)statusColor;

@end

NS_ASSUME_NONNULL_END
