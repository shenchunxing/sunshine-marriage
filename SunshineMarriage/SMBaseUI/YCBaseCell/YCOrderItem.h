//
//  YCOrderItem.h
//  YCBaseUI
//
//  Created by shenweihang on 2019/9/5.
//

#import "YCTableViewItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCOrderItem : YCTableViewItem

/* 数据源 */
@property (nonatomic, strong) id model;
/* 合作组织 */
@property (nonatomic, copy) NSString *groupName;
/* 业务员 */
@property (nonatomic, copy) NSString *salesman;
/* 客户名 */
@property (nonatomic, copy) NSString *name;
/* 性别 */
@property (nonatomic, assign) NSInteger sex;
/* 颜色 */
@property (nonatomic, strong) UIColor *statusColor;
/* 状态/类型 */
@property (nonatomic, copy) NSString *status;
/* 身份证 */
@property (nonatomic, copy) NSString *idCardNo;
/* 订单编号 */
@property (nonatomic, copy) NSString *title1;
@property (nonatomic, copy) NSString *value1;

/* 贷款银行 */
@property (nonatomic, copy) NSString *title2;
@property (nonatomic, copy) NSString *value2;

@property (nonatomic, copy) NSString *title3;
@property (nonatomic, copy) NSString *value3;

@property (nonatomic, assign) BOOL showTitle3;

+ (instancetype)orderItem;
@end

NS_ASSUME_NONNULL_END
