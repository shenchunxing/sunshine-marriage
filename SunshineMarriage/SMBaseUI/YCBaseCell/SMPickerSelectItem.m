//
//  SMPickerSelectItem.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/2/6.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMPickerSelectItem.h"
#import "NSString+Size.h"

@implementation SMPickerSelectItem

- (BOOL)needChangeContentHeight {
    self.text = self.model.obtainName ;
   //宽度=屏幕宽度-标题宽度-左右缩进-距离-内间距
   CGFloat maxWidth = [UIScreen mainScreen].bounds.size.width - 30 - 60 - 10 - 52.5*2;
    
     NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
            [style setLineBreakMode:NSLineBreakByCharWrapping];
    style.lineSpacing = 7;
    NSDictionary *attributes = @{ NSFontAttributeName : [UIFont yc_17], NSParagraphStyleAttributeName : style };

   CGSize size = [self.text boundingRectWithSize:CGSizeMake(maxWidth, HUGE_VAL) options:NSStringDrawingUsesLineFragmentOrigin |
   NSStringDrawingUsesFontLeading attributes:attributes context:nil].size;
    
   CGFloat height = ceilf(size.height) + 2 * 12;
   //textview高度增加时，cell高度也增加
   if (height >= self.cellHeight) {
       self.cellHeight = height;
       return YES;
   }
   //textview高度减小时，cell也响应减小高度，最小不能小于标题文本高度
   if (height < self.cellHeight && height >= self.titleHeight) {
       self.cellHeight = height;
       return YES;
   }
   return NO;
}

- (void)setModel:(YCOptionModel *)model{
    _model = model;
    [self needChangeContentHeight];
}

@end
