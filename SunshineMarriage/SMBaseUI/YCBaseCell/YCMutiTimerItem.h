//
//  YCMutiTimerItem.h
//  YCBaseUI
//
//  Created by haima on 2019/3/29.
//

#import "YCBaseItem.h"
#import "YCPickerStrategy.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCMutiTimerItem : YCBaseItem

/* 开始时间 */
@property (nonatomic, copy) NSString *startTime;
/* 开始时间占位符 */
@property (nonatomic, copy) NSString *startPlaceholder;
/* 结束时间 */
@property (nonatomic, copy) NSString *endTime;
/* 结束时间占位符 */
@property (nonatomic, copy) NSString *endPlaceholder;

//策略模式
/* 开始时间策略 */
@property (nonatomic, strong) YCPickerStrategy *startStrategy;
/* 结束时间策略 */
@property (nonatomic, strong) YCPickerStrategy *endStrategy;

//block模式
/* 开始时间回调 */
@property (nonatomic, strong) void(^clickStartHandler)(void);
/* 结束时间 */
@property (nonatomic, strong) void(^clickEndHandler)(void);


+ (instancetype)mutiTimerItem;
@end

NS_ASSUME_NONNULL_END
