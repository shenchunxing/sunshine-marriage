//
//  YCDoubleChooseItem.m
//  YCBaseUI
//
//  Created by haima on 2019/3/30.
//

#import "YCDoubleChooseItem.h"

@implementation YCDoubleChooseItem

+ (instancetype)doubleChooseItemWithTitle:(NSString *)title {
    YCDoubleChooseItem *item = [[YCDoubleChooseItem alloc] init];
    item.title = title;
    return item;
}

- (NSString *)selectedOptionNameByCode:(NSString *)code {
    
    for (id<YCOptionProtocol> option in self.optionModels) {
        if ([code isEqualToString:[option obtainCode]]) {
            return [option obtainName];
        }
    }
    return nil;
}

- (BOOL)valid {
    if (self.code && self.code.length > 0) {
        return YES;
    }
    return NO;
}
@end
