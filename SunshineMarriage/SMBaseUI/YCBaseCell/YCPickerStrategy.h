//
//  YCPickerStrategy.h
//  YCBaseUI
//
//  Created by haima on 2019/3/29.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YCPickerStrategy : NSObject

//需要子类实现
- (void)invoke;

+ (instancetype)stragety;

@end

NS_ASSUME_NONNULL_END
