//
//  YCOrderItem.m
//  YCBaseUI
//
//  Created by shenweihang on 2019/9/5.
//

#import "YCOrderItem.h"

@implementation YCOrderItem

+ (instancetype)orderItem {
    
    YCOrderItem *orderItem = [[YCOrderItem alloc] init];
    orderItem.title1 = @"订单编号";
    orderItem.title2 = @"贷款银行";
    return orderItem;
}

@end
