//
//  YCAddTransactionCell.m
//  YCBaseUI
//
//  Created by haima on 2019/4/29.
//

#import "YCAddTransactionCell.h"
#import <Masonry/Masonry.h>
#import "UIFont+Style.h"
#import "UIColor+Style.h"
#import "YCCategoryModule.h"
#import "YCAddTransactionItem.h"
#import "SMDefine.h"

@interface YCAddTransactionCell ()

@property (nonatomic, strong) UIButton *button;
@property (nonatomic, strong) YCAddTransactionItem *item;
@end

@implementation YCAddTransactionCell

- (void)cellDidLoad {
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.button];
    [self.button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
}

+ (CGFloat)heightForCellWithItem:(YCAddTransactionItem *)item {
    if (item.shouldShow) {
        return item.cellH;
    }
    return 0;
}

- (void)configCellWithItem:(YCAddTransactionItem *)item {
    
    self.item = item;
    self.button.enabled = item.enable;
    self.button.hidden = !item.shouldShow;
    [self.button setTitle:item.title forState:UIControlStateNormal];
    if (item.transactionType == YCAddTransactionTypeFill) {
        [self.button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.button setImage:[UIImage yc_resourceModuleImgWithName:@"icon_add_white"] forState:UIControlStateNormal];
        UIColor *color = item.enable ? [UIColor yc_hex_108EE9] : [UIColor yc_hex_CACCD4];
        [self.button setBackgroundColor:color];
    }
    if (item.transactionType == YCAddTransactionTypeBorder) {
        self.button.layer.borderColor = [UIColor yc_hex_108EE9].CGColor;
        self.button.layer.borderWidth = kLineHeight;
        [self.button setBackgroundColor:[UIColor whiteColor]];
        [self.button setTitleColor:[UIColor yc_hex_108EE9] forState:UIControlStateNormal];
        [self.button setImage:[UIImage yc_resourceModuleImgWithName:@"icon_add_blue"] forState:UIControlStateNormal];
    }
    [self.button yc_imagePosition:YCImagePositionLeft space:8];
    [self.button mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.leading.offset(item.margin);
        make.trailing.offset(-item.margin);
        make.top.bottom.equalTo(self.contentView);
    }];
}

- (void)buttonClick:(UIButton *)button {
    
    if (self.item.buttonClickHandler) {
        self.item.buttonClickHandler();
    }
}
#pragma mark - getter
- (UIButton *)button {
    
    if (_button == nil) {
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        _button.layer.cornerRadius = 2.0;
        _button.titleLabel.font = [UIFont yc_18];
        [_button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _button;
}
@end
