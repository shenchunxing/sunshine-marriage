//
//  YCLookOverPictureItem.m
//  YCBaseUI
//
//  Created by haima on 2019/5/20.
//

#import "YCLookOverPictureItem.h"
#import "YCTableViewKit.h"
#import "YCCategoryModule.h"

@interface YCLookOverPictureItem ()<YCValidatableProtocol>

@end

@implementation YCLookOverPictureItem

+ (instancetype)itemWithTitle:(NSString *)title value:(NSString *)value {
    YCLookOverPictureItem *item = [[YCLookOverPictureItem alloc] init];
    item.title = title;
    item.value = value;
    
    return item;
}

- (instancetype)init {
    
    if (self = [super init]) {
        self.placeholder = @"选填";
        self.shouldShow = YES;
    }
    return self;
}

- (BOOL)valid {
    if (self.isRequired && [NSString yc_isEmptyStr:self.value]) {
        return NO;
    }
    return YES;
}
- (NSString *)validatedTitle {
    return [NSString stringWithFormat:@"请上传%@",self.title];
}
@end
