//
//  YCSwitchCell.h
//  YCMineModule
//
//  Created by 沈春兴 on 2019/4/29.
//

#import <UIKit/UIKit.h>
#import "YCTableViewKit.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCSwitchCell : UITableViewCell<YCTableViewCellProtocol>


@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UIView *lineView;

@end

NS_ASSUME_NONNULL_END
