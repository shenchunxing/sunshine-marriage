//
//  YCPickerUnitItem.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/5/14.
//

#import "YCPickerItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCPickerUnitItem : YCPickerItem

/* 单位 */
@property (nonatomic, copy) NSString *unit;

@end

NS_ASSUME_NONNULL_END
