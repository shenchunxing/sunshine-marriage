//
//  YCButtonViewItem.m
//  AFNetworking
//
//  Created by 沈春兴 on 2019/4/12.
//

#import "YCButtonViewItem.h"

@implementation YCButtonViewItem

+ (instancetype)item {
    YCButtonViewItem *item = [[YCButtonViewItem alloc] init];
    return item;
}

- (instancetype)initWithTitles:(NSArray *)titles buttonItemTypes:(NSArray *)buttonItemTypes
{
    if(self = [super init]){
        self.cellHeight = 100;
        self.hasIcon = NO;
        self.isRequired = NO;
        self.buttonItemTypes = buttonItemTypes;
        self.buttonTitles = titles;
    }
    return self;
}

@end
