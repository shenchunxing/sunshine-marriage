//
//  YCPickerCell.m
//  YCBaseUI
//
//  Created by haima on 2019/3/29.
//

#import "YCPickerUnitCell.h"
#import <Masonry/Masonry.h>
#import "YCPickerUnitItem.h"
#import "UIImage+Resources.h"

@interface YCPickerUnitCell ()

/* > */
@property (nonatomic, strong) UILabel *unitLabel;
/* 占位文字 */
@property (nonatomic, strong) UILabel *placeholderLbl;
/* item */
@property (nonatomic, strong) YCPickerUnitItem *pickerItem;

/* 展示文字 */
@property (nonatomic, strong) UILabel *contentLabel;

@end

@implementation YCPickerUnitCell


- (void)cellDidLoad {
    
    [super cellDidLoad];
    [self.contentView addSubview:self.contentLabel];
    [self.contentView addSubview:self.unitLabel];
    [self.contentView addSubview:self.placeholderLbl];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(cellMargin);
        make.left.equalTo(self.titleView.mas_right).offset(5);
        make.right.equalTo(self.unitLabel.mas_left).offset(-2);
    }];
    [self.placeholderLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(cellMargin);
        make.left.equalTo(self.titleView.mas_right).offset(5);
        make.right.equalTo(self.unitLabel.mas_left).offset(-2);
    }];
    [self.unitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentLabel).offset(4);
        make.size.mas_equalTo(CGSizeMake(50, 16));
        make.right.equalTo(self.contentView.mas_right).offset(-cellMargin);
    }];
    
}

- (void)configCellWithItem:(YCPickerUnitItem *)item {
    
    [super configCellWithItem:item];
    self.pickerItem = item;
    self.contentLabel.text = item.text;
    self.placeholderLbl.hidden = (item.text && item.text.length > 0);
    self.placeholderLbl.text = item.placeHolder;
    self.placeholderLbl.textColor = item.placeHolderColor;
    self.unitLabel.text = item.unit;
    
    if (item.alignedRight) {
        self.placeholderLbl.textAlignment = NSTextAlignmentRight;
        self.contentLabel.textAlignment = NSTextAlignmentRight;
    }
    
    if (item.editStyle == YCEditStyleEditable && !item.selectItemHandler) {
        
        //MARK:外部未实现回调时，使用策略模式
        __weak typeof(YCPickerItem *) weakItem = item;
        item.selectItemHandler = ^(NSIndexPath * _Nonnull indexPath) {
            [weakItem.strategy invoke];
        };
    }
    
    self.contentLabel.textColor = item.editStyle == YCEditStyleEditable ? self.pickerItem.textColor : [UIColor yc_hex_5B6071];
   
    if (item.editStyle == YCEditStyleEditable) {
        [self.contentLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.unitLabel.mas_left).offset(-2);
        }];
    }else{
        [self.contentLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.mas_right).offset(-4*cellMargin);
        }];
    }
}

#pragma mark - getter
- (UILabel *)unitLabel {
    
    if (_unitLabel == nil) {
        _unitLabel = [[UILabel alloc] init];
        _unitLabel.textColor = [UIColor yc_hex_3C3D49];
        _unitLabel.font = [UIFont yc_15];
        _unitLabel.textAlignment = NSTextAlignmentRight;
    }
    return _unitLabel;
}

- (UILabel *)contentLabel {
    
    if (_contentLabel == nil) {
        _contentLabel = [[UILabel alloc] init];
        _contentLabel.font = [UIFont yc_major_font];
        _contentLabel.textAlignment = NSTextAlignmentLeft;
        _contentLabel.numberOfLines = 0;
    }
    return _contentLabel;
}
- (UILabel *)placeholderLbl {
    
    if (_placeholderLbl == nil) {
        _placeholderLbl = [[UILabel alloc] init];
        _placeholderLbl.textColor = [UIColor yc_hex_B3B7C2];
        _placeholderLbl.font = [UIFont yc_15];
        _placeholderLbl.textAlignment = NSTextAlignmentLeft;
        _placeholderLbl.numberOfLines = 0;
    }
    return _placeholderLbl;
}
@end
