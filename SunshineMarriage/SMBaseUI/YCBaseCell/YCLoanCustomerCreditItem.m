//
//  YCLoanCustomerCreditItem.m
//  YCAuditManagementModule
//
//  Created by shenweihang on 2019/8/30.
//

#import "YCLoanCustomerCreditItem.h"
#import "SMDefine.h"
#import "YCBaseUI.h"
#import "YCCategoryModule.h"
@implementation YCLoanCustomerCreditItem

- (void)setCreditType:(YCLoanCustomerCreditType)creditType {
    
    _creditType = creditType;
    switch (creditType) {
            case YCLoanCustomerCreditTypeBank:
                self.title = @"银行征信";
            break;
            case YCLoanCustomerCreditTypeSocial:
                self.title = @"综合评估";
            break;
        default:
            break;
    }
}

- (void)setRemarks:(NSString *)remarks {
    
    _remarks = remarks;
    [self calCellHeightWithText:remarks];
}

- (void)calCellHeightWithText:(NSString *)text {
    
    self.isFolded = [NSString yc_isEmptyStr:text];
    if (self.isFolded) {
        self.height = 48.0;
        return;
    }
   CGSize size = [text boundingRectWithSize:CGSizeMake(kWIDTH-28*2-22*2, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont yc_14]} context:nil].size;
    self.height = 48.0 + 8 + size.height + 8 + 4;
}

- (UIColor *)statusColor {
    
    if (self.creditType == YCLoanCustomerCreditTypeBank) {
        if ([@"通过" isEqualToString:self.status]) {
            return [UIColor yc_hex_59B50A];
        }else if ([@"拒绝" isEqualToString:self.status]) {
            return [UIColor yc_hex_FF3B30];
        }else {
            return [UIColor yc_hex_F5A623];
        }
    }
    if (self.creditType == YCLoanCustomerCreditTypeSocial) {
        if ([@"通过" isEqualToString:self.status]) {
            return [UIColor yc_hex_59B50A];
        }else if ([@"不通过" isEqualToString:self.status]) {
            return [UIColor yc_hex_FF3B30];
        }else {
            return [UIColor yc_hex_F5A623];
        }
    }
    return [UIColor yc_hex_5B6071];
}

@end
