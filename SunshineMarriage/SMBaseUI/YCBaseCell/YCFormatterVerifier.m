//
//  YCFormatterVerifier.m
//  YCBaseUI
//
//  Created by haima on 2019/7/29.
//

#import "YCFormatterVerifier.h"

@implementation YCFormatterVerifier

+ (BOOL(^)(id value))twoDecimalPlacesValidator {
    
    return ^(id value){
        NSString *text = value;
        if ([text hasPrefix:@"."]) {
            //第一位不能为小数点
            return NO;
        }
        NSArray *spArr = [text componentsSeparatedByString:@"."];
        if (spArr.count > 2) {
            return NO;
        }
        if (spArr.count > 1 && [spArr.lastObject length] > 2) {
            return NO;
        }
        return YES;
    };
}

+ (BOOL(^)(NSString *value))phoneNumberValidator {
    
    return ^(NSString *value){

        NSString *text = [value stringByReplacingOccurrencesOfString:@" " withString:@""];
        if ([text rangeOfString:@"-"].location != NSNotFound) {
            text = [text stringByReplacingOccurrencesOfString:@"-" withString:@""];
        }
        if (text.length > 11) {
            return NO;
        }
        return YES;
    };
}
@end
