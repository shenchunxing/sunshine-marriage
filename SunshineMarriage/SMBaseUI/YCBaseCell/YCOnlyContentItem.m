//
//  YCCreditOnlyContentItem.m
//  YCCreditModule
//
//  Created by 刘成 on 2019/4/8.
//

#import "YCOnlyContentItem.h"
#import "NSString+Size.h"
#import "YCBaseUI.h"

@implementation YCOnlyContentItem

- (void)setMaxWidth:(CGFloat)maxWidth {
    if (maxWidth == 0) {
        return;
    }
    [super setMaxWidth:maxWidth];
    [self needChangeContentHeight];
}
- (BOOL)needChangeContentHeight {
    
    //宽度=屏幕宽度-标题宽度-左右缩进-距离-内间距
    CGFloat maxWidth = self.maxWidth -30;

    CGSize size = [self.text yc_boundingRectWithSize:CGSizeMake(maxWidth, HUGE_VAL) font:[UIFont yc_16]];
    CGFloat height = ceil(size.height) + 2 * cellMargin;
    //textview高度增加时，cell高度也增加
    if (height > self.cellHeight) {
        self.cellHeight = height;
        return YES;
    }
    //textview高度减小时，cell也响应减小高度，最小不能小于标题文本高度
    if (height < self.cellHeight && height >= self.titleHeight) {
        self.cellHeight = height;
        return YES;
    }
    return NO;
}

@end
