//
//  SMOptionPickerStrategy.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/28.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMOptionPickerStrategy.h"
#import "SMOptionPickerView.h"

@interface SMOptionPickerStrategy ()<SMOptionPickerViewDelegate>

@end

@implementation SMOptionPickerStrategy

+ (instancetype)stragetyWithBlock:(SMOptionPickerStrategyBlock)block  {
    SMOptionPickerStrategy *strategy =  [[SMOptionPickerStrategy alloc] init];
    strategy.block = block;
    return strategy;
}

- (void)invoke {
    SMOptionPickerView *dateView = [[SMOptionPickerView alloc] initWithOptions:self.options];
    dateView.selectedModel = self.selectedModel ;
    dateView.block = self.block;
    [dateView addItems];
    [dateView showWithAnimation:YES];
}

@end
