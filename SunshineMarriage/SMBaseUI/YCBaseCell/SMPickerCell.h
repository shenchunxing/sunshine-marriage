//
//  SMPickerCell.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/28.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "YCBaseCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface SMPickerCell : YCBaseCell

/* > */
@property (nonatomic, strong) UIImageView *nextImageView;

/* 展示文字 */
@property (nonatomic, strong) UILabel *contentLabel;
/* 占位文字 */
@property (nonatomic, strong) UILabel *placeholderLbl;

@end

NS_ASSUME_NONNULL_END
