//
//  YCBaseItem.h
//  YCBaseUI
//
//  Created by haima on 2019/3/29.
//

#import "YCTableViewKit.h"
#import "UIColor+Style.h"
#import "UIFont+Style.h"

typedef NS_ENUM(NSInteger,YCEditStyle) {
    YCEditStyleEditable,        //可编辑
    YCEditStyleUnEditable       //不可编辑
};

typedef NS_ENUM(NSUInteger, YCTableViewCellSeparatorStyle) {
    YCTableViewCellSeparatorStyleIndentNo,          //不缩进
    YCTableViewCellSeparatorStyleIndent,            //双侧缩进
    YCTableViewCellSeparatorStyleIndentLeft,        //左侧缩进
    YCTableViewCellSeparatorStyleIndentRight,       //右侧缩进
    YCTableViewCellSeparatorStyleIndentNone,        //不显示
};


//默认cell高度
static CGFloat kDefaultCellHeight = 45.0;
//cell内容左右缩进值
static CGFloat cellMargin = 14.0;
//提示符号宽度
static CGFloat tipWidth = 6.0;
//图标宽度
static CGFloat iconWidth = 15.0;
//title间距
static CGFloat kTitlePadding = 24.0;


@interface YCBaseItem : YCTableViewItem<YCValidatableProtocol>

/* 是否必填 */
@property (nonatomic, assign) BOOL isRequired;
/* 是否有图标 */
@property (nonatomic, assign) BOOL hasIcon;
/* 是否显示帮助按钮 */
@property (nonatomic, assign) BOOL showHelp;
/* 帮助按钮回调 */
@property (nonatomic, strong) void(^helpHandler)(NSIndexPath *indexPath);
/* 图标名 */
@property (nonatomic, copy) NSString *iconName;
/* 标题 */
@property (nonatomic, copy) NSString *title;
/* 标题颜色 默认#333333 */
@property (nonatomic, strong) UIColor *titleColor;
/* 文本 */
@property (nonatomic, copy) NSString *text;
/* 文本颜色 默认#0088FF*/
@property (nonatomic, strong) UIColor *textColor;
/* 占位文本 */
@property (nonatomic, copy) NSString *placeHolder;
/* 占位文本颜色 默认#CCCCCC*/
@property (nonatomic, strong) UIColor *placeHolderColor;
/* 编辑类型 默认可编辑 */
@property (nonatomic, assign) YCEditStyle editStyle;
/* 分割线样式 默认左右缩进 */
@property (nonatomic, assign) YCTableViewCellSeparatorStyle separatorStyle;
/* 根据标题内容自动计算，用于换行比较 */
@property (nonatomic, assign) CGFloat titleHeight;
/* cell 高度, 默认45.0 */
@property (nonatomic, assign) CGFloat cellHeight;

//靠右对齐
@property (nonatomic,assign) BOOL alignedRight;


/** 绑定item */
@property (nonatomic, copy) NSString * key;

@property (nonatomic,strong) UIFont *titleFont;

@property (nonatomic,strong) UIFont *textFont;

@property (nonatomic,assign) BOOL noChangeContent;//不根据内容改变高度

//背景色
@property (nonatomic,strong) UIColor *backgroundColor;
@property (nonatomic,assign) BOOL selectStyleIsNone;//选中无效果

- (void)changerTitleHeight;
/**
 计算内容高度方法

 @return 是否需要刷新cell
 */
- (BOOL)needChangeContentHeight;


/**
 获取必填字段提示文本

 @return 获取必填字段提示文本
 */
- (NSString *)getRequireNilTipStr;

+ (instancetype)normalItemWithTitle:(NSString *)title;

- (BOOL)valid;

//顺序加载
+ (instancetype)itemWithArray:(NSArray *)itemArr;


@end

