//
//  YCNoDataItem.m
//  YCBaseUI
//
//  Created by shenweihang on 2019/9/4.
//

#import "YCNoDataItem.h"

@implementation YCNoDataItem

+ (instancetype)noDataItem {
    
    YCNoDataItem *item  = [[YCNoDataItem alloc] init];
    item.des = @"暂无附件";
    return item;
}
@end
