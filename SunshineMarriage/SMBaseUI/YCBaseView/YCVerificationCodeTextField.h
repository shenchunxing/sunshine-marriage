//
//  YCVerificationCodeTextField.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/5/24.
//

#import "YCTextField.h"
@class VerificationLabel;
NS_ASSUME_NONNULL_BEGIN

@interface YCVerificationCodeTextField : YCTextField

@property (nonatomic,copy) void(^sendMessageHandle)(void);

//手机号码
@property (nonatomic,copy) NSString *phoneNumer;
@property (nonatomic,strong) VerificationLabel *verificationLabel;
@property (nonatomic, assign) BOOL refreshConstraints;
@property (nonatomic, strong) UIColor *countDownColor;
@property (nonatomic, strong) UIColor *countDownBgColor;
@end

@interface VerificationLabel : UILabel

@end

NS_ASSUME_NONNULL_END
