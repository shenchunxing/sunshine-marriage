//
//  YCSingleChooseView.h
//  YCBaseUI
//
//  Created by haima on 2019/3/30.
//

#import <UIKit/UIKit.h>

@protocol YCSingleViewDelegate <NSObject>

- (void)chooseSingleViewAtIndex:(NSInteger)index;

@end

NS_ASSUME_NONNULL_BEGIN

@interface YCSingleChooseView : UIView

/* 序号 */
@property (nonatomic, assign) NSInteger index;
/* 按钮 */
@property (nonatomic, strong) UIButton *chooseButton;
/* 文本 */
@property (nonatomic, strong) UILabel *contentLabel;
/* 代理 */
@property (nonatomic, weak) id<YCSingleViewDelegate> delegate;

- (void)setContent:(NSString *)content chooseStatus:(BOOL)choosed;
- (void)cancelSelectedState;
@end

NS_ASSUME_NONNULL_END
