//
//  YCEncryptionTextField.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/5/26.
//

#import "YCEncryptionTextField.h"
#import <Masonry/Masonry.h>
#import "YCCategoryModule.h"
#import "SMDefine.h"
#import "YCDataCenter.h"
#import "YCSystemConfigure+UIColor.h"

@interface YCEncryptionTextField ()<UITextFieldDelegate>
@end

@implementation YCEncryptionTextField

- (instancetype)init{
    if (self = [super init]) {
        [self addObserver:self forKeyPath:@"text" options:NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"text"]) {
        NSString *text = change[NSKeyValueChangeNewKey];
        self.encryptionBtn.hidden = text.length <=0;
        self.line.backgroundColor = text.length >0 ? [YCSystemConfigure mainColor] : [UIColor yc_hex_EEEEEE];
    }
}


- (void)setUp{
    [super setUp];
    
    [self.textField mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).offset(-43);
    }];
    
    [self addSubview:self.encryptionBtn];
    [self.encryptionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(37);
        make.right.mas_equalTo(self);
        make.top.mas_equalTo(self).offset(10);
    }];
    
    self.isOpenEye = NO;
    self.encryptionBtn.hidden = YES;
}

- (void)yc_textFiledEditChanged:(UITextField *)textField{
    self.encryptionBtn.hidden = textField.text.length <=0;
}

- (void)encryptionBtnClick{
    self.isOpenEye = !self.isOpenEye;
    [self.encryptionBtn setImage:[UIImage yc_loginImgWithName:self.isOpenEye ? @"icon_zhengyan":@"icon_biyan" bundle:[YCDataCenter isCarDealer] ? @"YCCDLoginModule": @"YCLoginModule"] forState:UIControlStateNormal];
    self.textField.secureTextEntry = !self.isOpenEye;

}


- (UIButton *)encryptionBtn{
    if (!_encryptionBtn) {
        _encryptionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _encryptionBtn.imageEdgeInsets = UIEdgeInsetsMake(17, 17, 0, 0);
        [_encryptionBtn setImage:[UIImage yc_loginImgWithName:@"icon_biyan" bundle:[YCDataCenter isCarDealer] ? @"YCCDLoginModule" : @"YCLoginModule" ] forState:UIControlStateNormal];
        [_encryptionBtn addTarget:self action:@selector(encryptionBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _encryptionBtn;
}

@end
