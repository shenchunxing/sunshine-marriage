//
//  YCSingleChooseView.m
//  YCBaseUI
//
//  Created by haima on 2019/3/30.
//

#import "YCSingleChooseView.h"
#import <Masonry/Masonry.h>
#import "UIFont+Style.h"
#import "UIColor+Style.h"
#import "UIImage+Resources.h"

@implementation YCSingleChooseView


- (instancetype)init{
    
    if (self = [super init]) {
        [self addSubview:self.chooseButton];
        [self addSubview:self.contentLabel];
        [self initViews];
        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapClick)];
        [self addGestureRecognizer:gesture];
    }
    return self;
}

- (void)initViews {
    
    [self.chooseButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.centerY.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(16, 16));
    }];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.chooseButton.mas_right).offset(8);
        make.centerY.equalTo(self);
        make.height.right.equalTo(self);
    }];
}


- (void)setContent:(NSString *)content chooseStatus:(BOOL)choosed {
    
    self.contentLabel.text = content;
    self.chooseButton.selected = choosed;
}

- (void)cancelSelectedState {
    
    self.chooseButton.selected = NO;
}


- (void)onTapClick {
    
    BOOL selected = self.chooseButton.selected;
    self.chooseButton.selected = !selected;
    if (self.delegate && [self.delegate respondsToSelector:@selector(chooseSingleViewAtIndex:)]) {
        [self.delegate chooseSingleViewAtIndex:self.index];
    }
}


- (UIButton *)chooseButton {
    
    if (_chooseButton == nil) {
        _chooseButton = [[UIButton alloc] init];
        [_chooseButton setBackgroundImage:[UIImage imageNamed:@"icon_unselected"] forState:UIControlStateNormal];
        [_chooseButton setBackgroundImage:[UIImage imageNamed:@"icon_selected"] forState:UIControlStateSelected];
        _chooseButton.userInteractionEnabled = NO;
    }
    return _chooseButton;
}

- (UILabel *)contentLabel {
    
    if (_contentLabel == nil) {
        _contentLabel = [[UILabel alloc] init];
        _contentLabel.textColor = [UIColor yc_hex_3C3D49];
        _contentLabel.textAlignment = NSTextAlignmentLeft;
        _contentLabel.font = [UIFont yc_major_font];
    }
    return _contentLabel;
}
@end
