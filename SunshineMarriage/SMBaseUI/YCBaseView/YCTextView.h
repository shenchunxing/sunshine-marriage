//
//  YCTextView.h
//  YCBaseUI
//
//  Created by haima on 2019/3/29.
//

#import <UIKit/UIKit.h>

@interface YCTextView : UITextView

/* 占位文本 */
@property (nonatomic, copy) NSString *placeholder;

/* 占位文本颜色 */
@property (nonatomic, strong) UIColor *placeholderColor;

/* 占位lable */
@property (nonatomic, strong) UILabel *placeholderLabel;


@property (nonatomic,assign) BOOL rightAlignment;
/* 右对齐时，显示清空按钮时的偏移量 */
@property (nonatomic, assign) CGFloat rightOffset;
@end


