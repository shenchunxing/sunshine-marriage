//
//  YCTextField.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/5/24.
//

#import "YCBaseTextField.h"



NS_ASSUME_NONNULL_BEGIN

@interface YCImageTextField : YCBaseTextField<UITextFieldDelegate>


@property (nonatomic, assign) BOOL showLine;

@end

NS_ASSUME_NONNULL_END
