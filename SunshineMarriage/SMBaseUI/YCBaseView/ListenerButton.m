//
//  ListenerButton.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/12/15.
//

#import "ListenerButton.h"
#import "UIColor+Style.h"
@implementation ListenerButton
- (void)setEditTextFields:(NSArray *)editTextFields {
    _editTextFields = editTextFields;
    for (YCBaseTextField *baseTextField in editTextFields) {
        baseTextField.listenerDelegate = self;
    }
}

- (BOOL)checkAllEdit {
    for (id textField in self.editTextFields) {
        if ([textField isKindOfClass:[YCBaseTextField class]]) {
            YCBaseTextField *bsTextField = (YCBaseTextField *)textField;
            if (bsTextField.text.length <= 0)  return NO;
        }
    }
    return YES;
}

- (void)listenerTextFiledEditChanged:(UITextField *)textField {
    self.userInteractionEnabled = [self checkAllEdit];
    self.backgroundColor = [self checkAllEdit] ? [UIColor yc_hex_F8513B]: [UIColor yc_hex_CACCD4];
}

@end
