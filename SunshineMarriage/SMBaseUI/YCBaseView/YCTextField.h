//
//  YCTextField.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/5/24.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,YCTextFieldType) {
    YCPhoneTextFieldType,//手机
    YCPasswordTextFieldType,//密码加密
    YCSendMessageTextFieldType,//发送验证码
    YCChangePasswordTextFieldType//修改密码
};

@protocol YCTextFieldDelegate <NSObject>

- (BOOL)yc_textFieldShouldReturn:(UITextField *)textField;

@end


@interface YCTextField : UIView

@property (nonatomic,strong) UITextField *textField;
//提示文字
@property (nonatomic,copy) NSString *placeholder;
//图片名字
@property (nonatomic,copy) NSString *imageName;
//类型
@property (nonatomic,assign) YCTextFieldType textFieldType;
//是否可以编辑
@property (nonatomic,assign) BOOL canEdit;
//文字
@property (nonatomic,copy) NSString *text;
//底部线条
@property (nonatomic,strong) UIView *line;
//是否加密
@property (nonatomic,assign) BOOL  secureTextEntry ;
//线条颜色是否需要变换
@property (nonatomic,assign) BOOL lineChanged;
//最小输入长度
@property (nonatomic,assign) NSInteger minLength;
//最大输入长度
@property (nonatomic,assign) NSInteger maxLength;
//textfield名称
@property (nonatomic,copy) NSString *name;

//@property (nonatomic, strong) YCIconInfo *iconInfo;
@property (nonatomic, assign) BOOL showIconFont;
@property (nonatomic, assign) UIReturnKeyType returnKeyType;

- (void)setUp;
- (instancetype)initWithTextFieldType:(YCTextFieldType)textFieldType;
- (instancetype)initWithName:(NSString *)name;
- (instancetype)initWithName:(NSString *)name showIconFont:(BOOL)showIconFont;


//车商
@property (nonatomic,copy) NSString *cdimageName;

@property (nonatomic, assign) id<YCTextFieldDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
