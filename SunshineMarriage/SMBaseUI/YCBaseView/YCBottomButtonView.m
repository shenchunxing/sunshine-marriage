//
//  YCBottomButtonView.m
//  YCBaseUI
//
//  Created by haima on 2019/4/8.
//

#import "YCBottomButtonView.h"
#import <Masonry/Masonry.h>
#import "UIColor+Style.h"
#import "UIFont+Style.h"
#import "SMDefine.h"
#import "UIView+Extension.h"

@implementation YCBottomButtonTransaction

+ (instancetype)transaction {
    YCBottomButtonTransaction *transaction = [[YCBottomButtonTransaction alloc] init];
    transaction.backgroundColorType = YCBottomButtonColorTypeBlue;
    transaction.hasSeparatorLines = NO;
    return transaction;
}

- (instancetype)init {
    
    if (self = [super init]) {
        self.isActive = YES;
    }
    return self;
}

@end

@interface YCBottomButtonView ()

@property (nonatomic, strong, readwrite) NSArray<YCBottomButtonTransaction *> *buttonItems;
@property (nonatomic, strong) NSMutableArray *footerButtons;
/* 顶部分割线 */
@property (nonatomic, strong) UIView *topLineView;
@end

@implementation YCBottomButtonView

- (instancetype)initWithTitles:(NSArray <NSString *>*)titles backgroundColorTypes:(NSArray *)backgroundColorTypes clickHandle:(void(^)(NSUInteger))clickHandle
{
    if (self = [super init]) {
        NSMutableArray *array = [NSMutableArray array];
        [titles enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            YCBottomButtonTransaction *transaction = [YCBottomButtonTransaction transaction];
            transaction.title = titles[idx];
            transaction.backgroundColorType = [backgroundColorTypes[idx] intValue];
            [transaction setBottomButtonClick:^{
                if(clickHandle) clickHandle(idx);
            }];
            [array addObject:transaction];
        }];
        [self configBottomViewWithTwoItems:array];
    }
    return self;
}


- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor yc_hex_F4F4F4];
        [self addSubview:self.topLineView];
        [self.topLineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.equalTo(self);
            make.height.equalTo(@(kLineHeight));
        }];
    }
    return self;
}

- (void)configBottomViewWithItems:(NSArray<YCBottomButtonTransaction *> *)items
{
    [self.footerButtons makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.footerButtons removeAllObjects];
    
    self.buttonItems = items;
    if (items == nil || items.count == 0) {
        return;
    }
    self.topLineView.hidden = !(items.count > 1);
    
    __block CGFloat width = 0.0;
    __block NSInteger itemCount = items.count;
//    if (itemCount == 2) {
//        //第一个按钮占三分之一
//        width = [UIScreen mainScreen].bounds.size.width / 3;
//    }else{
    if (self.totalWidth) {
        width = self.totalWidth/items.count;
    }else{
        width = [UIScreen mainScreen].bounds.size.width / items.count;
    }
    
//    }
    __block CGFloat leftOffset = 0.0;
    [items enumerateObjectsUsingBlock:^(YCBottomButtonTransaction * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        UIButton *button = [self buttonWithTransaction:obj];
        button.tag = 10000 + idx;
        [self addSubview:button];
        [self.footerButtons addObject:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.height.equalTo(@48);
            make.left.mas_equalTo(leftOffset);
            make.width.mas_equalTo(width);
        }];
        leftOffset += width;
        if (itemCount > 1) {
            UIView *line = [self lineView];
            [self addSubview:line];
            [line mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(leftOffset);
                make.top.equalTo(self);
                make.height.equalTo(@48);
                make.width.mas_equalTo(kLineHeight);
            }];
        }
        if (itemCount == 2) {
            width = [UIScreen mainScreen].bounds.size.width - width;
        }

    }];
}

//平均，有间距
- (void)configBottomViewWithCornerItems:(NSArray<YCBottomButtonTransaction *> *)items{
    
    [self.footerButtons makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.footerButtons removeAllObjects];
    
    self.buttonItems = items;
    if (items == nil || items.count == 0) {
        return;
    }
    __block CGFloat width = ([UIScreen mainScreen].bounds.size.width - 14*2 - 15*(items.count -1) )/ items.count;//平均
    [items enumerateObjectsUsingBlock:^(YCBottomButtonTransaction * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIButton *button = [self buttonWithTransaction:obj];
        button.layer.cornerRadius  = 4;
        button.layer.borderColor =[UIColor yc_hex_108EE9].CGColor;
        button.layer.borderWidth = 1;
        button.layer.masksToBounds = YES;
        button.tag = 10000 + idx;
        [self addSubview:button];
        [self.footerButtons addObject:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.height.equalTo(@48);
            make.width.mas_equalTo(width);
            make.left.mas_equalTo(idx*(width+15));
        }];
    }];
}

- (void)configBottomViewWithTwoItems:(NSArray<YCBottomButtonTransaction *> *)items{
    
    [self.footerButtons makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.footerButtons removeAllObjects];
    
    self.buttonItems = items;
    if (items == nil || items.count == 0) {
        return;
    }
    __block CGFloat width = 132*kScaleFit;
    [items enumerateObjectsUsingBlock:^(YCBottomButtonTransaction * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        UIButton *button = [self buttonWithTransaction:obj];
        button.tag = 10000 + idx;
        [self addSubview:button];
        [self.footerButtons addObject:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.height.equalTo(@48);
            make.width.mas_equalTo(idx == 1 ? kWIDTH - width:width);
            make.left.mas_equalTo(idx*width);
        }];
    }];
}

- (void)configBottomViewWithUpDownItems:(NSArray<YCBottomButtonTransaction *> *)items{
    
    [self.footerButtons makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.footerButtons removeAllObjects];
    
    self.buttonItems = items;
    if (items == nil || items.count == 0) {
        return;
    }
    __block CGFloat width = kWIDTH - 14*2;
    [items enumerateObjectsUsingBlock:^(YCBottomButtonTransaction * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        UIButton *button = [self buttonWithTransaction:obj];
        button.layer.cornerRadius  = 4;
        button.layer.borderColor = button.backgroundColor.CGColor;
        button.layer.borderWidth = 1;
        button.layer.masksToBounds = YES;
        button.tag = 10000 + idx;
        [self addSubview:button];
        [self.footerButtons addObject:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(idx*48);
            make.height.equalTo(@48);
            make.width.mas_equalTo(width);
            make.left.mas_equalTo(14);
        }];
    }];
}

- (void)onButtonClick:(UIButton *)button {
    
    NSInteger index = button.tag - 10000;
    YCBottomButtonTransaction *transaction = self.buttonItems[index];
    if (transaction.isActive) {
        !transaction.bottomButtonClick?:transaction.bottomButtonClick();
    }
}

- (UIButton *)buttonWithTransaction:(YCBottomButtonTransaction *)transaction {
    
    UIColor *backgroundColor = nil;
    UIColor *titleColor = nil;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    switch (transaction.backgroundColorType) {
        case YCBottomButtonColorTypeBlue:
            backgroundColor = [UIColor yc_hex_108EE9];
            titleColor = [UIColor whiteColor];
            break;
        case YCBottomButtonColorTypeWathetBlue:
            backgroundColor = [UIColor yc_hex_color:0x03B2F3];
            titleColor = [UIColor whiteColor];
            break;
        case YCBottomButtonColorTypeGrayBlue:
            backgroundColor = [UIColor yc_hex_F4F4F4];
            titleColor = [UIColor yc_hex_108EE9];
            break;
        case YCBottomButtonColorTypeYellowWhite:
            backgroundColor = [UIColor yc_hex_FFA91A];
            titleColor = [UIColor whiteColor];
            break;
            
        case YCBottomButtonColor_03B2F3:
            backgroundColor = [UIColor yc_hex_03B2F3];
            titleColor = [UIColor whiteColor];
            break;
        case YCBottomButtonColor_FF3B30:
            backgroundColor = [UIColor yc_hex_FF3B30];
            titleColor = [UIColor whiteColor];
            break;
        case YCBottomButtonColorTypeGreen:
            backgroundColor = [UIColor yc_hex_59B50A];
            titleColor = [UIColor whiteColor];
            break;
    }
    if (transaction.isBorder) {
        button.layer.borderWidth = 1;
        button.layer.borderColor = titleColor.CGColor;
    }
    button.titleLabel.font = [UIFont yc_18];
    [button setTitle:transaction.title forState:UIControlStateNormal];
    [button setBackgroundColor:backgroundColor];
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    [button addTarget:self action:@selector(onButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    button.userInteractionEnabled = transaction.isActive;
    return button;
}

- (UIView *)lineView {
    
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor yc_hex_98D1FA];
    return view;
}


- (NSMutableArray *)footerButtons {
    
    if (_footerButtons == nil) {
        _footerButtons = [[NSMutableArray alloc] init];
    }
    return _footerButtons;
}

- (UIView *)topLineView {
    
    if (_topLineView == nil) {
        _topLineView = [[UIView alloc] init];
        _topLineView.backgroundColor = [UIColor yc_hex_98D1FA];
        _topLineView.hidden = YES;
    }
    return _topLineView;
}
@end
