//
//  ListenerButton.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/12/15.
//

#import <UIKit/UIKit.h>
#import "YCBaseTextField.h"
NS_ASSUME_NONNULL_BEGIN
@interface ListenerButton : UIButton<YCBaseTextFieldListenerDelegate>
@property (nonatomic, copy) NSArray *editTextFields;
@end

NS_ASSUME_NONNULL_END
