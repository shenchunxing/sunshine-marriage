//
//  YCBaseTextField+Extension.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/12/12.
//

#import "YCBaseTextField.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCBaseTextField (Extension)
- (BOOL)compareString:(NSString *)string;
- (BOOL)compareTextField:(YCBaseTextField *)textField;
@end

NS_ASSUME_NONNULL_END
