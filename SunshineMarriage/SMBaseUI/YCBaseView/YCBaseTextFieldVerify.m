//
//  YCBaseTextFieldVerify.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/12/17.
//

#import "YCBaseTextFieldVerify.h"
#import "YCBaseTextField.h"
#import "NSObject+Helper.h"
#import "NSString+Extension.h"
#import "NSString+STRegex.h"
#import "UIViewController+CustomToast.h"
#import "YCTitleTextField.h"
static NSString *const phoneVerifyError = @"请输入正确的手机号";
static NSString *const minLengthVerifyError = @"长度太短";

@implementation YCBaseTextFieldVerify
+ (BOOL)verifyTextfields:(NSArray <YCBaseTextField *>*)textfields{
   for (YCBaseTextField *tf in textfields) {
       if (tf.isPhoneNum) {
           if (![self verifyPhoneNum:tf]) {
               [[self yc_getCurrentViewController] showToast:phoneVerifyError];
               return NO;
           }
       }
       if (![self verifyMinLength:tf]) {
           [[self yc_getCurrentViewController] showToast:[self tooShortString:tf]];
           return NO;
       }
    }
    return YES;
}

+ (BOOL)verifyPhoneNum:(YCBaseTextField *)tf {
    return [[tf.text yc_deleteEmpty] isValidPhoneNum];
}

+ (BOOL)verifyMinLength:(YCBaseTextField *)tf {
    if (tf.minLength) {
        return tf.minLength <= tf.text.length;
    }
    return YES;
}

+ (NSString *)tooShortString:(YCBaseTextField *)tf {
    NSString *tooShort = nil;
    if ([tf isKindOfClass:[YCTitleTextField class]]) {
        YCTitleTextField *titleTf = (YCTitleTextField *)tf;
        tooShort = titleTf.name;
    }
    if(tf.errorString) {
        if ([tf.errorString containsString:@"请输入"]) {
            tooShort = [tf.errorString stringByReplacingCharactersInRange:NSMakeRange(0, 3) withString:@""];
        }else {
            tooShort = tf.errorString;
        }
    }
    return [NSString stringWithFormat:@"%@%@",tooShort,minLengthVerifyError];
}

@end
