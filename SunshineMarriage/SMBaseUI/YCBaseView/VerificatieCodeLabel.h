//
//  VerificatieCodeLabel.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/11/22.
//

#import <UIKit/UIKit.h>
@class YCBaseTextField;
NS_ASSUME_NONNULL_BEGIN
@interface VerificatieCodeLabel : UILabel
@property (nonatomic, strong) YCBaseTextField *titleTextField;
@property (nonatomic,copy) NSString *phoneNumer;
@property (nonatomic, strong) UIColor *countDownColor;
@property (nonatomic, strong) UIColor *countDownBgColor;
@property (nonatomic,copy) void(^sendMessageCodeHandle)(void);
@end
NS_ASSUME_NONNULL_END
