//
//  YCTextFieldEditManager.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/7/26.
//

#import <Foundation/Foundation.h>
#import "YCTextField.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCTextFieldEditManager : NSObject

+ (BOOL)isVerifyBylengthInController:(UIViewController *)controller;

/// 验证指定textfield是不是在设定的范围内
/// @param textfields 指定的textfield数组
+ (BOOL)verifyTextfields:(NSArray <YCTextField *>*)textfields;

/// 验证superview里面的所有etxtfield是不是非空
/// @param superView 父视图
+ (BOOL)verifyTextfieldsInSuperview:(UIView *)superView;



@end

NS_ASSUME_NONNULL_END
