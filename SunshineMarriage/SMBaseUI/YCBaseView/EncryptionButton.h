//
//  EncryptionButton.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/11/22.
//

#import <UIKit/UIKit.h>


typedef void(^EncryptionStatusBlock)(BOOL isOpen);

NS_ASSUME_NONNULL_BEGIN

@interface EncryptionButton : UIButton

@property (nonatomic,assign) BOOL isOpenEye;
@property (nonatomic, assign) BOOL showIconFont;
@property (nonatomic, copy) EncryptionStatusBlock encryptionStatusBlock;
- (instancetype)initWithShowIconFont:(BOOL)showIconFont;
@end

NS_ASSUME_NONNULL_END
