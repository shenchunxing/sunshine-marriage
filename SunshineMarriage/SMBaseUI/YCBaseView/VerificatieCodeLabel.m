//
//  VerificatieCodeLabel.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/11/22.
//

#import "VerificatieCodeLabel.h"
#import "YCBaseUI.h"
#import "YCCategoryModule.h"
#import "SMDefine.h"
#import "YCDataCenter.h"
#import "YCSystemConfigure+UIColor.h"

@interface VerificatieCodeLabel ()<UITextFieldDelegate>
@end

@implementation VerificatieCodeLabel

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(getVerificationCode:)];
        [self addGestureRecognizer:tap];
    }
    return self;
}
//短信验证码
- (void)getVerificationCode:(UITapGestureRecognizer *)tap{
    if (![[self.titleTextField.text yc_deleteEmpty] isValidPhoneNum]) {
        [[self yc_getCurrentViewController] showToast:@"请输入正确的手机号码"];
         return;
    }
    [[YCGCDTimer timerManager] loadTimerWithCountDownHandle:^(NSInteger countDown) {
        self.text = [NSString stringWithFormat:@"还剩%ld秒",(long)countDown];
        self.textColor = self.countDownColor?: [UIColor yc_hex_B3B7C2];
        self.backgroundColor = self.countDownBgColor?:self.backgroundColor;
        self.userInteractionEnabled = NO;
        self.layer.borderColor = self.backgroundColor.CGColor;
    } finishHandle:^{
        self.userInteractionEnabled = YES;
        self.text = @"重新发送";
        self.textColor = [YCSystemConfigure mainColor];
        self.backgroundColor = [UIColor whiteColor];
        self.layer.borderColor = [UIColor yc_hex_CCCCCC].CGColor;
    }];
    //发送网络请求
    if (self.sendMessageCodeHandle) {
        self.sendMessageCodeHandle();
    }
}

@end
