//
//  YCBaseTextField+Extension.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/12/12.
//

#import "YCBaseTextField+Extension.h"
#import "YCAlertView.h"
@implementation YCBaseTextField (Extension)
- (BOOL)compareString:(NSString *)string {
   return [self.textField.text isEqualToString:string];
}

- (BOOL)compareTextField:(YCBaseTextField *)textField {
   return [self.textField.text isEqualToString:textField.text];
}
@end
