//
//  YCPushOpenTipsView.h
//  YCHomeModule
//
//  Created by 沈春兴 on 2019/5/14.
//

#import <UIKit/UIKit.h>
@class YCTableViewSection;
NS_ASSUME_NONNULL_BEGIN

//typedef NS_ENUM(NSInteger,PushOpenTipsType) {
//    PushOpenTips_Default,
//    PushOpenTips_BankSaas,
//};

@interface YCPushOpenTipsView : UIView
//@property (nonatomic, assign) PushOpenTipsType pushOpenTipstype;
//@property (nonatomic, strong) ConfigureObject *configureObject;
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, strong) UIColor *btnBackgroundColor;
- (void)showAsHeaderViewOnSection:(YCTableViewSection *)section;
- (void)showOnSection:(YCTableViewSection *)section byNotificationSwitchOnOff:(BOOL)notificationSwitchOnOff;
//+ (void)showAsHeaderViewOnSection:(YCTableViewSection *)section type:(PushOpenTipsType)type;
@end


NS_ASSUME_NONNULL_END
