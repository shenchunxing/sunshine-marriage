//
//  YCPushOpenTipsView.m
//  YCHomeModule
//
//  Created by 沈春兴 on 2019/5/14.
//

#import "YCPushOpenTipsView.h"
#import "YCCategoryModule.h"
#import "YCBaseUI.h"
#import "SMDefine.h"
#import "YCDataCenter.h"
@interface YCPushOpenTipsView ()

@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) UIButton *openBtn;

@end

@implementation YCPushOpenTipsView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor yc_hex_FCFEEB];
        [self addSubview:self.titleLabel];
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(14);
            make.top.mas_equalTo(12);
        }];
        
        [self addSubview:self.openBtn];
        [self.openBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-14);
            make.top.mas_equalTo(8);
            make.width.mas_equalTo(60);
            make.height.mas_equalTo(28);
        }];
    }
    return self;
}

- (void)showOnSection:(YCTableViewSection *)section byNotificationSwitchOnOff:(BOOL)notificationSwitchOnOff{
   section.headerHeight = notificationSwitchOnOff ? 44.f : 0.01f;
   section.headerView = notificationSwitchOnOff ? self :nil;
}

- (void)showAsHeaderViewOnSection:(YCTableViewSection *)section {
   section.headerHeight = ![[YCDataCenter sharedData] isUserNotificationEnable] ? 44.f : 0.01f;
   section.headerView = ![[YCDataCenter sharedData] isUserNotificationEnable] ? self :nil;
}


- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [UILabel yc_labelWithText:@"开启推送通知，及时查收消息" textFont:[UIFont yc_15] textColor:[UIColor yc_hex_F5A623]];
    }
    return _titleLabel;
}

- (UIButton *)openBtn{
    if (!_openBtn) {
        _openBtn = [UIButton yc_customTextWithTitle:@"去开启" titleColor:[UIColor whiteColor] fontSize:[UIFont yc_13] target:self action:@selector(openBtnClick)];
        [_openBtn yc_radiusWithRadius:2];
        [_openBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         [_openBtn yc_setGradientBackgroundWithColors:@[[UIColor yc_hex_FF6C00],[UIColor yc_hex_FF8700]] locations:nil startPoint:CGPointMake(0, 1) endPoint:CGPointMake(1, 1)];
    }
    return _openBtn;
}

- (void)openBtnClick{
    [[YCDataCenter sharedData] goToAppSystemSetting];
}

- (void)setTextColor:(UIColor *)textColor {
    _textColor = textColor;
    self.titleLabel.textColor = textColor;
}

- (void)setBtnBackgroundColor:(UIColor *)btnBackgroundColor {
    _btnBackgroundColor = btnBackgroundColor;
    [self.openBtn setBackgroundColor:btnBackgroundColor];
}

@end

