//
//  YCImageEncryptionTextField.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/11/28.
//

#import "YCImageTextField.h"
#import "EncryptionButton.h"
NS_ASSUME_NONNULL_BEGIN

@interface YCImageEncryptionTextField : YCImageTextField
@property (nonatomic, strong) EncryptionButton *encryptionBtn;
//@property (nonatomic, assign) BOOL showEye;

@end

NS_ASSUME_NONNULL_END
