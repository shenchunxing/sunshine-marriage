//
//  YCTextEncryptionTextField.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/5/26.
//

#import "YCTextEncryptionTextField.h"
#import "YCBaseUI.h"
#import "YCCategoryModule.h"
#import "SMDefine.h"

@interface YCTextEncryptionTextField ()<UITextFieldDelegate>
@property (nonatomic,strong) UILabel *textLabel;
@end

@implementation YCTextEncryptionTextField


- (void)setUp{
    [self addSubview:self.textLabel];
    [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(14);
        make.top.mas_equalTo(12);
        make.height.mas_equalTo(21);
        make.width.mas_equalTo(76);
    }];
    
    [self addSubview:self.textField];
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.textLabel.mas_right);
        make.top.mas_equalTo(12);
        make.height.mas_equalTo(21);
        make.right.mas_equalTo(self).offset(-43);
    }];
    
    if (!self.hideLine) {
        [self addSubview:self.line];
        [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(14);
            make.right.mas_equalTo(self).offset(-14);
            make.bottom.mas_equalTo(self);
            make.height.mas_equalTo(kLineHeight);
        }];
    }
    
    self.backgroundColor = [UIColor whiteColor];
    self.textFieldType = YCChangePasswordTextFieldType;
    self.lineChanged = NO;
    
    
    [self addSubview:self.encryptionBtn];
    self.encryptionBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.encryptionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(37);
        make.right.mas_equalTo(self);
        make.top.mas_equalTo(self).offset(4);
    }];
}

- (void)hideLineClearBtn{
    [self.encryptionBtn removeFromSuperview];
    [self.line removeFromSuperview];
}


- (UILabel *)textLabel{
    if (!_textLabel) {
        _textLabel = [UILabel yc_labelWithText:@"原密码" textFont:[UIFont yc_15] textColor:[UIColor yc_hex_5B6071]];
    }
    return _textLabel;
}


- (void)setName:(NSString *)name{
    [super setName:name];
    self.textLabel.text = name;
}

@end
