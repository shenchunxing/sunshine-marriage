//
//  YCTextInputView.h
//  YCBaseUI
//
//  Created by haima on 2019/4/18.
//

#import <UIKit/UIKit.h>
#import "YCTextView.h"
NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, YCTextViewClearMode) {
    YCTextViewClearModeNever,           //不显示
    YCTextViewClearModeWhileEditing,    //编辑时显示
    YCTextViewClearModeModeAlways       //一直显示
};

@protocol YCTextInputViewDelegate <UITextViewDelegate>



@end

@interface YCTextInputView : UIView

/* 输入框 */
@property (nonatomic, strong) YCTextView *textView;

/* 清除按钮显示模式 */
@property (nonatomic, assign) YCTextViewClearMode clearMode;

/* 键盘类型 默认UIReturnKeyDone */
@property(nonatomic, assign) UIReturnKeyType returnKeyType;

/* 代理 */
@property (nonatomic, weak) id<YCTextInputViewDelegate> textViewDelegate;

- (instancetype)initWitDelegate:(id<YCTextInputViewDelegate>)delegate;

- (void)changeContentWithText:(NSString *)text textColor:(UIColor *)textColor
                  placeholder:(NSString *)placeholder
             placeholderColor:(UIColor *)placehoderColor;

@property (nonatomic,copy) NSString *placeHolder;

@end

NS_ASSUME_NONNULL_END
