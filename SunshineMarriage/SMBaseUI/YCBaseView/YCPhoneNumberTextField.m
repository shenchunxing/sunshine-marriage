//
//  YCPhoneNumberTextField.m
//  YCLoginModule
//
//  Created by 沈春兴 on 2019/5/23.
//

#import "YCPhoneNumberTextField.h"
#import "NSString+STRegex.h"
#import "NSString+Extension.h"
#import "UIColor+Style.h"
#import "YCDataCenter.h"
#import "NSObject+Helper.h"
#import "UIViewController+CustomToast.h"

@interface YCPhoneNumberTextField()<UITextFieldDelegate>

@end
@implementation YCPhoneNumberTextField

- (instancetype)init{
    if (self = [super init]) {
        self.textFieldType = YCPhoneTextFieldType;
        [self addObserver:self forKeyPath:@"text" options:NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"text"]) {
        NSString *text = change[NSKeyValueChangeNewKey];
        text = [self noneSymbolString:text];
        
        self.line.backgroundColor = text.length >0 ? ([YCDataCenter isCarDealer] ? [UIColor yc_hex_397AE6]: [UIColor yc_hex_108EE9]) : [UIColor yc_hex_EEEEEE];
    }
}

- (BOOL)validPhoneNum{
    return  [[self.textField.text yc_deleteEmpty] isValidPhoneNum];
}

- (void)dealloc {
//    @try {
//         [self removeObserver:self forKeyPath:@"text" context:nil];
//        
//    } @catch (NSException *exception) {
//        NSLog(@"exception = %@",exception);
//    }
   
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (![textField.text yc_deleteEmpty].isValidPhoneNum && textField.text.length > 0) {
        [[self yc_getCurrentViewController] showToast:@"手机号码格式不正确"];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField) {
        //输入手机号进行格式化
        NSString* text = textField.text;
        //删除
        if([string isEqualToString:@" "]){
            //删除一位
            if(range.length == 1){
                //最后一位,遇到空格则多删除一次
                if (range.location == text.length-1 ) {
                    if ([text characterAtIndex:text.length-1] == '-') {
                        [textField deleteBackward];
                    }
                    return YES;
                }
                else{
                    //从中间删除
                    NSInteger offset = range.location;
                    
                    if (range.location < text.length && [text characterAtIndex:range.location] == '-' && [textField.selectedTextRange isEmpty]) {
                        [textField deleteBackward];
                        offset --;
                    }
                    [textField deleteBackward];
                    textField.text = [self parseString:textField.text];
                    UITextPosition *newPos = [textField positionFromPosition:textField.beginningOfDocument offset:offset];
                    textField.selectedTextRange = [textField textRangeFromPosition:newPos toPosition:newPos];
                    return NO;
                }
            }
            else if (range.length > 1) {
                BOOL isLast = NO;
                //如果是从最后一位开始
                if(range.location + range.length == textField.text.length ){
                    isLast = YES;
                }
                [textField deleteBackward];
                textField.text = [self parseString:textField.text];
                
                NSInteger offset = range.location;
                if (range.location == 3 || range.location  == 8) {
                    offset ++;
                }
                if (isLast) {
                    //光标直接在最后一位了
                }else{
                    UITextPosition *newPos = [textField positionFromPosition:textField.beginningOfDocument offset:offset];
                    textField.selectedTextRange = [textField textRangeFromPosition:newPos toPosition:newPos];
                }
                
                return NO;
            }
            else{
                return YES;
            }
        }
        
        else if(string.length >0){
            
            //限制输入字符个数
            if (([self noneSymbolString:textField.text].length + string.length - range.length > 11) ) {
                return NO;
            }
            //判断是否是纯数字(千杀的搜狗，百度输入法，数字键盘居然可以输入其他字符)
            if(![string isPureNumber]){
                return NO;
            }
            [textField insertText:string];
            textField.text = [self parseString:textField.text];
            
            NSInteger offset = range.location + string.length;
            if (range.location == 3 || range.location  == 8) {
                offset ++;
            }
            UITextPosition *newPos = [textField positionFromPosition:textField.beginningOfDocument offset:offset];
            textField.selectedTextRange = [textField textRangeFromPosition:newPos toPosition:newPos];
            return NO;
        }else{
            return YES;
        }
        
    }
    
    return YES;
    
}

- (NSString*)noneSymbolString:(NSString*)string {
    return [string stringByReplacingOccurrencesOfString:@" " withString:@""];
}

- (NSString*)parseString:(NSString*)string {
    if (!string) {
        return nil;
    }
    NSMutableString* mStr = [NSMutableString stringWithString:[string stringByReplacingOccurrencesOfString:@" " withString:@""]];
    if (mStr.length >3) {
        [mStr insertString:@" " atIndex:3];
    }if (mStr.length > 8) {
        [mStr insertString:@" " atIndex:8];
    }
    return  mStr;
}


@end
