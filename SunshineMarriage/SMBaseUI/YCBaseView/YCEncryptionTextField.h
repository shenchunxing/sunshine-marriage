//
//  YCEncryptionTextField.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/5/26.
//

#import "YCTextField.h"


NS_ASSUME_NONNULL_BEGIN

@interface YCEncryptionTextField : YCTextField

@property (nonatomic,strong) UIButton *encryptionBtn;

@property (nonatomic,assign) BOOL isOpenEye;

- (instancetype)initWithName:(NSString *)name showIconFont:(BOOL)showIconFont isOpenEye:(BOOL)isOpenEye;

@end

NS_ASSUME_NONNULL_END
