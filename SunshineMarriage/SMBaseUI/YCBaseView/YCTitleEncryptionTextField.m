//
//  YCTitleEncryptionTextField.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/11/22.
//

#import "YCTitleEncryptionTextField.h"
#import <Masonry/Masonry.h>

@implementation YCTitleEncryptionTextField

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.textField.secureTextEntry = YES;
        self.encryptionBtn.hidden = YES;
        [self addSubview:self.encryptionBtn];
        [self.encryptionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.mas_equalTo(40);
            make.right.mas_equalTo(self);
            make.top.mas_equalTo(self).offset(2.5);
        }];
        [self addSubview:self.textField];
        [self.textField mas_updateConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-40);
        }];
    }
    return self;
}

- (void)yc_textFiledEditChanged:(UITextField *)textField {
   self.encryptionBtn.hidden = textField.text.length <=0;
}

- (EncryptionButton *)encryptionBtn {
    if (!_encryptionBtn) {
        _encryptionBtn = [[EncryptionButton alloc] initWithShowIconFont:YES];
        _encryptionBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
        __weak typeof(self) weakSelf = self;
        [_encryptionBtn setEncryptionStatusBlock:^(BOOL isOpen) {
            weakSelf.textField.secureTextEntry = !isOpen;
        }];
    }
    return _encryptionBtn;
}

@end
