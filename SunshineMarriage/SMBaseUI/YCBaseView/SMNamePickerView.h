//
//  SMNamePickerView.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/28.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^EnterBlock)(NSString *text);

@interface SMNamePickerView : UIView

//@property (nonatomic, weak) id<SMOperationPicketViewDelegate> delegate;

@property (nonatomic, copy) EnterBlock enterBlock;
- (void)showWithAnimation:(BOOL)animation;

@end

NS_ASSUME_NONNULL_END
