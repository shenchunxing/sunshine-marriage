//
//  YCEncryptionTextField.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/5/24.
//

#import "YCVerificationCodeTextField.h"
#import "YCBaseUI.h"
#import "YCCategoryModule.h"
#import "SMDefine.h"
#import "YCDataCenter.h"
#import "YCSystemConfigure+UIColor.h"

@interface YCVerificationCodeTextField ()<UITextFieldDelegate>

@end

@implementation YCVerificationCodeTextField

- (void)setUp{
    [super setUp];
    self.textFieldType = YCSendMessageTextFieldType;
    [self addSubview:self.verificationLabel];
    [self.verificationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(100);
        make.right.mas_equalTo(self);
        make.top.mas_equalTo(25);
        make.height.mas_equalTo(24);
    }];
    
    [self.textField mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-122);
    }];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    NSLog(@"----------");
    if (self.refreshConstraints) {
        [self.verificationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(85);
            make.right.mas_equalTo(self);
            make.top.mas_equalTo(21);
            make.height.mas_equalTo(30);
        }];
    }
}

//短信验证码
- (void)getVerificationCode:(UITapGestureRecognizer *)tap{
    //先对手机号码验证
    if (self.phoneNumer.length<=0) {
        [[self yc_getCurrentViewController] showToast:@"请输入手机号码"];
        return;
    }
    if (![self.phoneNumer isValidPhoneNum]) {
        [[self yc_getCurrentViewController] showToast:@"请输入正确的手机号码"];
         return;
    }
    [[YCGCDTimer timerManager] loadTimerWithCountDownHandle:^(NSInteger countDown) {
        self.verificationLabel.text = [NSString stringWithFormat:@"%ld秒后重发",(long)countDown];
        self.verificationLabel.textColor = self.countDownColor?: [UIColor yc_hex_B3B7C2];
        self.verificationLabel.backgroundColor = self.countDownBgColor?:self.verificationLabel.backgroundColor;
        self.verificationLabel.userInteractionEnabled = NO;
    } finishHandle:^{
        self.verificationLabel.userInteractionEnabled = YES;
        self.verificationLabel.text = @"重新发送";
        self.verificationLabel.textColor = [YCSystemConfigure mainColor];
    }];
    //发送网络请求
    if (self.sendMessageHandle) {
        self.sendMessageHandle();
    }
}

- (VerificationLabel *)verificationLabel{
    if (!_verificationLabel) {
        _verificationLabel = (VerificationLabel *)[VerificationLabel yc_labelWithText:@"获取验证码" textFont:[UIFont yc_17] textColor:[YCSystemConfigure mainColor] textAlignment:NSTextAlignmentRight];
        _verificationLabel.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(getVerificationCode:)];
        [_verificationLabel addGestureRecognizer:tap];
    }
    return _verificationLabel;
}


@end

@implementation VerificationLabel



@end
