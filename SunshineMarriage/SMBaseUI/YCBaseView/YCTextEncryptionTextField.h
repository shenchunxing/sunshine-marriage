//
//  YCTextEncryptionTextField.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/5/26.
//

#import "YCEncryptionTextField.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCTextEncryptionTextField : YCEncryptionTextField

@property (nonatomic,assign) BOOL hideLine;
@property (nonatomic, assign) CGFloat inputPosition;

- (void)hideLineClearBtn;

@end

NS_ASSUME_NONNULL_END
