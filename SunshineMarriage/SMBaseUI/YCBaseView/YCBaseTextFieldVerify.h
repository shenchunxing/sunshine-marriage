//
//  YCBaseTextFieldVerify.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/12/17.
//

#import <Foundation/Foundation.h>
@class YCBaseTextField;
NS_ASSUME_NONNULL_BEGIN

@interface YCBaseTextFieldVerify : NSObject
+ (BOOL)verifyTextfields:(NSArray <YCBaseTextField *>*)textfields;
@end

NS_ASSUME_NONNULL_END
