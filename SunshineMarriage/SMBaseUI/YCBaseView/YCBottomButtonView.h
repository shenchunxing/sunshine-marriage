//
//  YCBottomButtonView.h
//  YCBaseUI
//
//  Created by haima on 2019/4/8.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, YCBottomButtonColorType) {
    YCBottomButtonColorTypeBlue = 0,        //蓝色底，白字
    YCBottomButtonColorTypeWathetBlue,  //浅蓝色底，白字
    YCBottomButtonColorTypeGrayBlue , //灰底，蓝字
    YCBottomButtonColorTypeYellowWhite , //黄底，白字
    YCBottomButtonColorTypeGreen,        //绿底，白色
    
    //UI 更改后的效果
    YCBottomButtonColor_03B2F3,//浅蓝色底，白字
    YCBottomButtonColor_FF3B30,//红底，白字
};

typedef NS_ENUM(NSUInteger,YCBottomButtonLayout){
    YCBottomButtonViewLayoutSingle,//单个
    YCBottomButtonViewLayoutAverage,//平均长度,没有间距
    YCBottomButtonViewLayoutAverageHasSpace,//平均长度,有间距
    YCBottomButtonViewLayoutTwoNoAverage,//两个按钮但是不平均
    YCBottomButtonViewLayoutUpDown//上下布局
} ;

@interface YCBottomButtonTransaction : NSObject

/* 标题 */
@property (nonatomic, copy) NSString *title;
/* 颜色 */
@property (nonatomic, assign) YCBottomButtonColorType backgroundColorType;
/* 是否需要分割 */
@property (nonatomic, assign) BOOL hasSeparatorLines;
/* 默认YES可点击 */
@property (nonatomic, assign) BOOL isActive;
/* 点击回调 */
@property (nonatomic, copy) void(^bottomButtonClick)(void);
/* 是否需要描边 默认NO */
@property (nonatomic, assign) BOOL isBorder;



+ (instancetype)transaction;



@end

NS_ASSUME_NONNULL_BEGIN



@interface YCBottomButtonView : UIView

@property (nonatomic,assign) YCBottomButtonLayout buttonLayOut;

@property (nonatomic,assign) CGFloat totalWidth;

/**
 快捷创建
 @param titles 标题数组
 @param backgroundColorTypes 枚举数组
 @param clickHandle 回调
 */
- (instancetype)initWithTitles:(NSArray <NSString *>*)titles
          backgroundColorTypes:(NSArray *)backgroundColorTypes clickHandle:(void(^)(NSUInteger))clickHandle;

@property (nonatomic, strong, readonly) NSArray<YCBottomButtonTransaction *> *buttonItems;

/**
 底部左侧按钮列表
 @param items 按钮列表
 */
- (void)configBottomViewWithItems:(NSArray<YCBottomButtonTransaction *> *)items;

//默认形式，只不过不是平均分配的
- (void)configBottomViewWithTwoItems:(NSArray<YCBottomButtonTransaction *> *)items;

/**
 带圆角的button，平均等份
 */
- (void)configBottomViewWithCornerItems:(NSArray<YCBottomButtonTransaction *> *)items;
//上下布局
- (void)configBottomViewWithUpDownItems:(NSArray<YCBottomButtonTransaction *> *)items;



@end

NS_ASSUME_NONNULL_END
