//
//  YCCreditPersonSelectView.m
//  YCCreditModule
//
//  Created by 刘成 on 2019/4/18.
//

#import "YCCreditPersonSelectView.h"
#import "YCBaseUI.h"
#import "YCCategoryModule.h"
#import "SMDefine.h"

@interface YCCreditPersonSelectView ()
{
    YCCreditPersonSelectType _type;
    
    NSArray *_dataArr;
}
@property (copy, nonatomic) YCCreditPersonSelectHandle selectHandle;

@property (strong, nonatomic) UIView *popView;

@property (strong, nonatomic) UIView *backView;

@property (nonatomic, copy) NSArray *addPersons;


@end

@implementation YCCreditPersonSelectView

- (instancetype)initWithSelectHandle:(YCCreditPersonSelectHandle)selectHandle type:(YCCreditPersonSelectType)type iconType:(YCCreditPersonSelectIconType)iconType{
    self.iconType = iconType;
    return [self initWithSelectHandle:selectHandle type:type];
}

- (instancetype)initWithSelectHandle:(YCCreditPersonSelectHandle)selectHandle{
    
    return [self initWithSelectHandle:selectHandle type:YCCreditPersonSelectTypeCommon];
}

- (instancetype)initWithSelectHandle:(YCCreditPersonSelectHandle)selectHandle type:(YCCreditPersonSelectType)type{

    if (self = [super initWithFrame:CGRectMake(0, 0, kWIDTH, kScreenHEIGHT)]) {
        _type = type;
        _selectHandle = selectHandle;
        [self createUI];
    }
    return self;
}


- (void)createUI{
    
    UIView *backView = [[UIView alloc] initWithFrame:self.bounds];
    backView.backgroundColor = [UIColor blackColor];
    backView.alpha = 0.6;
    [self addSubview:backView];
    _backView = backView;

    UIView *popView = [[UIView alloc] initWithFrame:CGRectMake(0, kScreenHEIGHT, kWIDTH, 442)];
    popView.backgroundColor = [UIColor yc_hex_F4F4F4];
    [self addSubview:popView];
    self.popView = popView;
    
    UILabel *titleLabel = [UILabel labelWithFrame:CGRectMake(0, 0, VW(popView), 54) text:[self getTitle] textFont:[UIFont yc_16] textColor:[UIColor yc_hex_3C3D49] textAlignment:NSTextAlignmentCenter];
    titleLabel.backgroundColor = [UIColor whiteColor];
    [popView addSubview:titleLabel];
    
    UIButton *closeBtn = [UIButton customWithFrame:CGRectMake(VW(popView)-24-14, 0, 24, 24) image:nil target:self action:@selector(hide)];
    closeBtn.centerY = titleLabel.centerY;
    [closeBtn setImage:[self deleteImage] forState:UIControlStateNormal];
    [popView addSubview:closeBtn];
    
    UIScrollView *scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 16+FH(titleLabel), VW(popView), VH(popView)-70)];
    scroll.backgroundColor = [UIColor clearColor];
    [popView addSubview:scroll];
   
    
    NSArray *arr = [self getDataArr];
    _dataArr = arr;
    
    for (int i=0; i<arr.count; i++) {
        UIButton *btn = [UIButton customWithFrame:CGRectMake(12, i*92, VW(scroll)-24, 80) bgImage:nil target:self action:@selector(btnClick:)];
        btn.layer.cornerRadius = 2;
        btn.layer.masksToBounds = YES;
        btn.tag = 100+i;
        [btn setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage imageWithColor:[UIColor yc_hex_F4F4F4]] forState:UIControlStateHighlighted];
        [scroll addSubview:btn];
        
        
        UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(16, 0, 28, 28)];
        icon.image = [self iconImageWithIndex:i];
        icon.centerY = VH(btn)/2.0;
        [btn addSubview:icon];
        
        UILabel *label = [UILabel labelWithFrame:CGRectMake(FW(icon)+12, 16, 200, 24) text:arr[i][0] textFont:[UIFont yc_17] textColor:[UIColor yc_hex_3C3D49]];
        label.centerY = VH(btn)/2.0;
        [btn addSubview:label];
        
//        UILabel *label2 = [UILabel labelWithFrame:CGRectMake(FW(icon)+12, FH(label)+4, 300, 20) text:arr[i][1] textFont:[UIFont yc_14] textColor:[UIColor yc_hex_9B9EA8]];
//        [btn addSubview:label2];
//        
//        if (label2.text.length==0) {
//            label2.hidden = YES;
//            label.centerY = icon.centerY;
//        }
        
        if (i==arr.count-1) {
            scroll.contentSize = CGSizeMake(VW(scroll), FH(btn)+10);
        }
    }
    
}

- (UIImage *)deleteImage {
    return [UIImage yc_resourceModuleImgWithName:@"img_dibu_cancel"];
}

- (UIImage *)iconImageWithIndex:(NSUInteger)index {
  if (self.iconType == YCCreditPersonSelectIconTypeBS) {
        return nil;
    }
    return [UIImage yc_imgWithName:[NSString stringWithFormat:@"icon_credit_list_%@",[self getDataArr][index][2]] bundle:@"YCCreditModule"];
}

- (NSString *)getTitle{
    switch (_type) {
        case YCCreditPersonSelectTypeCommon:
        case YCCreditPersonSelectTypeCommonWithOutCommonLender:
//        case YCCreditPersonSelectTypeHaerbinHave:
        case YCCreditPersonSelectTypeHaerbinNone:
        case YCCreditPersonSelectTypeHaerbinNoneWithOutCommonLender:

            return @"选择与主贷人的关系";
            break;
            
        case YCCreditPersonSelectTypeLoan:
        case YCCreditPersonSelectTypeLoanHaerbin:
            return @"请选择角色";
            break;
            
        default:
            return @"";
            break;
    }
}

- (NSArray *)getDataArr{
    switch (_type) {
        case YCCreditPersonSelectTypeCommon:
            return @[@[@"共贷人",@"共贷人为配偶",@"2"],
                     @[@"公司担保人",@"公司担保人不可为主贷人的配偶",@"3"],
                     @[@"银行担保人",@"银行担保人不可为主贷人的配偶",@"4"],
                     @[@"特殊关联人",@"特殊关联人不可为主贷人的配偶",@"5"],
                     ];
            break;
        case YCCreditPersonSelectTypeCommonWithOutCommonLender:
            return @[
                     @[@"公司担保人",@"公司担保人不可为主贷人的配偶",@"3"],
                     @[@"银行担保人",@"银行担保人不可为主贷人的配偶",@"4"],
                     @[@"特殊关联人",@"特殊关联人不可为主贷人的配偶",@"5"],
                     ];
            break;
        case YCCreditPersonSelectTypeHaerbinNone:
            return @[@[@"共贷人",@"共贷人为配偶",@"2"],
                     @[@"公司担保人",@"公司担保人不可为主贷人的配偶",@"3"],
                     @[@"银行担保人",@"银行担保人不可为主贷人的配偶",@"4"],
                     @[@"特殊关联人",@"特殊关联人不可为主贷人的配偶",@"5"],
                     ];
            break;
        case YCCreditPersonSelectTypeHaerbinNoneWithOutCommonLender:
            return @[
                     @[@"公司担保人",@"公司担保人不可为主贷人的配偶",@"3"],
                     @[@"银行担保人",@"银行担保人不可为主贷人的配偶",@"4"],
                     @[@"特殊关联人",@"特殊关联人不可为主贷人的配偶",@"5"],
                     ];
            break;
//        case YCCreditPersonSelectTypeHaerbinHave:
//            return @[@[@"共贷人",@"共贷人为配偶",@"2"],
//                     @[@"公司担保人",@"公司担保人不可为主贷人的配偶",@"3"],
//                     @[@"银行担保人",@"银行担保人不可为主贷人的配偶",@"4"],
//                     @[@"特殊关联人",@"特殊关联人不可为主贷人的配偶",@"5"],
//                     ];
//            break;
//        case YCCreditPersonSelectTypeHaerbinHaveWithOutCommonLender:
//            return @[
//                     @[@"公司担保人",@"公司担保人不可为主贷人的配偶",@"3"],
//                     @[@"银行担保人",@"银行担保人不可为主贷人的配偶",@"4"],
//                     @[@"特殊关联人",@"特殊关联人不可为主贷人的配偶",@"5"],
//                     ];
//            break;
        case YCCreditPersonSelectTypeLoan:
            return @[@[@"主贷人",@"有且仅有1人作为主贷人",@"1"],
                     @[@"共贷人",@"有且仅有1人作为共贷人",@"2"],
                     @[@"公司担保人",@"",@"3"],
                     @[@"银行担保人",@"",@"4"],
                     @[@"特殊关联人",@"",@"5"],
                     ];
            break;
        case YCCreditPersonSelectTypeLoanHaerbin:
            return @[@[@"主贷人",@"有且仅有1人作为主贷人",@"1"],
                     @[@"共贷人",@"有且仅有1人作为共贷人",@"2"],
                     @[@"公司担保人",@"",@"3"],
                     @[@"银行担保人",@"",@"4"],
                     @[@"特殊关联人",@"",@"5"],
                     ];
            break;
        default:
            break;
    }
}

- (void)btnClick:(UIButton *)btn{
    NSInteger tag = btn.tag-100;
    
    NSArray *arr = _dataArr[tag];
    NSString *custType = [arr lastObject];
    
    if (_selectHandle) {
        _selectHandle(custType);
    }
    
    [self hide];
}

- (void)show{
    
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [window addSubview:self];
    
    _backView.alpha = 0;
    [UIView animateWithDuration:0.3 animations:^{
        self.popView.frame = CGRectMake(0, kScreenHEIGHT-442, kWIDTH, 442);
        self.backView.alpha = 0.6;
    }];
    
}

- (void)hide{
    
    [UIView animateWithDuration:0.3 animations:^{
        self.backView.alpha = 0.0;
        self.popView.frame = CGRectMake(0, kScreenHEIGHT, kWIDTH, 442);

    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (_backView.alpha >= 0.6) {
        [self hide];
    }
}

@end
