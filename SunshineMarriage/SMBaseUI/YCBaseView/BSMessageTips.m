//
//  YCVehicleValuationTipsView.m
//  YCBusinessManagementModule
//
//  Created by 沈春兴 on 2019/4/22.
//

#import "BSMessageTips.h"
#import "YCCategoryModule.h"
#import "YCBaseUI.h"

@interface BSMessageTips ()
@property (nonatomic, strong) UILabel *noticeLabel;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, strong) BSMessageTipsStyle *style;
@end
@implementation BSMessageTips

- (instancetype)initWithTipsContent:(NSString *)content style:(BSMessageTipsStyle *)style {
    if (self = [super init]) {
        self.content = content;
        self.style = style;
        [self setUp];
    }
    return self;
}

- (instancetype)init {
    return [self initWithTipsContent:@"88条+业务通知" style:[BSMessageTipsStyle defaultStyle]] ;
}

- (void)setUp {
      [self yc_radiusWithRadius:2];
           
       //背景黑色
       UIView *back = [[UIView alloc] init];
       back.backgroundColor = [UIColor blackColor];
       back.alpha = 0.85;
       [back yc_radiusWithRadius:2];
       [self addSubview:back];
       [back mas_makeConstraints:^(MASConstraintMaker *make) {
           make.left.top.right.mas_equalTo(self);
           make.height.mas_equalTo(36);
       }];
       
       //显示内容
       UILabel *label = [UILabel yc_labelWithText:self.content textFont:[UIFont yc_13] textColor:[UIColor whiteColor]];
       [self addSubview:label];
       [label mas_makeConstraints:^(MASConstraintMaker *make) {
           make.top.mas_equalTo(self.style.topSpace);
           make.left.mas_equalTo(self.style.leftSpace);
       }];
       self.noticeLabel = label;
       
       //删除按钮
       UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
       [btn setImage:[UIImage yc_resourceModuleImgWithName:@"img_dibu_cancel_baise"] forState:UIControlStateNormal];
       [btn addTarget:self action:@selector(delete) forControlEvents:UIControlEventTouchUpInside];
       [self addSubview:btn];
       [btn mas_makeConstraints:^(MASConstraintMaker *make) {
           make.top.mas_equalTo(2);
           make.height.width.equalTo(@32);
           make.right.mas_equalTo(-2);
       }];
       
       //三角
       UIImageView *arrow = [[UIImageView alloc] init];
       arrow.image = [UIImage yc_resourceModuleImgWithName:@"img_sanjiao_bottom"];
       [self addSubview:arrow];
       [arrow mas_makeConstraints:^(MASConstraintMaker *make) {
           make.top.mas_equalTo(36);
           make.centerX.mas_equalTo(self);
           make.width.mas_equalTo(13);
           make.height.mas_equalTo(6);
       }];
       
       [self addTarget:self action:@selector(delete) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self updateFrame];
}

- (void)updateFrame {
    CGFloat width = [self.noticeLabel.text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 13) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont yc_13]} context:nil].size.width+self.style.leftSpace+32;
    CGRect frame = self.frame;
     frame.size.width = width;
    frame.size.height = self.style.height;
    self.frame = frame;
}

- (void)setNoticesNum:(NSUInteger)noticesNum {
    _noticesNum = noticesNum;
    NSString *text = [NSString stringWithFormat:@"%ld条业务通知",(long)noticesNum > 99 ? 99:noticesNum];
    self.noticeLabel.text = text;
    [self  updateFrame];
    //更新位置
    self.center = CGPointMake(self.superview.width* 5/8 , self.superview.center.y - self.style.height);
}


- (void)delete{
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0;
    }completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end

@implementation BSMessageTipsStyle

+ (BSMessageTipsStyle *)defaultStyle {
    BSMessageTipsStyle *style = [[BSMessageTipsStyle alloc] init];
    style.topSpace = 9;
    style.leftSpace = 12;
    style.height = 51;
    return style;
}

@end
