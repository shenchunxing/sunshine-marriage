//
//  SMNamePickerView.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/28.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMNamePickerView.h"
#import "SMDefine.h"
#import <Masonry/Masonry.h>
#import "YCCategoryModule.h"
#import "UIViewController+CustomToast.h"

@interface SMNamePickerView ()<UITextViewDelegate>
@property (nonatomic, strong) UIButton *cancleButton;
@property (nonatomic, strong) UIButton *sureButton;
@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, strong) UIView *vLine;
@property (nonatomic, strong) UIView *hLine;
@property (nonatomic, strong) UIView *nameLine;
@property (nonatomic, strong) UITextField *textField;

@end

@implementation SMNamePickerView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initViews];
    }
    return self;
}

- (void)initViews {
    [self addSubview:self.backgroundView];
    [self addSubview:self.containerView];
    
    [self.containerView addSubview:self.textField];
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@28.5);
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.equalTo(@41);
    }];
    
    [self.containerView addSubview:self.nameLine];
    [self.nameLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textField.mas_bottom);
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.equalTo(@1);
    }];
    
    [self.containerView addSubview:self.hLine];
    [self.hLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@100);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.equalTo(@0.5);
    }];
    
    [self.containerView addSubview:self.cancleButton];
    [self.cancleButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.hLine.mas_bottom);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(self.containerView).multipliedBy(0.5);
        make.height.equalTo(@44.5);
    }];
    
    [self.containerView addSubview:self.vLine];
    [self.vLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.hLine.mas_bottom);
        make.left.mas_equalTo(self.containerView.width/2);
        make.width.mas_equalTo(@0.5);
        make.height.equalTo(@44.5);
    }];
    
    [self.containerView addSubview:self.sureButton];
    [self.sureButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.hLine.mas_bottom);
        make.left.mas_equalTo(self.containerView.width/2);
        make.right.mas_equalTo(self.containerView);
        make.height.equalTo(@44.5);
    }];
    
    self.frame = [UIScreen mainScreen].bounds;
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    [keyWindow addSubview:self];
}


#pragma mark - 弹出视图方法
- (void)showWithAnimation:(BOOL)animation {
    self.containerView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.05, 1.05);
    self.backgroundView.alpha = 0;
    [UIView animateWithDuration:0.3 animations:^{
        self.containerView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
        self.backgroundView.alpha = 0.6;

    }] ;
}

#pragma mark - 关闭视图方法
- (void)dismissWithAnimation:(BOOL)animation {
    [UIView animateWithDuration:0.3 animations:^{
        self.backgroundView.alpha = 0.0;
        self.containerView.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - overwrite
//取消
- (void)cancelPickerView {
    [self dismissWithAnimation:YES];
}

//确定
- (void)confirmPickerView {
    
//    if (self.dataSource) {
//        self.selectedOption = self.dataSource[self.index];
//    }
//    if (self.delegate && [self.delegate respondsToSelector:@selector(optionPickerView:atIndex:option:)]) {
//        [self.delegate optionPickerView:self atIndex:self.index option:self.selectedOption];
//    }
    if (self.enterBlock) {
        self.enterBlock(self.textField.text);
    }
    
    [self dismissWithAnimation:YES];
}

//点击背景图层
- (void)didTapBackgroundView:(UIGestureRecognizer *)gesture {
    [self dismissWithAnimation:YES];
}

- (void)textFiledEditChanged:(UITextField *)textField{
    self.nameLine.backgroundColor =  textField.text.length>0 ? [UIColor yc_hex_5360FF]:[UIColor yc_hex_CBCFF7];
    self.sureButton.userInteractionEnabled = textField.text.length>0 ;
    [self.sureButton setTitleColor:textField.text.length>0 ? [UIColor yc_hex_5360FF]:[UIColor yc_hex_CACCD4] forState:UIControlStateNormal];
    
   NSString *toBeString = textField.text;
   UITextRange *selectedRange = [textField markedTextRange];
   UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
   // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
   if (!position){
       if (toBeString.length > 14){
           NSRange rangeIndex = [toBeString rangeOfComposedCharacterSequenceAtIndex:14];
           if (rangeIndex.length == 1){
               textField.text = [toBeString substringToIndex:14];
           }
           else{
               NSRange rangeRange = [toBeString rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, 14)];
               textField.text = [toBeString substringWithRange:rangeRange];
           }
       }
   }
}

- (UIButton *)cancleButton {
    if (!_cancleButton) {
        _cancleButton = [UIButton yc_customTextWithTitle:@"取消" titleColor:[UIColor yc_hex_333333] fontSize:[UIFont yc_16] target:self action:@selector(cancelPickerView)];
    }
    return _cancleButton;
}

- (UIButton *)sureButton {
    if (!_sureButton) {
        _sureButton = [UIButton yc_customTextWithTitle:@"确定" titleColor:[UIColor yc_hex_CACCD4] fontSize:[UIFont yc_16] target:self action:@selector(confirmPickerView)];
        _sureButton.userInteractionEnabled = NO;
    }
    return _sureButton;
}

#pragma mark - getter
- (UIView *)backgroundView {
    if (_backgroundView == nil) {
        _backgroundView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _backgroundView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5f];
        _backgroundView.userInteractionEnabled = YES;
        UITapGestureRecognizer *myTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didTapBackgroundView:)];
        [_backgroundView addGestureRecognizer:myTap];
    }
    return _backgroundView;
}

- (UIView *)containerView {
    if (_containerView == nil) {
        _containerView = [[UIView alloc] initWithFrame:CGRectMake(52.5, (kScreenHEIGHT - 400) /2, kWIDTH - 105, 145)];
        _containerView.backgroundColor = [UIColor whiteColor];
        [_containerView yc_radiusWithRadius:5];
    }
    return _containerView;
}

- (UITextField *)textField {
    if (!_textField) {
        _textField = [[UITextField alloc] init];
        _textField.delegate = self;
        _textField.placeholder = @"新的昵称";
        _textField.font = [UIFont yc_17];
        _textField.textColor = [UIColor yc_hex_333333];
        [_textField becomeFirstResponder];
        [_textField addTarget:self action:@selector(textFiledEditChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return _textField;
}

- (UIView *)vLine {
    if (!_vLine) {
        _vLine = [[UIView alloc] init];
        _vLine.backgroundColor = [UIColor yc_hex_CCCCCC];
    }
    return _vLine;
}

- (UIView *)hLine {
    if (!_hLine) {
        _hLine = [[UIView alloc] init];
        _hLine.backgroundColor = [UIColor yc_hex_CCCCCC];
    }
    return _hLine;
}

- (UIView *)nameLine {
    if (!_nameLine) {
        _nameLine = [[UIView alloc] init];
        _nameLine.backgroundColor = [UIColor yc_hex_CBCFF7];
    }
    return _nameLine;
}

@end
