//
//  TextChangeListener.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/11/29.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "YCBaseTextField.h"
NS_ASSUME_NONNULL_BEGIN
@interface TextChangeListener : NSObject<YCBaseTextFieldListenerDelegate>
@property (nonatomic, strong) UIView *listenerView;
@property (nonatomic, copy) NSArray  *editTextFields;
- (instancetype)initWithListenerView:(UIView *)listenerView;
- (void)addEditTextFields:(NSArray *)editTextFields;
- (void)start;
@end

NS_ASSUME_NONNULL_END
