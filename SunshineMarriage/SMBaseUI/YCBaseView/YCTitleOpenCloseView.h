//
//  YCTitleView.h
//  YCBaseUI
//
//  Created by haima on 2019/4/3.
//

#import <UIKit/UIKit.h>

extern CGFloat const kYCTitleViewHeight;

typedef NS_ENUM(NSInteger,YCTitleOpenCloseType){
    YCTitleOpenType = 0,//展开
    YCTitleCloseType//闭合
};

@interface YCTitleOpenCloseView : UIView

@property (nonatomic,copy) void(^rightBtnBlock)(YCTitleOpenCloseView *view);

@property (nonatomic,assign) YCTitleOpenCloseType statusType;
- (instancetype)initWithTitle:(NSString *)title ;
@property (nonatomic,copy) NSString *title;

@end

