//
//  TextChangeListener.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/11/29.
//

#import "TextChangeListener.h"
#import "YCBaseTextField.h"
#import "UIColor+Style.h"

@interface TextChangeListener ()

@end
@implementation TextChangeListener 

- (instancetype)initWithListenerView:(UIView *)listenerView {
   if (self = [super init]) {
        self.listenerView = listenerView;
    }
    return self;
}

- (void)start {
    NSLog(@"开始监听--------");
}

- (void)addEditTextFields:(NSArray *)editTextFields {
    self.editTextFields = editTextFields;
    for (YCBaseTextField *baseTextField in editTextFields) {
        baseTextField.listenerDelegate = self;
    }
}

- (BOOL)checkAllEdit {
    for (id textField in self.editTextFields) {
        if ([textField isKindOfClass:[YCBaseTextField class]]) {
            YCBaseTextField *bsTextField = (YCBaseTextField *)textField;
            if (bsTextField.text.length <= 0)  return NO;
        }
    }
    return YES;
}

- (void)listenerTextFiledEditChanged:(UITextField *)textField {
    self.listenerView.userInteractionEnabled = [self checkAllEdit];
    self.listenerView.backgroundColor = [self checkAllEdit] ? [UIColor yc_hex_F8513B]: [UIColor yc_hex_CACCD4];
}

@end
