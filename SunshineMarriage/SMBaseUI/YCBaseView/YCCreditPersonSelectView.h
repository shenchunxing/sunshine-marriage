//
//  YCCreditPersonSelectView.h
//  YCCreditModule
//
//  Created by 刘成 on 2019/4/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^YCCreditPersonSelectHandle)(NSString * cusType);

typedef NS_ENUM(NSInteger, YCCreditPersonSelectType) {
    YCCreditPersonSelectTypeCommon = 0, //通用
    YCCreditPersonSelectTypeCommonWithOutCommonLender, //通用没有共贷人
    YCCreditPersonSelectTypeHaerbinNone,  //哈尔滨没有主担保人时
    YCCreditPersonSelectTypeHaerbinNoneWithOutCommonLender,  //哈尔滨没有主担保人时
//    YCCreditPersonSelectTypeHaerbinHave,  //哈尔滨有担保人时
//    YCCreditPersonSelectTypeHaerbinHaveWithOutCommonLender,  //哈尔滨有担保人时
    YCCreditPersonSelectTypeLoan,
    YCCreditPersonSelectTypeLoanHaerbin,
};

typedef NS_ENUM(NSInteger,YCCreditPersonSelectIconType) {
//    YCCreditPersonSelectIconTypeDefault,
    YCCreditPersonSelectIconTypeBS,
};

@interface YCCreditPersonSelectView : UIView
@property (nonatomic, assign) YCCreditPersonSelectIconType iconType;
- (instancetype)initWithSelectHandle:(YCCreditPersonSelectHandle)selectHandle type:(YCCreditPersonSelectType)type iconType:(YCCreditPersonSelectIconType)iconType;
-(instancetype)initWithSelectHandle:(YCCreditPersonSelectHandle)selectHandle;
- (instancetype)initWithSelectHandle:(YCCreditPersonSelectHandle)selectHandle type:(YCCreditPersonSelectType)type;
- (void)show;

- (instancetype)WithSelectHandle:(YCCreditPersonSelectHandle)selectHandle addPersons:(NSArray *)persons;

@end

NS_ASSUME_NONNULL_END
