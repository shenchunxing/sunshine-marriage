//
//  YCTitleVerificateCodeTextField.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/11/22.
//

#import "YCImageVerificateCodeTextField.h"
#import <Masonry/Masonry.h>
#import "YCCategoryModule.h"

@implementation YCImageVerificateCodeTextField

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.verificateLabel];
        [self.verificateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self);
            make.top.mas_equalTo(21);
            make.width.mas_equalTo(85);
            make.height.mas_equalTo(30);
        }];
        [self addSubview:self.textField];
        [self.textField mas_updateConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-85);
        }];
    }
    return self;
}

- (void)setListenTextField:(YCBaseTextField *)listenTextField {
     self.verificateLabel.titleTextField = listenTextField;
}

- (VerificatieCodeLabel *)verificateLabel {
    if (!_verificateLabel) {
        _verificateLabel = [[VerificatieCodeLabel alloc] init];
        _verificateLabel.text = @"发送验证码";
        _verificateLabel.font = [UIFont yc_13];
        _verificateLabel.layer.cornerRadius = 15;
        _verificateLabel.layer.borderWidth = 1;
        _verificateLabel.layer.borderColor = [UIColor yc_hex_CCCCCC].CGColor;
        _verificateLabel.textAlignment = NSTextAlignmentCenter;
        _verificateLabel.textColor = [UIColor yc_hex_333440];
        _verificateLabel.backgroundColor = [UIColor whiteColor];
        _verificateLabel.layer.masksToBounds = YES;
        _verificateLabel.countDownColor = [UIColor yc_hex_5B6071];
        _verificateLabel.countDownBgColor = [UIColor yc_hex_F5F6FA];
    }
    return _verificateLabel;
}

@end
