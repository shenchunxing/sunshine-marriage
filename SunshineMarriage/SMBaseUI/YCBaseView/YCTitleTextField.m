//
//  YCTextField.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/5/24.
//

#import "YCTitleTextField.h"
#import <Masonry/Masonry.h>
#import "YCCategoryModule.h"

@interface YCTitleTextField ()
@property (nonatomic, strong) UILabel *leftLabel;
@end

@implementation YCTitleTextField

- (void)addLeftView {
    [self addSubview:self.leftLabel];
    [self.leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(14);
        make.top.mas_equalTo(12);
        make.width.mas_equalTo(90);
    }];
    self.leftView = self.leftLabel;
    
    [self addSubview:self.textField];
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.leftLabel.mas_right).offset(12);
        make.top.mas_equalTo(12);
        make.height.mas_equalTo(21);
        make.right.mas_equalTo(self).mas_offset(-14);
    }];
}

- (UILabel *)leftLabel {
    if (!_leftLabel) {
        _leftLabel = [UILabel yc_labelWithText:@"短信验证码" textFont:[UIFont yc_15] textColor:[UIColor yc_hex_5B6071]];
    }
    return _leftLabel;
}

- (void)setName:(NSString *)name {
    _name = name;
    self.leftLabel.text = name;
}

@end
