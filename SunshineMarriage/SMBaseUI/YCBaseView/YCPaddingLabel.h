//
//  YCPaddingLabel.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/4/13.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YCPaddingLabel : UILabel

@property (nonatomic, assign) UIEdgeInsets textInsets;
- (CGSize)intrinsicContentSize;

@end

NS_ASSUME_NONNULL_END
