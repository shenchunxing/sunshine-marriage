//
//  YCTextView.m
//  YCBaseUI
//
//  Created by haima on 2019/3/29.
//

#import "YCTextView.h"
#import "UIFont+Style.h"
#import "UIColor+Style.h"
#import <Masonry/Masonry.h>
#import "SMDefine.h"
#import "YCCategoryModule.h"

@interface YCTextView ()

@end

@implementation YCTextView

- (instancetype)init {
    
    if (self = [super init]) {
        self.scrollEnabled = NO;
        [self addSubview:self.placeholderLabel];
        [self.placeholderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.leading.offset(2);
        }];
        //去除文本边距，上下文字对齐问题
        self.textContainer.lineFragmentPadding = 0;
        self.textContainerInset = UIEdgeInsetsZero;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didTextChange) name:UITextViewTextDidChangeNotification object:nil];
    }
    return self;
}

- (void)rightAligment{
    
}


- (void)didTextChange {
    
    self.placeholderLabel.hidden = self.hasText;
}

- (void)layoutSubviews {

    [super layoutSubviews];

//    [self.placeholderLabel sizeToFit];
    //靠右对齐
    if (self.rightAlignment) {
        self.textAlignment = NSTextAlignmentRight;
        self.placeholderLabel.textAlignment = NSTextAlignmentRight;
        if (self.isFirstResponder) {
            [self.placeholderLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(self.width-self.rightOffset);
                make.top.mas_equalTo(self);
            }];
        }else{
            [self.placeholderLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(self.width);
                make.top.mas_equalTo(self);
            }];
        }
    }
}

- (void)setPlaceholder:(NSString *)placeholder {
    
    self.placeholderLabel.text = placeholder;
}

- (void)setPlaceholderColor:(UIColor *)placeholderColor {
    
    self.placeholderLabel.textColor = placeholderColor;
}

#pragma mark - getter
- (UILabel *)placeholderLabel {
    
    if (_placeholderLabel == nil) {
        _placeholderLabel = [[UILabel alloc] init];
        _placeholderLabel.textColor = [UIColor yc_hex_B3B7C2];
        _placeholderLabel.font = [UIFont yc_major_font];
        _placeholderLabel.textAlignment = NSTextAlignmentLeft;
        _placeholderLabel.numberOfLines = 0;
    }
    return _placeholderLabel;
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
