//
//  BSMessageTips.h
//  YCBSMixingModule
//
//  Created by 沈春兴 on 2019/11/27.
//

#import <UIKit/UIKit.h>
@class BSMessageTipsStyle;

NS_ASSUME_NONNULL_BEGIN

@interface BSMessageTips : UIButton
@property (nonatomic, assign) NSUInteger noticesNum;

- (instancetype)initWithTipsContent:(NSString *)content style:(BSMessageTipsStyle *)style;
@end

@interface BSMessageTipsStyle : NSObject
@property (nonatomic, assign) CGFloat topSpace;
@property (nonatomic, assign) CGFloat leftSpace;
@property (nonatomic, assign) CGFloat height;

+ (BSMessageTipsStyle *)defaultStyle;

@end

NS_ASSUME_NONNULL_END
