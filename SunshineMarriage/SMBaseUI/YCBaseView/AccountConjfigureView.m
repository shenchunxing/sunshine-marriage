//
//  AccountConjfigureView.m
//  YCBSMixingModule
//
//  Created by 沈春兴 on 2019/11/25.
//

#import "AccountConjfigureView.h"
#import "YCCategoryModule.h"
#import "SMDefine.h"
#import <Masonry/Masonry.h>

@implementation AccountConjfigureView

- (void)setItems:(NSArray *)items {
    _items = items;
    [items enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        static UILabel *recrodLabel =nil;
        UILabel *label = [UILabel yc_labelWithText:obj textFont:[UIFont yc_12] textColor:[UIColor yc_hex_477CBD]];
        label.layer.borderWidth = 0.5;
        label.layer.borderColor = [UIColor yc_hex_477CBD].CGColor;
        label.textAlignment = NSTextAlignmentCenter;
        label.layer.cornerRadius = 12;
        label.layer.masksToBounds  = YES;
        
        CGRect rect = [obj boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width -28, 16) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont yc_12]} context:nil];
         CGFloat W = rect.size.width + 20;
         CGFloat H = rect.size.height + 8;
         [self addSubview:label];
        
         if (idx == 0) {
             label.frame =CGRectMake(14, 12, W, H);
         }
         else{
             CGFloat yuWidth = self.frame.size.width - 28 -recrodLabel.frame.origin.x -recrodLabel.frame.size.width;
             if (yuWidth >= rect.size.width) {
                 label.frame =CGRectMake(recrodLabel.frame.origin.x +recrodLabel.frame.size.width + 8, recrodLabel.frame.origin.y, W, H);
             }else{
                 label.frame =CGRectMake(14, recrodLabel.frame.origin.y+recrodLabel.frame.size.height+8, W, H);
             }
         }
         self.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width - 28,CGRectGetMaxY(label.frame)+12);
         recrodLabel = label;
    }];
}

@end
