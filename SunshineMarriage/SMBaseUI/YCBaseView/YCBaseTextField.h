//
//  YCBaseTextField.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/9/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol YCBaseTextFieldDelegate <NSObject>
- (void)yc_textFiledEditChanged:(UITextField *)textField;
@end

@protocol YCBaseTextFieldReturnDelegate <NSObject>
- (BOOL)yc_textFieldShouldReturn:(UITextField *)textField;
@end

@protocol YCBaseTextFieldListenerDelegate <NSObject>
- (void)listenerTextFiledEditChanged:(UITextField *)textField;
@end

@interface YCBaseTextField : UIView<UITextFieldDelegate,YCBaseTextFieldDelegate,YCBaseTextFieldListenerDelegate>
@property (nonatomic,copy) NSString *placeholder;
@property (nonatomic,copy) NSString *text;
@property (nonatomic,assign) BOOL secureTextEntry ;
@property (nonatomic,assign) NSInteger minLength;
@property (nonatomic,assign) NSInteger maxLength;
@property (nonatomic,assign) UIKeyboardType keyboardType;
@property (nonatomic,copy) NSString *input;
@property (nonatomic, assign) BOOL hideLine;
@property (nonatomic, strong) UIColor *lineColor;
@property (nonatomic, strong) UIView *leftView;
@property (nonatomic,strong) UITextField *textField;
@property (nonatomic, assign) id<YCBaseTextFieldDelegate> delegate;
@property (nonatomic, assign) id<YCBaseTextFieldListenerDelegate> listenerDelegate;
@property (nonatomic, assign) id<YCBaseTextFieldReturnDelegate> returnDelegate;
@property (nonatomic, assign) BOOL isPhoneNum;
@property (nonatomic, strong) YCBaseTextField *listenTextField;
@property (nonatomic, copy) NSString *errorString; //报错提示
@property (nonatomic, assign) UIReturnKeyType returnKeyType;
@property (nonatomic, assign) BOOL enablesReturnKeyAutomatically;

- (void)setUp;
- (void)addLeftView;

@end

NS_ASSUME_NONNULL_END
