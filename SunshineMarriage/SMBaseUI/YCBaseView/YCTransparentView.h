//
//  TransparentView.h
//  TransparentNavigationBar
//
//  Created by Michael on 15/11/20.
//  Copyright © 2015年 com.51fanxing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YCTransparentView : UIView

@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIView *stretchView;


//头部视图
+ (instancetype)dropHeaderViewWithFrame:(CGRect)frame contentView:(UIView *)contentView stretchView:(UIView *)stretchView;

- (instancetype)initWithFrame:(CGRect)frame contentView:(UIView *)contentView stretchView:(UIView *)stretchView;

@end
