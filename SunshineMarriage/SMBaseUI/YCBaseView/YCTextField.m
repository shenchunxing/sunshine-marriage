//
//  YCTextField.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/5/24.
//

#import "YCTextField.h"
#import "YCBaseUI.h"
#import "YCCategoryModule.h"
#import "SMDefine.h"
#import "YCDataCenter.h"
#import "YCSystemConfigure+UIColor.h"

@interface YCTextField ()<UITextFieldDelegate>

@property (nonatomic,strong) UIImageView *iconImageView;
@property (nonatomic,strong) UILabel *leftLabel;
@property (nonatomic,assign) UIKeyboardType keyboardType;
@property (nonatomic,copy) NSString *enterString;

@end

@implementation YCTextField

- (instancetype)initWithName:(NSString *)name showIconFont:(BOOL)showIconFont{
    if (self = [super init]) {
        self.showIconFont = showIconFont;
        self.name = name;
        [self setUp];
    }
    return self;
}

- (instancetype)initWithName:(NSString *)name{
    if (self = [super init]) {
        self.name = name;
        [self setUp];
    }
    return self;
}

- (instancetype)init{
    if (self = [super init]) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithTextFieldType:(YCTextFieldType)textFieldType{
    if (self = [super init]) {
        [self setUp];
        self.textFieldType = textFieldType;
    }
    return self;
}

- (void)setUp{
    [self addSubview:self.iconImageView];
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self);
        make.top.mas_equalTo(24);
        make.height.width.mas_equalTo(24);
    }];
    
    [self addSubview:self.textField];
    
    
    
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.iconImageView.mas_right).offset(16);
        make.top.mas_equalTo(25);
        make.height.mas_equalTo(24);
        make.right.mas_equalTo(self);
    }];
    
    [self addSubview:self.line];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self);
        make.height.mas_equalTo(1);
    }];
    
    self.lineChanged = YES;
}


- (void)textFiledEditChanged:(UITextField *)textField{
    if (self.lineChanged) {
        self.line.backgroundColor =  textField.text.length>0 ? [YCSystemConfigure mainColor]:[UIColor yc_hex_EEEEEE];
    }
    NSString *toBeString = textField.text;
    UITextRange *selectedRange = [textField markedTextRange];
    UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
    // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
    if (!position){
        if (toBeString.length > self.maxLength){
            NSRange rangeIndex = [toBeString rangeOfComposedCharacterSequenceAtIndex:self.maxLength];
            if (rangeIndex.length == 1){
                textField.text = [toBeString substringToIndex:self.maxLength];
            }
            else{
                NSRange rangeRange = [toBeString rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, self.maxLength)];
                textField.text = [toBeString substringWithRange:rangeRange];
            }
        }
    }
    
    //可能会有其他的一些操作
    [self yc_textFiledEditChanged:textField];
}

- (void)yc_textFiledEditChanged:(UITextField *)textField{};

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(range.location<=self.maxLength - 1){
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:self.enterString] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    return NO;
}

#pragma mark --- Getter
- (UIImageView *)iconImageView{
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] init];
    }
    return _iconImageView;
}

- (UITextField *)textField{
    if (!_textField) {
        _textField = [[UITextField alloc] init];
        _textField.keyboardType = UIKeyboardTypeNumberPad;
//        _textField.placeholder = [NSString stringWithFormat:@"请输入%@",self.name];
        //ios 13不再允许kvc访问_placeholderLabel
//        [_textField setValue:[UIColor yc_hex_B3B7C2] forKeyPath:@"_placeholderLabel.textColor"];
//        [_textField setValue:[UIFont yc_15] forKeyPath:@"_placeholderLabel.font"];
//        [_textField setPlaceHolderLabel:[UIColor yc_hex_B3B7C2] font:[UIFont yc_15]];
        [_textField setPlaceHolder:[NSString stringWithFormat:@"请输入%@",self.name] color:[UIColor yc_hex_B3B7C2] font:[UIFont yc_15]];
        _textField.textColor = [UIColor yc_hex_3C3D49];
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textField.delegate = self;
        [_textField addTarget:self action:@selector(textFiledEditChanged:) forControlEvents:UIControlEventEditingChanged];
       
    }
    return _textField;
}

- (UIView *)line{
    if (!_line) {
        _line = [[UIView alloc] init];
        _line.backgroundColor = [UIColor  yc_hex_EEEEEE];
    }
    return _line;
}

#pragma mark -- Setter
- (void)setImageName:(NSString *)imageName{
    self.iconImageView.image = [UIImage yc_loginImgWithName:imageName];
}

- (void)setCdimageName:(NSString *)cdimageName{
      self.iconImageView.image = [UIImage yc_cdloginImgWithName:cdimageName];
}

- (void)setPlaceholder:(NSString *)placeholder{
    self.textField.placeholder = placeholder;
}

- (void)setTextFieldType:(YCTextFieldType)textFieldType{
    _textFieldType = textFieldType;
    switch (textFieldType) {
        case YCPhoneTextFieldType:
            self.keyboardType = UIKeyboardTypeNumberPad;
            self.minLength = 13;
            self.enterString = NUM;
            break;
        case YCSendMessageTextFieldType:
            self.keyboardType = UIKeyboardTypeNumberPad;
            self.minLength = 6;
            self.enterString = NUM;
            break;
        case YCPasswordTextFieldType:
            self.keyboardType = UIKeyboardTypeASCIICapable;
            self.minLength = 16;
            self.enterString = ALPHANUM;
            self.secureTextEntry = YES;
            break;
        case YCChangePasswordTextFieldType:
            self.keyboardType = UIKeyboardTypeASCIICapable;
            self.minLength = 16;
            self.enterString = ALPHANUM;
            self.secureTextEntry = YES;
            break;
        default:
            break;
    }
    self.textField.keyboardType = self.keyboardType;
}

- (BOOL)canEdit{
    return self.textField.text.length > 0;
}

- (NSString *)text{
    return self.textField.text;
}

- (void)setText:(NSString *)text{
    self.textField.text = text;
}

- (void)setSecureTextEntry:(BOOL)secureTextEntry{
    self.textField.secureTextEntry = secureTextEntry;
}

- (void)setReturnKeyType:(UIReturnKeyType)returnKeyType {
    _returnKeyType = returnKeyType;
    self.textField.returnKeyType = returnKeyType;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([self.delegate respondsToSelector:@selector(yc_textFieldShouldReturn:)]) {
       return [self.delegate yc_textFieldShouldReturn:textField];
    }
    return YES;
}

@end
