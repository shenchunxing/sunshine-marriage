//
//  YCTitleVerificateCodeTextField.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/11/22.
//

#import "YCImageTextField.h"
#import "VerificatieCodeLabel.h"
NS_ASSUME_NONNULL_BEGIN
@interface YCImageVerificateCodeTextField : YCImageTextField
@property (nonatomic, strong) VerificatieCodeLabel *verificateLabel;
@end

NS_ASSUME_NONNULL_END
