//
//  YCTitleEncryptionTextField.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/11/22.
//

#import "YCTitleTextField.h"
#import "EncryptionButton.h"
NS_ASSUME_NONNULL_BEGIN

@interface YCTitleEncryptionTextField : YCTitleTextField
@property (nonatomic, strong) EncryptionButton *encryptionBtn;
@end

NS_ASSUME_NONNULL_END
