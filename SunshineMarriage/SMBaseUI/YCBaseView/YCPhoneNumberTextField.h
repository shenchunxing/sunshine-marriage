//
//  YCPhoneNumberTextField.h
//  YCLoginModule
//
//  Created by 沈春兴 on 2019/5/23.
//

#import "YCTextField.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCPhoneNumberTextField : YCTextField

@property (nonatomic,assign) BOOL validPhoneNum;

@end

NS_ASSUME_NONNULL_END
