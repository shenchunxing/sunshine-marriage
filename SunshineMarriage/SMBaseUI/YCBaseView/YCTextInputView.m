//
//  YCTextInputView.m
//  YCBaseUI
//
//  Created by haima on 2019/4/18.
//

#import "YCTextInputView.h"
#import <Masonry/Masonry.h>
#import "UIImage+Resources.h"
#import "UIFont+Style.h"
#import "UIColor+Style.h"

@interface YCTextInputView ()<UITextViewDelegate>
/* 清空按钮 */
@property (nonatomic, strong) UIButton *clearBtn;
@end
@implementation YCTextInputView

- (instancetype)initWitDelegate:(id<YCTextInputViewDelegate>)delegate {
    
    if (self = [self initWithFrame:CGRectZero]) {
        self.textViewDelegate = delegate;
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.textView];
        [self addSubview:self.clearBtn];
        [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        [self.clearBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.size.mas_equalTo(CGSizeMake(16, 16));
            make.right.equalTo(self);
        }];
        [self.clearBtn addObserver:self forKeyPath:@"hidden" options:NSKeyValueObservingOptionNew context:nil];
        [self.textView addObserver:self forKeyPath:@"userInteractionEnabled" options:NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}

- (void)dealloc {
    [self.clearBtn removeObserver:self forKeyPath:@"hidden"];
    [self.textView removeObserver:self forKeyPath:@"userInteractionEnabled"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    NSNumber *value = change[NSKeyValueChangeNewKey];
    if ([keyPath isEqualToString:@"hidden"]) {
        if ([value boolValue]) {
            self.textView.textContainerInset = UIEdgeInsetsZero;
        }else{
            self.textView.textContainerInset = UIEdgeInsetsMake(0, 0, 0, 26);
        }
    }
    if ([keyPath isEqualToString:@"userInteractionEnabled"]) {
        if (![value boolValue]) {
            self.clearBtn.hidden = YES;
        }
    }
}

- (void)changeContentWithText:(NSString *)text textColor:(UIColor *)textColor
                  placeholder:(NSString *)placeholder
             placeholderColor:(UIColor *)placehoderColor {
    
    self.textView.text = [NSString stringWithFormat:@"%@",text?:@""];
    self.textView.textColor = textColor;
    self.textView.placeholder = placeholder;
    self.textView.placeholderColor = placehoderColor;
    self.textView.placeholderLabel.hidden = (self.textView.hasText);
}

- (void)onClearButtonClick:(UIButton *)button {
    
    self.textView.text = @"";
    self.textView.placeholderLabel.hidden = (self.textView.hasText);
    [self textViewDidChange:self.textView];
}

- (void)setClearMode:(YCTextViewClearMode)clearMode {
    
    _clearMode = clearMode;
    switch (clearMode) {
        case YCTextViewClearModeNever:
        case YCTextViewClearModeWhileEditing:
            self.clearBtn.hidden = YES;
            self.textView.rightOffset = 26;
            break;
        case YCTextViewClearModeModeAlways:
            self.clearBtn.hidden = NO;
            self.textView.rightOffset = 0;
            break;
    }
}

- (void)changeClearButton:(BOOL)hidden {
    
    if (self.clearMode == YCTextViewClearModeModeAlways || self.clearMode == YCTextViewClearModeNever) {
        return;
    }
    
    if (self.clearBtn.hidden != hidden) {
        self.clearBtn.hidden = hidden;
    }

}

- (void)changeClear{
    if (self.clearMode == YCTextViewClearModeWhileEditing) {
        if (self.textView.text.length > 0 && self.clearMode == YCTextViewClearModeWhileEditing) {
            self.clearBtn.hidden = NO;
        }else{
            self.clearBtn.hidden = YES;
        }
    }
}


#pragma mark - UITextViewDelegate 只实现部分常用协议方法
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {

    if (self.textViewDelegate && [self.textViewDelegate respondsToSelector:@selector(textViewShouldEndEditing:)]) {
      return  [self.textViewDelegate textViewShouldEndEditing:textView];
    }
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    if (self.textViewDelegate && [self.textViewDelegate respondsToSelector:@selector(textViewShouldEndEditing:)]) {
        return  [self.textViewDelegate textViewShouldEndEditing:textView];
    }
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
//    [self changeClearButton:(self.clearMode != YCTextViewClearModeWhileEditing)];
    [self changeClear];
    
    if (self.textViewDelegate && [self.textViewDelegate respondsToSelector:@selector(textViewDidBeginEditing:)]) {
        [self.textViewDelegate textViewDidBeginEditing:textView];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    [self changeClearButton:YES];
    if (self.textViewDelegate && [self.textViewDelegate respondsToSelector:@selector(textViewDidEndEditing:)]) {
        [self.textViewDelegate textViewDidEndEditing:textView];
    }
}

- (void)textViewDidChange:(UITextView *)textView {
    [self changeClear];
    if (self.textViewDelegate && [self.textViewDelegate respondsToSelector:@selector(textViewDidChange:)]) {
        [self.textViewDelegate textViewDidChange:textView];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if (self.textViewDelegate && [self.textViewDelegate respondsToSelector:@selector(textView:shouldChangeTextInRange:replacementText:)]) {
       return [self.textViewDelegate textView:textView shouldChangeTextInRange:range replacementText:text];
    }
    return YES;
}

- (void)setPlaceHolder:(NSString *)placeHolder
{
    _placeHolder = placeHolder;
    self.textView.placeholder = placeHolder;
}

#pragma mark - getter
- (UIButton *)clearBtn {
    
    if (_clearBtn == nil) {
        _clearBtn = [[UIButton alloc] init];
        _clearBtn.hidden = YES;
        [_clearBtn setImage:[UIImage yc_imageWithNamed:@"icon_clear"] forState:UIControlStateNormal];
        [_clearBtn addTarget:self action:@selector(onClearButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _clearBtn;
}

- (YCTextView *)textView {
    
    if (_textView == nil) {
        _textView = [[YCTextView alloc] init];
        _textView.delegate = self;
        _textView.textAlignment = NSTextAlignmentLeft;
        _textView.font = [UIFont yc_13];
        _textView.textColor = [UIColor yc_hex_999999];
        _textView.delegate = self;
        _textView.textContainerInset = UIEdgeInsetsZero;
        _textView.returnKeyType = UIReturnKeyDone;
    }
    return _textView;
}
@end
