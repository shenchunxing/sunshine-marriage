//
//  YCTitleView.m
//  YCBaseUI
//
//  Created by haima on 2019/4/3.
//

#import "YCTitleOpenCloseView.h"
#import <Masonry/Masonry.h>
#import "UIColor+Style.h"
#import "UIFont+Style.h"
#import "YCCategoryModule.h"


@interface YCTitleOpenCloseView ()
/* 标题 */
@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic,strong) UILabel *detailLabel;
@property (nonatomic,strong) UIImageView *arrowImageView;
@property (nonatomic,strong) UIView *colorView;

@end

@implementation YCTitleOpenCloseView

- (instancetype)initWithTitle:(NSString *)title {
    
    if (self = [super init]) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        [self addSubview:self.colorView];
        [self addSubview:self.titleLabel];
        
        [self.colorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(14);
            make.size.mas_equalTo(CGSizeMake(4, 18));
            make.centerY.equalTo(self.titleLabel);
        }];
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).offset(20);
            make.left.equalTo(self.colorView.mas_right).offset(10);
            make.bottom.equalTo(self).offset(-12);
        }];
        
        [self.titleLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
        self.titleLabel.text = title;

        [self addSubview:self.arrowImageView];
        [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self).offset(-14);
            make.centerY.mas_equalTo(self);
            make.width.height.mas_equalTo(16);
        }];
        
        [self addSubview:self.detailLabel];
        [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.arrowImageView.mas_left).offset(-8);
            make.centerY.mas_equalTo(self);
        }];
        
        self.statusType = YCTitleCloseType;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
        [self addGestureRecognizer:tap];
        
    }
    return self;
}

- (void)setStatusType:(YCTitleOpenCloseType)statusType{
    _statusType = statusType;
    switch (statusType) {
        case YCTitleOpenType:
            self.detailLabel.text = @"展开";
            self.arrowImageView.image = [UIImage yc_businessImgWithName:@"icon_bottom"];
            break;
        case YCTitleCloseType:
            self.detailLabel.text = @"收起";
            self.arrowImageView.image = [UIImage yc_businessImgWithName:@"icon_top"];
            break;
        default:
            break;
    }
}

- (void)setTitleView:(NSString *)title {
    self.titleLabel.text = title;
}

- (UIView *)colorView {
    if (_colorView == nil) {
        _colorView = [[UIView alloc] init];
        _colorView.backgroundColor = [UIColor yc_hex_F5A623];
    }
    return _colorView;
}


- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor yc_hex_3C3D49];
        _titleLabel.font = [UIFont yc_16_bold];
        _titleLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _titleLabel;
}


- (UILabel *)detailLabel{
    if (!_detailLabel) {
        _detailLabel = [UILabel yc_labelWithText:@"收起" textFont:[UIFont yc_15] textColor:[UIColor yc_hex_9B9EA8]];
    }
    return _detailLabel;
}

- (UIImageView *)arrowImageView{
    if (!_arrowImageView) {
        _arrowImageView = [[UIImageView alloc] init];
        _arrowImageView.image = [UIImage yc_businessImgWithName:@"icon_top"];
    }
    return _arrowImageView;
}


- (void)tap:(UITapGestureRecognizer *)geusture{
    self.statusType = !self.statusType;
    if (self.rightBtnBlock) {
        self.rightBtnBlock(self);
    }
}

- (void)setTitle:(NSString *)title{
    _title = title;
    self.titleLabel.text = title;
}

@end
