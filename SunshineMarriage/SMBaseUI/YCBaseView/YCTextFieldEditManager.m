//
//  YCTextFieldEditManager.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/7/26.
//

#import "YCTextFieldEditManager.h"
#import "YCTextField.h"
#import "UIViewController+CustomToast.h"
#import "YCPhoneNumberTextField.h"
#import "NSObject+Helper.h"
#import "YCAlertView.h"

@implementation YCTextFieldEditManager

+ (BOOL)isVerifyBylengthInController:(UIViewController *)controller{
    for (UIView *view in controller.view.subviews) {
        if ([view isKindOfClass:[YCTextField class]]) {
            YCTextField *yc_textField = (YCTextField *)view;
            //设置了最小值
            if (yc_textField.minLength) {
                if (yc_textField.textField.text.length < yc_textField.minLength) {
                    [controller showToast:[NSString stringWithFormat:@"%@长度小于%ld",yc_textField.name, [yc_textField isKindOfClass:[YCPhoneNumberTextField class]] ? yc_textField.minLength - 2 :  yc_textField.minLength]];
                    return NO;
                }
            }
        }
    }
    return YES;
}

+ (BOOL)verifyTextfields:(NSArray <YCTextField *>*)textfields{
   for (YCTextField *yc_textField in textfields) {
        if (yc_textField.minLength) {
            if (yc_textField.textField.text.length < yc_textField.minLength) {
                [[self yc_getCurrentViewController] showToast:[NSString stringWithFormat:@"%@长度小于%ld",yc_textField.name, [yc_textField isKindOfClass:[YCPhoneNumberTextField class]] ? yc_textField.minLength - 2 :  yc_textField.minLength]];
                return NO;
            }
        }
    }
    return YES;
}

+ (BOOL)verifyTextfieldsInSuperview:(UIView *)superView {
   for (UIView *view in superView.subviews) {
        if ([view isKindOfClass:[YCTextField class]]) {
            YCTextField *tf = (YCTextField *)view;
            if(tf.text.length <= 0) return NO;
        }
    }
    return YES;
}

@end
