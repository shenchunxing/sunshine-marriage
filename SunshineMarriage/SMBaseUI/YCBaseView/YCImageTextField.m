//
//  YCTextField.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/5/24.
//

#import "YCImageTextField.h"
#import <Masonry/Masonry.h>
#import "YCCategoryModule.h"

@interface YCImageTextField ()
@property (nonatomic, strong) UIImageView *leftImageView;
@end

@implementation YCImageTextField

- (void)addLeftView {
   [self addSubview:self.leftImageView];
    [self.leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(27);
        make.width.height.mas_equalTo(20);
    }];
    self.leftView = self.leftImageView;
}

- (UIImageView *)leftImageView {
    if (!_leftImageView) {
        _leftImageView = [[UIImageView alloc] init];
    }
    return _leftImageView;
}


@end
