//
//  YCTextField.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/5/24.
//

#import "YCBaseTextField.h"
#import "YCBaseUI.h"
#import "YCCategoryModule.h"
#import "SMDefine.h"
#import "YCDataCenter.h"
#import "YCSystemConfigure+UIColor.h"

@interface YCBaseTextField ()

@property (nonatomic, strong) UIView *line;
@end

@implementation YCBaseTextField

- (instancetype)init{
    if (self = [super init]) {
        self.delegate = self;
        self.backgroundColor = [UIColor whiteColor];
        [self setUp];
    }
    return self;
}

- (void)setUp{
    
    [self addLeftView];
    
    [self addSubview:self.textField];
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.leftView.mas_right).offset(16);
        make.top.mas_equalTo(25);
        make.height.mas_equalTo(24);
        make.right.mas_equalTo(self);
    }];
    
    [self addSubview:self.line];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(self.mas_bottom).mas_offset(-kLineHeight);
        make.height.mas_equalTo(kLineHeight);
        make.right.mas_equalTo(self);
    }];
}

- (void)textFiledEditChanged:(UITextField *)textField{
    NSString *toBeString = textField.text;
    UITextRange *selectedRange = [textField markedTextRange];
    UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
    // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
    if (!position){
        if (toBeString.length > self.maxLength){
            NSRange rangeIndex = [toBeString rangeOfComposedCharacterSequenceAtIndex:self.maxLength];
            if (rangeIndex.length == 1){
                textField.text = [toBeString substringToIndex:self.maxLength];
            }
            else{
                NSRange rangeRange = [toBeString rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, self.maxLength)];
                textField.text = [toBeString substringWithRange:rangeRange];
            }
        }
    }
    
    if ([self.listenerDelegate respondsToSelector:@selector(listenerTextFiledEditChanged:)]) {
        [self.listenerDelegate listenerTextFiledEditChanged:textField];
    }
   
    if ([self.delegate respondsToSelector:@selector(yc_textFiledEditChanged:)]) {
        [self.delegate yc_textFiledEditChanged:textField];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (self.isPhoneNum) {
        if (textField) {
          //输入手机号进行格式化
          NSString* text = textField.text;
          //删除
          if([string isEqualToString:@" "]){
              //删除一位
              if(range.length == 1){
                  //最后一位,遇到空格则多删除一次
                  if (range.location == text.length-1 ) {
                      if ([text characterAtIndex:text.length-1] == '-') {
                          [textField deleteBackward];
                      }
                      return YES;
                  }
                  else{
                      //从中间删除
                      NSInteger offset = range.location;
                      
                      if (range.location < text.length && [text characterAtIndex:range.location] == '-' && [textField.selectedTextRange isEmpty]) {
                          [textField deleteBackward];
                          offset --;
                      }
                      [textField deleteBackward];
                      textField.text = [self parseString:textField.text];
                      UITextPosition *newPos = [textField positionFromPosition:textField.beginningOfDocument offset:offset];
                      textField.selectedTextRange = [textField textRangeFromPosition:newPos toPosition:newPos];
                      return NO;
                  }
              }
              else if (range.length > 1) {
                  BOOL isLast = NO;
                  //如果是从最后一位开始
                  if(range.location + range.length == textField.text.length ){
                      isLast = YES;
                  }
                  [textField deleteBackward];
                  textField.text = [self parseString:textField.text];
                  
                  NSInteger offset = range.location;
                  if (range.location == 3 || range.location  == 8) {
                      offset ++;
                  }
                  if (isLast) {
                      //光标直接在最后一位了
                  }else{
                      UITextPosition *newPos = [textField positionFromPosition:textField.beginningOfDocument offset:offset];
                      textField.selectedTextRange = [textField textRangeFromPosition:newPos toPosition:newPos];
                  }
                  
                  return NO;
              }
              else{
                  return YES;
              }
          }
          
          else if(string.length >0){
              
              //限制输入字符个数
              if (([self noneSymbolString:textField.text].length + string.length - range.length > 11) ) {
                  return NO;
              }
              //判断是否是纯数字(千杀的搜狗，百度输入法，数字键盘居然可以输入其他字符)
              if(![string isPureNumber]){
                  return NO;
              }
              [textField insertText:string];
              textField.text = [self parseString:textField.text];
              
              NSInteger offset = range.location + string.length;
              if (range.location == 3 || range.location  == 8) {
                  offset ++;
              }
              UITextPosition *newPos = [textField positionFromPosition:textField.beginningOfDocument offset:offset];
              textField.selectedTextRange = [textField textRangeFromPosition:newPos toPosition:newPos];
              return NO;
          }else{
              return YES;
          }
          
        }
    }else {
        if(range.location<=self.maxLength - 1){
            NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:self.input] invertedSet];
            NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
            return [string isEqualToString:filtered];
        }
        return NO;
    }
    return YES;
}

- (NSString*)noneSymbolString:(NSString*)string {
    return [string stringByReplacingOccurrencesOfString:@" " withString:@""];
}

- (NSString*)parseString:(NSString*)string {
    if (!string) {
        return nil;
    }
    NSMutableString* mStr = [NSMutableString stringWithString:[string stringByReplacingOccurrencesOfString:@" " withString:@""]];
    if (mStr.length >3) {
        [mStr insertString:@" " atIndex:3];
    }if (mStr.length > 8) {
        [mStr insertString:@" " atIndex:8];
    }
    return  mStr;
}


#pragma mark --- Getter
- (UITextField *)textField{
    if (!_textField) {
        _textField = [[UITextField alloc] init];
        _textField.keyboardType = UIKeyboardTypeNumberPad;
        [_textField setPlaceHolder:@"请输入" color:[UIColor yc_hex_B3B7C2] font:[UIFont yc_15]];
        _textField.textColor = [UIColor yc_hex_3C3D49];
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textField.delegate = self;
        [_textField addTarget:self action:@selector(textFiledEditChanged:) forControlEvents:UIControlEventEditingChanged];
       
    }
    return _textField;
}

- (UIView *)line {
    if (!_line) {
        _line = [[UIView alloc] init];
        _line.backgroundColor = [UIColor yc_hex_EFEFEF];
    }
    return _line;
}

- (void)setPlaceholder:(NSString *)placeholder{
    self.textField.placeholder = placeholder;
    self.errorString = [placeholder copy];
}

- (BOOL)canEdit{
    return self.textField.text.length > 0;
}

- (NSString *)text{
    return self.textField.text;
}

- (void)setText:(NSString *)text{
    self.textField.text = text;
}

- (void)setSecureTextEntry:(BOOL)secureTextEntry{
    self.textField.secureTextEntry = secureTextEntry;
}

- (void)setLineColor:(UIColor *)lineColor {
    _lineColor = lineColor;
    self.line.backgroundColor = lineColor;
}

- (void)setHideLine:(BOOL)hideLine {
    _hideLine = hideLine;
    if (hideLine) {
        [self.line removeFromSuperview];
        self.line = nil;
    }
}

- (BOOL)isEqual:(id)object {
    if ([object isKindOfClass:[NSString class]]) {
        return [self.text isEqualToString:object];
    }
    if ([object isKindOfClass:[YCBaseTextField class]]) {
        YCBaseTextField *tf = object;
        return [self.text isEqualToString:tf.text];
    }
    return NO;
}

- (void)setReturnKeyType:(UIReturnKeyType)returnKeyType {
    _returnKeyType = returnKeyType;
    self.textField.returnKeyType = returnKeyType;
}

- (void)setKeyboardType:(UIKeyboardType)keyboardType {
    _keyboardType = keyboardType;
    self.textField.keyboardType = keyboardType;
}

- (void)setEnablesReturnKeyAutomatically:(BOOL)enablesReturnKeyAutomatically {
    _enablesReturnKeyAutomatically = enablesReturnKeyAutomatically;
    self.textField.enablesReturnKeyAutomatically = enablesReturnKeyAutomatically;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([self.returnDelegate respondsToSelector:@selector(yc_textFieldShouldReturn:)]) {
       return [self.returnDelegate yc_textFieldShouldReturn:textField];
    }
    return YES;
}

@end
