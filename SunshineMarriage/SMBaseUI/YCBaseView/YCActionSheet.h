//
//  YCAationSheet.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/6/20.
//

#import <UIKit/UIKit.h>
@class YCActionSheet;
@protocol YCActionSheetDelegate;

@protocol YCActionSheetDelegate <NSObject>

- (void)actionSheetCancel:(YCActionSheet *)actionSheet;
- (void)actionSheet:(YCActionSheet *)sheet clickedButtonIndex:(NSInteger)buttonIndex;

@end

@interface YCActionSheet : UIView

@property (weak, nonatomic) id<YCActionSheetDelegate> delegate;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *cancelButtonTitle;

- (id)initWithTitle:(NSString *)title delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION;

- (void)show;

- (void)hide;

- (void)setTitleColor:(UIColor *)color fontSize:(CGFloat)size;

- (void)setButtonTitleColor:(UIColor *)color bgColor:(UIColor *)bgcolor fontSize:(CGFloat)size atIndex:(int)index;

- (void)setCancelButtonTitleColor:(UIColor *)color bgColor:(UIColor *)bgcolor fontSize:(CGFloat)size;

@end


