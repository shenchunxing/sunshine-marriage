//
//  TransparentView.m
//  TransparentNavigationBar
//
//  Created by Michael on 15/11/20.
//  Copyright © 2015年 com.51fanxing. All rights reserved.
//

#import "YCTransparentView.h"
#import "YCCategoryModule.h"

@interface YCTransparentView ()<UIScrollViewDelegate>

@property (nonatomic, assign) CGFloat initOffsetY;
@property (nonatomic, assign) CGRect initStretchViewFrame;
@property (nonatomic, assign) CGFloat initSelfHeight;
@property (nonatomic, assign) CGFloat initContentHeight;

@end
@implementation YCTransparentView

+ (instancetype)dropHeaderViewWithFrame:(CGRect)frame contentView:(UIView *)contentView stretchView:(UIView *)stretchView
{
    YCTransparentView *dropHeaderView = [[YCTransparentView alloc] init];
    dropHeaderView.frame = frame;
    dropHeaderView.contentView = contentView;
    dropHeaderView.stretchView = stretchView;
    
    stretchView.contentMode = UIViewContentModeScaleAspectFill;
    stretchView.clipsToBounds = YES;
    return dropHeaderView;
}

- (instancetype)initWithFrame:(CGRect)frame contentView:(UIView *)contentView stretchView:(UIView *)stretchView
{
    if(self = [super initWithFrame:frame]){
        self.contentView = contentView;
        self.stretchView = stretchView;
        self.contentMode = UIViewContentModeScaleAspectFill;
        self.clipsToBounds = YES;
    }
    return self;
}
//父视图必须是滚动视图类型
- (void)willMoveToSuperview:(UIView *)newSuperview
{
    [self.superview removeObserver:self forKeyPath:@"contentOffset"];
    if (newSuperview != nil) {
        [newSuperview addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
        UIScrollView *scrollView = (UIScrollView *)newSuperview;
       
        self.initOffsetY = scrollView.contentOffset.y;
        self.initStretchViewFrame = self.stretchView.frame;
        self.initSelfHeight = self.frame.size.height;
        self.initContentHeight = self.contentView.frame.size.height;

    }
    
}

- (void)setContentView:(UIView *)contentView
{
    _contentView = contentView;
    [self addSubview:contentView];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    CGFloat offsetY = [change[@"new"] CGPointValue].y - self.initOffsetY;
//    NSLog(@"offsetY=%f",offsetY);
//    if (offsetY > -10) {
//
//        self.stretchView.y = self.initStretchViewFrame.origin.y -10;
//        self.stretchView.height = self.initStretchViewFrame.size.height +10;
//
//    }else{
//
        self.stretchView.y = self.initStretchViewFrame.origin.y + offsetY;
        self.stretchView.height = self.initStretchViewFrame.size.height - offsetY;
    
//    }
    
}


@end
