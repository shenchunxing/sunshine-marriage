//
//  EncryptionButton.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/11/22.
//

#import "EncryptionButton.h"
#import "YCCategoryModule.h"

#import "YCDataCenter.h"

@implementation EncryptionButton

- (instancetype)initWithShowIconFont:(BOOL)showIconFont {
    if (self = [super init]) {
        self.showIconFont = showIconFont;
        self.isOpenEye = NO;
           
        [self addTarget:self action:@selector(encryptionBtnClick) forControlEvents:UIControlEventTouchUpInside];
     }
    
    return self;
}

- (instancetype)init {
    return [self initWithShowIconFont:NO];
}

- (void)encryptionBtnClick{
    self.isOpenEye = !self.isOpenEye;
    if (self.encryptionStatusBlock) {
        self.encryptionStatusBlock(self.isOpenEye);
    }
}

@end
