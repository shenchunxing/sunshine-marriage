
//
//  YCNavigationBar.h
//  Pods-YCHomeModule_Example
//
//  Created by 沈春兴 on 2019/4/2.
//

#import <UIKit/UIKit.h>

@protocol YCNavigationClickListenerProtocol <NSObject>

@optional
/**
 右侧按钮点击回调
 */
- (void)yc_viewControllerDidRightClick:(UIViewController *)viewController;

/**
 左侧按钮点击回调
 默认会触发 pop
 */
- (void)yc_viewControllerDidLeftClick:(UIViewController *)viewController;

/**
 做 pop 相关操作，没有实现的话，默认 pop 一层
 如果要跳转多层，可以实现这个协议方法
 */
- (void)yc_viewControllerDidTriggerPopAction:(UIViewController *)viewController;

@end

/**
 *  公用的导航栏控件
 */
@interface YCNavigationBar : UIView<YCNavigationClickListenerProtocol>

/**
 *  标题文字
 */
@property(nonatomic, copy) NSString *title;

/**
 *  titleview
 */
@property(nonatomic, strong) UIView *titleView;

/**
 *  标题的 label
 */
@property(nonatomic, readonly) UILabel *titleLabel;

/**
 *  左侧按钮
 */
@property(nonatomic, strong) UIView *leftBarButton;

/**
 *  右侧按钮
 */
@property(nonatomic, strong) UIView *rightBarButton;

/**
 *  放置内容的 view，不包含状态栏
 */
@property(nonatomic, readonly) UIView *containerView;

/**
 *  底部 border 颜色
 */
@property(nonatomic, strong) UIColor *bottomBorderColor UI_APPEARANCE_SELECTOR;

/**
 *  title 颜色
 */
@property(nonatomic, strong) UIColor *titleColor UI_APPEARANCE_SELECTOR;


/**
 *  导航栏背景颜色
 */
- (UIColor *)backgroundColor;

/**
 *  底部 border 隐藏
 */
@property (nonatomic,assign) BOOL bottomBorderHiden;

- (void)removeLeftBtn;

/* 导航事件监听者 */
@property (nonatomic, weak) id<YCNavigationClickListenerProtocol> yc_listener;


- (void)yc_setupRightItemWithImage:(UIImage *)image;

- (void)yc_setupTitleViewWithTitle:(NSString *)title;


@end
