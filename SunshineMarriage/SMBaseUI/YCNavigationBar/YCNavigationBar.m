
//
//  YCNavigationBar.h
//  Pods-YCHomeModule_Example
//
//  Created by 沈春兴 on 2019/4/2.
//

#import "YCNavigationBar.h"
#import "YCCategoryModule.h"
#import "SMDefine.h"
#import "UIColor+Style.h"
#import "UIFont+Style.h"
#import "YCSystemConfigure.h"
static NSInteger const TAG_TITLELABEL_NAVIGATIONBAR = 50000;

@interface YCNavigationBar()

@property(nonatomic, strong) UIView *backgroundView;
@property(nonatomic, strong) UIView *containerView;
@property(nonatomic, strong) UIView *bottomBorder;

@end

@implementation YCNavigationBar

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
   
        self.clipsToBounds = NO;
        self.backgroundView = [[UIView alloc] initWithFrame:self.bounds];
        self.containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height - [[UIApplication sharedApplication] statusBarFrame].size.height )];
        self.containerView.bottom = self.height;
        [self addSubview:self.backgroundView];
        [self addSubview:self.containerView];
        
        //描边
        self.bottomBorder = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.containerView.width, 1 / [UIScreen mainScreen].scale)];
        self.bottomBorder.bottom = self.containerView.height;
        self.bottomBorder.backgroundColor = [UIColor blackColor];
        self.bottomBorder.alpha = 0.1;
        self.bottomBorder.hidden = ![YCSystemConfigure sharedInstance].showBottomBorder;
        [self.containerView addSubview:self.bottomBorder];
    }
    return self;
}

- (void)removeLeftBtn
{
    [self.leftBarButton removeFromSuperview];
    self.leftBarButton = nil;
}

- (void)setBottomBorderHiden:(BOOL)bottomBorderHiden{
    _bottomBorderHiden = bottomBorderHiden;
    self.bottomBorder.hidden = bottomBorderHiden;
}

- (void)setBackgroundColor:(UIColor *)backgroundColor
{
    self.backgroundView.backgroundColor = backgroundColor;
}

- (void)setTitle:(NSString *)title{
    _title = title;
    if (nil == _titleView) {
        _titleView = [[UIView alloc] initWithFrame:CGRectMake((self.containerView.width - 100)/2, 0, 100, self.containerView.height)];
    }else {
        [_titleView removeAllSubviews];
    }
    _titleView.frame = CGRectMake((self.containerView.width - 200)/2, 10, 200,25);
    _titleView.backgroundColor = [UIColor clearColor];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:_titleView.bounds];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = self.titleColor ? : [UIColor yc_hex_121D32];
    titleLabel.text = _title;
    titleLabel.font = [UIFont yc_18_bold];
    titleLabel.textAlignment = NSTextAlignmentCenter;
//    titleLabel.adjustsFontSizeToFitWidth = YES;
//    titleLabel.minimumScaleFactor = 14 / 18.f;
    titleLabel.tag = TAG_TITLELABEL_NAVIGATIONBAR;
    [_titleView addSubview:titleLabel];
    
    [_titleView removeFromSuperview];
    [self.containerView addSubview:_titleView];
}

- (void)setTitleColor:(UIColor*)color
{
    _titleColor = color;
    UILabel *titleLabel = (UILabel*)[_titleView viewWithTag:TAG_TITLELABEL_NAVIGATIONBAR];
    titleLabel.textColor = color;
}

- (void)setTitleView:(UIView *)titleView
{
    [_titleView removeFromSuperview];
    _titleView = nil;
    if (titleView) {
        _titleView = titleView;
        _titleView.center = CGPointMake(self.containerView.width / 2, self.containerView.height / 2) ;
        [self.containerView addSubview:_titleView];
    }
}

- (UILabel *)titleLabel
{
    UILabel *titleLabel = (UILabel*)[_titleView viewWithTag:TAG_TITLELABEL_NAVIGATIONBAR];
    return titleLabel;
}

- (void)setLeftBarButton:(UIView *)leftBarButton
{
    [_leftBarButton removeFromSuperview];
    _leftBarButton = nil;
    if (leftBarButton) {
        _leftBarButton = leftBarButton;
        _leftBarButton.left = 14;
        [self.containerView addSubview:_leftBarButton];
    }
}

- (void)setRightBarButton:(UIView *)rightBarButton
{
    [_rightBarButton removeFromSuperview];
    _rightBarButton = nil;
    if (rightBarButton) {
        _rightBarButton = rightBarButton;
        _rightBarButton.right = self.containerView.width - 14;
        [self.containerView addSubview:_rightBarButton];
        [self.containerView bringSubviewToFront:_rightBarButton];
    }
}

- (void)setBottomBorderColor:(UIColor*)color
{
    self.bottomBorder.backgroundColor = color;
}

- (UIColor *)backgroundColor
{
    return self.backgroundView.backgroundColor;
}



@end
