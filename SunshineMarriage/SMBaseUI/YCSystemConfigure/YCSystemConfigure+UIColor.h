//
//  YCSystemConfigure+UIColor.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/11/20.
//


#import "YCSystemConfigure.h"
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YCSystemConfigure (UIColor)

+ (UIColor *)mainColor; //主题色

@end

NS_ASSUME_NONNULL_END
