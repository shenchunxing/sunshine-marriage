//
//  YCSystemConfigure.h
//  YCBankSaasApp
//
//  Created by 沈春兴 on 2019/11/20.
//  Copyright © 2019 云车. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIViewController+NavigationBarConfigure.h"
typedef NS_ENUM(NSInteger,SystemType) {
    YCPartner,
    YCCarDealer,
    YCBankSaas,
};
NS_ASSUME_NONNULL_BEGIN

@interface YCSystemConfigure : NSObject
@property (nonatomic, assign) SystemType systemType;
@property (nonatomic, assign) YCNavigationBarType navigationBarTp;
@property (nonatomic, assign) BOOL useIconFont;
@property (nonatomic, strong) UIColor *backgroundColor;
@property (nonatomic, assign) BOOL showBottomBorder;
@property (nonatomic, assign) UIStatusBarStyle statusBarStyle;
+ (instancetype)sharedInstance;

@end

NS_ASSUME_NONNULL_END
