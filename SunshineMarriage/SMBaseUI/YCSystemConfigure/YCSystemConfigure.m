//
//  YCSystemConfigure.m
//  YCBankSaasApp
//
//  Created by 沈春兴 on 2019/11/20.
//  Copyright © 2019 云车. All rights reserved.
//

#import "YCSystemConfigure.h"
#import "UIColor+Style.h"

static YCSystemConfigure *instance = nil;

@implementation YCSystemConfigure

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (instancetype)init {
    if (self = [super init]) {
        self.navigationBarTp = YCNavigationBarTypeCustom;
        self.backgroundColor = [UIColor yc_hex_F4F4F4];
        self.statusBarStyle = UIStatusBarStyleLightContent;
    }
    return self;
}


@end
