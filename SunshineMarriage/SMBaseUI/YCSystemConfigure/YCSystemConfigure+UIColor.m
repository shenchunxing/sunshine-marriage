//
//  YCSystemConfigure+UIColor.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/11/20.
//

#import "YCSystemConfigure+UIColor.h"
#import "UIColor+Style.h"

@implementation YCSystemConfigure (UIColor)

+ (UIColor *)mainColor {
    switch ([YCSystemConfigure sharedInstance].systemType) {
        case YCPartner:
            return [UIColor yc_hex_108EE9];
            break;
        case YCCarDealer:
            return [UIColor yc_hex_108EE9];
            break;
        case YCBankSaas:
            return [UIColor yc_hex_333440];
            break;
    }
}

@end
