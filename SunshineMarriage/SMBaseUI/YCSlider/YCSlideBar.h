//
//  YCOpenCardApplicationCell.h
//  YCBaseUI-YCBaseUI
//
//  Created by 沈春兴 on 2019/4/9.
//

#import <UIKit/UIKit.h>

typedef void(^YCSlideBarItemSelectedCallback)(NSUInteger idx);

@interface YCSlideBar : UIView

@property (copy, nonatomic) NSArray *itemsTitle;
@property (strong, nonatomic) UIColor *itemColor;
@property (strong, nonatomic) UIColor *itemSelectedColor;
@property (strong, nonatomic) UIColor *sliderColor;

@property (strong, nonatomic) UIView *sliderView;

- (instancetype)initWithItemsTitle:(NSArray *)itemsTitle tableView:(UITableView *)tableView;


- (void)slideBarItemSelectedCallback:(YCSlideBarItemSelectedCallback)callback;
- (void)selectSlideBarItemAtIndex:(NSUInteger)index;

@end
