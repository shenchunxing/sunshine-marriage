//
//  YCOpenCardApplicationCell.h
//  YCBaseUI-YCBaseUI
//
//  Created by 沈春兴 on 2019/4/9.
//

#import "YCSlideBar.h"
#import "YCSlideBarItem.h"
#import "SMDefine.h"
#import "UIColor+Style.h"

#define DEVICE_WIDTH CGRectGetWidth([UIScreen mainScreen].bounds)
#define SLIDER_VIEW_HEIGHT 2

@interface YCSlideBar () <YCSlideBarItemDelegate>
{
    BOOL isMove;//判断是否滑动手势还是点击手势
}
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) NSMutableArray *items;


@property (strong, nonatomic) YCSlideBarItem *selectedItem;
@property (strong, nonatomic) YCSlideBarItemSelectedCallback callback;

@property (strong, nonatomic) UITableView *tableView;


@end

@implementation YCSlideBar

#pragma mark - Lifecircle

- (instancetype)init {
    CGRect frame = CGRectMake(0, 0, DEVICE_WIDTH, 46);
    return [self initWithFrame:frame];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self= [super initWithFrame:frame]) {
        _items = [NSMutableArray array];
        [self initScrollView];
        [self initSliderView];
    }
    return self;
}

- (instancetype)initWithItemsTitle:(NSArray *)itemsTitle tableView:(UITableView *)tableView{
    
    if (self= [super initWithFrame:CGRectMake(0, kNavigationHeight, DEVICE_WIDTH, 46)]) {
        _items = [NSMutableArray array];
        [self initScrollView];
        [self initSliderView];
        isMove = NO;
        self.itemsTitle = itemsTitle;
        self.tableView = tableView;
    }
    return self;
}
- (void)willMoveToWindow:(UIWindow *)newWindow{
    if (newWindow) {
        [kNotificationCenter addObserver:self selector:@selector(dragTable) name:@"isDragTable" object:nil];
        [_tableView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
    }else{
        [_tableView removeObserver:self forKeyPath:@"contentOffset"];
        [kNotificationCenter removeObserver:self];
    }
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
//    DELog(@"point-----%f",point.y);
    isMove = NO;
    return [super hitTest:point withEvent:event];
}

- (void)dragTable{
    isMove = YES;
}

#pragma - mark Custom Accessors

- (void)setItemsTitle:(NSArray *)itemsTitle {
    _itemsTitle = itemsTitle;
    [self setupItems];
}

- (void)setItemColor:(UIColor *)itemColor {
    for (YCSlideBarItem *item in _items) {
        [item setItemTitleColor:itemColor];
    }
}

- (void)setItemSelectedColor:(UIColor *)itemSelectedColor {
    for (YCSlideBarItem *item in _items) {
        [item setItemSelectedTitleColor:itemSelectedColor];
    }
}

- (void)setSliderColor:(UIColor *)sliderColor {
    _sliderColor = sliderColor;
    self.sliderView.backgroundColor = _sliderColor;
}

- (void)setSelectedItem:(YCSlideBarItem *)selectedItem {
    _selectedItem.selected = NO;
    _selectedItem = selectedItem;
}


#pragma - mark Private

- (void)initScrollView {
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    _scrollView.backgroundColor = [UIColor clearColor];
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.bounces = NO;
    [self addSubview:_scrollView];
}

- (void)initSliderView {
    _sliderView = [[UIView alloc] init];
    _sliderColor = [UIColor whiteColor];
    _sliderView.backgroundColor = _sliderColor;
    [_scrollView addSubview:_sliderView];
}

- (void)setupItems {
    CGFloat itemX = 0;
    for (NSString *title in _itemsTitle) {
        YCSlideBarItem *item = [[YCSlideBarItem alloc] init];
        item.delegate = self;
        
        // Init the current item's frame
//        CGFloat itemW = [YCSlideBarItem widthForTitle:title];
        item.frame = CGRectMake(itemX, 0, kWIDTH/4, CGRectGetHeight(_scrollView.frame));
        [item setItemTitle:title];
        [_items addObject:item];
        
        [_scrollView addSubview:item];
        

        itemX = CGRectGetMaxX(item.frame);
    }
    

    _scrollView.contentSize = CGSizeMake(itemX, CGRectGetHeight(_scrollView.frame));
    

    YCSlideBarItem *firstItem = [self.items firstObject];
    firstItem.selected = YES;
    _selectedItem = firstItem;
    

    _sliderView.frame = CGRectMake((firstItem.frame.size.width - 24)/2, self.frame.size.height - 4,24, SLIDER_VIEW_HEIGHT);
}

- (void)scrollToVisibleItem:(YCSlideBarItem *)item {
    NSInteger selectedItemIndex = [self.items indexOfObject:_selectedItem];
    NSInteger visibleItemIndex = [self.items indexOfObject:item];
    
    if (selectedItemIndex == visibleItemIndex) {
        return;
    }
    
    CGPoint offset = _scrollView.contentOffset;

    if (CGRectGetMinX(item.frame) > offset.x && CGRectGetMaxX(item.frame) < (offset.x + CGRectGetWidth(_scrollView.frame))) {
        return;
    }

    if (selectedItemIndex < visibleItemIndex) {

        if (CGRectGetMaxX(_selectedItem.frame) < offset.x) {
            offset.x = CGRectGetMinX(item.frame);
        } else {
            offset.x = CGRectGetMaxX(item.frame) - CGRectGetWidth(_scrollView.frame);
        }
    } else {

        if (CGRectGetMinX(_selectedItem.frame) > (offset.x + CGRectGetWidth(_scrollView.frame))) {
            offset.x = CGRectGetMaxX(item.frame) - CGRectGetWidth(_scrollView.frame);
        } else {
            offset.x = CGRectGetMinX(item.frame);
        }
    }
    _scrollView.contentOffset = offset;
}

- (void)addAnimationWithSelectedItem:(YCSlideBarItem *)item {

    CGFloat dx = CGRectGetMidX(item.frame) - CGRectGetMidX(_selectedItem.frame);
    
    NSLog(@"%@ %@",item,_selectedItem);
    
    CABasicAnimation *positionAnimation = [CABasicAnimation animation];
    positionAnimation.keyPath = @"position.x";
    positionAnimation.fromValue = @(_sliderView.layer.position.x);
    positionAnimation.toValue = @(_sliderView.layer.position.x + dx);
    
    
//    CABasicAnimation *boundsAnimation = [CABasicAnimation animation];
//    boundsAnimation.keyPath = @"bounds.size.width";
//    boundsAnimation.fromValue = @(CGRectGetWidth(_sliderView.layer.bounds));
//    boundsAnimation.toValue = @(CGRectGetWidth(item.frame));

    CAAnimationGroup *animationGroup = [CAAnimationGroup animation];
    animationGroup.animations = @[positionAnimation];
    animationGroup.duration = 0.1;
    [_sliderView.layer addAnimation:animationGroup forKey:@"basic"];
    

    _sliderView.layer.position = CGPointMake(_sliderView.layer.position.x + dx, _sliderView.layer.position.y);
    CGRect rect = _sliderView.layer.bounds;
//    rect.size.width = CGRectGetWidth(item.frame);
    rect.size.width = 24;
    _sliderView.layer.bounds = rect;
    
    
}



#pragma mark - Public

- (void)slideBarItemSelectedCallback:(YCSlideBarItemSelectedCallback)callback {
    _callback = callback;
}

- (void)selectSlideBarItemAtIndex:(NSUInteger)index {
    YCSlideBarItem *item = [self.items objectAtIndex:index];
    if (item == _selectedItem) {
        return;
    }
    
    item.selected = YES;
//    [self scrollToVisibleItem:item];
    [self refreshContenOffsetWithItem:item];
    [self addAnimationWithSelectedItem:item];
    self.selectedItem = item;
}

#pragma mark - YCSlideBarItemDelegate

- (void)slideBarItemSelected:(YCSlideBarItem *)item {
    if (item == _selectedItem) {
        return;
    }
    
    [self addAnimationWithSelectedItem:item];
    self.selectedItem = item;
    
    if (_callback && !isMove) {
        _callback([self.items indexOfObject:item]);
    }
    else if (!isMove){
    
        CGRect rect = [self.tableView rectForSection:[self.items indexOfObject:item]];
        self.tableView.contentOffset = CGPointMake(0, rect.origin.y);

    }
    
    [self refreshContenOffsetWithItem:item];
    
}


// 让选中的item位于中间
- (void)refreshContenOffsetWithItem:(YCSlideBarItem *)item {
    CGRect frame = item.frame;
    CGFloat itemX = frame.origin.x;
    CGFloat width = self.scrollView.frame.size.width;
    CGSize contentSize = self.scrollView.contentSize;
    if (itemX > width/2) {
        CGFloat targetX;
        if ((contentSize.width-itemX) <= width/2) {
            targetX = contentSize.width - width;
        } else {
            targetX = frame.origin.x - width/2 + frame.size.width/2;
        }
        
        if (targetX + width > contentSize.width) {
            targetX = contentSize.width - width;
        }
        [self.scrollView setContentOffset:CGPointMake(targetX, 0) animated:YES];
    } else {
        [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (isMove) {
        CGFloat offsetY = [change[@"new"] CGPointValue].y;
//        NSLog(@"==%f.",offsetY);
        NSIndexPath *indexPath = [_tableView indexPathForRowAtPoint:CGPointMake(0, offsetY)];
        //奇怪的错误
        if (indexPath.section == 0 && indexPath.row == 0) {
            [self slideBarItemSelected:_selectedItem];
        }else{
            if (self.items.count>indexPath.section) {
                [self slideBarItemSelected:self.items[indexPath.section]];
            }
        }
       
    }

}
@end
