//
//  YCOpenCardApplicationCell.h
//  YCBaseUI-YCBaseUI
//
//  Created by 沈春兴 on 2019/4/9.
//

#import <UIKit/UIKit.h>

@protocol YCSlideBarItemDelegate;

@interface YCSlideBarItem : UIView

@property (assign, nonatomic) BOOL selected;
@property (weak, nonatomic) id<YCSlideBarItemDelegate> delegate;

- (void)setItemTitle:(NSString *)title;
- (void)setItemTitleFont:(CGFloat)fontSize;
- (void)setItemTitleColor:(UIColor *)color;
- (void)setItemSelectedTileFont:(CGFloat)fontSize;
- (void)setItemSelectedTitleColor:(UIColor *)color;

+ (CGFloat)widthForTitle:(NSString *)title;

@end

@protocol YCSlideBarItemDelegate <NSObject>

- (void)slideBarItemSelected:(YCSlideBarItem *)item;

@end
