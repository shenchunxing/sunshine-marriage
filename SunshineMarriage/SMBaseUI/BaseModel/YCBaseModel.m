//
//  YCBaseModel.m
//  YCFunctionModule
//
//  Created by 刘成 on 2019/4/23.
//

#import "YCBaseModel.h"

@implementation YCBaseModel
- (void)setValue:(id)value forUndefinedKey:(NSString *)key{}

- (NSString *)getValueWithKey:(NSString *)key{
    if (self) {
        NSString *value = [[[self keyValues] allKeys] containsObject:key]?[self valueForKey:key]:@"";
        value = [self configValue:value key:key];
        return value;
    }
    return @"";
}
- (NSString *)configValue:(NSString *)value key:(NSString *)key{
    return value;
}

#pragma mark  /*打印模型*/
+ (void)logModelWithDict:(NSDictionary *)dict{
    NSMutableString *str = [NSMutableString new];
    for (NSString *key in dict) {
        NSString *s = [NSString stringWithFormat:@"@property (copy, nonatomic) NSString *%@;\n",key];
        [str appendString:s];
    }
    NSLog(@"\n%@",str);
}

@end
