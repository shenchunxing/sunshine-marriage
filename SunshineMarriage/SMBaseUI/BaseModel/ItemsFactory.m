//
//  ItemsFactory.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/8/26.
//

#import "ItemsFactory.h"
#import "YCTextViewItem.h"
#import "YCTextUnitItem.h"
#import "YCPickerItem.h"
#import "YCPickerUnitItem.h"

@implementation ItemsFactory

+ (void)factoryWithTitle:(NSString *)title key:(NSString *)key text:(NSString *)text selectedHandle:(selectedHandle)selectedHandle
{
    YCBaseItem *item = [[YCBaseItem alloc] init];
    item.title = title;
    item.key = key;
    item.text = text;
    __weak typeof(item) wItem = item ;
    item.selectItemHandler = ^(NSIndexPath *indexPath) {
        selectedHandle(wItem,indexPath);
    };
}

@end
