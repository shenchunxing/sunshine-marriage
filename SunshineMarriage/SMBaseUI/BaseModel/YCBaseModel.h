//
//  YCBaseModel.h
//  YCFunctionModule
//
//  Created by 刘成 on 2019/4/23.
//

#import <Foundation/Foundation.h>
#import <MJExtension/MJExtension.h>
#import "YCEnumSwap.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCBaseModel : NSObject
- (void)setValue:(id)value forUndefinedKey:(NSString *)key;
- (NSString *)getValueWithKey:(NSString *)key;
- (NSString *)configValue:(NSString *)value key:(NSString *)key;

#pragma mark  /*打印模型*/
+ (void)logModelWithDict:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
