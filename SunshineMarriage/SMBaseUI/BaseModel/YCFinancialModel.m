//
//  YCFinancialModel.m
//  YCFunctionModule
//
//  Created by 沈春兴 on 2019/6/27.
//

#import "YCFinancialModel.h"

@implementation YCFinancialModel

- (NSString *)configValue:(NSString *)value key:(NSString *)key{
    
    if ([key isEqualToString:@"firstPaymentRatio"] || [key isEqualToString:@"loanRatio"] || [key isEqualToString:@"stageRatio"] || [key isEqualToString:@"servingRatio"]) {
        
        return [NSString stringWithFormat:@"%.2f%%",value.floatValue];
    }
    
    if ([key isEqualToString:@"exeRatio"]) {
        
        return value.length>0?[NSString stringWithFormat:@"%.2f",value.floatValue]:@"";
    }
    
    if ([key isEqualToString:@"firstMoney"] || [key isEqualToString:@"stageMoney"] || [key isEqualToString:@"servingMoney"] || [key isEqualToString:@"bankFee"] || [key isEqualToString:@"monthMoney"] || [key isEqualToString:@"firstMonthMoney"]) {
        
        return [NSString stringWithFormat:@"%@元",value];
    }
    if ([key isEqualToString:@"loanPeriod"]){
        return [NSString stringWithFormat:@"%@",value].length>0?[NSString stringWithFormat:@"%@期",value]:@"";
    }
    if ([key isEqualToString:@"accountRate"]) {
        return value.length>0?[NSString stringWithFormat:@"%@%%",value]:@"";
    }
    return value;
}

@end
