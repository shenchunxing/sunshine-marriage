//
//  YCCarBoardInfoModel.h
//  YCFunctionModule
//
//  Created by 沈春兴 on 2019/6/27.
//

#import "YCBaseModel.h"
@class YCCarInfoModel;
@class YCBoardInfoModel;

//车辆和上牌信息详情
@interface YCCarBoardInfoModel : YCBaseModel

@property (nonatomic,strong) YCCarInfoModel *carInfo;//车辆信息

@property (nonatomic,strong) YCBoardInfoModel *boardInfo;//上牌信息

@end

@interface YCCarInfoModel : YCBaseModel

@property (copy, nonatomic) NSString *boardType;   //0,
@property (copy, nonatomic) NSString *carAttr;   //1,
@property (copy, nonatomic) NSString *carBrandId;   //9,
@property (copy, nonatomic) NSString *carBrandName;   //"",
@property (copy, nonatomic) NSString *carCityId;   //120100000000,
@property (copy, nonatomic) NSString *carCityName;   //"",
@property (copy, nonatomic) NSString *carDetailId;   //121700,
@property (copy, nonatomic) NSString *carDetailName;   //"",
@property (copy, nonatomic) NSString *carFrameNo;   //"348998",
@property (copy, nonatomic) NSString *carGuidePrice;   //2222,
@property (copy, nonatomic) NSString *carModelId;   //5191,
@property (copy, nonatomic) NSString *carModelName;   //"",
@property (copy, nonatomic) NSString *carProvinceId;   //120000000000,
@property (copy, nonatomic) NSString *carProvinceName;   //"",
@property (copy, nonatomic) NSString *carStyle;   //"多用途乘用车",
@property (copy, nonatomic) NSString *carType;   //0,
@property (copy, nonatomic) NSString *color;   //"红色",
@property (copy, nonatomic) NSString *gmtCreate;   //null,
@property (copy, nonatomic) NSString *id;   //1,
@property (copy, nonatomic) NSString *mileage;   //12,
@property (copy, nonatomic) NSString *orderId;   //19040909414003464,
@property (copy, nonatomic) NSString *registDate;   //1556035200000

@end



@interface  YCBoardInfoModel: YCBaseModel
@property (copy, nonatomic) NSString *acceptanceCityId;   //120100000000,
@property (copy, nonatomic) NSString *acceptanceCityName;   //"市辖区",
@property (copy, nonatomic) NSString *acceptanceProvinceId;   //120000000000,
@property (copy, nonatomic) NSString *acceptanceProvinceName;   //"天津市",
@property (copy, nonatomic) NSString *boardCityId;   //110100000000,
@property (copy, nonatomic) NSString *boardCityName;   //"市辖区",
@property (copy, nonatomic) NSString *boardProvinceId;   //110000000000,
@property (copy, nonatomic) NSString *boardProvinceName;   //"北京市",
@property (copy, nonatomic) NSString *boardType;   //1,
@property (copy, nonatomic) NSString *businessSource;   //"f399e266eb6d4231a6d82f820b852f60",
@property (copy, nonatomic) NSString *businessSourceId;   //"",
@property (copy, nonatomic) NSString *gmtCreate;   //1556267363000,
@property (copy, nonatomic) NSString *gmtUpdate;   //1556521412000,
@property (copy, nonatomic) NSString *gpsAmount;   //2,
@property (copy, nonatomic) NSString *id;   //3,
@property (copy, nonatomic) NSString *leaveKey;   //1,
@property (copy, nonatomic) NSString *neOwner;   //"55",
@property (copy, nonatomic) NSString *orderId;   //19040909414003464,
@property (copy, nonatomic) NSString *remarks;   //"3"

@end
