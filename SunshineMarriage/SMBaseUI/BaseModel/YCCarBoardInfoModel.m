//
//  YCCarBoardInfoModel.m
//  YCFunctionModule
//
//  Created by 沈春兴 on 2019/6/27.
//

#import "YCCarBoardInfoModel.h"

#import "YCCategoryModule.h"

@implementation YCCarBoardInfoModel


@end


@implementation YCCarInfoModel

- (NSString *)configValue:(NSString *)value key:(NSString *)key{
    
    if ([key isEqualToString:@"registDate"]) {
        return [value timeItervalSwitchWithYearMonthDay];
    }

    else if ([key isEqualToString:@"carAllName"]) {
        return [NSString stringWithFormat:@"%@%@%@",_carBrandName?:@"",_carModelName?:@"",_carDetailName?:@""];
    }
    else if ([key isEqualToString:@"carType"]) {
        return [[YCEnumSwap sharedInstancetype] getEnumSwapWithKey:@"carType" swapStr:value];
    }
    else if ([key isEqualToString:@"carAttr"]) {
        return [[YCEnumSwap sharedInstancetype] getEnumSwapWithKey:@"carAttr" swapStr:value];
    }
    //
    else if ([key isEqualToString:@"carCityName"]) {
        return [NSString stringWithFormat:@"%@%@",self.carProvinceName,self.carCityName];
    }
    else if ([key isEqualToString:@"mileage"] && value.length >0) {
        return [NSString stringWithFormat:@"%@万公里",self.mileage];
    }
    return value;
}
@end


@implementation YCBoardInfoModel

- (NSString *)configValue:(NSString *)value key:(NSString *)key{
    if ([key isEqualToString:@"shangPaiDi"]) {
        return [NSString stringWithFormat:@"%@%@",_boardProvinceName?:@"",_boardCityName?:@""];
    }
    
    else if ([key isEqualToString:@"leaveKey"]) {
        return [[YCEnumSwap sharedInstancetype] getEnumSwapWithKey:@"carKey" swapStr:value];
    }
    
    else if ([key isEqualToString:@"boardType"]) {
        return [[YCEnumSwap sharedInstancetype] getEnumSwapWithKey:@"boardType" swapStr:value];
    }
    else if ([key isEqualToString:@"acceptanceAddr"]) {
        return [NSString stringWithFormat:@"%@%@",_acceptanceProvinceName?:@"",_acceptanceCityName?:@""];
    }
//    else if ([key isEqualToString:@"businessSource"]) {
//        return [[YCEnumSwap sharedInstancetype] getEnumSwapWithKey:@"businessSource" swapStr:value];
//    }
    
    
    return value;
}

@end
