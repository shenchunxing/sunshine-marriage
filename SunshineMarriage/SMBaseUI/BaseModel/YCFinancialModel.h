//
//  YCFinancialModel.h
//  YCFunctionModule
//
//  Created by 沈春兴 on 2019/6/27.
//

#import "YCBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCFinancialModel : YCBaseModel

@property (copy, nonatomic) NSString *financialProductId;  //    否    int    金融方案id
@property (copy, nonatomic) NSString *financialProductName;  //    否    string    金融方案名字
@property (copy, nonatomic) NSString *loanPeriod;  //    否    int    贷款期限
@property (copy, nonatomic) NSString *exeRatio;  //    否    string    执行利率
@property (copy, nonatomic) NSString *loanMoney;  //    否    string    实际贷款金额
@property (copy, nonatomic) NSString *tailMoney;  //    否    string    车辆尾款
@property (copy, nonatomic) NSString *firstMoney;  //   否    string    实际首付款
@property (copy, nonatomic) NSString *firstPaymentRatio;  //    否    string    实际首付比例(%)
@property (copy, nonatomic) NSString *stageMoney;  //    否    string    银行分期金额
@property (copy, nonatomic) NSString *stageRatio;  //    否    string    分期比例（%）
@property (copy, nonatomic) NSString *loanRatio;  //    否    string    贷款比例（%）
@property (copy, nonatomic) NSString *servingMoney;  //    否    string    担保服务费
@property (copy, nonatomic) NSString *servingRatio;  //    否    string    担保服务费比例
@property (copy, nonatomic) NSString *bankFee;  //    否    string    银行手续费
@property (copy, nonatomic) NSString *firstMonthMoney;  //    否    string    首月还款
@property (copy, nonatomic) NSString *monthMoney;  //    否    string    月还款
@property (copy, nonatomic) NSString *totalMoney;  //    否    string    本息合计

@property (copy, nonatomic) NSString *integrateCost;  //融入费用
@property (copy, nonatomic) NSString *totalCost;  //

@property (copy, nonatomic) NSString *bankId;  //
@property (copy, nonatomic) NSString *bankName;  //

@property (copy, nonatomic) NSString *carPrice;   //1000000,
@property (copy, nonatomic) NSString *companyAssessPrice;   //12,
@property (copy, nonatomic) NSString *carAssessPrice;   //77,

@property (copy, nonatomic) NSString *carType;   //77,

@property (copy, nonatomic) NSString *accountRate;   //77,
@property (copy, nonatomic) NSString *companyOperateCost;   //77,
@property (copy, nonatomic) NSString *teamOperateCost;   //77,


//本地字段
@property (copy, nonatomic) NSString *formula;  //

@end

NS_ASSUME_NONNULL_END
