//
//  YCDMVModel.h
//  YCFunctionModule
//
//  Created by 沈春兴 on 2019/6/30.
//

#import "YCBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCDMVModel : YCBaseModel

@property (nonatomic,copy) NSString *address;
@property (nonatomic,copy) NSString *areaId;
@property (nonatomic,copy) NSString *createTime;
@property (nonatomic,copy) NSString *id;
@property (nonatomic,copy) NSString *logicDelete;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *phone;
@property (nonatomic,copy) NSString *updateTime;
@property (nonatomic,copy) NSString *updator;

@end

NS_ASSUME_NONNULL_END
