//
//  UIViewController+BackgroundConfigure.h
//  YCBaseUI
//
//  Created by haima on 2019/4/2.
//

#import <UIKit/UIKit.h>

@interface UIViewController (BackgroundConfigure)

/* 背景图 */
@property (nonatomic, strong) UIView *yc_backgroundView;

/**
 设置背景图层
 */
- (void)yc_setupBackgroundView;

@end
