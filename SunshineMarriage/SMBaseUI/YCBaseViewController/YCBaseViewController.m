//
//  YCBaseViewController.m
//  YCBaseUI
//
//  Created by haima on 2019/4/2.
//

#import "YCBaseViewController.h"
#import <MJRefresh/MJRefresh.h>
#import "UIViewController+BackgroundConfigure.h"
#import "UIColor+Style.h"
#import "YCNavigationBar.h"
#import "SMDefine.h"
#import "UIImage+Resources.h"
#import "YCCategoryModule.h"
#import "YCRefreshHeader.h"
#import "UIFont+Style.h"
#import "YCSystemConfigure.h"

@interface YCBaseViewController ()


@property (nonatomic,strong) UIButton *backBtn;

@end

@implementation YCBaseViewController

#pragma mark - Init

#pragma mark - LifeCycle

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self yc_updateStatusBarType:self.yc_navigationBarStyle];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    [UIApplication sharedApplication].statusBarStyle = [YCSystemConfigure sharedInstance].statusBarStyle;
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationController.navigationBar.hidden = YES;
    
    [self config];

    [self initData];
    
    [self initView];
    
    [self loadData];
    
    [self yc_setupNavigationBarType:YCNavigationBarTypeCustom];
    
}

- (void)config{}

- (void)initData{}

- (void)initView{}

- (void)loadData{}


- (void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    NSString *str = NSStringFromClass([self class]);
    
    NSLog(@"%@ dealloc!",str);
}

- (void)generatePullRefreshTableHeaderView:(UITableView *)tableView {

    YCRefreshHeader *header = [YCRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRefreshAction)];
    tableView.mj_header = header;
}

- (void)headerRefreshAction {
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass.", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

- (void)generateLoadMoreTableFooterView:(UITableView *)tableView {
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerLoadMoreAction)];
    footer.refreshingTitleHidden = YES;
    footer.stateLabel.hidden = YES;
    
    tableView.mj_footer = footer;
    
}

- (void)footerLoadMoreAction {
    
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass.", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

#pragma mark - Setter
- (void)setNavLineHidden:(BOOL)navLineHidden{
    [self lineImageView].hidden = navLineHidden;
}

- (UIImageView *)lineImageView{
    UIImageView *line = [self findHarirLineImageViewUnder:self.navigationController.navigationBar];
    return line;
}

//去掉view和Navigation的横线
- (UIImageView *)findHarirLineImageViewUnder:(UIView *)view
{
    if ([view isKindOfClass:[UIImageView class]]&&view.bounds.size.height <=1.0) {
        return (UIImageView *)view;
    }
    for (UIView * subview in view.subviews) {
        UIImageView * imageView = [self findHarirLineImageViewUnder:subview];
        if (imageView) {
            return imageView;
        }
    }
    return nil;
}

- (YCAlertHUDPresnter *)myPresenter{
    if (_myPresenter == nil) {
        _myPresenter = [YCAlertHUDPresnter HUDWithView:self.view];
    }
    return _myPresenter;
}

@end
