//
//  YCSearchViewControllerProtocol.h
//  YCBusinessManagementModule
//
//  Created by 沈春兴 on 2020/1/1.
//

#import <Foundation/Foundation.h>
@class YCTableViewItem;
NS_ASSUME_NONNULL_BEGIN

@protocol YCSearchViewControllerProtocol <NSObject>

/// 搜索
- (void)getSearchListData;

/// 搜索提示
- (NSString *)searchBar_placeHolder;


@end

NS_ASSUME_NONNULL_END
