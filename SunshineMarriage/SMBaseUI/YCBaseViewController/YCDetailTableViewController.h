//
//  YCTableViewDetailController.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/9/25.
//

#import "YCTableRootViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCDetailTableViewController : YCTableRootViewController

@property (nonatomic, assign) BOOL canEdit; //是否可编辑

@end

NS_ASSUME_NONNULL_END
