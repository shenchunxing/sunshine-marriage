//
//  YCTableViewControllerProtocol.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2020/1/1.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol YCTableViewControllerProtocol <NSObject>

/// 解析数据
/// @param response 返回的数据
- (void)resolvingReturnData:(id)response;

/// 配置item
/// @param items items
- (void)configurationItemsToSection:(NSArray <YCTableViewItem *> *)items;

/// push进入下一个控制器
/// @param parameter 参数
- (void)pushToViewController:(id)parameter;

@end

NS_ASSUME_NONNULL_END
