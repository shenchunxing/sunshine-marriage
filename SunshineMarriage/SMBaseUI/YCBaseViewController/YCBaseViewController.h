//
//  YCBaseViewController.h
//  YCBaseUI
//
//  Created by haima on 2019/4/2.
//

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>
#import "UIViewController+NavigationBarConfigure.h"
#import "UIViewController+BackgroundConfigure.h"
#import "YCAlertHUDPresnter.h"
#import <YYModel/YYModel.h>

@class YCNavigationBar;

NS_ASSUME_NONNULL_BEGIN

@interface YCBaseViewController : UIViewController<YCNavigationClickListenerProtocol>

@property (assign, nonatomic) BOOL navLineHidden;

@property (strong, nonatomic) YCNavigationBar *navigationBar;

@property (nonatomic, strong) YCAlertHUDPresnter *myPresenter;

- (void)initData;

/**
 初始化子视图
 */
- (void)initView;

/**
 加载数据
 */
- (void)loadData;

#pragma mark - 添加刷新控件
- (void)generatePullRefreshTableHeaderView:(UITableView *)tableView;
- (void)generateLoadMoreTableFooterView:(UITableView *)tableView;

#pragma mark - overwrite
- (void)headerRefreshAction;
- (void)footerLoadMoreAction;

- (void)setupNavigationBar;

- (void)config;

@end

NS_ASSUME_NONNULL_END
