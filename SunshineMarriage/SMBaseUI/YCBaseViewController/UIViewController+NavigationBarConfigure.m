//
//  UIViewController+NavigationBarConfigure.m
//  YCBaseUI
//
//  Created by haima on 2019/4/2.
//

#import "UIViewController+NavigationBarConfigure.h"
#import <objc/runtime.h>
#import "UIColor+Style.h"
#import "UIFont+Style.h"
#import "UIImage+Resources.h"
#import "SMDefine.h"
#import "NSString+Size.h"
#import "YCNavgationBarAdapter.h"

static const CGFloat kYCNBCMaxButtonWidth = 150.0f;
static const CGFloat kYCNBCMinButtonWidth = 60.0f;
static const CGFloat kYCNBCButtonImageWidth = 20.0f;

static const char kYCNavigationBarTypeKey;
static const char kYCLeftButtonKey;
static const char kYCRightButtonKey;

@implementation UIViewController (NavigationBarConfigure)
@dynamic yc_leftButton;
@dynamic yc_rightButton;

- (void)yc_setupNavigationBarType:(YCNavigationBarType)navigationBarType {
    
    switch (navigationBarType) {
        case YCNavigationBarTypeNormal:
            //白色导航栏
            self.yc_navigationBarStyle = YCNavigationBarStyleWhite;
            [self _tdf_setupNavigationBarWithStyle:YCNavigationBarStyleWhite];
                break;
        case YCNavigationBarTypeCustom:
            //蓝色主题导航栏
            self.yc_navigationBarStyle = YCNavigationBarStyleBlue;
            [self _tdf_setupNavigationBarWithStyle:YCNavigationBarStyleBlue];
            break;
        default:
            break;
    }
}


- (void)yc_updateStatusBarType:(YCNavigationBarStyle)style {
    
    self.yc_navigationBarStyle = style;
    switch (style) {
        case YCNavigationBarStyleWhite:
            //白色导航栏
            [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
            break;
        case YCNavigationBarStyleBlue:
            //蓝色主题导航栏
            [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
            break;
        default:
            break;
    }
}

- (void)setupNavigationBar{
    if(!self.navigationBar){
        self.navigationBar = [[YCNavigationBar alloc] initWithFrame:CGRectMake(0, 0, kWIDTH, kNavigationHeight)];
        self.navigationBar.backgroundColor = [UIColor whiteColor];
        self.navigationBar.yc_listener = self;
    }
    if(!self.navigationBar.superview){
        [self.view addSubview:self.navigationBar];
    }
}

- (void)yc_setupNavigationBarBackgroundColor:(UIColor *)color {
    
    self.navigationBar.backgroundColor = color;
}

//MARK:设置导航栏左侧项，只能显示图片
- (void)yc_setupLeftItemWithImage:(UIImage *)image {
    
    [self _yc_setupLeftItemWithTitle:nil image:image];
}

//MARK:设置导航栏左侧项，只能显示文字
- (void)yc_setupLeftItemWithTitle:(NSString *)title{
    
    [self _yc_setupLeftItemWithTitle:title image:nil];
}

//MARK:设置导航栏右侧项，只显示文字
- (void)yc_setupRightItemWithTitle:(NSString *)title {
    
    [self _yc_setupRightItemWithTitle:title image:nil];
}

- (void)yc_setupRightItemWithImage:(UIImage *)image {
    
    self.yc_rightButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    [self _yc_setupRightItemWithTitle:nil image:image];
}

- (void)yc_setupTitleView {
    
    // 设置导航栏标题颜色
    NSMutableDictionary *textAttributes = [NSMutableDictionary dictionary];
    textAttributes[NSForegroundColorAttributeName] = [UIColor yc_hex_121D32];
    textAttributes[NSFontAttributeName] = [UIFont yc_18];
    
    // 5.去除阴影
    NSShadow *shadow = [[NSShadow alloc]init];
    shadow.shadowOffset = CGSizeZero;
    textAttributes[NSShadowAttributeName] = shadow;
    [self.navigationController.navigationBar setTitleTextAttributes:textAttributes];
    self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void)yc_setupTitleViewWithTitle:(NSString *)title {
    
    self.navigationBar.title = title;
}

- (void)yc_setupTitleViewWithTitleColor:(UIColor *)titleColor {
    self.navigationBar.titleColor = titleColor;
}

- (void)yc_setTitle:(NSString *)title isRootVc:(BOOL)isRootVc{
    self.navigationBar.title = title;
    if (isRootVc) {
        [self.navigationBar.leftBarButton removeFromSuperview];
        self.navigationBar.leftBarButton = nil;
    }
}

- (void)yc_setupTitleWithCustomView:(UIView *)customView {
    
    [self.navigationBar.containerView addSubview:customView];
    CGSize size = customView.frame.size;
    CGRect frame = self.navigationBar.containerView.frame;
    CGFloat x = (frame.size.width - size.width)/2.0;
    customView.frame = CGRectMake(x, 0, size.width, size.height);
    
}

- (void)yc_setupTitleWithCustomizeView:(UIView *)customView {
    
    [self.navigationBar.containerView addSubview:customView];
    
}

- (void)setShowBottomBorder:(BOOL)showBottomBorder {
    self.navigationBar.bottomBorderHiden = !showBottomBorder;
}

#pragma mark - 设置导航栏
//MARK:设置导航栏风格样式
- (void)_tdf_setupNavigationBarWithStyle:(YCNavigationBarStyle)style {
    
    [self yc_updateStatusBarType:style];
    [[YCNavgationBarAdapter shareInstance] adapterNavigationBarStyle:style];
    [self _yc_setupNavigationBar:style];
    [self _yc_setupLeftItemWithTitle:nil image:[[YCNavgationBarAdapter shareInstance] adapterNavigationBarBackImageWithDefaultImage:[UIImage imageNamed:@"arrow_left"]]];
}

- (void)_yc_setupNavigationBar:(YCNavigationBarStyle)barStyle {
    
    if(!self.navigationBar){
        self.navigationBar = [[YCNavigationBar alloc] initWithFrame:CGRectMake(0, 0, kWIDTH, kNavigationHeight)];
        self.navigationBar.yc_listener = self;
    }
    self.navigationBar.backgroundColor = [self _yc_navigationBarColor:barStyle];
    self.navigationBar.titleColor = [self _yc_navigationBarTitleColor:barStyle];
    if(!self.navigationBar.superview){
        [self.view addSubview:self.navigationBar];
    }
    [self.view bringSubviewToFront:self.navigationBar];
}

//MARK:导航栏背景颜色
- (UIColor *)_yc_navigationBarColor:(YCNavigationBarStyle)barStyle {
    
    switch (barStyle) {
        case YCNavigationBarStyleWhite:
            return [UIColor whiteColor];
            break;
        case YCNavigationBarStyleBlue:
            return [UIColor yc_hex_5360FF];
            break;
    }
}

//MARK:导航栏标题颜色
- (UIColor *)_yc_navigationBarTitleColor:(YCNavigationBarStyle)barStyle {

    switch (barStyle) {
        case YCNavigationBarStyleWhite:
            return [UIColor yc_hex_333333];
            break;
        case YCNavigationBarStyleBlue:
            return [UIColor whiteColor];
            break;
    }
}

//MARK:导航栏左侧按钮
- (void)_yc_setupLeftItemWithTitle:(NSString *)title image:(UIImage *)image {
    
    if (title.length <= 0 && !image) {
        self.navigationBar.leftBarButton = nil;
        return;
    }
    self.yc_leftButton.hidden = NO;
    [self _yc_bindListner];
    [self _yc_updateNavigationButton:self.yc_leftButton withTitle:title image:image];
    self.navigationBar.leftBarButton = self.yc_leftButton;
}

//MARK:导航栏右侧按钮
- (void)_yc_setupRightItemWithTitle:(NSString *)title image:(UIImage *)image {
    if (title.length <= 0 && !image) {
        self.navigationBar.rightBarButton = nil;
        return;
    }
    self.yc_rightButton.hidden = NO;
    [self _yc_bindListner];
    [self _yc_updateNavigationButton:self.yc_rightButton withTitle:title image:image];
    self.navigationBar.rightBarButton = self.yc_rightButton;
}

- (void)_yc_updateNavigationButton:(UIButton *)button withTitle:(NSString *)title image:(UIImage *)image {
    [button setTitleColor:[self _yc_buttonTitleColorWithStyle:self.yc_navigationBarStyle] forState:UIControlStateNormal];
    [button setImage:image forState:UIControlStateNormal];
    [button setImage:image forState:UIControlStateHighlighted];
    [button setTitle:title forState:UIControlStateNormal];
    CGRect frame = button.frame;
    frame.size.width = MAX(MIN(kYCNBCMaxButtonWidth, [title yc_sizeForFont:[UIFont yc_15]].width + kYCNBCButtonImageWidth), kYCNBCMinButtonWidth);
    button.frame = frame;
}

- (UIColor *)_yc_buttonTitleColorWithStyle:(YCNavigationBarStyle)navigationBarStyle {
    switch (navigationBarStyle) {
        case YCNavigationBarStyleWhite: {
            return [[YCNavgationBarAdapter shareInstance] adapterNavigationBarButtonColorWithDefaultColor: [UIColor yc_hex_108EE9]];
        } break;
        case YCNavigationBarStyleBlue: {
            return [[YCNavgationBarAdapter shareInstance] adapterNavigationBarButtonColorWithDefaultColor:[UIColor whiteColor]];
        } break;
    }
    return nil;
}

#pragma mark - 绑定监听者
- (void)_yc_bindListner {
 
    if (!self.yc_listener) {
        self.yc_listener = self;
    }
}

#pragma mark - 导航栏按钮事件
//MARK:返回事件
- (void)_yc_viewControllerDidTriggerLeftClick:(UIButton *)button {
    
    //当前vc有实现返回方法执行自己的返回逻辑
    if ([self.yc_listener respondsToSelector:@selector(yc_viewControllerDidLeftClick:)]) {
        [self.yc_listener yc_viewControllerDidLeftClick:self];
        return;
    }
    //直接pop视图
    [self _yc_popViewController];
}

//MARK:右侧按钮事件
- (void)_yc_viewControllerDidTriggerRightClick:(UIButton *)button {
    
    if ([self.yc_listener respondsToSelector:@selector(yc_viewControllerDidRightClick:)]) {
        [self.yc_listener yc_viewControllerDidRightClick:self];
    }
}

//MARK:pop逻辑处理
- (void)_yc_popViewController {
    
    //执行自定义pop
    if ([self.yc_listener respondsToSelector:@selector(yc_viewControllerDidTriggerPopAction:)]) {
        [self.yc_listener yc_viewControllerDidTriggerPopAction:self];
        return;
    }
    //直接pop
    if ([self _yc_isModal]) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else if ([self.navigationController.viewControllers containsObject:self]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (BOOL)_yc_isModal {
    if ([self presentingViewController]) {
        NSArray *viewcontrollers = self.navigationController.viewControllers;
        if (viewcontrollers.count > 1) {
            if ([[viewcontrollers objectAtIndex:viewcontrollers.count - 1] isEqual:self]) {
                return NO;
            }
        } else {
            return YES;
        }
    }
    if ([[self presentingViewController] presentedViewController] == self) {
        return YES;
    }
    return [[[self tabBarController] presentingViewController] isKindOfClass:[UITabBarController class]];
}

#pragma mark - getter / setter

- (void)setYc_navigationBarStyle:(YCNavigationBarStyle)yc_navigationBarStyle {
    
    objc_setAssociatedObject(self, &kYCNavigationBarTypeKey, @(yc_navigationBarStyle), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
}

- (YCNavigationBarStyle)yc_navigationBarStyle {
    
    return [objc_getAssociatedObject(self, &kYCNavigationBarTypeKey) integerValue];
}


- (id<YCNavigationClickListenerProtocol>)yc_listener {
    return objc_getAssociatedObject(self, @selector(yc_listener));
}

- (void)setYc_listener:(id<YCNavigationClickListenerProtocol>)listener {
    objc_setAssociatedObject(self, @selector(yc_listener), listener, OBJC_ASSOCIATION_ASSIGN);
}

- (UIButton *)yc_leftButton {
    UIButton *leftButton = objc_getAssociatedObject(self, &kYCLeftButtonKey);
    if (!leftButton) {
        leftButton = [self _yc_generateNavigationButtonWithSelector:@selector(_yc_viewControllerDidTriggerLeftClick:)];
        leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        leftButton.imageEdgeInsets = UIEdgeInsetsMake(14, 0, 10, 0);
        leftButton.titleEdgeInsets = UIEdgeInsetsMake(13, 0, 11, 0);
        objc_setAssociatedObject(self, &kYCLeftButtonKey, leftButton, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return leftButton;
}

- (UIButton *)yc_rightButton {
    UIButton *rightButton = objc_getAssociatedObject(self, &kYCRightButtonKey);
    if (!rightButton) {
        rightButton = [self _yc_generateNavigationButtonWithSelector:@selector(_yc_viewControllerDidTriggerRightClick:)];
        rightButton.imageEdgeInsets = UIEdgeInsetsMake(7, 0, 11, 0);
        rightButton.titleEdgeInsets = UIEdgeInsetsMake(12, 0, 11, 0);
        rightButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        objc_setAssociatedObject(self, &kYCRightButtonKey, rightButton, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return rightButton;
}


#pragma mark - 生成按钮
- (UIButton *)_yc_generateNavigationButtonWithSelector:(SEL)selector {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0.0f, 0.0f, 80.f, 44.f);
    [button.titleLabel setFont:[UIFont yc_15]];
    [button addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    return button;
}

@end
