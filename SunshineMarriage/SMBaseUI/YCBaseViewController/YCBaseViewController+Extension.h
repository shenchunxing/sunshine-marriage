//
//  YCBaseViewController+Extension.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/12/15.
//
#import "YCBaseViewController.h"
#import "YCTabBar.h"
NS_ASSUME_NONNULL_BEGIN

@interface YCBaseViewController (Extension)
- (YCTabBar *)getTabBar;
@end

NS_ASSUME_NONNULL_END
