//
//  YCSearchBarViewController.m
//  YCFunctionModule
//
//  Created by 沈春兴 on 2019/9/25.
//

#import "YCSearchBarViewController.h"
#import "YCRefreshHeader.h"
#import <MJRefresh/MJRefresh.h>

@implementation YCSearchBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBar.hidden = YES;
}

- (void)initView{
    [super initView];
    [self.view addSubview:self.searchView];
    self.searchText = @"";
    [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.searchView.bottom);
    }];
    [self addRefreshLoadMore];
}

- (void)addRefreshLoadMore {
    @weakify(self);
       self.tableView.mj_header = [YCRefreshHeader headerWithRefreshingBlock:^{
           @strongify(self);
           self.currentPage = 1;
           [self getSearchListData];
       }];
       self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
           @strongify(self);
           self.currentPage++;
           [self getSearchListData];
       }];
       self.currentPage = 1;
       self.tableView.mj_footer.hidden = YES;
}

- (void)resolvingReturnData:(id)response {
   [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    NSArray *items = [NSArray yy_modelArrayWithClass:[NSClassFromString(self.registerItems.firstObject)  class] json:response];
    if (self.currentPage == 1) {
        [self.section removeAllItems];
    }
    if (items == nil || items.count < 10) {
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }
    [self configurationItemsToSection:items];
    self.tableView.mj_footer.hidden = items.count < 10;
}

- (void)configurationItemsToSection:(NSArray <YCTableViewItem *> *)items {
    for (YCTableViewItem *item in items) {
        @weakify(item);
        @weakify(self);
        [item setSelectItemHandler:^(NSIndexPath * _Nonnull indexPath) {
            @strongify(self);
            @strongify(item);
            [self pushToViewController:item];
        }];
        [self.section addItem:item];
    }
    [self.manager reloadData];
}

- (YCAlertHUDPresnter *)presenter {
    if (!_presenter) {
        _presenter = [YCAlertHUDPresnter HUDWithView:self.view];
    }
    return _presenter;
}

- (YCSearchView *)searchView{
    if (!_searchView) {
        @weakify(self);
        _searchView =  [[YCSearchView alloc]initWithFrame:CGRectMake(0,49, kWIDTH, 60) placeHolderStr:self.searchBar_placeHolder searchHandle:^(NSString * _Nonnull searchText) {
            @strongify(self)
            self.searchText = searchText;
            [self getSearchListData];
        }];
    }
    return _searchView;
}

- (NSString *)searchBar_placeHolder {
    return @"搜索客户姓名";
}


@end
