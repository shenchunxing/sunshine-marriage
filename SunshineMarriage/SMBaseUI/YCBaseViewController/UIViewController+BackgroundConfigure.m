//
//  UIViewController+BackgroundConfigure.m
//  YCBaseUI
//
//  Created by haima on 2019/4/2.
//

#import "UIViewController+BackgroundConfigure.h"
#import <objc/runtime.h>
#import "UIColor+Style.h"
#import <Masonry/Masonry.h>

@implementation UIViewController (BackgroundConfigure)

- (void)yc_setupBackgroundView {
    
    [self.view insertSubview:[self yc_backgroundView] atIndex:0];
    [self.yc_backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

- (void)setYc_backgroundView:(UIView *)backgroundView {
    objc_setAssociatedObject(self, _cmd, backgroundView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIView *)yc_backgroundView {
    if (!objc_getAssociatedObject(self, _cmd)) {
        UIView *view = [[UIView alloc] init];
        view.backgroundColor = [UIColor yc_hex_F4F4F4];
        objc_setAssociatedObject(self, _cmd, view, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return objc_getAssociatedObject(self, _cmd);
}
@end
