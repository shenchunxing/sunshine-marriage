//
//  YCTableRootViewController.m
//  YCFunctionModule
//
//  Created by 沈春兴 on 2019/9/25.
//

#import "YCTableRootViewController.h"
#import "UIScrollView+EmptyData.h"
@interface YCTableRootViewController ()<YCEmptyViewDelegate,YCEmptyViewDataSource>
@end
@implementation YCTableRootViewController
- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)initView{
    [super initView];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kNavigationHeight);
        make.left.right.equalTo(self.view);
        make.bottom.mas_equalTo(self.view.mas_bottomMargin);
    }];
}

#pragma mark - YCTableViewControllerProtocol
- (NSArray <NSString *> *)registerItems {
    return nil;
}

- (NSArray <YCTableViewSection *> *)addManagerSections {
    return @[self.section];
}

#pragma mark - Getter
- (YCTableViewManager *)manager {
    if (_manager == nil) {
        _manager = [[YCTableViewManager alloc] initWithTableView:self.tableView];
        _manager.managerDelegate = self;
        [_manager registerItems:[self registerItems]];
        [_manager addManagerSections:[self addManagerSections]];
    }
    return _manager;
}

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:[self tableViewStyle]];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [self tableViewBgColor];
        _tableView.tableHeaderView = [self tableHeaderView];
        _tableView.tableFooterView = [self tableFooterView];
        _tableView.emptyViewDelegate = self;
        _tableView.emptyViewDataSource = self;
    }
    return _tableView;
}

- (YCTableViewSection *)section {
    if (!_section) {
        _section = [YCTableViewSection section];
    }
    return _section;
}

- (UIColor *)tableViewBgColor {
    return [UIColor yc_hex_F5F6FA];
}

- (UITableViewStyle)tableViewStyle {
    return UITableViewStylePlain;
}

- (UIView *)tableHeaderView {
    return nil;
}

- (UIView *)tableFooterView {
    return nil;
}

@end
