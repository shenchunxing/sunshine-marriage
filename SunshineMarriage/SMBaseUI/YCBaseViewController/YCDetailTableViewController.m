//
//  YCTableViewDetailController.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/9/25.
//

#import "YCDetailTableViewController.h"

@interface YCDetailTableViewController ()

@end

@implementation YCDetailTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (UITableViewStyle)tableViewStyle {
    return UITableViewStyleGrouped;
}

- (UIColor *)tableViewBgColor {
    return [UIColor clearColor];
}

@end
