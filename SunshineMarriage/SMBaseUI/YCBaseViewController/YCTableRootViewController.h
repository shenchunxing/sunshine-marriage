//
//  YCTableRootViewController.h
//  YCFunctionModule
//
//  Created by 沈春兴 on 2019/9/25.
//

#import "YCBaseViewController.h"
#import "YCTableViewKit.h"
#import "SMDefine.h"
#import "UIColor+Style.h"
#import "YCTableViewConfigurationProtocol.h"
NS_ASSUME_NONNULL_BEGIN
@interface YCTableRootViewController : YCBaseViewController<YCTableViewManagerDelegate,YCTableViewConfigurationProtocol>

@property (nonatomic, strong) YCTableViewManager *manager;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) YCTableViewSection *section;
@end

NS_ASSUME_NONNULL_END
