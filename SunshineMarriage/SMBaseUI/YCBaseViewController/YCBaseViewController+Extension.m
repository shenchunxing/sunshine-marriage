//
//  YCBaseViewController+Extension.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/12/15.
//

#import "YCBaseViewController+Extension.h"
@implementation YCBaseViewController (Extension)
- (YCTabBar *)getTabBar {
    return self.tabBarController.tabBar.subviews[1];
}
@end
