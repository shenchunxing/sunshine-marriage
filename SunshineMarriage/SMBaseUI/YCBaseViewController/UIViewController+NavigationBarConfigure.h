//
//  UIViewController+NavigationBarConfigure.h
//  YCBaseUI
//
//  Created by haima on 2019/4/2.
//

#import <UIKit/UIKit.h>
#import "YCNavigationBar.h"
#import "YCNavgationBarAdapter.h"

//导航栏类型
typedef NS_ENUM(NSUInteger, YCNavigationBarType) {
    YCNavigationBarTypeNormal,       //普通,默认白色
    YCNavigationBarTypeCustom,       //自定义类型
};


@interface UIViewController (NavigationBarConfigure)<YCNavigationClickListenerProtocol>

/* 导航事件监听者 */
@property (nonatomic, weak) id<YCNavigationClickListenerProtocol> yc_listener;

/* 导航栏左侧按钮 */
@property (nonatomic, strong) UIButton *yc_leftButton;
/* 导航栏右侧按钮 */
@property (nonatomic, strong) UIButton *yc_rightButton;

/* 导航栏风格 */
@property (nonatomic, assign) YCNavigationBarStyle yc_navigationBarStyle;

@property (nonatomic,strong) YCNavigationBar *navigationBar;

/**
 设置导航栏类型

 @param navigationBarType 类型
 */
- (void)yc_setupNavigationBarType:(YCNavigationBarType)navigationBarType;
- (void)bs_setupRedNavigationBarType;

/**
 更新状态栏文字颜色

 @param style 导航栏风格
 */
- (void)yc_updateStatusBarType:(YCNavigationBarStyle)style;


/**
 设置返回导航栏类型
 */
- (void)yc_setupLeftItemWithImage:(UIImage *)image;

/**
 设置确定导航栏类型
 */
- (void)yc_setupRightItemWithTitle:(NSString *)title;


/**
 设置titleView
 */
- (void)yc_setupTitleView ;

//MARK:设置导航栏左侧项，只能显示文字
- (void)yc_setupLeftItemWithTitle:(NSString *)title;

- (void)yc_setupRightItemWithImage:(UIImage *)image;

- (void)yc_setupTitleViewWithTitle:(NSString *)title;

- (void)yc_setupNavigationBarBackgroundColor:(UIColor *)color;

- (void)yc_setupTitleWithCustomView:(UIView *)customView;
- (void)yc_setupTitleWithCustomizeView:(UIView *)customView;

- (void)yc_setupTitleViewWithTitleColor:(UIColor *)titleColor;
/**
 是否隐藏左侧返回按钮
 */
- (void)yc_setTitle:(NSString *)title isRootVc:(BOOL)isRootVc;

- (void)setShowBottomBorder:(BOOL)showBottomBorder;

@end
