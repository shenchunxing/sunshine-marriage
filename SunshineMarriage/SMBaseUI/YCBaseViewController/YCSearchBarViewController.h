//
//  YCSearchBarViewController.h
//  YCFunctionModule
//
//  Created by 沈春兴 on 2019/9/25.
//

#import "YCTableRootViewController.h"
#import "YCSearchView.h"
#import "YCDataCenter.h"
#import "UIView+Extension.h"
#import "YCSearchViewControllerProtocol.h"
#import "YCTableViewControllerProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCSearchBarViewController : YCTableRootViewController<YCSearchViewControllerProtocol,YCTableViewControllerProtocol>
/// 状态码
@property (nonatomic, copy) NSString *status;
/// 当前页
@property (nonatomic, assign) NSUInteger currentPage;

@property (nonatomic, strong) YCAlertHUDPresnter *presenter;
/// 搜索
@property (nonatomic, strong) YCSearchView *searchView;

@property (nonatomic, copy) NSString *searchText;

@end

NS_ASSUME_NONNULL_END
