//
//  YCTableViewConfigurationProtocol.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/10/28.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol YCTableViewConfigurationProtocol <NSObject>
@optional

//UI
- (UIColor *)tableViewBgColor; // 背景色
- (UITableViewStyle)tableViewStyle; //风格
- (UIView *)tableHeaderView; //头部
- (UIView *)tableFooterView; //尾部
- (CGRect)footerFrame;

//items
- (NSArray <NSString *> *)registerItems;

//sections
- (NSArray <YCTableViewSection *> *)addManagerSections;

@end

NS_ASSUME_NONNULL_END
