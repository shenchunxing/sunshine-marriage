//
//  YCTabBarManager.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/12/15.
//

#import "YCTabBarManager.h"
#import "YCBaseViewController.h"
#import "UIView+Extension.h"
#import "YCBaseViewController+Extension.h"
#import "NSObject+Helper.h"

@interface YCTabBarManager ()
@property (nonatomic, strong) YCTabBar *tabBar;
@end

static YCTabBarManager *instance = nil;
@implementation YCTabBarManager
+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (UIView *)tabbar_hitTest:(CGPoint)point withEvent:(UIEvent *)event view:(UIView *)view{
    if (self.showNoticeTips) {
        CGPoint pointInCenterBtn = [self.tabBar convertPoint:point toView:self.tipsView];
        if ([self.tipsView pointInside:pointInCenterBtn withEvent:event]) {
            return self.tipsView;
        }
    }
    return view;
}

-  (BSMessageTips *)tipsView {
    if (!_tipsView) {
        _tipsView = [[BSMessageTips alloc] initWithFrame:CGRectMake(0, 0, 125, 41)];
        _tipsView.center = CGPointMake(self.tabBar.width* 5/8 , self.tabBar.center.y - 51);
    }
    return _tipsView;
}

- (void)setNoticeNumber:(NSInteger)number {
    if (number <=0) {
        [self.tipsView removeFromSuperview];
        return;
    }
    self.showNoticeTips = number > 0;
    [self.tabBar addSubview:self.tipsView];
    self.tipsView.noticesNum = number;
}

- (void)addRedPointToTabbarSelectedIndex:(NSInteger)index {
    if (index < self.tabBar.tabBarItems.count) {
        YCTabBarItem *todoItem = self.tabBar.tabBarItems[index];
        todoItem.showRedPoint = YES;
    }
}

- (void)removeRedPointToTabbarSelectedIndex:(NSInteger)index {
    if (index < self.tabBar.tabBarItems.count) {
           YCTabBarItem *todoItem = self.tabBar.tabBarItems[index];
           todoItem.showRedPoint = NO;
       }
}

- (YCTabBar *)tabBar {
    if (!_tabBar) {
        _tabBar = [self yc_getCurrentViewController].tabBarController.tabBar.subviews[1];
        _tabBar.hit_Delegate = self;
    }
    return _tabBar;
}

@end
