//
//  YCTableViewController.h
//  Pods-YCHomeModule_Example
//
//  Created by 沈春兴 on 2019/4/2.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, YCTabBarItemType) {
	YCTabBarItemNormal = 0,
	YCTabBarItemRise,
};

extern NSString *const kYCTabBarItemAttributeTitle;// NSString
extern NSString *const kYCTabBarItemAttributeNormalImageName;// NSString
extern NSString *const kYCTabBarItemAttributeSelectedImageName;// NSString
extern NSString *const kYCTabBarItemAttributeType;// NSNumber, YCTabBarItemType

@interface YCTabBarItem : UIButton

@property (nonatomic, assign) YCTabBarItemType tabBarItemType;

@property (nonatomic,assign) NSInteger todoNumber;
@property (nonatomic, assign) BOOL showRedPoint;

@end
