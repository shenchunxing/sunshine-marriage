//
//  YCTableViewController.h
//  Pods-YCHomeModule_Example
//
//  Created by 沈春兴 on 2019/4/2.
//

#import "YCTabBarItem.h"
#import <Masonry/Masonry.h>
#import "YCCategoryModule.h"
#import "UIColor+Style.h"
#import "UIFont+Style.h"

NSString *const kYCTabBarItemAttributeTitle = @"YCTabBarItemAttributeTitle";
NSString *const kYCTabBarItemAttributeNormalImageName = @"YCTabBarItemAttributeNormalImageName";
NSString *const kYCTabBarItemAttributeSelectedImageName = @"YCTabBarItemAttributeSelectedImageName";
NSString *const kYCTabBarItemAttributeType = @"YCTabBarItemAttributeType";

@interface YCTabBarItem ()
@property (nonatomic,strong) UILabel *numberLabel;
@property (nonatomic, strong) UIView *redView;

@end

@implementation YCTabBarItem

- (instancetype)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	
	if (self) {
		[self config];
	}
	
	return self;
}

- (instancetype)init {
	self = [super init];
	
	if (self) {
		[self config];
	}
	
	return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	
	if (self) {
		[self config];
	}
	
	return self;
}

- (void)config {
	self.adjustsImageWhenHighlighted = NO;
	self.imageView.contentMode = UIViewContentModeScaleAspectFit;
}

- (void)layoutSubviews {
	[super layoutSubviews];
	
    self.imageView.frame = CGRectMake((self.width - 22)/2, 7, 22, 22);
    self.titleLabel.frame = CGRectMake(0, self.imageView.bottom+2, self.width, 16);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    //增大触摸范围
    if(self.tabBarItemType == YCTabBarItemRise){
        self.height = 25+49;
        self.top = -25;
       self.imageView.frame = CGRectMake((self.width - 50)/2, 0, 50, 50);
       self.titleLabel.frame = CGRectMake(0, self.imageView.bottom+5, self.width, 16);
    }
}

- (UILabel *)numberLabel{
    if (!_numberLabel) {
        _numberLabel = [UILabel yc_labelWithText:@"8" textFont:[UIFont yc_10] textColor:[UIColor whiteColor] textAlignment:NSTextAlignmentCenter];
        _numberLabel.backgroundColor = [UIColor yc_hex_FF3B30];
        _numberLabel.layer.cornerRadius = 7.5;
        _numberLabel.layer.borderWidth = 1;
        _numberLabel.layer.borderColor = [UIColor whiteColor].CGColor;
        _numberLabel.layer.masksToBounds = YES;
    }
    return _numberLabel;
}


- (void)setTodoNumber:(NSInteger)todoNumber{
    _todoNumber = todoNumber;
    if (todoNumber<=0) {
        if (self.numberLabel.superview) {
            [self.numberLabel removeFromSuperview];
            self.numberLabel = nil;
        }
        return;
    }
    else{
        [self addSubview:self.numberLabel];
        self.numberLabel.text = [NSString stringWithFormat:@"%ld",todoNumber];
        if (todoNumber<10){
            self.numberLabel.frame = CGRectMake(self.imageView.right - 5, 2, 15, 15);
        }else if (todoNumber<=99){
            self.numberLabel.frame = CGRectMake(self.imageView.right - 5, 2, 20, 15);
        }else{
            self.numberLabel.text = @"99+";
            self.numberLabel.frame = CGRectMake(self.imageView.right - 5, 2, 26, 15);
        }
    }
}

- (void)setShowRedPoint:(BOOL)showRedPoint {
    _showRedPoint = showRedPoint;
    if (showRedPoint) {
        [self addSubview:self.redView];
    }else{
        if (self.redView.superview) {
            [self.redView removeFromSuperview];
            self.redView = nil;
        }
    }
}

- (UIView *)redView {
    if (!_redView) {
        _redView = [[UIView alloc] init];
        _redView.backgroundColor = [UIColor yc_hex_F8513B];
        _redView.frame = CGRectMake(self.imageView.right - 5, 2, 10, 10);
        _redView.layer.cornerRadius = 5;
        _redView.layer.borderWidth = 1;
        _redView.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    return _redView;
}

@end
