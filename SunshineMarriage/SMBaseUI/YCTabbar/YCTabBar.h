//
//  YCTabBar.h
//  YCRiseTabBarDemo
//
//  Created by Meilbn on 10/18/15.
//  Copyright © 2015 meilbn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YCTabBarItem.h"
#import "BSMessageTips.h"
@class YCTabBar;
@protocol YCTabBarDelegate <NSObject>

@optional
- (void)tabBarDidSelectedRiseButton;
//提供外部修改item样式
- (void)tabBar:(YCTabBar *)tabBar barItem:(YCTabBarItem *)item atIndex:(NSInteger)index;
- (BOOL)tabBar:(YCTabBar *)tabBar shouldSelectItemAtIndex:(NSInteger)index;
@end

@protocol YCTabBarHitTestDelegate <NSObject>
- (UIView *)tabbar_hitTest:(CGPoint)point withEvent:(UIEvent *)event view:(UIView *)view;
@end

@interface YCTabBar : UIView

@property (nonatomic, assign) BOOL showNoticeTips;
@property (nonatomic, copy) NSArray<NSDictionary *> *tabBarItemAttributes;
@property (nonatomic, weak) id <YCTabBarDelegate> delegate;
@property (nonatomic, weak) id <YCTabBarHitTestDelegate> hit_Delegate;
@property (strong, nonatomic) NSMutableArray *tabBarItems;
//@property (nonatomic, strong) BSMessageTips *tipsView;

- (void)setSelectedIndex:(NSInteger)index;
- (void)setToDoNumber:(NSInteger)todoNumber AtIndex:(NSInteger)index;
//- (void)addRedPoint:(NSInteger)index;
//- (void)setNoticeNumber:(NSInteger)number;
@end
