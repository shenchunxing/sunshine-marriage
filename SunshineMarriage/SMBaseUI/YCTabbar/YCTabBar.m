//
//  YCTabBar.m
//  YCRiseTabBarDemo
//
//  Created by Meilbn on 10/18/15.
//  Copyright © 2015 meilbn. AYC rights reserved.
//

#import "YCTabBar.h"
#import "YCCategoryModule.h"
#import "UIColor+Style.h"
#import "UIFont+Style.h"

#import "YCDataCenter.h"
#import "YCSystemConfigure.h"
#import "YCSystemConfigure+UIColor.h"
#import "SMDefine.h"
@interface YCTabBar ()

@property (nonatomic,strong) UILabel *numberLabel;
@property (nonatomic, strong) YCTabBarItem *item;

@end

@implementation YCTabBar

#pragma mark - Lifecycle

- (instancetype)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	
	if (self) {
		[self config];
        
        self.layer.shadowColor = [UIColor yc_hex_tabbarShadow].CGColor;
        self.layer.shadowOffset = CGSizeMake(0,-3);
        self.layer.shadowOpacity = 0.4;
        self.layer.shadowRadius = 3;
	}
	
	return self;
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
    UIView *view = [super hitTest:point withEvent:event];
    if (!self.isHidden) {
        CGPoint pointInCenterBtn = [self convertPoint:point toView:self.item];
        if ([self.item pointInside:pointInCenterBtn withEvent:event]) {
            return self.item;
        }
//        if (self.showNoticeTips) {
//            CGPoint pointInCenterBtn = [self convertPoint:point toView:self.tipsView];
//            if ([self.tipsView pointInside:pointInCenterBtn withEvent:event]) {
//                return self.tipsView;
//            }
//        }
        if (self.hit_Delegate && [self.hit_Delegate respondsToSelector:@selector(tabbar_hitTest:withEvent:view:)]) {
           return [self.hit_Delegate tabbar_hitTest:point withEvent:event view:view];
        }
        return view;
    }
    return view;
    
}



#pragma mark - Private Method

- (void)config {
    self.backgroundColor = [UIColor whiteColor];
    
    if ([YCSystemConfigure sharedInstance].systemType == YCPartner) {
        UIImageView *bImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, -38, self.size.width, 87)];
        bImageView.image = [UIImage yc_loginImgWithName:@"img_huxing"];
        [self addSubview:bImageView];
    }
}

- (void)setSelectedIndex:(NSInteger)index {
    
    BOOL havePermission = YES;
    if (self.delegate && [self.delegate respondsToSelector:@selector(tabBar:shouldSelectItemAtIndex:)]) {
       havePermission = [self.delegate tabBar:self shouldSelectItemAtIndex:index];
    }
    if (!havePermission) {
        return;
    }
	for (YCTabBarItem *item in self.tabBarItems) {
		if (item.tag == index) {
			item.selected = YES;
		} else {
			item.selected = NO;
		}
	}
	
	UIWindow *keyWindow = [[[UIApplication sharedApplication] delegate] window];
	UITabBarController *tabBarControYCer = (UITabBarController *)keyWindow.rootViewController;
	if (tabBarControYCer) {
		tabBarControYCer.selectedIndex = index;
	}
}

#pragma mark - Touch Event

- (void)itemSelected:(YCTabBarItem *)sender {
	if (sender.tabBarItemType != YCTabBarItemRise) {
		[self setSelectedIndex:sender.tag];
	} else {
		if (self.delegate) {
			if ([self.delegate respondsToSelector:@selector(tabBarDidSelectedRiseButton)]) {
				[self.delegate tabBarDidSelectedRiseButton];
			}
		}
	}
}

#pragma mark - Setter

- (void)setTabBarItemAttributes:(NSArray<NSDictionary *> *)tabBarItemAttributes {
    _tabBarItemAttributes = tabBarItemAttributes.copy;
    
    CGFloat itemWidth =  [UIScreen mainScreen].bounds.size.width / _tabBarItemAttributes.count;
    CGFloat tabBarHeight = CGRectGetHeight(self.frame);

    __block NSInteger itemTag = 0;
    __block BOOL passedRiseItem = NO;
    self.tabBarItems = [NSMutableArray arrayWithCapacity:tabBarItemAttributes.count];
    [_tabBarItemAttributes enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        YCTabBarItemType type = [obj[kYCTabBarItemAttributeType] integerValue];
        CGRect frame = CGRectMake(itemTag * itemWidth + (passedRiseItem ? itemWidth : 0), 0, itemWidth, tabBarHeight);
        
        YCTabBarItem *tabBarItem = [self tabBarItemWithFrame:frame
                                                       title:obj[kYCTabBarItemAttributeTitle]
                                             normalImageName:obj[kYCTabBarItemAttributeNormalImageName]
                                           selectedImageName:obj[kYCTabBarItemAttributeSelectedImageName] tabBarItemType:type];
        if (self.delegate && [self.delegate respondsToSelector:@selector(tabBar:barItem:atIndex:)]) {
            [self.delegate tabBar:self barItem:tabBarItem atIndex:idx];
        }
        if (itemTag == 0) {
            tabBarItem.selected = YES;
        }
        
        [tabBarItem addTarget:self action:@selector(itemSelected:) forControlEvents:UIControlEventTouchUpInside];
        
        if (tabBarItem.tabBarItemType != YCTabBarItemRise) {
            tabBarItem.tag = itemTag;
            itemTag++;
        } else {
            passedRiseItem = YES;
            self.item = tabBarItem;
        }
        
        [self.tabBarItems addObject:tabBarItem];
        [self addSubview:tabBarItem];
    }];
}

- (YCTabBarItem *)tabBarItemWithFrame:(CGRect)frame title:(NSString *)title normalImageName:(NSString *)normalImageName selectedImageName:(NSString *)selectedImageName tabBarItemType:(YCTabBarItemType)tabBarItemType {
    YCTabBarItem *item = [[YCTabBarItem alloc] initWithFrame:frame];
    [item setTitle:title forState:UIControlStateNormal];
    [item setTitle:title forState:UIControlStateSelected];
    item.titleLabel.font = [UIFont yc_11];
    UIImage *normalImage = [UIImage yc_homeImgWithName:normalImageName];
    UIImage *selectedImage =  [UIImage yc_homeImgWithName:selectedImageName];
    [item setImage:normalImage forState:UIControlStateNormal];
    [item setImage:selectedImage forState:UIControlStateSelected];
    [item setTitleColor:[UIColor yc_hex_9B9EA8] forState:UIControlStateNormal];
    item.tabBarItemType = tabBarItemType;
    
    if (item.tabBarItemType != YCTabBarItemRise) {
        [item setTitleColor:[YCSystemConfigure mainColor] forState:UIControlStateSelected];
    }
    
    return item;
}



- (void)setToDoNumber:(NSInteger)todoNumber AtIndex:(NSInteger)index {
    
    if (index < _tabBarItems.count) {
        YCTabBarItem *todoItem = _tabBarItems[index];
        todoItem.todoNumber = todoNumber;
    }
}


@end
