//
//  YCTabBarManager.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/12/15.
//

#import <Foundation/Foundation.h>
#import "YCTabBar.h"
#import "BSMessageTips.h"
@class YCBaseViewController;
NS_ASSUME_NONNULL_BEGIN
@interface YCTabBarManager : NSObject<YCTabBarHitTestDelegate>
@property (nonatomic, strong) BSMessageTips *tipsView;
@property (nonatomic, assign) BOOL showNoticeTips;

+ (instancetype)sharedInstance;
- (void)setNoticeNumber:(NSInteger)number;
- (void)addRedPointToTabbarSelectedIndex:(NSInteger)index;
- (void)removeRedPointToTabbarSelectedIndex:(NSInteger)index;

@end

NS_ASSUME_NONNULL_END
