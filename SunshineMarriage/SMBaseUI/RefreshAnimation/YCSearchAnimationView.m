//
//  YCSearchAnimationView.m
//  YCBaseUI
//
//  Created by shenweihang on 2019/11/28.
//

#import "YCSearchAnimationView.h"
#import <Masonry/Masonry.h>
#import "YCCategoryModule.h"
#import "SMDefine.h"

@interface YCSearchAnimationView ()

@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) UIImageView *topImageView;
@property (nonatomic, strong) UIImageView *underImageView;

@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UIButton *leftbtn;
@property (nonatomic, strong) UIButton *rightBtn;

@end
@implementation YCSearchAnimationView

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.containerView];
        [self.containerView addSubview:self.underImageView];
        [self.containerView addSubview:self.topImageView];
        [self.containerView addSubview:self.titleLbl];
        [self addSubview:self.leftbtn];
        [self addSubview:self.rightBtn];
        [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.equalTo(self);
            make.height.mas_equalTo(294);
        }];
        [self.underImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(28);
            make.centerX.equalTo(self.containerView);
            make.size.mas_equalTo(CGSizeMake(206, 206));
        }];
        [self.topImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(40, 40));
            make.centerY.equalTo(self.underImageView);
            make.centerX.equalTo(self.underImageView.mas_centerX).offset(20);
        }];
        [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.underImageView.mas_bottom).offset(8);
            make.leading.offset(14);
            make.trailing.offset(-14);
            make.height.mas_equalTo(20);
        }];
        [self.leftbtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.containerView.mas_bottom).offset(32);
            make.leading.mas_equalTo(27);
            make.size.mas_equalTo(CGSizeMake(148, 44));
        }];
        [self.rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.trailing.offset(-27);
            make.size.mas_equalTo(CGSizeMake(148, 44));
            make.centerY.equalTo(self.leftbtn);
        }];
        [self showAnimation];
    }
    return self;
}

- (void)showAnimation {

    CAKeyframeAnimation *pathAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    //在动画设置一些变量
    pathAnimation.calculationMode = kCAAnimationPaced;
    //我们希望动画持续
    //如果我们动画从左到右的东西——我们想要呆在新位置,
    //然后我们需要这些参数
    pathAnimation.fillMode = kCAFillModeForwards;
    pathAnimation.removedOnCompletion = NO;
    pathAnimation.duration = 2.5;//完成动画的时间
    //让循环连续演示
    pathAnimation.repeatCount = MAXFLOAT;
    //设置的路径动画
    CGMutablePathRef curvedPath = CGPathCreateMutable();
    //起始位置
    CGFloat offset = 5.0;
    CGFloat leftMargin = (kWIDTH-206)/2;
    CGFloat topMargin = 28.0;
    CGPathMoveToPoint(curvedPath, NULL, leftMargin+138+offset, 122+topMargin+offset);
    
    CGPathAddRoundedRect(curvedPath, NULL, CGRectMake(leftMargin+72+offset, 79+topMargin+offset, 66, 86), 33, 43);
    //现在我们的路径,我们告诉动画我们想使用这条路径,那么我们发布的路径
    pathAnimation.path = curvedPath;
    CGPathRelease(curvedPath);
    
    //添加动画circleView——一旦你添加动画层,动画开始
    [self.topImageView.layer addAnimation:pathAnimation forKey:@"moveTheCircle"];
}

- (void)onLeftBtnClick:(UIButton *)btn {
    
    if (self.rightHandler) {
        self.rightHandler();
    }
}

- (void)onRightBtnClick:(UIButton *)btn {
    
    btn.enabled = NO;
    btn.layer.borderColor = [UIColor yc_hex_9B9EA8].CGColor;
    //刷新间隔时间3秒
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        btn.enabled = YES;
        btn.layer.borderColor = [UIColor yc_hex_F8513B].CGColor;
    });
    if (self.leftHandler) {
        self.leftHandler();
    }
}

#pragma mark - getter
- (UIView *)containerView {
    
    if (!_containerView) {
        _containerView = [[UIView alloc] init];
        _containerView.backgroundColor = [UIColor whiteColor];
    }
    return _containerView;
}

- (UIImageView *)topImageView {

    if (!_topImageView) {
        _topImageView = [[UIImageView alloc] init];
        _topImageView.image = [UIImage yc_BaseImgWithName:@"image_search_top"];
    }
    return _topImageView;
}
- (UIImageView *)underImageView {

    if (!_underImageView) {
        _underImageView = [[UIImageView alloc] init];
        _underImageView.image = [UIImage yc_BaseImgWithName:@"image_search_under"];
    }
    return _underImageView;
}
- (UILabel *)titleLbl {
    
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc] init];
        _titleLbl.font = [UIFont yc_18];
        _titleLbl.textColor = [UIColor yc_hex_5B6071];
        _titleLbl.textAlignment = NSTextAlignmentCenter;
        _titleLbl.text = @"查询中...";
    }
    return _titleLbl;
}
- (UIButton *)rightBtn {
    
    if (!_rightBtn) {
        _rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_rightBtn setTitle:@"刷新" forState:UIControlStateNormal];
        [_rightBtn setTitle:@"已刷新" forState:UIControlStateDisabled];
        [_rightBtn setTitleColor:[UIColor yc_hex_F8513B] forState:UIControlStateNormal];
        [_rightBtn setTitleColor:[UIColor yc_hex_9B9EA8] forState:UIControlStateDisabled];
        _rightBtn.titleLabel.font = [UIFont yc_16];
        _rightBtn.layer.cornerRadius = 22.0;
        _rightBtn.layer.borderColor = [UIColor yc_hex_F8513B].CGColor;
        _rightBtn.layer.borderWidth = kLineHeight;
        [_rightBtn addTarget:self action:@selector(onRightBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rightBtn;
}

- (UIButton *)leftbtn {
    
    if (!_leftbtn) {
        _leftbtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_leftbtn setTitle:@"返回" forState:UIControlStateNormal];
        [_leftbtn setTitleColor:[UIColor yc_hex_F8513B] forState:UIControlStateNormal];
        _leftbtn.titleLabel.font = [UIFont yc_16];
        _leftbtn.layer.cornerRadius = 22.0;
        _leftbtn.layer.cornerRadius = 22.0;
        _leftbtn.layer.borderColor = [UIColor yc_hex_F8513B].CGColor;
        _leftbtn.layer.borderWidth = kLineHeight;
        [_leftbtn addTarget:self action:@selector(onLeftBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _leftbtn;
}


@end
