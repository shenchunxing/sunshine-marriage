//
//  YCRefreshHeader.h
//  YCFunctionModule
//
//  Created by haima on 2019/4/22.
//

#import "MJRefreshHeader.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCRefreshHeader : MJRefreshHeader

/* 图视图 */
@property (nonatomic, strong) UIView *animationView;

@end

NS_ASSUME_NONNULL_END
