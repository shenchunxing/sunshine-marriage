//
//  YCSearchAnimationView.h
//  YCBaseUI
//
//  Created by shenweihang on 2019/11/28.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YCSearchAnimationView : UIView

/* <#回调#> */
@property (nonatomic, copy) void(^leftHandler)(void);
/* <#回调#> */
@property (nonatomic, copy) void(^rightHandler)(void);

@end

NS_ASSUME_NONNULL_END
