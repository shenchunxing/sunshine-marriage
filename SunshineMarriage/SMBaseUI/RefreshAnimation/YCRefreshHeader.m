//
//  YCRefreshHeader.m
//  YCFunctionModule
//
//  Created by haima on 2019/4/22.
//

#import "YCRefreshHeader.h"
#import "YCBaseUI.h"
#import "SMDefine.h"
#import "YCDataCenter.h"
#import "YCCategoryModule.h"

@interface YCRefreshHeader ()
/* 容器 */
@property (nonatomic, strong) UIView *containerView;
/* 灰色背景圆环 */
@property (nonatomic, strong) CAShapeLayer *bgLayer;
/* 下拉绘制圆环 */
@property (nonatomic, strong) CAShapeLayer *circleLayer;
/* 刷新动画圆环 */
@property (nonatomic, strong) CAShapeLayer *refreshingLayer;
@end

@implementation YCRefreshHeader



- (void)prepare {
    [super prepare];
    UIBezierPath *circlePath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, 22, 22)];
    self.bgLayer.path = circlePath.CGPath;
    self.circleLayer.path = circlePath.CGPath;
    self.refreshingLayer.path = circlePath.CGPath;
}

- (void)placeSubviews {
    [super placeSubviews];
    
    self.containerView.mj_x = (self.mj_w - 22) / 2.0;
    self.containerView.mj_y = 22;
    self.containerView.mj_w = 22;
    self.containerView.mj_h = 22;
}

- (void)scrollViewContentOffsetDidChange:(NSDictionary *)change {
    [super scrollViewContentOffsetDidChange:change];
    
    CGFloat offsetY = self.scrollView.mj_offsetY;
    [self setProgress:-offsetY];
}

- (void)setProgress:(CGFloat)progress{
    
    if (progress <= 0) {
        return;
    }
    CGFloat end = 0;
    CGFloat offset = 32;
    if (progress >= offset) {
        end = ((progress-offset/MJRefreshHeaderHeight-offset)<1.0)?(progress/MJRefreshHeaderHeight):1.0;
    }
    self.circleLayer.strokeEnd = end;
}

- (void)setState:(MJRefreshState)state {
    
    MJRefreshCheckState
    // 根据状态做事情
    if (state == MJRefreshStateIdle) {
        if (oldState == MJRefreshStateRefreshing) {

            // 如果执行完动画发现不是idle状态，就直接返回，进入其他状态
            if (self.state != MJRefreshStateIdle) return;
            [self stopAnimation];
        } else {
            [self stopAnimation];
        }
    } else if (state == MJRefreshStatePulling) {
        [self stopAnimation];

    } else if (state == MJRefreshStateRefreshing) {

        [self startAnimation];
    }
}

- (void)startAnimation {
    
    self.circleLayer.hidden = YES;
    self.refreshingLayer.hidden = NO;
    [self addRotationAniToLayer:self.containerView.layer];
}

- (void)addRotationAniToLayer:(CALayer *)layer {
    
    CABasicAnimation *rotationAni = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAni.fromValue = @(0);
    rotationAni.toValue = @(M_PI * 2);
    rotationAni.duration = 1.0;
    rotationAni.repeatCount = HUGE;
    rotationAni.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    rotationAni.fillMode = kCAFillModeForwards;
    rotationAni.removedOnCompletion = NO;
    [layer addAnimation:rotationAni forKey:@"rotationAni"];
}

- (void)stopAnimation {
    
    self.refreshingLayer.hidden = YES;
    [self.containerView.layer removeAllAnimations];
    [self resetCircleLayer];
}

- (void)resetCircleLayer {
    self.circleLayer.hidden = NO;
    self.circleLayer.strokeStart = 0.0;
    self.circleLayer.strokeEnd = 0.0;
}

- (CAShapeLayer *)bgLayer {
    
    if (_bgLayer == nil) {
        _bgLayer = [CAShapeLayer layer];
        //线条大小
        _bgLayer.lineWidth = 2.f;
        //线条颜色
        _bgLayer.strokeColor = [UIColor yc_hex_CCCCCC].CGColor;
        //路径开始位置
        _bgLayer.strokeStart = 0.f;
        //路径结束位置
        _bgLayer.strokeEnd = 1.f;
        _bgLayer.fillColor = [UIColor clearColor].CGColor;
        _bgLayer.position = self.containerView.center;
        [self.containerView.layer addSublayer:_bgLayer];
    }
    return _bgLayer;
}

- (CAShapeLayer *)circleLayer {
    
    if (_circleLayer == nil) {
        _circleLayer = [CAShapeLayer layer];
        //线条大小
        _circleLayer.lineWidth = 2.f;
        //线条颜色
        _circleLayer.strokeColor = ([YCDataCenter isCarDealer] ? [UIColor yc_hex_397AE6] : [UIColor yc_hex_108EE9]).CGColor;
        _circleLayer.lineCap = @"round";
        //路径开始位置
        _circleLayer.strokeStart = 0.f;
        //路径结束位置
        _circleLayer.strokeEnd = 0.f;
        _circleLayer.fillColor = [UIColor clearColor].CGColor;
        [self.containerView.layer addSublayer:_circleLayer];
    }
    return _circleLayer;
}

- (CAShapeLayer *)refreshingLayer {
    
    if (_refreshingLayer == nil) {
        _refreshingLayer = [CAShapeLayer layer];
        //线条大小
        _refreshingLayer.lineWidth = 2.f;
        //线条颜色
        _refreshingLayer.strokeColor = ([YCDataCenter isCarDealer] ? [UIColor yc_hex_397AE6] : [UIColor yc_hex_108EE9]).CGColor;
        _refreshingLayer.lineCap = @"round";
        //路径开始位置
        _refreshingLayer.strokeStart = 0.f;
        //路径结束位置
        _refreshingLayer.strokeEnd = 0.1f;
        _refreshingLayer.fillColor = [UIColor clearColor].CGColor;
        _refreshingLayer.hidden = YES;
        [self.containerView.layer addSublayer:_refreshingLayer];
    }
    return _refreshingLayer;
}

- (UIView *)containerView {
    
    if (_containerView == nil) {
        _containerView = [[UIView alloc] init];
        [self addSubview:_containerView];
    }
    return _containerView;
}
@end
