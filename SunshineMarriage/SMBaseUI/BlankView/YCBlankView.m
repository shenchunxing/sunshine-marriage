//
//  YCBlankView.m
//  YCFunctionModule
//
//  Created by haima on 2019/4/5.
//

#import "YCBlankView.h"
#import <Masonry/Masonry.h>
#import "SMDefine.h"
#import "YCCategoryModule.h"

@implementation YCBlankView

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor clearColor];
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return self;
}

- (void)didMoveToSuperview {
    
    CGRect superviewBounds = self.superview.bounds;
    self.frame = CGRectMake(0.0, 0.0, CGRectGetWidth(superviewBounds), CGRectGetHeight(superviewBounds));
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).priorityHigh();
    }];
    
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(120*kScaleFitHeight);
        make.centerX.equalTo(self.containerView);
        make.size.mas_equalTo(CGSizeMake(150*kScaleFit, 122.5*kScaleFit));
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imageView.mas_bottom).offset(20*kScaleFitHeight);
        make.centerX.equalTo(self.containerView);
        make.height.mas_equalTo(11.5*kScaleFit);
    }];

    [self.button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(189*kScaleFitHeight);
        make.leading.offset(14*kScaleFit);
        make.trailing.offset(-14*kScaleFit);
        make.height.equalTo(@(48*kScaleFitHeight));
    }];
    
}

- (void)setupBlankViewWithTitle:(NSAttributedString *)title
                   buttonTitle:(NSString *)buttonTitle
                       offsetY:(CGFloat)offsetY
                  buttonHidden:(BOOL)hidden
                 reloadHandler:(void(^)(id sender))reloadHandler {
    
    self.titleLabel.attributedText = title;
    [self.button setTitle:buttonTitle forState:UIControlStateNormal];
    self.imageView.image = [UIImage yc_resourceModuleImgWithName:@"img_nodata"];
    self.button.hidden = hidden;
    [self.imageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(offsetY*kScaleFit);
    }];
}

- (void)setupBlankViewOffsetY:(CGFloat)offsetY height:(CGFloat)height{
    [self.imageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(offsetY*kScaleFit);
    }];
    CGRect superviewBounds = self.superview.bounds;
    self.frame = CGRectMake(0.0, 0.0, CGRectGetWidth(superviewBounds),height);
}

- (void)reloadButtonClicked:(UIButton *)button {
    
    if (self.reloadBtnClick) {
        self.reloadBtnClick(button);
    }
}

- (void)setEmptyStyle:(YCEmptyStyle *)emptyStyle {
    _emptyStyle = emptyStyle;
    self.titleLabel.attributedText = emptyStyle.titleStr;
    [self.button setTitle:emptyStyle.buttonTitle forState:UIControlStateNormal];
    [self.button setTitleColor:emptyStyle.buttonColor forState:UIControlStateNormal];
    self.button.layer.borderColor = emptyStyle.buttonBorderColor.CGColor;
    self.button.hidden = !emptyStyle.showButton;
    self.imageView.image = emptyStyle.emptyImage;
    [self.imageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(FitScale(emptyStyle.imageTopOffset));
    }];
}

#pragma mark - getter
- (UIView *)containerView {
    
    if (_containerView == nil) {
        _containerView = [[UIView alloc] init];
        _containerView.backgroundColor = [UIColor clearColor];
        [self addSubview:_containerView];
    }
    return _containerView;
}

- (UIImageView *)imageView {
    
    if (_imageView == nil) {
        _imageView = [[UIImageView alloc] init];
        [self.containerView addSubview:_imageView];
    }
    return _imageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont yc_12];
        _titleLabel.textColor = [UIColor yc_hex_666666];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.numberOfLines = 2;
        [self.containerView addSubview:_titleLabel];
    }
    return _titleLabel;
}

- (UIButton *)button {
    if (!_button) {
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        _button.hidden = YES;
        _button.titleLabel.font = [UIFont yc_17];
        [_button setTitleColor:[UIColor yc_hex_108EE9] forState:UIControlStateNormal];
        _button.layer.cornerRadius = 2;
        _button.layer.masksToBounds = YES;
        _button.layer.borderColor = [UIColor yc_hex_108EE9].CGColor;
        _button.layer.borderWidth = 1.0;
        [_button addTarget:self action:@selector(reloadButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.containerView addSubview:_button];
    }
    return _button;
}



@end
