//
//  YCEmptyStyle.m
//  YCBaseUI
//
//  Created by shenweihang on 2019/11/20.
//

#import "YCEmptyStyle.h"
#import "YCCategoryModule.h"

@implementation YCEmptyStyle

+ (YCEmptyStyle *)defaultStyle {
    YCEmptyStyle *style = [[YCEmptyStyle alloc] init];
    style.titleStr = [[NSAttributedString alloc] initWithString:@"暂无数据"];
    style.imageTopOffset = 120.0;
    style.emptyImage = [UIImage yc_BaseImgWithName:@"img_no_data"];
    style.buttonTitle = @"重新加载";
    style.buttonColor = [UIColor yc_hex_108EE9];
    style.buttonBorderColor = [UIColor yc_hex_108EE9];
    style.showButton = NO;
    return style;
}

+ (YCEmptyStyle *)bankOrderStyle {
    
    YCEmptyStyle *style = [YCEmptyStyle defaultStyle];
    style.emptyImage = [UIImage yc_BaseImgWithName:@"img_no_order"];
    style.titleStr = [[NSAttributedString alloc] initWithString:@"暂无相关订单"];
    return style;
}

+ (YCEmptyStyle *)forthcomingStyle {
    YCEmptyStyle *style = [YCEmptyStyle defaultStyle];
    style.emptyImage = [UIImage yc_BaseImgWithName:@"img_forthcoming"];
    style.titleStr = [[NSAttributedString alloc] initWithString:@"即将推出 敬请期待"];
    return style;
}

+ (YCEmptyStyle *)reportStyle {
    YCEmptyStyle *style = [YCEmptyStyle defaultStyle];
    style.emptyImage = [UIImage yc_BaseImgWithName:@"baogao_kong"];
    style.titleStr = [[NSAttributedString alloc] initWithString:@"暂无报告数据"];
    return style;
}

@end
