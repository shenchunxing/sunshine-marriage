//
//  UIScrollView+EmptyData.m
//  YCFunctionModule
//
//  Created by haima on 2019/4/5.
//

#import "UIScrollView+EmptyData.h"
#import <objc/runtime.h>

//匿名分类
@interface UIScrollView ()
@property (nonatomic, readonly) YCBlankView *blankView;
@end

#pragma mark - YCWeakObjectContainer
@interface YCWeakObjectContainer : NSObject

@property (nonatomic, readonly, weak) id weakObject;
- (instancetype)initWithWeakObject:(id)object;

@end

@implementation YCWeakObjectContainer

- (instancetype)initWithWeakObject:(id)object {
    self = [super init];
    if (self) {
        _weakObject = object;
    }
    return self;
}
@end

static char const * const kEmptyViewDataSource =     "EmptyViewDataSource";
static char const * const kEmptyViewDelegate =   "EmptyViewDelegate";
static char const * const kBlankView =       "BlankView";

@implementation UIScrollView (EmptyData)

#pragma mark - YCEmptyViewDelegate绑定/获取
- (id<YCEmptyViewDelegate>)emptyViewDelegate {
    YCWeakObjectContainer *container = objc_getAssociatedObject(self, kEmptyViewDelegate);
    return container.weakObject;
}

- (void)setEmptyViewDelegate:(id<YCEmptyViewDelegate>)emptyViewDelegate {
    objc_setAssociatedObject(self, kEmptyViewDelegate, [[YCWeakObjectContainer alloc] initWithWeakObject:emptyViewDelegate], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - EmptyViewDataSource绑定/获取
- (id<YCEmptyViewDataSource>)emptyViewDataSource {
    
    YCWeakObjectContainer *container = objc_getAssociatedObject(self, kEmptyViewDataSource);
    return container.weakObject;
}

- (void)setEmptyViewDataSource:(id<YCEmptyViewDataSource>)emptyViewDataSource {
    
    if (!emptyViewDataSource || ![self yc_canDisplay]) {
        [self yc_invalidate];
    }
    
    objc_setAssociatedObject(self, kEmptyViewDataSource, [[YCWeakObjectContainer alloc] initWithWeakObject:emptyViewDataSource], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    // We add method sizzling for injecting -dzn_reloadData implementation to the native -reloadData implementation
    [self swizzleIfPossible:@selector(reloadData)];
    
    // Exclusively for UITableView, we also inject -dzn_reloadData to -endUpdates
    if ([self isKindOfClass:[UITableView class]]) {
        [self swizzleIfPossible:@selector(endUpdates)];
    }
}

- (void)reloadEmptyView {
    
    [self yc_reloadEmptyDataSet];
}

- (void)yc_reloadEmptyDataSet {
    
    if (![self yc_canDisplay]) {
        [self yc_invalidate];
        return;
    }
    
    if ([self yc_itemsCount] == 0) {
        
        YCBlankView *blankView = self.blankView;
        if (!blankView.superview) {
            // Send the view all the way to the back, in case a header and/or footer is present, as well as for sectionHeaders or any other content
            if (([self isKindOfClass:[UITableView class]] || [self isKindOfClass:[UICollectionView class]]) && self.subviews.count > 1) {
                [self insertSubview:blankView atIndex:0];
            }
            else {
                [self addSubview:blankView];
            }
        }
        //默认暂无数据
        YCEmptyStyle *style = [self styleForEmptyView];
        style.showButton = (self.emptyViewDelegate && [self.emptyViewDelegate respondsToSelector:@selector(emptyView:didTapButton:)]);
        blankView.emptyStyle = style;
        __weak __typeof(self) weak_self = self;
        [blankView setReloadBtnClick:^(id sender) {
            __strong __typeof(weak_self) self = weak_self;
            [self.emptyViewDelegate emptyView:self didTapButton:sender];
        }];
    }else if ([self yc_isBlankViewVisible]) {
        [self yc_invalidate];
    }
}

- (void)yc_invalidate {
    
    if (self.blankView) {
        [self.blankView removeFromSuperview];
    }
    self.scrollEnabled = YES;
}

- (BOOL)yc_canDisplay {
    if (self.emptyViewDataSource && [self.emptyViewDataSource conformsToProtocol:@protocol(YCEmptyViewDataSource)]) {
        if ([self isKindOfClass:[UITableView class]] || [self isKindOfClass:[UICollectionView class]] || [self isKindOfClass:[UIScrollView class]]) {
            return YES;
        }
    }
    return NO;
}

- (YCBlankView *)blankView {
    
    YCBlankView *blankView = objc_getAssociatedObject(self, kBlankView);
    if (!blankView) {
        blankView = [[YCBlankView alloc] init];
         objc_setAssociatedObject(self, kBlankView, blankView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return blankView;
}

- (void)setBlankView:(YCBlankView *)blankView {
    objc_setAssociatedObject(self, kBlankView, blankView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

//MARK:获取数据项
- (NSInteger)yc_itemsCount {
    
    NSInteger items = 0;
    
    // UIScollView doesn't respond to 'dataSource' so let's exit
    if (![self respondsToSelector:@selector(dataSource)]) {
        return items;
    }
    
    // UITableView support
    if ([self isKindOfClass:[UITableView class]]) {
        
        UITableView *tableView = (UITableView *)self;
        id <UITableViewDataSource> dataSource = tableView.dataSource;
        
        NSInteger sections = 1;
        
        if (dataSource && [dataSource respondsToSelector:@selector(numberOfSectionsInTableView:)]) {
            sections = [dataSource numberOfSectionsInTableView:tableView];
        }
        
        if (dataSource && [dataSource respondsToSelector:@selector(tableView:numberOfRowsInSection:)]) {
            for (NSInteger section = 0; section < sections; section++) {
                items += [dataSource tableView:tableView numberOfRowsInSection:section];
            }
        }
    }
    // UICollectionView support
    else if ([self isKindOfClass:[UICollectionView class]]) {
        
        UICollectionView *collectionView = (UICollectionView *)self;
        id <UICollectionViewDataSource> dataSource = collectionView.dataSource;
        
        NSInteger sections = 1;
        
        if (dataSource && [dataSource respondsToSelector:@selector(numberOfSectionsInCollectionView:)]) {
            sections = [dataSource numberOfSectionsInCollectionView:collectionView];
        }
        
        if (dataSource && [dataSource respondsToSelector:@selector(collectionView:numberOfItemsInSection:)]) {
            for (NSInteger section = 0; section < sections; section++) {
                items += [dataSource collectionView:collectionView numberOfItemsInSection:section];
            }
        }
    }
    
    return items;
}

- (BOOL)yc_isBlankViewVisible {
    UIView *view = objc_getAssociatedObject(self, kBlankView);
    return view ? !view.hidden : NO;
}

#pragma mark - 空白页类型
- (YCEmptyStyle *)styleForEmptyView {
    
    if (self.emptyViewDataSource && [self.emptyViewDataSource respondsToSelector:@selector(styleForEmptyView:)]) {
        return [self.emptyViewDataSource styleForEmptyView:self];
    }
    return [YCEmptyStyle defaultStyle];
}

#pragma mark - Method Swizzling

static NSMutableDictionary *_impLookupTable;
static NSString *const DZNSwizzleInfoPointerKey = @"pointer";
static NSString *const DZNSwizzleInfoOwnerKey = @"owner";
static NSString *const DZNSwizzleInfoSelectorKey = @"selector";

// Based on Bryce Buchanan's swizzling technique http://blog.newrelic.com/2014/04/16/right-way-to-swizzle/
// And Juzzin's ideas https://github.com/juzzin/JUSEmptyViewController

void yc_original_implementation(id self, SEL _cmd)
{
    // Fetch original implementation from lookup table
    Class baseClass = yc_baseClassToSwizzleForTarget(self);
    NSString *key = yc_implementationKey(baseClass, _cmd);
    
    NSDictionary *swizzleInfo = [_impLookupTable objectForKey:key];
    NSValue *impValue = [swizzleInfo valueForKey:DZNSwizzleInfoPointerKey];
    
    IMP impPointer = [impValue pointerValue];
    
    // We then inject the additional implementation for reloading the empty dataset
    // Doing it before calling the original implementation does update the 'isEmptyDataSetVisible' flag on time.
    [self yc_reloadEmptyDataSet];
    
    // If found, call original implementation
    if (impPointer) {
        ((void(*)(id,SEL))impPointer)(self,_cmd);
    }
}

NSString *yc_implementationKey(Class class, SEL selector)
{
    if (!class || !selector) {
        return nil;
    }
    
    NSString *className = NSStringFromClass([class class]);
    
    NSString *selectorName = NSStringFromSelector(selector);
    return [NSString stringWithFormat:@"%@_%@",className,selectorName];
}

Class yc_baseClassToSwizzleForTarget(id target)
{
    if ([target isKindOfClass:[UITableView class]]) {
        return [UITableView class];
    }
    else if ([target isKindOfClass:[UICollectionView class]]) {
        return [UICollectionView class];
    }
    else if ([target isKindOfClass:[UIScrollView class]]) {
        return [UIScrollView class];
    }
    
    return nil;
}

- (void)swizzleIfPossible:(SEL)selector
{
    // Check if the target responds to selector
    if (![self respondsToSelector:selector]) {
        return;
    }
    
    // Create the lookup table
    if (!_impLookupTable) {
        _impLookupTable = [[NSMutableDictionary alloc] initWithCapacity:3]; // 3 represent the supported base classes
    }
    
    // We make sure that setImplementation is called once per class kind, UITableView or UICollectionView.
    for (NSDictionary *info in [_impLookupTable allValues]) {
        Class class = [info objectForKey:DZNSwizzleInfoOwnerKey];
        NSString *selectorName = [info objectForKey:DZNSwizzleInfoSelectorKey];
        
        if ([selectorName isEqualToString:NSStringFromSelector(selector)]) {
            if ([self isKindOfClass:class]) {
                return;
            }
        }
    }
    
    Class baseClass = yc_baseClassToSwizzleForTarget(self);
    NSString *key = yc_implementationKey(baseClass, selector);
    NSValue *impValue = [[_impLookupTable objectForKey:key] valueForKey:DZNSwizzleInfoPointerKey];
    
    // If the implementation for this class already exist, skip!!
    if (impValue || !key || !baseClass) {
        return;
    }
    
    // Swizzle by injecting additional implementation
    Method method = class_getInstanceMethod(baseClass, selector);
    IMP dzn_newImplementation = method_setImplementation(method, (IMP)yc_original_implementation);
    
    // Store the new implementation in the lookup table
    NSDictionary *swizzledInfo = @{DZNSwizzleInfoOwnerKey: baseClass,
                                   DZNSwizzleInfoSelectorKey: NSStringFromSelector(selector),
                                   DZNSwizzleInfoPointerKey: [NSValue valueWithPointer:dzn_newImplementation]};
    
    [_impLookupTable setObject:swizzledInfo forKey:key];
}


@end
