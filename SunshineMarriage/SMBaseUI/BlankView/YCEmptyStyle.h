//
//  YCEmptyStyle.h
//  YCBaseUI
//
//  Created by shenweihang on 2019/11/20.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YCEmptyStyle : NSObject

/* 标题文字 */
@property (nonatomic, copy) NSAttributedString *titleStr;
/* 图片顶部便宜距离，默认120 */
@property (nonatomic, assign) CGFloat imageTopOffset;
/* 空白页图片 */
@property (nonatomic, strong) UIImage *emptyImage;
/* 按钮标题文字 */
@property (nonatomic, copy) NSString *buttonTitle;
/* 按钮颜色 默认#108EE9*/
@property (nonatomic, strong) UIColor *buttonColor;
/* 按钮描边颜色 默认#108EE9*/
@property (nonatomic, strong) UIColor *buttonBorderColor;
/* 是否显示按钮 */
@property (nonatomic, assign) BOOL showButton;

@property (nonatomic, assign) CGRect emptyFrame;

+ (YCEmptyStyle *)defaultStyle;

+ (YCEmptyStyle *)bankOrderStyle;

+ (YCEmptyStyle *)forthcomingStyle;
+ (YCEmptyStyle *)reportStyle;
@end

NS_ASSUME_NONNULL_END
