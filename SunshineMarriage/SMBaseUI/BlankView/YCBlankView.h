//
//  YCBlankView.h
//  YCFunctionModule
//
//  Created by haima on 2019/4/5.
//

#import <UIKit/UIKit.h>
#import "YCEmptyStyle.h"

@interface YCBlankView : UIView

/* 容器 */
@property (nonatomic, strong) UIView *containerView;
/* 图片 */
@property (nonatomic, strong) UIImageView *imageView;
/* 标题 */
@property (nonatomic, strong) UILabel *titleLabel;
/* 按钮 */
@property (nonatomic, strong) UIButton *button;
/* 空白页样式 */
@property (nonatomic, strong) YCEmptyStyle *emptyStyle;

/* 回调 */
@property (nonatomic, strong) void(^reloadBtnClick)(id sender);

- (void)setupBlankViewWithTitle:(NSAttributedString *)title
                   buttonTitle:(NSString *)buttonTitle
                       offsetY:(CGFloat)offsetY
                  buttonHidden:(BOOL)hidden
                 reloadHandler:(void(^)(id sender))reloadHandler;

- (void)setupBlankViewOffsetY:(CGFloat)offsetY height:(CGFloat)height ;


@end

