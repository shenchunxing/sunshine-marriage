//
//  UIScrollView+EmptyData.h
//  YCFunctionModule
//
//  Created by haima on 2019/4/5.
//

#import <UIKit/UIKit.h>
#import "YCBlankView.h"

NS_ASSUME_NONNULL_BEGIN

@protocol YCEmptyViewDataSource;
@protocol YCEmptyViewDelegate;
@interface UIScrollView (EmptyData)

/* 空白页数据源协议代理 */
@property (nonatomic, weak) id<YCEmptyViewDataSource> emptyViewDataSource;
/* 空白页事件协议代理 */
@property (nonatomic, weak) id<YCEmptyViewDelegate> emptyViewDelegate;

- (void)reloadEmptyView;
@end


#pragma  mark -- emptyPage datasource

@protocol YCEmptyViewDataSource <NSObject>
@optional
/**
 空白页自定义样式

 @param scrollView scrollView
 @return 样式
 */
- (YCEmptyStyle *)styleForEmptyView:(UIScrollView *)scrollView;

@end


#pragma mark -- emptyPage delegate
@protocol YCEmptyViewDelegate <NSObject>

//刷新按钮，代理回调
@optional
- (void)emptyView:(UIScrollView *)scrollView didTapButton:(UIButton *)button;

@end

NS_ASSUME_NONNULL_END
