//
//  YCTipSelectView.h
//  Pods
//
//  Created by 刘成 on 2019/4/15.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^YCTipSelectHandle)(NSInteger index);

@interface YCTipSelectView : UIView

- (instancetype)initWithFrame:(CGRect)frame icons:(NSArray <NSString *> *)icons titles:(NSArray <NSString *> *)titles selectHandle:(YCTipSelectHandle)selectHandle;

- (void)show;

- (void)hide;


@end

NS_ASSUME_NONNULL_END
