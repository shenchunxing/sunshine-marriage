//
//  YCTipSelectView.m
//  Pods
//
//  Created by 刘成 on 2019/4/15.
//

#import "YCTipSelectView.h"
#import "YCCategoryModule.h"
#import "UIFont+Style.h"

@interface YCTipSelectView ()
{
    NSArray *_iconArr;
    
    NSArray *_titleArr;
    
    YCTipSelectHandle _selectHandle;
}

@property (strong, nonatomic) UIView *tipView;

@end

@implementation YCTipSelectView

- (instancetype)initWithFrame:(CGRect)frame icons:(NSArray *)icons titles:(NSArray *)titles selectHandle:(YCTipSelectHandle)selectHandle{
    if (self = [super initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)]) {
        _selectHandle = selectHandle;
        _titleArr = titles;
        _iconArr = icons;
        
        [self createUIWithFrame:frame];
    }
    return self;
}

- (void)createUIWithFrame:(CGRect)frame{
    
    self.backgroundColor = [UIColor clearColor];
    
    _tipView = [[UIView alloc] initWithFrame:frame];
    _tipView.backgroundColor = [UIColor clearColor];
    [self addSubview:_tipView];
    
    UIImageView *jiantou = [UIImageView imageViewWithImage:@"img_sanjiao_chaoshang" bundle:@"YCBaseUI"];
    jiantou.frame = CGRectMake(13, 0, 16, 6);
    [_tipView addSubview:jiantou];
    
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 6, _tipView.frame.size.width, _tipView.frame.size.height-6)];
    contentView.layer.cornerRadius = 2;
    contentView.layer.masksToBounds = YES;
    [_tipView addSubview:contentView];
    
    CGFloat btnH = 53.5;
    for (int i=0; i<_iconArr.count; i++) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, i*btnH, contentView.frame.size.width, btnH)];
        btn.tag = 100+i;
        [btn setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRed:90/255.0 green:93/255.0 blue:99/255.0 alpha:1.0]] forState:UIControlStateHighlighted];
        [btn setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRed:96/255.0 green:100/255.0 blue:106/255.0 alpha:1.0]] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [contentView addSubview:btn];
        
        UIImageView *icon = [UIImageView imageViewWithImage:_iconArr[i] bundle:@"YCBaseUI"];
        icon.frame = CGRectMake(20, 0, 16, 16);
        icon.centerY = btn.frame.size.height/2.0;
        [btn addSubview:icon];
        
        UILabel *label = [UILabel labelWithFrame:CGRectMake(48, 0, 100, 30) text:_titleArr[i] textFont:[UIFont yc_15] textColor:[UIColor whiteColor]];
        label.centerY = btn.frame.size.height/2.0;
        [btn addSubview:label];
        
        if (i != _iconArr.count-1) {
            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(20, btn.frame.size.height+btn.frame.origin.y-1/[UIScreen mainScreen].scale, contentView.frame.size.width-20, 1/[UIScreen mainScreen].scale)];
            line.backgroundColor = [UIColor whiteColor];
            line.alpha = 0.14;
            [btn addSubview:line];
        }
        
    }
    
}

- (void)btnClick:(UIButton *)btn{
    NSInteger tag = btn.tag-100;
    
    if (_selectHandle) {
        _selectHandle(tag);
    }
    
    [self hide];
}

- (void)show{
    
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    [window addSubview:self];
    
    _tipView.alpha = 0;
    [UIView animateWithDuration:0.3 animations:^{
        self.tipView.alpha = 1;
    }];
    
}

- (void)hide{
    
    [UIView animateWithDuration:0.3 animations:^{
        self.tipView.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (_tipView.alpha == 1.0) {
        [self hide];
    }
}

@end
