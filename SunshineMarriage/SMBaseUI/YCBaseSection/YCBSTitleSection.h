//
//  YCBSTitleSection.h
//  YCBaseUI
//
//  Created by shenweihang on 2019/11/28.
//

#import "YCTableViewSection.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCBSTitleSection : YCTableViewSection

+ (YCBSTitleSection *)sectionWithTitle:(NSString *)title;

@end

NS_ASSUME_NONNULL_END
