//
//  YCIndexTitleSection.h
//  YCBaseUI
//
//  Created by haima on 2019/4/29.
//

#import "YCTableViewSection.h"


@interface YCIndexTitleSection : YCTableViewSection

+ (instancetype)sectionWithTitle:(NSString *)title index:(NSInteger)index;

@end
