//
//  YCIndexTitleView.h
//  YCBaseUI
//
//  Created by haima on 2019/4/29.
//

#import <UIKit/UIKit.h>

@interface YCIndexTitleView : UIView

/* 序号背景 */
@property (nonatomic, strong) UIView *indexView;
/* 序号 */
@property (nonatomic, strong) UILabel *indexLbl;

/* 标题 */
@property (nonatomic, strong) UILabel *titleLbl;

+ (instancetype)viewWithTitle:(NSString *)title index:(NSInteger)index;

- (void)setupTitleLabel:(NSString *)title;
- (void)setupIndexLabel:(NSInteger)index;

@end
