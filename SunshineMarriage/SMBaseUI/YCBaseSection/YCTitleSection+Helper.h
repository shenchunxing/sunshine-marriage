//
//  YCTitleSection+Helper.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/8/19.
//

#import "YCTitleSection.h"
@class YCBaseItem;
NS_ASSUME_NONNULL_BEGIN

@interface YCTitleSection (Helper)

/**
 根据key获取item
 
 @param key key
 @return item
 */
- (YCBaseItem *)getItemWithKey:(NSString *)key;


/**
 获取指定位置的items
 
 @param fromIndex 开始的位置
 @param length 长度
 @return items
 */
- (NSArray *)itemsWithFromIndex:(NSInteger)fromIndex length:(NSInteger)length;


/**
 设置items的隐藏和显示，但是要过滤掉部分items
 
 @param showFolding 展开/关闭
 @param keys 过滤掉一些item.keys
 */
- (void)setShowFolding:(BOOL)showFolding keys:(NSArray *)keys;

- (void)setShowFolding:(BOOL)showFolding;

- (void)setItemColor:(UIColor *)itemColor;

//展开关闭样式
+ (instancetype)sectionWithOpenCloseViewTitle:(NSString *)title rightBlock:(void(^)(void))rightBlock;
//设置展开关闭样式
- (void)setFoldSectionWithSection:(YCTitleSection *)section rightBlock:(void(^)(void))rightBlock;

+ (instancetype)sectionWithOpenCloseViewTitle:(NSString *)title fliterKeys:(NSArray *)keys rightBlock:(void(^)(void))rightBlock;

- (void)changeTitleOpenCloseViewWithTitle:(NSString *)title;

@end

NS_ASSUME_NONNULL_END
