//
//  YCTitleView.m
//  YCBaseUI
//
//  Created by haima on 2019/4/3.
//

#import "YCTitleView.h"
#import <Masonry/Masonry.h>
#import "UIColor+Style.h"
#import "UIFont+Style.h"
#import "YCCategoryModule.h"

CGFloat const kYCTitleViewHeight = 57.0;

@interface YCTitleView ()
/* 标题 */
@property (nonatomic, strong) UILabel *titleLabel;
/* 提示文本 */
@property (nonatomic, strong) UILabel *tipLabel;
/* 标题颜色块 */
@property (nonatomic, strong) UIView *colorView;

/* 右侧按钮 */
@property (nonatomic, strong) UIButton *rightBtn;

@property (nonatomic,strong) UIImageView *arrowImageView;

/* 编辑按钮 */
@property (nonatomic, strong) UIButton *editBtn;
@end

@implementation YCTitleView

- (instancetype)initWithTitle:(NSString *)title {
    
    return [self initWithTitle:title tipStr:nil rightBtnStr:nil];
}

- (instancetype)initWithTitle:(NSString *)title tipStr:(NSString *)tip {
    
    return [self initWithTitle:title tipStr:tip rightBtnStr:nil];
}

- (instancetype)initWithTitle:(NSString *)title rightBtnStr:(NSString *)btnStr{
    
    return [self initWithTitle:title tipStr:nil rightBtnStr:btnStr];
}

- (instancetype)initWithTitle:(NSString *)title tipStr:(NSString *)tip rightBtnStr:(NSString *)btnStr{
    
    if (self = [super init]) {
        
        [self addSubview:self.colorView];
        [self addSubview:self.titleLabel];
        
        [self.colorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.size.mas_equalTo(CGSizeMake(4, 18));
            make.centerY.equalTo(self.titleLabel);
        }];
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).offset(20);
            make.left.equalTo(self.colorView.mas_right).offset(10);
            make.bottom.equalTo(self).offset(-12);
        }];
        
        [self.titleLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
        self.titleLabel.text = title;
        
        if (tip) {
            [self addSubview:self.tipLabel];
            [self.tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.titleLabel.mas_right);
                make.right.lessThanOrEqualTo(self);
                make.centerY.equalTo(self.titleLabel);
            }];
            self.tipLabel.text = tip;
        }
        
        if (btnStr) {
            [self addSubview:self.rightBtn];
            [self.rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(self);
                make.centerY.equalTo(self.titleLabel.mas_centerY);
                make.width.equalTo(@90);
                make.height.equalTo(@40);
            }];
            [self.rightBtn setTitle:btnStr forState:UIControlStateNormal];
        }
        
    }
    return self;
}

- (instancetype)initWithTitle:(NSString *)title editHandler:(void(^)(void))block {
    
    if ([self initWithTitle:title]) {
        [self addSubview:self.editBtn];
        [self.editBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.titleLabel);
            make.size.mas_equalTo(CGSizeMake(18, 18));
            make.trailing.offset(-14);
        }];
        self.rightBtnBlock = block;
    }
    return self;
}

- (void)setTitleViewWithRightBtnTitle:(NSString *)title
{
    self.rightBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.rightBtn setImage:nil forState:UIControlStateNormal];
    self.rightBtn.layer.borderColor = [UIColor yc_hex_108EE9].CGColor;
    self.rightBtn.layer.borderWidth = 1/[UIScreen mainScreen].scale;
    self.rightBtn.layer.masksToBounds = YES;
    self.rightBtn.layer.cornerRadius = 2.0;
    [self.rightBtn setTitle:title forState:UIControlStateNormal];
    [self.rightBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@60);
        make.height.equalTo(@32);
    }];
}


- (void)setTitleView:(NSString *)title {
    
    self.titleLabel.text = title;
}
    
- (void)setTitleTip:(NSString *)tip {
    self.tipLabel.text = tip;
}

- (void)setRightBtnStr:(NSString *)btnStr{
    if (btnStr.length>0) {
        [self.rightBtn setTitle:btnStr forState:UIControlStateNormal];
        self.rightBtn.hidden = NO;
    }else{
        self.rightBtn.hidden = YES;
    }
}

- (UILabel *)titleLabel {
    
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor yc_hex_121D32];
        _titleLabel.font = [UIFont yc_17_bold];
        _titleLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _titleLabel;
}

- (UIView *)colorView {
    
    if (_colorView == nil) {
        _colorView = [[UIView alloc] init];
        _colorView.backgroundColor = [UIColor yc_hex_F5A623];
    }
    return _colorView;
}
    
- (UILabel *)tipLabel {
    
    if (_tipLabel == nil) {
        _tipLabel = [[UILabel alloc] init];
        _tipLabel.textColor = [UIColor yc_hex_9B9EA8];
        _tipLabel.font = [UIFont yc_13];
        _tipLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _tipLabel;
}

- (UIButton *)rightBtn{
    if (_rightBtn == nil) {
        _rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_rightBtn setImage:[UIImage yc_BaseImgWithName:@"icon_arrow_right_blue"] forState:UIControlStateNormal];
        [_rightBtn setTitleColor:[UIColor yc_hex_108EE9] forState:UIControlStateNormal];
        _rightBtn.titleLabel.font = [UIFont yc_15];
        _rightBtn.titleLabel.textAlignment = NSTextAlignmentRight;
        _rightBtn.imageEdgeInsets = UIEdgeInsetsMake(12, 80, 12, 0);
        [_rightBtn addTarget:self action:@selector(rightBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rightBtn;
}

- (UIButton *)editBtn {
    
    if (_editBtn == nil) {
        _editBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_editBtn setImage:[UIImage yc_resourceModuleImgWithName:@"icon_edit_default"] forState:UIControlStateNormal];
        [_editBtn addTarget:self action:@selector(onEditClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _editBtn;
}

- (void)onEditClick:(UIButton *)button {
    if (self.rightBtn) {
        self.rightBtnBlock();
    }
}

- (void)rightBtnClick
{
    if (self.rightBtnBlock) {
        self.rightBtnBlock();
    }
}

- (void)setLeftMargin:(CGFloat)leftMargin{
    [self.colorView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(leftMargin);
    }];
}

- (void)setTitleFont:(UIFont *)titleFont{
    self.titleLabel.font = titleFont;
}

- (void)setStraightColor:(UIColor *)straightColor {
    self.colorView.backgroundColor = straightColor;
}

- (void)setStraightHeight:(CGFloat)straightHeight {
    self.colorView.size = CGSizeMake(self.colorView.size.width, straightHeight);
    [self.colorView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(4, straightHeight));
    }];
}

@end
