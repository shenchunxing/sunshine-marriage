//
//  YCTitleSection.h
//  YCBaseUI
//
//  Created by haima on 2019/4/3.
//

#import "YCTableViewKit.h"

@class YCTitleSection;

@interface YCTitleSection : YCTableViewSection
/* 右侧按钮回调 */
@property (nonatomic, copy) void(^rightBtnHandler)(void);

- (void)changeTitleViewWithTitle:(NSString *)title;
- (void)changeTitleViewWithTitle:(NSString *)title tip:(NSString *)tip;
- (void)changeTitleViewWithTitle:(NSString *)title rightBtnStr:(NSString *)btnStr;
- (void)changeTitleViewWithRightTitle:(NSString *)title handle:(void(^)(void))handle;


+ (instancetype)sectionWithTitle:(NSString *)title;
+ (instancetype)sectionWithTitle:(NSString *)title tipText:(NSString *)tip;
+ (instancetype)sectionWithTitle:(NSString *)title rightBtnStr:(NSString *)btnStr ;
+ (instancetype)sectionWithTitle:(NSString *)title tipText:(NSString *)tip rightBtnStr:(NSString *)btnStr;
+ (instancetype)sectionWithTitle:(NSString *)title block:(void(^)(void))block;


//背景色
@property (nonatomic,copy) UIColor *backgroundColor;
//左侧间距
@property (nonatomic,assign) CGFloat leftMargin;
//标题字号
@property (nonatomic,strong) UIFont *titleFont;
@property (nonatomic, strong) UIColor *straightColor;
@property (nonatomic, assign) CGFloat straightHeight;


@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) void(^foldBlock)(BOOL isFold);
@property (nonatomic,copy) NSArray  *itemsArr;

@property (nonatomic,assign) BOOL canEdit;



@end
