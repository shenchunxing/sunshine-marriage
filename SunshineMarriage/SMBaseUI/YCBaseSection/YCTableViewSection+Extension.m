//
//  YCTableViewSection+Extension.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/9/16.
//

#import "YCTableViewSection+Extension.h"
#import "YCTextUnitItem.h"
#import "YCTextViewItem.h"

@implementation YCTableViewSection (Extension)

- (void)insertItem:(NSString *)itemName key:(NSString *)key title:(NSString *)title {
    if ([itemName isEqualToString:@"YCTextViewItem"]){
       YCTextViewItem *item = [[YCTextViewItem alloc] init];
       item.title = title;
       item.key = key;
       item.placeHolder = @"请输入";
       item.isRequired = YES;
       [self addItem:item];
       
       [item setTextUpdateHandle:^(NSString *key, NSString *text) {
          
       }];
    }
}

- (void)show {
    
}

@end
