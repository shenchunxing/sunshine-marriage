//
//  YCTitleSection.m
//  YCBaseUI
//
//  Created by haima on 2019/4/3.
//

#import "YCTitleSection.h"
#import "YCTitleView.h"
#import "YCTitleOpenCloseView.h"
#import "SMDefine.h"

@implementation YCTitleSection

+ (instancetype)sectionWithTitle:(NSString *)title {
    
    return [self sectionWithTitle:title tipText:nil rightBtnStr:nil];
}

+ (instancetype)sectionWithTitle:(NSString *)title tipText:(NSString *)tip {
 
    return [self sectionWithTitle:title tipText:tip rightBtnStr:nil];
}

+ (instancetype)sectionWithTitle:(NSString *)title rightBtnStr:(NSString *)btnStr{
    
    return [self sectionWithTitle:title tipText:nil rightBtnStr:btnStr];
}


+ (instancetype)sectionWithTitle:(NSString *)title tipText:(NSString *)tip rightBtnStr:(NSString *)btnStr{
    
    YCTitleSection *section = [[self alloc] init];
    section.title = title;
    YCTitleView *titleView = [[YCTitleView alloc] initWithTitle:title tipStr:tip rightBtnStr:btnStr];
    section.headerView = titleView;
    section.headerHeight = kYCTitleViewHeight;
    if (!title) {
        section.headerView = nil;
        section.headerHeight = 0;
    }
    
    return section;
}



+ (instancetype)sectionWithTitle:(NSString *)title block:(void(^)(void))block {
    
    YCTitleSection *section = [[YCTitleSection alloc] init];
    section.title = title;
    YCTitleView *titleView = [[YCTitleView alloc] initWithTitle:title editHandler:block];
    section.headerView = titleView;
    section.headerHeight = kYCTitleViewHeight;
    
    return section;
}


- (void)changeTitleViewWithTitle:(NSString *)title {
    
    YCTitleView *titleView = (YCTitleView *)self.headerView;
    [titleView setTitleView:title];
}
    
- (void)changeTitleViewWithTitle:(NSString *)title tip:(NSString *)tip {
    YCTitleView *titleView = (YCTitleView *)self.headerView;
    [titleView setTitleView:title];
    [titleView setTitleTip:tip];
}

- (void)changeTitleViewWithTitle:(NSString *)title rightBtnStr:(NSString *)btnStr{
    YCTitleView *titleView = (YCTitleView *)self.headerView;
    [titleView setTitleView:title];
    [titleView setRightBtnStr:btnStr];
}

- (void)changeTitleViewWithRightTitle:(NSString *)title handle:(void (^)(void))handle
{
    YCTitleView *titleView = (YCTitleView *)self.headerView;
    [titleView setTitleViewWithRightBtnTitle:title];
    [titleView setRightBtnBlock:^{
        handle();
    }];
    
}

- (void)setRightBtnHandler:(void (^)(void))rightBtnHandler {
    
    if (self.headerView) {
        YCTitleView *titleView = (YCTitleView *)self.headerView;
        [titleView setRightBtnBlock:rightBtnHandler];
    }
}

- (void)setBackgroundColor:(UIColor *)backgroundColor{
    YCTitleView *titleView = (YCTitleView *)self.headerView;
    if (backgroundColor) {
         titleView.backgroundColor = backgroundColor;
    }
}

- (void)setLeftMargin:(CGFloat)leftMargin{
    YCTitleView *titleView = (YCTitleView *)self.headerView;
    titleView.leftMargin = leftMargin;
}

- (void)setTitleFont:(UIFont *)titleFont{
    YCTitleView *titleView = (YCTitleView *)self.headerView;
    titleView.titleFont = titleFont;
}

- (void)setStraightColor:(UIColor *)straightColor {
    YCTitleView *titleView = (YCTitleView *)self.headerView;
    titleView.straightColor = straightColor;
}

- (void)setStraightHeight:(CGFloat)straightHeight{
    YCTitleView *titleView = (YCTitleView *)self.headerView;
    titleView.straightHeight = straightHeight;
}


@end
