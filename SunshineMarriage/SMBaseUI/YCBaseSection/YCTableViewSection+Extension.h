//
//  YCTableViewSection+Extension.h
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/9/16.
//

#import "YCTableViewSection.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCTableViewSection (Extension)

- (void)show;
- (void)insertItem:(NSString *)itemName key:(NSString *)key title:(NSString *)title;


@end

NS_ASSUME_NONNULL_END
