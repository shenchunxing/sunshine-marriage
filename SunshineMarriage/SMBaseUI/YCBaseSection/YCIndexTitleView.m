//
//  YCIndexTitleView.m
//  YCBaseUI
//
//  Created by haima on 2019/4/29.
//

#import "YCIndexTitleView.h"
#import <Masonry/Masonry.h>
#import "UIFont+Style.h"
#import "UIColor+Style.h"

@implementation YCIndexTitleView

+ (instancetype)viewWithTitle:(NSString *)title index:(NSInteger)index {
    
    YCIndexTitleView *view = [[YCIndexTitleView alloc] init];
    [view initViews];
    view.indexLbl.text = [NSString stringWithFormat:@"%ld",index];
    view.titleLbl.text = title;
    return view;
}

- (void)initViews {
    [self addSubview:self.indexView];
    [self addSubview:self.indexLbl];
    [self addSubview:self.titleLbl];
    
    [self.indexView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.leading.offset(14);
        make.height.equalTo(@18);
        make.width.greaterThanOrEqualTo(@18);
    }];
    
    [self.indexLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.indexView);
        make.left.equalTo(self.indexView.mas_left).offset(5);
        make.right.equalTo(self.indexView.mas_right).offset(-5);
    }];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.indexView);
        make.left.equalTo(self.indexView.mas_right).offset(8);
    }];
}

#pragma mark - getter
- (UIView *)indexView {
    
    if (_indexView == nil) {
        _indexView = [[UIView alloc] init];
        _indexView.backgroundColor = [UIColor yc_hex_F5A623];
    }
    return _indexView;
}
- (UILabel *)indexLbl {
    
    if (_indexLbl == nil) {
        _indexLbl = [[UILabel alloc] init];
        _indexLbl.textColor = [UIColor whiteColor];
        _indexLbl.font = [UIFont yc_13];
        _indexLbl.textAlignment = NSTextAlignmentCenter;
    }
    return _indexLbl;
}
- (UILabel *)titleLbl {
    
    if (_titleLbl == nil) {
        _titleLbl = [[UILabel alloc] init];
        _titleLbl.textColor = [UIColor yc_hex_5B6071];
        _titleLbl.font = [UIFont yc_14];
        _titleLbl.textAlignment = NSTextAlignmentLeft;
    }
    return _titleLbl;
}
@end
