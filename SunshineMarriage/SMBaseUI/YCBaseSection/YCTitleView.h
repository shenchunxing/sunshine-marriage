//
//  YCTitleView.h
//  YCBaseUI
//
//  Created by haima on 2019/4/3.
//

#import <UIKit/UIKit.h>

extern CGFloat const kYCTitleViewHeight;

@interface YCTitleView : UIView

@property (nonatomic,copy) void(^rightBtnBlock)(void);
- (void)setTitleView:(NSString *)title;
- (void)setTitleTip:(NSString *)tip;
- (void)setRightBtnStr:(NSString *)btnStr;

- (instancetype)initWithTitle:(NSString *)title;
- (instancetype)initWithTitle:(NSString *)title tipStr:(NSString *)tip;
- (instancetype)initWithTitle:(NSString *)title rightBtnStr:(NSString *)btnStr;
- (instancetype)initWithTitle:(NSString *)title tipStr:(NSString *)tip rightBtnStr:(NSString *)btnStr;
//- (instancetype)initWithTitle:(NSString *)title tipStr:(NSString *)tip cornerRightBtnStr:(NSString *)btnStr;
- (void)setTitleViewWithRightBtnTitle:(NSString *)title;

@property (nonatomic,assign) CGFloat leftMargin;//左侧边距
@property (nonatomic,strong) UIFont *titleFont;//字体大小
@property (nonatomic, strong) UIColor *straightColor;
@property (nonatomic, assign) CGFloat straightHeight;


- (instancetype)initWithTitle:(NSString *)title editHandler:(void(^)(void))block;

@end

