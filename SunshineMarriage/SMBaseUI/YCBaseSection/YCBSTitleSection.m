//
//  YCBSTitleSection.m
//  YCBaseUI
//
//  Created by shenweihang on 2019/11/28.
//

#import "YCBSTitleSection.h"
#import <Masonry/Masonry.h>
#import "YCCategoryModule.h"
#import "SMDefine.h"

@interface YCBSTitleView : UIView

@property (nonatomic, strong) UIView *blockView;
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UIView *lineView;

- (instancetype)initWithTitle:(NSString *)title;

@end

@implementation YCBSTitleSection

+ (YCBSTitleSection *)sectionWithTitle:(NSString *)title {
    
    YCBSTitleSection *section = [[YCBSTitleSection alloc] init];
    YCBSTitleView *titleView = [[YCBSTitleView alloc] initWithTitle:title];
    section.headerView = titleView;
    section.headerHeight = 59.0;
    return section;
}

@end

@implementation YCBSTitleView

- (instancetype)initWithTitle:(NSString *)title {
    
    if (self = [super init]) {
        self.titleLbl.text = title;
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.blockView];
        [self addSubview:self.titleLbl];
        [self addSubview:self.lineView];
        [self.blockView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.centerY.equalTo(self);
            make.size.mas_equalTo(CGSizeMake(4, 25));
        }];
        [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.blockView.mas_right).offset(10);
            make.centerY.equalTo(self);
        }];
        [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.offset(14);
            make.trailing.offset(-14);
            make.height.mas_equalTo(kLineHeight);
            make.bottom.equalTo(self);
        }];
    }
    return self;
}

- (UIView *)blockView {
    
    if (!_blockView) {
        _blockView = [[UIView alloc] init];
        _blockView.backgroundColor = [UIColor yc_hex_F8513B];
    }
    return _blockView;
}
- (UILabel *)titleLbl {
    
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc] init];
        _titleLbl.font = [UIFont yc_18_bold];
        _titleLbl.textColor = [UIColor yc_hex_121D32];
        _titleLbl.textAlignment = NSTextAlignmentLeft;
    }
    return _titleLbl;
}
- (UIView *)lineView {
    
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = [UIColor yc_hex_EFEFEF];
    }
    return _lineView;
}
@end
