//
//  YCTitleSection+Helper.m
//  YCBaseUI
//
//  Created by 沈春兴 on 2019/8/19.
//

#import "YCTitleSection+Helper.h"
#import "YCBaseItem.h"
#import "YCTitleOpenCloseView.h"

@implementation YCTitleSection (Helper)

- (YCBaseItem *)getItemWithKey:(NSString *)key{
    for (YCTableViewItem *item in self.items) {
        if ([item isKindOfClass:[YCBaseItem class]]) {
            YCBaseItem *baseItem = (YCBaseItem *)item;
            if ([baseItem.key isEqualToString:key]) {
                return baseItem;
            }
        }
    }
    return nil;
}


- (void)setShowFolding:(BOOL)showFolding{
    for (YCTableViewItem *item in self.items) {
        if ([item isKindOfClass:[YCBaseItem class]]) {
            item.shouldShow = !showFolding;
        }
    }
}

- (void)setShowFolding:(BOOL)showFolding keys:(NSArray *)keys{
    for (YCTableViewItem *item in self.items) {
        if ([item isKindOfClass:[YCBaseItem class]]) {
            if ([keys containsObject:item.requestKey]) {
                item.shouldShow = !showFolding;
            }
        }
    }
}

- (NSArray *)itemsWithFromIndex:(NSInteger)fromIndex length:(NSInteger)length{
    NSMutableArray *arr = [NSMutableArray new];
    for (int i = 0; i<self.items.count; i++) {
        if (i >=fromIndex && i<fromIndex+length) {
            [arr addObject:self.items[i]];
        }
    }
    return [arr copy];
}

- (void)setItemColor:(UIColor *)itemColor{
    for (YCBaseItem *item in self.items) {
        item.backgroundColor = itemColor;
    }
}

+ (instancetype)sectionWithOpenCloseViewTitle:(NSString *)title rightBlock:(void(^)(void))rightBlock{
    YCTitleSection *section = [[YCTitleSection alloc] init];
    YCTitleOpenCloseView *titleView = [[YCTitleOpenCloseView alloc] initWithTitle:title];
    [titleView setRightBtnBlock:^(YCTitleOpenCloseView *view) {
        section.showFolding = !view.statusType;
        rightBlock();
    }];
    section.headerView = titleView;
    section.headerHeight = 54.f;
    if (!title) {
        section.headerView = nil;
        section.headerHeight = 0;
    }
    
    return section;
}


- (void)setFoldSectionWithSection:(YCTitleSection *)section rightBlock:(void(^)(void))rightBlock{
    YCTitleOpenCloseView *titleView = [[YCTitleOpenCloseView alloc] initWithTitle:section.title];
    [titleView setRightBtnBlock:^(YCTitleOpenCloseView *view) {
        for (YCTableViewItem *item in section.shouldShowItems) {
            item.shouldShow = view.statusType;
        }
        rightBlock();
    }];
    section.headerView = titleView;
    section.headerHeight = 54.f;
    if (!section.title) {
        section.headerView = nil;
        section.headerHeight = 0;
    }
}

+ (instancetype)sectionWithOpenCloseViewTitle:(NSString *)title fliterKeys:(NSArray *)keys rightBlock:(void(^)(void))rightBlock{
    YCTitleSection *section = [[YCTitleSection alloc] init];
    YCTitleOpenCloseView *titleView = [[YCTitleOpenCloseView alloc] initWithTitle:title];
    [titleView setRightBtnBlock:^(YCTitleOpenCloseView *view) {
        for (YCTableViewItem *item in section.items) {
            if (![keys containsObject:item.requestKey]) {
                item.shouldShow = view.statusType;
            }
        }
        rightBlock();
    }];
    section.headerView = titleView;
    section.headerHeight = 54.f;
    
    return section;
}

- (void)changeTitleOpenCloseViewWithTitle:(NSString *)title {
    YCTitleOpenCloseView *titleView = (YCTitleOpenCloseView *)self.headerView;
    [titleView setTitle:title];
}



@end
