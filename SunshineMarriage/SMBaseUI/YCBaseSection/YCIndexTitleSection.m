//
//  YCIndexTitleSection.m
//  YCBaseUI
//
//  Created by haima on 2019/4/29.
//

#import "YCIndexTitleSection.h"
#import "YCIndexTitleView.h"

@implementation YCIndexTitleSection

+ (instancetype)sectionWithTitle:(NSString *)title index:(NSInteger)index {

    YCIndexTitleSection *section = [[YCIndexTitleSection alloc] init];
    YCIndexTitleView *headerView = [YCIndexTitleView viewWithTitle:title index:index];
    section.headerView = headerView;
    section.headerHeight = 44.0;
    return section;
}

@end
