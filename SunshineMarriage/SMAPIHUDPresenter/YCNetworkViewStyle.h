//
//  YCNetworkViewStyle.h
//  YCAPIHUDPresenter
//
//  Created by shenweihang on 2019/11/25.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YCNetworkViewStyle : NSObject

/* 图片 */
@property (nonatomic, copy) NSString *imageName;
/* 标题 */
@property (nonatomic, copy) NSString *title;
/* 描述 */
@property (nonatomic, copy) NSString *describe;

+ (instancetype)defaultNoNetworkStyle;
+ (instancetype)defaultNetworkErrorStyle;

+ (instancetype)bankNoNetworkStyle;
+ (instancetype)bankNetworkErrorStyle;
@end

NS_ASSUME_NONNULL_END
