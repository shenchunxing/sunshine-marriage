//
//  YCAPIHUDPresenter.h
//  YCAPIHUDPresenter
//
//  Created by haima on 2019/3/27.
//


//用于提示网络错误及api错误信息
#import "YCAlertHUDPresnter.h"

//只用于提示api错误信息
#import "YCAPIErrorHUDPresenter.h"

//银行端网络请求错误提示hud
#import "YCAPIBankPresenter.h"
