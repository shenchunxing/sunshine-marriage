//
//  YCAPIBankPresenter.m
//  YCAPIHUDPresenter
//
//  Created by shenweihang on 2019/11/25.
//

#import "YCAPIBankPresenter.h"
#import "YCAPIKit.h"
#import "YCNetworkErrorView.h"
#import "UIView+Toast.h"

@implementation YCAPIBankPresenter
- (void)api:(id <YCAPIProtocol>)api showFailureHUD:(NSError *)error {
    
    [super api:api showFailureHUD:error];
    
    if ([error.userInfo[@"code"] isEqualToString:@"EC00000404"] && self.showNetworkErrorView) {
        __weak typeof(api) weakAPI = api;
        YCNetworkViewStyle *style = [YCNetworkViewStyle bankNetworkErrorStyle];
        [YCNetworkErrorView showNetworkViewInView:self.view style:style block:^{
            __strong typeof(weakAPI) strongAPI = weakAPI;
            [strongAPI retry];
        }];
    }else if ([error.userInfo[@"code"] isEqualToString:@"EC00000000"] && self.showNetworkErrorView) {
        __weak typeof(api) weakAPI = api;
        YCNetworkViewStyle *style = [YCNetworkViewStyle bankNoNetworkStyle];
        [YCNetworkErrorView showNetworkViewInView:self.view style:style block:^{
            __strong typeof(weakAPI) strongAPI = weakAPI;
            [strongAPI retry];
        }];
    }
    else{
        [self.view makeToast:error.localizedDescription duration:1.5 position:CSToastPositionCenter];
    }
}
@end
