//
//  YCNetworkViewStyle.m
//  YCAPIHUDPresenter
//
//  Created by shenweihang on 2019/11/25.
//

#import "YCNetworkViewStyle.h"

@implementation YCNetworkViewStyle

+ (instancetype)defaultNoNetworkStyle {
    
    YCNetworkViewStyle *style = [[YCNetworkViewStyle alloc] init];
    style.imageName = @"img_no_network";
    style.describe = @"啊哦，你的网络走丢咯";
    return style;
}

+ (instancetype)defaultNetworkErrorStyle {
    YCNetworkViewStyle *style = [[YCNetworkViewStyle alloc] init];
    style.imageName = @"img_404";
    style.describe = @"页面出错了，攻城狮正在紧急搜救中…";
    return style;
}

+ (instancetype)bankNoNetworkStyle {
    
    YCNetworkViewStyle *style = [[YCNetworkViewStyle alloc] init];
    style.imageName = @"img_network_unavaliable";
    style.title = @"网络连接失败";
    style.describe = @"请检查您的网络或重新加载";
    return style;
}

+ (instancetype)bankNetworkErrorStyle {
    YCNetworkViewStyle *style = [[YCNetworkViewStyle alloc] init];
    style.imageName = @"img_server_error";
    style.title = @"服务器开小差";
    return style;
}

@end
