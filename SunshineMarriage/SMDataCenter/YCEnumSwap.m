//
//  YCEnumSwap.m
//  YCDataCenter
//
//  Created by 刘成 on 2019/4/22.
//

#import "YCEnumSwap.h"


@interface YCEnumSwap ()

@property (strong, nonatomic) NSDictionary *enumDict;

@end

@implementation YCEnumSwap

static YCEnumSwap *instance = nil;


+ (instancetype)sharedInstancetype{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (NSDictionary *)enumDict{
    if (_enumDict == nil) {
        NSString *path = [[NSBundle mainBundle] pathForResource: @"enumPlist" ofType:@"plist"];
        NSDictionary *dict =[NSDictionary dictionaryWithContentsOfFile:path];
        _enumDict = dict;
    }
    return _enumDict;
}



/**
 获取枚举对应的key或value
 @param key 后台字段对应的枚举字段
 @param swapStr key或value
 @return value或key
 */
- (NSString *)getEnumSwapWithKey:(NSString *)key swapStr:(NSString *)swapStr{
    
    NSDictionary *dict = self.enumDict[key];
    if (swapStr.length>0) {
        for (NSString *key in dict) {
            NSString *value = dict[key];
            if ([key isEqualToString:swapStr]) {
                return value;
            }
            if ([value isEqualToString:swapStr]) {
                return key;
            }
        }
    }
    return @"";
}


/**
 获取字段对应的枚举数据源

 @param key 后台字段对应的枚举字段
 @return 该key对应的数据源
 */
- (NSArray < id<YCOptionProtocol> > *)getDataSourceWithKey:(NSString *)key{
    
    return [self getDataSourceWithKey:key sortType:0];
}


/**
 获取字段对应的枚举数据源

 @param key 后台字段对应的枚举字段
 @param sortType 排序 0-从小到大  1-从大到小
 @return 该key对应的数据源
 */
- (NSArray < id<YCOptionProtocol> > *)getDataSourceWithKey:(NSString *)key sortType:(NSInteger)sortType{
    NSDictionary *dict = self.enumDict[key];
    NSMutableArray *marr = [NSMutableArray new];
    for (NSString *key in dict) {
        NSString *value = dict[key];
        YCOptionModel *model = [[YCOptionModel alloc] init];
        model.name = value;
        model.code = key;
        [marr addObject:model];
    }
    
    [marr sortUsingComparator:^NSComparisonResult(YCOptionModel *  _Nonnull obj1, YCOptionModel *  _Nonnull obj2) {
        if (sortType==0) {
            return obj1.code.integerValue > obj2.code.integerValue;
        }
        return obj1.code.integerValue <= obj2.code.integerValue;
    }];
    
    return marr;
}



@end
