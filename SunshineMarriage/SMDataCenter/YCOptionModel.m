//
//  YCOptionModel.m
//  YCFunctionModule
//
//  Created by haima on 2019/4/4.
//

#import "YCOptionModel.h"
#import <YYModel/YYModel.h>
@implementation YCOptionModel

+ (nullable NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    
    return @{@"code":@"id"};
}

- (NSString *)obtainCode {
    return self.code;
}

- (NSString *)obtainName {
    return self.name;
}


+ (YCOptionModel *)modelWithName:(NSString *)name code:(NSString *)code {
    YCOptionModel *model = [[YCOptionModel alloc] init];
    model.name = name;
    model.code = code;
    return model;
}

@end
