//
//  YCLoginStatusManager.m
//  YCDataCenter
//
//  Created by 沈春兴 on 2019/12/10.
//

#import "YCLoginStatusManager.h"
static YCLoginStatusManager *instance = nil;
@implementation YCLoginStatusManager
+ (instancetype)sharedData {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

//- (void)setLoginStatus:(LoginStatus)loginStatus {
//    _loginStatus = loginStatus;
//    
//}

@end
