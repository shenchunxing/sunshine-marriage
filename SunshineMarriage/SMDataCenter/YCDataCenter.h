//
//  YCDataCenter.h
//  YCDataCenter
//
//  Created by haima on 2019/3/28.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,AppType) {
    Partner,
    CarDealer
};


@interface YCDataCenter : NSObject

//#define PartnerServiceHead @"http://demo.yunchejinrong.com"//业务团队预发布环境
#define PartnerServiceHead @"http://system.yunchejinrong.com"//业务团队线上环境

#define BankSaasServiceHead @"http://192.168.2.23:8040"//默认资金端测试环境 //http://192.168.201.108:8040 ////http://192.168.200.250:8040  //http://192.168.2.23:8040

#define Port @"inparts/api"//业务团队通用端口

#define CardealerServiceHead @"http://192.168.2.16"//车商线上环境
#define CardealerPort @"cardealer/dealer/api"//业务团队通用端口

+ (instancetype)sharedData;

/* 用户名 */
@property (nonatomic, copy) NSString *userName;
/* 操作id */
@property (nonatomic, copy) NSString *operateId;
/* 操作人 */
@property (nonatomic, copy) NSString *operateName;
/* 角色 */
@property (nonatomic, copy) NSString *roleName;

@property (nonatomic, copy) NSString *userId;

@property (nonatomic, copy) NSString *token;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *partner;

@property (nonatomic, copy) NSString *permission;

@property (nonatomic,copy) NSString *loginPhone;
@property (nonatomic,copy) NSString *loginPassword;

@property (nonatomic,copy) NSString *partnerId;


#pragma mark - 阳光婚姻 -- begin
/// 微信登录成功后的信息
@property (nonatomic, copy) NSString *headimgurl; //头像
@property (nonatomic, copy) NSString *nickname; //昵称
@property (nonatomic, assign) int sex; //性别
@property (nonatomic, copy) NSString *phone; //手机号
@property (nonatomic, copy) NSDictionary *weChatInfo;
@property (nonatomic, copy) NSString *appusercode; //thirdCode
@property (nonatomic, assign) double insertTime;
@property (nonatomic, copy) NSString *shareId; //分享id
@property (nonatomic, copy) NSString *thirdCode; //thirdCode
@property (nonatomic, copy) NSString *thirdType; //thirdType
@property (nonatomic, copy) NSString *authorization; //authorization
//@property (nonatomic, copy) NSString *firstLogin; //首次登录

- (void)clearLoginInfo;
#pragma mark - 阳光婚姻 -- end


//目前报表中使用
/* 权限数据 */
@property (nonatomic, strong) NSDictionary *permissionData;
/* 是否是业务团队 */
@property (nonatomic, assign) BOOL isPartner;


@property (nonatomic,copy) NSString *url;//车商头像url

- (id)getObjectByKey:(NSString *)key;

- (void)saveValue:(id)value key:(NSString *)key;

- (void)saveValues:(NSDictionary *)dict dataSaveKeys:(NSArray *)keys;

- (void)removeAllValuesKeys;

- (void)removeValueWithKey:(NSString *)key;

- (void)removeAllValuesIgnoreKeys:(NSArray *)keys;

// 显示缓存大小
- (NSString *)filePath;

- (void)clearCaches;

+ (NSString *)appName;

+ (NSString*)bundleID;
//版本号
+ (NSString *)versionCode;

//登录状态
+ (BOOL)isLogined;

/** 获取手势密码 */
+ (NSString *)gesturesPassword;

//记录过手势密码
+ (BOOL)haveGesturePassword;

/** 是否开启手势密码 */
+ (BOOL)haveOpenGesturePassword;

//开启手势密码进行登录
+ (BOOL)openGesturePasswordLogin;

//首次进入检测
+ (BOOL)firstLaunch;

//是否是车商app
+ (BOOL)isCarDealer;

//是否是业务团队app
+ (BOOL)isPartner;

//极光推送的key
+ (NSString *)JpushAppKey;


- (BOOL)isUserNotificationEnable;

- (void)goToAppSystemSetting;

+ (NSString *)registrationID;

+ (NSString *)uid;

+ (BOOL)showGestureTip;

+ (AppType)appType;



@end

