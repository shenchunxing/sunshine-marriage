//
//  YCBankSaasDataCenter.h
//  YCDataCenter
//
//  Created by 沈春兴 on 2019/12/10.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YCBankSaasDataCenter : NSObject
+ (instancetype)sharedData;
@property (nonatomic, copy) NSString *vcUsername;
@property (nonatomic, copy,nullable) NSString *vcPassword;
@property (nonatomic, copy,nullable) NSString *tokenId;
@property (nonatomic, copy) NSString *keyId;
- (NSString *)uuid;
- (NSString *)phoneName; //设备名称
- (NSString *)platformString; //设备型号
@end

NS_ASSUME_NONNULL_END
