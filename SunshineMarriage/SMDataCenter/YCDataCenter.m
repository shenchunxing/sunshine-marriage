//
//  YCDataCenter.m
//  YCDataCenter
//
//  Created by haima on 2019/3/28.
//

#import "YCDataCenter.h"
#import <UIKit/UIKit.h>

static YCDataCenter *instance = nil;

@implementation YCDataCenter

+ (instancetype)sharedData {

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (NSString *)userName {
    
    return [self getObjectByKey:@"user_name"];
}

- (NSString *)userId {
    
    return [self getObjectByKey:@"userId"];
}

- (NSString *)token {
    
    return [self getObjectByKey:@"YCToken"];
}

- (NSString *)name {
    
    return [self getObjectByKey:@"name"];
}

- (NSString *)partner {
    
    return [self getObjectByKey:@"partner"];
}

- (NSString *)partnerId {
    
    return [self getObjectByKey:@"partnerId"];
}

- (NSString *)permission {
    
    return [self getObjectByKey:@"permission"];
}

- (void)setUserName:(NSString *)userName {
    
    [self saveValue:userName key:@"user_name"];
}

- (NSString *)loginPhone{
    return [self getObjectByKey:@"loginPhone"];
}

- (NSString *)loginPassword{
    return [self getObjectByKey:@"loginPassword"];
}

- (NSString *)url{
    return [self getObjectByKey:@"url"];
}

- (id)getObjectByKey:(NSString *)key {
   
    return [[NSUserDefaults standardUserDefaults] valueForKey:key];
}

- (void)saveValue:(id)value key:(NSString *)key {
    
    if (key) {
        [[NSUserDefaults standardUserDefaults] setValue:value forKey:key];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
   
}

- (void)saveValues:(NSDictionary *)dict dataSaveKeys:(NSArray *)keys
{
    for (NSString *key in dict) {
        if (![[dict objectForKey:key] isEqual:[NSNull null]] && [dict objectForKey:key]) {
            id value = [dict objectForKey:key];
            if ([keys containsObject:key]) {
                NSData *data  = [NSKeyedArchiver archivedDataWithRootObject:value];
                 [[NSUserDefaults standardUserDefaults] setObject:data forKey:key];
            }else{
                if ([key isEqualToString:@"token"]) {
                    [[NSUserDefaults standardUserDefaults] setObject:value forKey:@"YCToken"];
                }else{
                    [[NSUserDefaults standardUserDefaults] setObject:value forKey:key];
                }
            }
           
        }
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (void)removeAllValuesKeys{
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSDictionary* dict = [defs dictionaryRepresentation];
    for(id key in dict){
        [defs removeObjectForKey:key];
    }
    [defs synchronize];
}

- (void)removeAllValuesIgnoreKeys:(NSArray *)keys{
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSDictionary* dict = [defs dictionaryRepresentation];
    for(id key in dict){
        if ([keys containsObject:key]) {
             continue;
        }else{
            [defs removeObjectForKey:key];
        }
    }
    [defs synchronize];
}

- (void)removeValueWithKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

// 显示缓存大小
- (NSString *)filePath{
    NSString * cachPath = [ NSSearchPathForDirectoriesInDomains ( NSCachesDirectory , NSUserDomainMask , YES ) firstObject ];
    return [NSString stringWithFormat:@"%.2fM",[self folderSizeAtPath :cachPath]];
}

- (float) folderSizeAtPath:( NSString *) folderPath{
    NSFileManager * manager = [ NSFileManager defaultManager ];
    if (![manager fileExistsAtPath :folderPath]) return 0 ;
    NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath :folderPath] objectEnumerator ];
    NSString * fileName;
    long long folderSize = 0 ;
    while ((fileName = [childFilesEnumerator nextObject ]) != nil ){
        NSString * fileAbsolutePath = [folderPath stringByAppendingPathComponent :fileName];
        folderSize += [ self fileSizeAtPath :fileAbsolutePath];
    }
    return folderSize/( 1024.0 * 1024.0 );
}

- (long) fileSizeAtPath:( NSString *) filePath{
    NSFileManager * manager = [ NSFileManager defaultManager ];
    if ([manager fileExistsAtPath :filePath]){
        return [[manager attributesOfItemAtPath :filePath error : nil ] fileSize];
    }
    return 0 ;
}

#pragma mark ---- clearCaches
- (void)clearCaches{
    
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    NSURLCache * cache = [NSURLCache sharedURLCache];
    [cache removeAllCachedResponses];
    [cache setDiskCapacity:0];
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies])
    {
        [storage deleteCookie:cookie];
    }
    
   
    NSString * cachPath = [ NSSearchPathForDirectoriesInDomains ( NSCachesDirectory , NSUserDomainMask , YES ) firstObject ];
    NSArray * files = [[ NSFileManager defaultManager ] subpathsAtPath :cachPath];
    for ( NSString * p in files) {
        NSError * error = nil ;
        NSString * path = [cachPath stringByAppendingPathComponent :p];
        if ([[ NSFileManager defaultManager ] fileExistsAtPath :path]) {
            [[ NSFileManager defaultManager ] removeItemAtPath :path error :&error];
        }
    }
    [ self performSelectorOnMainThread : @selector (clearCachSuccess) withObject : nil waitUntilDone : YES ];
}

- (void)clearCachSuccess
{
    NSLog(@"清理成功");
}

+ (NSString *)appName{
    return [NSString stringWithFormat:@"%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"]];;
}

+ (NSString *)versionCode{
    return  [NSString stringWithFormat:@"%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
}

+ (NSString *)JpushAppKey{
    return @"61003e99cff377a2aa6f66d4";
}

+ (NSString *)registrationID{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"registrationID"];
}

+ (NSString *)uid{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"uid"];
}

+ (BOOL)isCarDealer{
    return [[YCDataCenter bundleID] isEqualToString:@"com.yunche.YCCarDealer"];
}

+ (BOOL)isPartner{
    return [[YCDataCenter bundleID] isEqualToString:@"com.yunche.digital"];
}

+ (NSString*)bundleID{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
}

+ (BOOL)isLogined{
    return [YCDataCenter sharedData].token.length>0 && [YCDataCenter sharedData].userId.length>0;
}

+ (AppType)appType
{
    if ([self isCarDealer]) {
        return CarDealer;
    }else if ([self isPartner]){
        return Partner;
    }
    return Partner;
}

/** 获取手势密码 */
+ (NSString *)gesturesPassword{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"GesturesPassword"];
}

//记录过手势密码
+ (BOOL)haveGesturePassword{
    return [self gesturesPassword].length > 0;
}

/** 是否开启手势密码 */
+ (BOOL)haveOpenGesturePassword{
    return [YCDataCenter gesturesPassword] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"OpenGesturesPassword"] boolValue];
}

+ (BOOL)openGesturePasswordLogin{
    return [self haveOpenGesturePassword] && [self haveGesturePassword] && [self isLogined];
}


+ (BOOL)firstLaunch{
    NSString *version =  [[NSUserDefaults standardUserDefaults] objectForKey:@"launchServiceAppVersion"];
    if (!version) {
        NSString *code = [self versionCode];
        [[NSUserDefaults standardUserDefaults] setValue:code?:@"" forKey:@"launchServiceAppVersion"];
        return YES;
    }
    return NO;
}

+ (BOOL)showGestureTip{
    NSString *countStr =  [[NSUserDefaults standardUserDefaults] objectForKey:@"gestureTipsCount"];
    if (!countStr) {
        [[NSUserDefaults standardUserDefaults] setValue:@"2" forKey:@"gestureTipsCount"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
         return YES;
    }else if([countStr intValue]>0){
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%d",[countStr intValue]-1] forKey:@"gestureTipsCount"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        return YES;
    }
    return NO;
}


//判断用户是否允许接收通知
- (BOOL)isUserNotificationEnable {
    UIUserNotificationSettings *setting = [[UIApplication sharedApplication] currentUserNotificationSettings];
    return !(UIUserNotificationTypeNone == setting.types);
}

- (void)goToAppSystemSetting {

    if (@available(iOS 8,*))
    {
        if (UIApplicationOpenSettingsURLString != NULL)
        {
            NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            
            if (@available(iOS 10,*)) {
                [[UIApplication sharedApplication]openURL:appSettings options:@{} completionHandler:^(BOOL success) {
                }];
            }
            else
            {
                [[UIApplication sharedApplication]openURL:appSettings];
            }
        }
    }
}



#pragma mark - 阳光婚姻

- (void)setWeChatInfo:(NSDictionary *)weChatInfo {
    [self saveValue:weChatInfo key:@"weChatInfo"];
}

- (NSDictionary *)weChatInfo {
   return [self getObjectByKey:@"weChatInfo"];
}

- (void)setHeadimgurl:(NSString *)headimgurl {
    [self saveValue:headimgurl key:@"headimgurl"];
}

- (NSDictionary *)headimgurl {
   return [self getObjectByKey:@"headimgurl"];
}

- (void)setNickname:(NSString *)nickname {
    [self saveValue:nickname key:@"nickname"];
}

- (NSString *)nickname {
   return [self getObjectByKey:@"nickname"];
}

- (void)setPhone:(NSString *)phone {
    [self saveValue:phone key:@"phone"];
}

- (NSString *)phone {
   return [self getObjectByKey:@"phone"];
}

//- (void)setFirstLogin:(NSString *)firstLogin {
//    [self saveValue:firstLogin key:@"firstLogin"];
//}
//
//- (NSString *)firstLogin {
//   return [self getObjectByKey:@"firstLogin"];
//}

- (void)setSex:(int)sex {
    [self saveValue:@(sex) key:@"sex"];
}

- (int)sex {
   return [[self getObjectByKey:@"sex"] intValue];
}

- (NSString *)appusercode {
   return [self getObjectByKey:@"appusercode"];
}

- (void)setAppusercode:(NSString *)appusercode {
    [self saveValue:appusercode key:@"appusercode"];
}

- (double)insertTime {
   return [[self getObjectByKey:@"insertTime"] doubleValue];
}

- (void)setInsertTime:(double)insertTime {
    [self saveValue:@(insertTime) key:@"insertTime"];
}

- (NSString *)shareId {
    return [self getObjectByKey:@"shareId"];
}

- (void)setShareId:(NSString *)shareId {
     [self saveValue:shareId key:@"shareId"];
}

- (NSString *)thirdCode {
    return [self getObjectByKey:@"thirdCode"];
}

- (void)setThirdCode:(NSString *)thirdCode {
     [self saveValue:thirdCode key:@"thirdCode"];
}

- (NSString *)thirdType {
    return [self getObjectByKey:@"thirdType"];
}

- (void)setThirdType:(NSString *)thirdType {
     [self saveValue:thirdType key:@"thirdType"];
}

- (NSString *)authorization {
    return [self getObjectByKey:@"authorization"];
}

- (void)setAuthorization:(NSString *)authorization {
     [self saveValue:authorization key:@"authorization"];
}

- (void)clearLoginInfo {
    [[YCDataCenter sharedData] removeValueWithKey:@"authorization"] ;
    [[YCDataCenter sharedData] removeValueWithKey:@"appusercode"] ;
    [[YCDataCenter sharedData] removeValueWithKey:@"insertTime"] ;
    [[YCDataCenter sharedData] removeValueWithKey:@"shareId"] ;
    [[YCDataCenter sharedData] removeValueWithKey:@"thirdCode"] ;
    [[YCDataCenter sharedData] removeValueWithKey:@"thirdType"] ;
    [[YCDataCenter sharedData] removeValueWithKey:@"headimgurl"] ;
    [[YCDataCenter sharedData] removeValueWithKey:@"nickname"] ;
    [[YCDataCenter sharedData] removeValueWithKey:@"sex"] ;
    [[YCDataCenter sharedData] removeValueWithKey:@"firstLogin"] ;
}

@end
