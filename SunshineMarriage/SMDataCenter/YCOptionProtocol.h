//
//  YCOptionProtocol.h
//  YCFunctionModule
//
//  Created by haima on 2019/4/4.
//

#import <Foundation/Foundation.h>

@protocol YCOptionProtocol <NSObject>

- (NSString *)obtainCode;
- (NSString *)obtainName;

@optional
- (NSString *)obtainValue;

@end
