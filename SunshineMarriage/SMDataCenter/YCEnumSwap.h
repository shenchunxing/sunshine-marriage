//
//  YCEnumSwap.h
//  YCDataCenter
//
//  Created by 刘成 on 2019/4/22.
//

#import <Foundation/Foundation.h>
#import "YCOptionModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCEnumSwap : NSObject

+ (instancetype)sharedInstancetype;

/**
 获取枚举对应的key或value
 @param key 后台字段对应的枚举字段
 @param swapStr key或value
 @return value或key
 */
- (NSString *)getEnumSwapWithKey:(NSString *)key swapStr:(NSString *)swapStr;

/**
 获取字段对应的枚举数据源
 
 @param key 后台字段对应的枚举字段
 @return 该key对应的数据源
 */
- (NSArray < id<YCOptionProtocol> > *)getDataSourceWithKey:(NSString *)key;

/**
 获取字段对应的枚举数据源
 
 @param key 后台字段对应的枚举字段
 @param sortType 排序 0-从小到大  1-从大到小
 @return 该key对应的数据源
 */
- (NSArray < id<YCOptionProtocol> > *)getDataSourceWithKey:(NSString *)key sortType:(NSInteger)sortType;

@end

NS_ASSUME_NONNULL_END
