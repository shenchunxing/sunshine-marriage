//
//  YCOptionModel.h
//  YCFunctionModule
//
//  Created by haima on 2019/4/4.
//

#import <Foundation/Foundation.h>
#import "YCOptionProtocol.h"
@interface YCOptionModel : NSObject<YCOptionProtocol>

/* code */
@property (nonatomic, copy) NSString *code;
/* name */
@property (nonatomic, copy) NSString *name;

+ (YCOptionModel *)modelWithName:(NSString *)name code:(NSString *)code ;

@end
