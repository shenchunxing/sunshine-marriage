//
//  SMTabBarViewController.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/21.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMTabBarViewController.h"
#import "YCNavigationController.h"
#import "SMHomeViewController.h"
#import "SMReportMainViewController.h"
#import "SMMineViewController.h"
#import "UIColor+Style.h"
#import "UIFont+Style.h"

@interface SMTabBarViewController ()

@end

@implementation SMTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupItemTitleTextAttributes];
    [self setupChildViewControllers];
    
}

/**
 *  设置所有UITabBarItem的文字属性
 */
- (void)setupItemTitleTextAttributes
{
    UITabBarItem *item = [UITabBarItem appearance];
    // 普通状态下的文字属性
    NSMutableDictionary *normalAttrs = [NSMutableDictionary dictionary];
    normalAttrs[NSFontAttributeName] = [UIFont yc_10];
    normalAttrs[NSForegroundColorAttributeName] = [UIColor yc_hex_999999];
    [item setTitleTextAttributes:normalAttrs forState:UIControlStateNormal];
    // 选中状态下的文字属性
    NSMutableDictionary *selectedAttrs = [NSMutableDictionary dictionary];
    selectedAttrs[NSFontAttributeName] = [UIFont yc_10];
    selectedAttrs[NSForegroundColorAttributeName] = [UIColor yc_hex_5360FF];
    [item setTitleTextAttributes:selectedAttrs forState:UIControlStateSelected];
    
    
}

/**
 *  添加子控制器
 */
- (void)setupChildViewControllers {
    [self setupOneChildViewController:[[YCNavigationController alloc] initWithRootViewController:[[SMHomeViewController alloc] init]] title:@"首页" image:@"-s-icon_home-nor.png" selectedImage:@"-s-icon_home_sel.png"];
    [self setupOneChildViewController:[[YCNavigationController alloc] initWithRootViewController:[[SMReportMainViewController alloc] init]] title:@"报告" image:@"-s-icon_baogao_nor.png" selectedImage:@"icon_baogao_sel.png"];
    [self setupOneChildViewController:[[YCNavigationController alloc] initWithRootViewController:[[SMMineViewController alloc] init]] title:@"我的" image:@"-s-icon_wode_nor.png" selectedImage:@"-s-icon_wode_sel.png"];
}

/**
 *  初始化一个子控制器
 *
 *  @param vc            子控制器
 *  @param title         标题
 *  @param image         图标
 *  @param selectedImage 选中的图标
 */
- (void)setupOneChildViewController:(UIViewController *)vc title:(NSString *)title image:(NSString *)image selectedImage:(NSString *)selectedImage {
    vc.tabBarItem.title = title;
    if (image.length) { // 图片名有具体值
        vc.tabBarItem.image = [UIImage imageNamed:image];
        vc.tabBarItem.selectedImage = [UIImage imageNamed:selectedImage];
    }
    [self addChildViewController:vc];
}

@end
