//
//  YCMediator+SMTabBar.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/22.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "YCMediator+SMTabBar.h"

NSString *const SMTabBar = @"SMTabBar";
NSString *const SMActionTabBarController = @"tabBarController";

@implementation YCMediator (SMLogin)

- (UIViewController *)tabBarControllerWithParmas:(NSDictionary * _Nullable)parmas {
    return [self performTarget:SMTabBar
                        action:SMActionTabBarController
                        params:parmas
             shouldCacheTarget:NO];
}
@end
