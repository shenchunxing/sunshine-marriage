//
//  YCMediator+SMTabBar.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/22.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "YCMediator.h"
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YCMediator (SMTabBar)

- (UIViewController *)tabBarControllerWithParmas:(NSDictionary * _Nullable)parmas ;

@end

NS_ASSUME_NONNULL_END
