//
//  YCMediator.m
//  YCMediator
//
//  Created by haima on 2019/3/22.
//

#import "YCMediator.h"

#define KYCAppScheme @"YCPartner"

static YCMediator *_instance = nil;

@interface YCMediator ()
/* cache target */
@property (nonatomic, strong) NSMutableDictionary *cachedTarget;
@end

@implementation YCMediator

#pragma mark - 绝对单例
+(instancetype) sharedInstance{
    if (_instance == nil) {
        _instance = [[super alloc]init];
    }
    return _instance;
}

+(instancetype)allocWithZone:(struct _NSZone *)zone{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [super allocWithZone:zone ];
    });
    return _instance;
}


-(id)copyWithZone:(NSZone *)zone{
    return _instance;
}

-(id)mutableCopyWithZone:(NSZone *)zone{
    return _instance;
}

#pragma mark - 远程调用入口

/**
url格式：
 scheme://[target]/[action]?key1=value1&key2=value2
 
 example：YCPartner://ModuleA/IndexView?id=22222
 */
- (id)performActionWithUrl:(NSURL *)url completion:(void (^)(NSDictionary * _Nonnull))completion {
    
    //MARK:url做简单的过滤
    if (![url.scheme isEqualToString:KYCAppScheme]) {
        return nil;
    }
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    //url后拼接的参数
    NSString *urlQuery = [url query];
    for (NSString *param in [urlQuery componentsSeparatedByString:@"&"]) {
        NSArray *elts = [param componentsSeparatedByString:@"="];
        if (elts.count < 2) continue;
        params[elts.firstObject] = elts.lastObject;
    }
    //action
    NSString *actionName = [url.path stringByReplacingOccurrencesOfString:@"/" withString:@""];
    //过滤本地调用
    if ([actionName hasPrefix:@"native"]) {
        return nil;
    }
    id result = [self performTarget:url.host action:actionName params:params shouldCacheTarget:NO];
    return result;
}

#pragma mark - 本地调用入口
- (id)performTarget:(NSString *)targetName action:(NSString *)actionName params:(NSDictionary *)params shouldCacheTarget:(BOOL)shouldCacheTarget {

    //target
    NSString *targetClassString = [NSString stringWithFormat:@"Target_%@", targetName];
    NSObject *target = self.cachedTarget[targetClassString];
    if (target == nil) {
        Class targetClass = NSClassFromString(targetClassString);
        target = [[targetClass alloc] init];
    }
    
    //action
    NSString *actionString = [NSString stringWithFormat:@"Action_%@:", actionName];
    SEL action = NSSelectorFromString(actionString);
    
    if (target == nil) {
        //如果没有响应的target，可以指定target处理无响应的情况，根据产品设计
        return nil;
    }
    
    if (shouldCacheTarget) {
        self.cachedTarget[targetClassString] = target;
    }
    
    if ([target respondsToSelector:action]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        return [target performSelector:action withObject:params];
#pragma clang diagnostic pop
    } else {
        SEL action = NSSelectorFromString(@"notFound:");
        
        if ([target respondsToSelector:action]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            return [target performSelector:action withObject:params];
#pragma clang diagnostic pop
        }else {
            //MARK:这里可以处理错误页面
            [self.cachedTarget removeObjectForKey:targetClassString];
            return nil;
        }
    }
}

- (void)clearCachedTarget:(NSString *)targetName {
    
    NSString *targetClassString = [NSString stringWithFormat:@"Target_%@", targetName];
    [self.cachedTarget removeObjectForKey:targetClassString];
}

#pragma mark - getter

- (NSMutableDictionary *)cachedTarget {
    
    if (_cachedTarget == nil) {
        _cachedTarget = [[NSMutableDictionary alloc] init];
        
    }
    return _cachedTarget;
}

@end
