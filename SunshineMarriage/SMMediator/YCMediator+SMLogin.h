//
//  YCMediator+SMLogin.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/22.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "YCMediator.h"
#import "SMNavigationController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YCMediator (SMLogin)

- (SMNavigationController *)loginViewControllerWithParmas:(NSDictionary * _Nullable)parmas ;

@end

NS_ASSUME_NONNULL_END
