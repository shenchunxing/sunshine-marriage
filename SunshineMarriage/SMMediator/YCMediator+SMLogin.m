//
//  YCMediator+SMLogin.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/22.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "YCMediator+SMLogin.h"

NSString *const SMLogin = @"SMLogin";
NSString *const SMActionNativeLoginViewController = @"nativeLoginViewController";

@implementation YCMediator (SMLogin)

- (SMNavigationController *)loginViewControllerWithParmas:(NSDictionary * _Nullable)parmas {
    return [self performTarget:SMLogin
                        action:SMActionNativeLoginViewController
                        params:parmas
             shouldCacheTarget:NO];
}
@end
