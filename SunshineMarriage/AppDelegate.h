//
//  AppDelegate.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/21.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WXApi.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,WXApiDelegate>

@property (nonatomic, strong) UIWindow *window;

@end

