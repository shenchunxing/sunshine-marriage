//
//  YCNetworkParamConstructorProtocol.h
//  YCNetworking
//
//  Created by shenweihang on 2019/12/9.
//

#import <Foundation/Foundation.h>
#import "YCRequestModel.h"
NS_ASSUME_NONNULL_BEGIN

@protocol YCNetworkParamConstructorProtocol <NSObject>

+ (instancetype)constructor;

//根据YCRequestModel，获取参数
- (NSMutableDictionary *)parametersForRequest:(YCRequestModel *)requestModel;

/**
 头部参数
 
 @param requestModel YCRequestModel
 @return 头部参数
 */
- (NSMutableDictionary *)headersForRequest:(YCRequestModel *)requestModel;

@end

NS_ASSUME_NONNULL_END
