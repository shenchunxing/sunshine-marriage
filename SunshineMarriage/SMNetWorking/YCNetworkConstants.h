//
//  YCNetworkConstants.h
//  YCNetworking
//
//  Created by shenweihang on 2019/9/23.
//

#import <Foundation/Foundation.h>

//精真估相关参数定义
FOUNDATION_EXPORT NSString * const _Nonnull kJZGToken;
FOUNDATION_EXPORT NSString * const _Nonnull kJZGUserId;
FOUNDATION_EXPORT NSString * const _Nonnull kJZGSignKey;
FOUNDATION_EXPORT NSString * const _Nonnull kJZGAPIRoot;


FOUNDATION_EXPORT NSString * _Nonnull kProjectAPIRoot;



