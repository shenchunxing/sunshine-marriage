//
//  YCRequestParamConstructor.m
//  YCNetworking
//
//  Created by haima on 2019/3/26.
//

#import "YCRequestParamConstructor.h"
#import "YCRequestModel.h"
#import "YCNetworkConfig.h"
#import "YCDataCenter.h"

@implementation YCRequestParamConstructor

+ (instancetype)constructor {
    return [[YCRequestParamConstructor alloc] init];
}

- (NSMutableDictionary *)parametersForRequest:(YCRequestModel *)requestModel {

    NSMutableDictionary *params = [NSMutableDictionary dictionary];

    //添加业务参数
    [params addEntriesFromDictionary:requestModel.parameters];
    
    return params;
}

- (NSMutableDictionary *)headersForRequest:(YCRequestModel *)requestModel {
    
    NSMutableDictionary *headers = [NSMutableDictionary dictionary];
    //添加共通参数
//    headers[@"authorization"] = @"b63a5fa5-aaef-4964-8fad-e8b5e84b943d"; // [YCDataCenter sharedData].authorization ;
//    headers[@"appusercode"] = @"081YPy4C1zVZS60uYJ6C18WG4C1YPy4M" ;// [YCDataCenter sharedData].thirdCode; //
    return headers;
}


//+ (NSString *)yc_operateId {
//    //TODO:添加操作id
//    return @"";
//}
//
//+ (NSString *)yc_operateName {
//    //TODO:添加操作名
//    return @"";
//}
//
//+ (NSString *)yc_roleName {
//    return @"";
//}

@end
