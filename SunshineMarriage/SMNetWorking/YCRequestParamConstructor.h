//
//  YCRequestParamConstructor.h
//  YCNetworking
//
//  Created by haima on 2019/3/26.
//

#import <Foundation/Foundation.h>
#import "YCNetworkParamConstructorProtocol.h"
NS_ASSUME_NONNULL_BEGIN

@class YCRequestModel;
@interface YCRequestParamConstructor : NSObject<YCNetworkParamConstructorProtocol>

@end

NS_ASSUME_NONNULL_END
