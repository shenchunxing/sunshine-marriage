//
//  SMNotificationMacros.h
//  SMDefineModule
//
//  Created by 沈春兴 on 2019/12/31.
//

#ifndef SMNotificationMacros_h
#define SMNotificationMacros_h

#define kMessageBusinessNoticeNotification  @"kMessageBusinessNoticeNotification"
#define kMessageSearchNotification  @"kMessageSearchNotification"
#define kMessageAllReadedNotification  @"kMessageAllReadedNotification"

#define kCreditDetailUpdateNotification  @"kCreditDetailUpdateNotification"
#define NOTI_WXLOGIN_USERCANCELLED   @"NOTI_WXLOGIN_USERCANCELLED"
#define NOTI_WXLOGIN_AUTHORIZED @"NOTI_WXLOGIN_AUTHORIZED"

#define kUpdatePersonInfoNotification @"kUpdatePersonInfoNotification"
#define kNotificationName_loginOut @"kNotificationName_loginOut"
#define kReportSubmitSuccessNotification @"kReportSubmitSuccessNotification"

#endif /* SMNotificationMacros_h */
