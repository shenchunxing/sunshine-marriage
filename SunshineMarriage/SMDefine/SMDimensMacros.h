//
//  Header.h
//  SMDefineModule
//
//  Created by 沈春兴 on 2019/12/30.
//

#ifndef SMDimensMacros_h
#define SMDimensMacros_h

/** 屏幕尺寸 */
#define isRetina ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)
#define isIphone4s ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)
#define isIphone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
#define isIphone6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)
#define isIphone6Plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size) : NO)
#define isIphoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)

//状态栏高度
#define kStatusBarHeight [UIApplication sharedApplication].statusBarFrame.size.height
//导航栏高度
#define kNavigationHeight (kStatusBarHeight + 44)
//底部的安全距离
#define kBottomSafeAreaHeight [UIApplication sharedApplication].delegate.window.safeAreaInsets.bottom
//tabbar高度
#define kTabBarHeight (kStatusBarHeight == 44 ? 83 : 49)

#define kScreenHEIGHT [UIScreen mainScreen].bounds.size.height
#define kWIDTH [UIScreen mainScreen].bounds.size.width
#define kHEIGHT (IPHONE_X?(kScreenHEIGHT-34):kScreenHEIGHT)

#define kIphoneXBottom  (IPHONE_X? 34:0)

//等比例适配
#define kScaleFit (kWIDTH / 375.0)
#define kScaleFitHeight (kScreenHEIGHT / 667.0)
#define FitScale(size)  size * kScaleFit

#define VW(view) (view.frame.size.width)
#define VH(view) (view.frame.size.height)
#define VX(view) (view.frame.origin.x)
#define VY(view) (view.frame.origin.y)
#define FW(view) (VW(view)+VX(view))
#define FH(view) (VH(view)+VY(view))

#define ABOVE_IOS10 (([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0) ? YES : NO)

#define IPHONE_X \
({BOOL isPhoneX = NO;\
if (@available(iOS 11.0, *)) {\
isPhoneX = [[UIApplication sharedApplication] delegate].window.safeAreaInsets.bottom > 0.0;\
}\
(isPhoneX);})

#define isIpad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#define kLineHeight 1/[UIScreen mainScreen].scale

#define kDefaultCellH 45.0

#endif /* SMDimensMacros_h */
