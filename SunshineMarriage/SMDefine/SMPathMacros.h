//
//  PathMacros.h
//  SMDefineModule
//
//  Created by 沈春兴 on 2019/12/30.
//

#ifndef SMPathMacros_h
#define SMPathMacros_h

//---------------------SandBox目录--------------------------
//文件目录
#define kPathTemp(path)             [NSTemporaryDirectory() stringByAppendingPathComponent:path]
#define kPathDocument(path)         [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:path]
#define kPathCache(path)            [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:path]


#endif /* SMPathMacros_h */
