//
//  SMRequestModel.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/30.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMRequestModel.h"

@implementation SMRequestModel

+ (instancetype)modelWithActionPath:(const NSString *)actionPath {
    SMRequestModel *requestModel = [[SMRequestModel alloc] init];
    requestModel.actionPath = (NSString *)actionPath;
    requestModel.portName = @"";
    requestModel.apiVersion = @"";
    return requestModel;
}

@end
