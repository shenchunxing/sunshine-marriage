//
//  SMProblemItem.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/24.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "YCTableViewItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface SMProblemItem : YCTableViewItem

@property (nonatomic, copy) NSString *text;
@property (nonatomic, assign) BOOL hide;

@end

NS_ASSUME_NONNULL_END
