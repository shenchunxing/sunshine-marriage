//
//  SMPersonInfoModel.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/2/2.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "YCBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SMPersonInfoModel : YCBaseModel

@property (nonatomic, copy) NSString *age;
@property (nonatomic, copy) NSString *education;
@property (nonatomic, assign) int id;
@property (nonatomic, copy) NSString *income;
@property (nonatomic, assign) double insertTime;
@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, copy) NSString *orgId;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *sex;
@property (nonatomic, copy) NSString *shareId;
@property (nonatomic, copy) NSString *thirdCode;
@property (nonatomic, copy) NSString *thirdType;
@property (nonatomic, copy) NSString *workType;


@end

NS_ASSUME_NONNULL_END
