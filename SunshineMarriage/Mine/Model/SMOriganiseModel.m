//
//  SMOriganiseModel.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/2/1.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMOriganiseModel.h"

@implementation SMOriganiseModel

- (id)copyWithZone:(NSZone *)zone{
    SMOriganiseModel *model =  [[[self class] allocWithZone:zone] init];
    model.name = self.name;
    model.des = self.des;
    model.id = self.id;
    model.level = self.level;
    model.parentId = self.parentId;
    return model ;
}

@end
