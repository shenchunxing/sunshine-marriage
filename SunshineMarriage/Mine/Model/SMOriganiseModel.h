//
//  SMOriganiseModel.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/2/1.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SMOriganiseModel : NSObject<NSCopying>

@property (nonatomic, copy) NSString *des;
@property (nonatomic, assign) int id;
@property (nonatomic, assign) int level;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) int parentId;

@end

NS_ASSUME_NONNULL_END
