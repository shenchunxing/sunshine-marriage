//
//  SMSettingItem.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/24.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "YCTableViewItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface SMSettingItem : YCTableViewItem

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *detailName;
@property (nonatomic, strong) UIColor *detailColor;

@end

NS_ASSUME_NONNULL_END
