//
//  SMSettingCell.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/24.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMSettingCell.h"
#import "SMSettingItem.h"
#import <Masonry/Masonry.h>
#import "UIFont+Style.h"
#import "UIColor+Style.h"
#import "UILabel+Extension.h"
#import "SMDefine.h"

@interface SMSettingCell ()
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *detailLabel;
@property (nonatomic, strong) UIImageView *arrowImageView;
@property (nonatomic, strong) UIView *line;
@end

@implementation SMSettingCell

- (void)cellDidLoad {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(25);
        make.centerY.mas_equalTo(self.contentView);
    }];
    
    [self.contentView addSubview:self.arrowImageView];
    [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-25);
        make.width.mas_equalTo(6.5);
        make.height.mas_equalTo(12);
        make.centerY.mas_equalTo(self.contentView);
    }];
    
    [self.contentView addSubview:self.detailLabel];
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-47.5);
        make.centerY.mas_equalTo(self.contentView);
    }];
    
    [self.contentView addSubview:self.line];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-25);
        make.left.mas_equalTo(25);
        make.height.mas_equalTo(kLineHeight);
        make.top.mas_equalTo(self.contentView.mas_bottom).mas_offset(-kLineHeight);
    }];
    
}

+ (CGFloat)heightForCellWithItem:(SMSettingItem *)item {
    return 60;
}

- (void)configCellWithItem:(SMSettingItem *)item {
    self.nameLabel.text = item.name;
    self.detailLabel.text = item.detailName;
    if (item.detailColor) {
        self.detailLabel.textColor = item.detailColor;
    }
}

- (UIView *)line {
    if (!_line) {
        _line = [[UIView alloc] init];
        _line.backgroundColor = [UIColor yc_hex_EEEEEE];
    }
    return _line;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel yc_labelWithText:nil textFont:[UIFont yc_15] textColor:[UIColor yc_hex_333333]];
    }
    return _nameLabel;
}

- (UILabel *)detailLabel {
    if (!_detailLabel) {
        _detailLabel = [UILabel yc_labelWithText:nil textFont:[UIFont yc_13] textColor:[UIColor yc_hex_333333]];
    }
    return _detailLabel;
}

- (UIImageView *)arrowImageView {
    if (!_arrowImageView) {
        _arrowImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"jiantou"]];
    }
    return _arrowImageView;
}

@end
