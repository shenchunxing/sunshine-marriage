//
//  SMMineFunctionView.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/24.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMMineFunctionView.h"
#import "UIColor+Style.h"
#import "UIFont+Style.h"
#import "SMDefine.h"
#import <Masonry/Masonry.h>
#import "UILabel+Extension.h"

@interface SMMineFunctionView ()
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UIImageView *arrowImageView;
@end

@implementation SMMineFunctionView

+ (instancetype)functionWithIconImageName:(NSString *)imageName name:(NSString *)name tapClick:(void(^)(void))tapClickBlock {
    SMMineFunctionView *function = [[SMMineFunctionView alloc] init];
    function.iconImageView.image = [UIImage imageNamed:imageName];
    function.nameLabel.text = name;
    function.tapClickBlock = tapClickBlock;
    return function;
}

- (instancetype)init {
    if (self = [super init]) {
        [self addSubview:self.iconImageView];
        [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self);
            make.left.mas_equalTo(13);
            make.width.height.mas_equalTo(22);
        }];
        
        [self addSubview:self.nameLabel];
        [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self);
            make.left.mas_equalTo(self.iconImageView.mas_right).mas_offset(13.5);
        }];
        
        [self addSubview:self.arrowImageView];
        [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self);
            make.right.mas_equalTo(-13);
            make.width.mas_equalTo(6.5);
            make.height.mas_equalTo(13);
        }];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick)];
        [self addGestureRecognizer:tap];
    }
    return self;
}

- (void)tapClick {
    !self.tapClickBlock?:self.tapClickBlock();
}

- (UIImageView *)iconImageView {
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] init];
    }
    return _iconImageView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel yc_labelWithText:nil textFont:[UIFont yc_15] textColor:[UIColor yc_hex_333333]];
    }
    return _nameLabel;
}

- (UIImageView *)arrowImageView {
    if (!_arrowImageView) {
        _arrowImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"jiantou"]];
    }
    return _arrowImageView;
}

@end
