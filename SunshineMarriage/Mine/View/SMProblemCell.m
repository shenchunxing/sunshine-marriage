//
//  SMProblemCell.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/24.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMProblemCell.h"
#import "SMProblemItem.h"
#import "UIColor+Style.h"
#import "UIFont+Style.h"
#import "SMDefine.h"
#import <Masonry/Masonry.h>
#import "UILabel+Extension.h"

@interface SMProblemCell ()
@property (nonatomic, strong) UILabel *contentLabel;
@end

@implementation SMProblemCell

- (void)cellDidLoad {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = [UIColor yc_hex_ECF0F5];
    [self.contentView addSubview:self.contentLabel];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(25);
        make.top.mas_equalTo(15.5);
        make.right.mas_equalTo(-25);
    }];
}

+ (CGFloat)heightForCellWithItem:(SMProblemItem *)item {
    if (item.hide) {
        return 0;
    }
    return [item.text boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width -51, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont yc_14]} context:nil].size.height +31;
}

- (void)configCellWithItem:(SMProblemItem *)item {
    self.contentLabel.text = item.text;
    self.contentLabel.hidden = item.hide;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [UILabel yc_labelWithText:nil textFont:[UIFont yc_14] textColor:[UIColor yc_hex_222222]];
        _contentLabel.numberOfLines = 0;
    }
    return _contentLabel;
}

@end
