//
//  SMMineFunctionView.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/24.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SMMineFunctionView : UIView

@property (nonatomic, copy) void(^tapClickBlock)(void) ;
+ (instancetype)functionWithIconImageName:(NSString *)imageName name:(NSString *)name tapClick:(void(^)(void))tapClickBlock;
@end

NS_ASSUME_NONNULL_END
