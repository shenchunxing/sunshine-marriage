//
//  SMAboutUsViewController.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/24.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMAboutUsViewController.h"
#import "SMDefine.h"
#import "YCCategoryModule.h"
#import "UIColor+Style.h"
#import "UIFont+Style.h"
#import <Masonry/Masonry.h>
#import "YCDataCenter.h"

@interface SMAboutUsViewController ()
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *verisonLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@end

@implementation SMAboutUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationBar.title = @"关于我们";
    
    [self.view addSubview:self.iconImageView];
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kNavigationHeight+60);
        make.width.height.mas_equalTo(80);
        make.centerX.mas_equalTo(self.view);
    }];
    
    [self.view addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view);
        make.top.mas_equalTo(self.iconImageView.mas_bottom).mas_offset(25);
    }];
    
    [self.view addSubview:self.verisonLabel];
    [self.verisonLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view);
        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(14);
    }];
    
    [self.view addSubview:self.contentLabel];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.verisonLabel.mas_bottom).mas_offset(68);
        make.left.mas_equalTo(28);
    }];
}

- (UIImageView *)iconImageView {
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon"]];
    }
    return _iconImageView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel yc_labelWithText:[YCDataCenter appName] textFont:[UIFont yc_24_bold] textColor:[UIColor yc_hex_333333] textAlignment:NSTextAlignmentCenter];
    }
    return _nameLabel;
}

- (UILabel *)verisonLabel {
    if (!_verisonLabel) {
        _verisonLabel = [UILabel yc_labelWithText:[NSString stringWithFormat:@"V%@",[YCDataCenter versionCode]] textFont:[UIFont yc_18] textColor:[UIColor yc_hex_333333] textAlignment:NSTextAlignmentCenter];
    }
    return _verisonLabel;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [UILabel yc_labelWithText:@"由浙江省婚姻家庭协会研发，旨在帮助用户婚前\n，婚内，婚后如何与对方相处，帮助夫妻间维持\n稳定的感情。" textFont:[UIFont yc_15] textColor:[UIColor yc_hex_333333] textAlignment:NSTextAlignmentLeft];
        _contentLabel.numberOfLines = 3;
    }
    return _contentLabel;
}

@end
