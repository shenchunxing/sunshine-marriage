//
//  SMPersonalInfoViewController.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/27.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMPersonalInfoViewController.h"
#import "YCTableViewKit.h"
#import "SMProblemItem.h"
#import "SMDefine.h"
#import "UIColor+Style.h"
#import "UIFont+Style.h"
#import <Masonry/Masonry.h>
#import "SMFoldSection.h"
#import "YCBaseUI.h"
#import "UIButton+Extension.h"
#import "SMPickerItem.h"
#import "SMNamePickerStrategy.h"
#import "SMOptionPickerStrategy.h"
#import "SMAppUserAPI.h"
#import "SMRequestModel.h"
#import "SMOriganiseModel.h"
#import <YYModel/YYModel.h>
#import "SMPersonInfoModel.h"

@interface SMPersonalInfoViewController ()
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) YCTableViewManager *manager;
@property (nonatomic, strong) UIButton *finishBtn;
@property (nonatomic, strong) YCGeneralAPI *userInfoAPI;
@property (nonatomic, strong) YCGeneralAPI *setUserInfoAPI;
@property (nonatomic, strong) YCGeneralAPI *origanizsAPI;
@property (nonatomic, strong) NSMutableArray <YCOptionModel *>*origanizs;

@property (nonatomic, strong) SMPickerItem *name;
@property (nonatomic, strong) YCTextViewItem *share;
@property (nonatomic, strong) SMPickerItem *sex;
@property (nonatomic, strong) SMPickerItem *age;
@property (nonatomic, strong) YCTextViewItem *phone;
@property (nonatomic, strong) SMPickerItem *level;
@property (nonatomic, strong) SMPickerItem *type;
@property (nonatomic, strong) SMPickerItem *income;
@property (nonatomic, strong) SMPickerItem *address;



@end

@implementation SMPersonalInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationBar.title = @"个人信息";
}

- (void)initView {
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kNavigationHeight);
        make.height.mas_equalTo(self.view).mas_offset(-kNavigationHeight);
        make.width.left.mas_equalTo(self.view);
    }];
  
    [self.view addSubview:self.finishBtn];
    [self.finishBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.view).mas_offset(-48);
        make.height.mas_equalTo(48);
        make.left.mas_equalTo(25);
        make.right.mas_equalTo(-25);
    }];
}

- (void)loadData {
    [self.origanizsAPI start];
}

- (void)initData {
    YCTableViewSection *baseSection = [YCTableViewSection section];
    [self.manager addSection:baseSection];

    YCTextViewItem *share = [YCTextViewItem textViewItem];
    share.title = @"分享码";
    share.alignedRight = YES;
    share.editStyle = YCEditStyleUnEditable;
    share.textColor = [UIColor yc_hex_999999];
    share.hideClearBtn = YES;
    share.text =[YCDataCenter sharedData].shareId ;
    share.key = @"shareId";
    [baseSection addItem:share];
    self.share = share;
    
    SMPickerItem *name = [SMPickerItem item];
    name.title = @"姓名";
    name.key = @"nickName";
    name.text = [YCDataCenter sharedData].nickname;
    [baseSection addItem:name];
    SMNamePickerStrategy *nameStrategy = [SMNamePickerStrategy stragetyWithNamePickerStrategyBlock:^(NSString *text) {
        name.text = text;
        [self.manager reloadData];
    }];
    name.strategy = nameStrategy ;
    self.name = name;
    
    SMPickerItem *sex = [SMPickerItem item];
    sex.title = @"性别";
    sex.key = @"sex";
    [baseSection addItem:sex];
    SMOptionPickerStrategy *sexStrategy = [SMOptionPickerStrategy stragetyWithBlock:^(YCOptionModel *model) {
        sex.optionModel = model;
        [self.manager reloadData];
    }];
    sexStrategy.options = [[YCEnumSwap sharedInstancetype] getDataSourceWithKey:@"sex"];
    sex.strategy = sexStrategy;
    self.sex = sex;
    
    SMPickerItem *age = [SMPickerItem item];
    age.title = @"年龄";
    age.key = @"age";
    [baseSection addItem:age];
    SMOptionPickerStrategy *ageStrategy = [SMOptionPickerStrategy stragetyWithBlock:^(YCOptionModel *model) {
        age.optionModel = model ;
        [self.manager reloadData];
    }];
    ageStrategy.options = [[YCEnumSwap sharedInstancetype] getDataSourceWithKey:@"age"];
    age.strategy = ageStrategy;
    self.age = age;
    
    
    YCTextViewItem *phone = [YCTextViewItem textViewItem];
    phone.alignedRight = YES;
    phone.title = @"手机号";
    phone.key = @"phone";
    phone.textColor = [UIColor yc_hex_999999];
    phone.hideClearBtn = YES;
    phone.keyBoardType = UIKeyboardTypeNumberPad ;
    phone.limitLength = 11;
    [baseSection addItem:phone];
    self.phone = phone;
    
    YCTableViewSection *otherSection = [YCTableViewSection section];
    otherSection.headerView = [UIView new];
    otherSection.headerHeight = 10;
    [self.manager addSection:otherSection];

    SMPickerItem *level = [SMPickerItem item];
    level.title = @"文化程度";
    level.key = @"education";
    [otherSection addItem:level];
    SMOptionPickerStrategy *levelStrategy = [SMOptionPickerStrategy stragetyWithBlock:^(YCOptionModel *model) {
        level.optionModel = model ;
        [self.manager reloadData];
    }];
    levelStrategy.options = [[YCEnumSwap sharedInstancetype] getDataSourceWithKey:@"level"];
    level.strategy = levelStrategy;
    self.level = level;
    
    SMPickerItem *type = [SMPickerItem item];
    type.title = @"工作类型";
    type.key = @"workType";
    [otherSection addItem:type];
    SMOptionPickerStrategy *typeStrategy = [SMOptionPickerStrategy stragetyWithBlock:^(YCOptionModel *model) {
        type.optionModel = model ;
        [self.manager reloadData];
    }];
    typeStrategy.options = [[YCEnumSwap sharedInstancetype] getDataSourceWithKey:@"type"];
    type.strategy = typeStrategy;
    self.type = type;
    
    SMPickerItem *income = [SMPickerItem item];
    income.title = @"收入（选填）";
    income.key = @"income";
    [otherSection addItem:income];
    SMOptionPickerStrategy *incomeStrategy = [SMOptionPickerStrategy stragetyWithBlock:^(YCOptionModel *model) {
        income.optionModel = model ;
        [self.manager reloadData];
    }];
    incomeStrategy.options = [[YCEnumSwap sharedInstancetype] getDataSourceWithKey:@"income"];;
    income.strategy = incomeStrategy;
    self.income = income;
    
    SMPickerItem *address = [SMPickerItem item];
    address.title = @"所属婚姻登记处";
    address.key = @"orgId";
    [otherSection addItem:address];
    self.address = address;
    
    [self.manager reloadData];
}

- (void)finish {
    if (![YCEditViewHelper sm_isAnyItemValiableInSections:self.manager.sections showMessage:YES]) {
        [self showCustomMessage_SM:@"请填写完所有信息再保存"];
        return ;
    }
    self.setUserInfoAPI.requestModel.actionPath = [NSString stringWithFormat:@"setUserInfo/?phone=%@&nickName=%@&sex=%@&age=%@&education=%@&workType=%@&income=%@&orgId=%@",self.phone.text,self.name.text,self.sex.optionModel.code,self.age.optionModel.code,self.level.optionModel.code,self.type.optionModel.code,self.income.optionModel.code,self.address.optionModel.code];
    [self.setUserInfoAPI start];
}

- (YCTableViewManager *)manager {
    if (!_manager) {
        _manager = [[YCTableViewManager alloc] initWithTableView:self.tableView];
        [_manager registerItems:@[@"SMPickerItem",@"YCTextViewItem"]];
    }
    return _manager;
}

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] init];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor yc_hex_F5F6FA];
    }
    return _tableView;
}

- (UIButton *)finishBtn {
    if (!_finishBtn) {
        _finishBtn = [UIButton yc_customTextWithTitle:@"完成" titleColor:[UIColor whiteColor] fontSize:[UIFont yc_17] target:self action:@selector(finish)];
        [_finishBtn yc_radiusWithRadius:24];
        _finishBtn.backgroundColor = [UIColor yc_hex_5360FF];
    }
    return _finishBtn;
}

- (YCGeneralAPI *)userInfoAPI {
    if (!_userInfoAPI) {
        _userInfoAPI = [SMAppUserAPI userWithType:GetUserInfoByShareId success:^(__kindof YCBaseAPI * _Nonnull api, id  _Nonnull response) {
            NSDictionary *resBizMap = response[@"resBizMap"];
            SMPersonInfoModel *model = [SMPersonInfoModel yy_modelWithJSON:resBizMap[@"data"]];
            [self configItemWithModel:model];
        }];
        _userInfoAPI.requestModel.actionPath = [NSString stringWithFormat:@"getUserInfoByShareId/?shareId=%@",[YCDataCenter sharedData].shareId];
    }
    return _userInfoAPI;
}

- (void)configItemWithModel:(SMPersonInfoModel *)model {
    self.share.text = model.shareId;
    self.name.text = model.nickName?:[YCDataCenter sharedData].nickname ;
    self.phone.text = model.phone ;
    
    self.sex.optionModel = [YCOptionModel modelWithName:[[YCEnumSwap sharedInstancetype] getEnumSwapWithKey:@"sex" swapStr:model.sex?:[NSString stringWithFormat:@"%d",[YCDataCenter sharedData].sex]] code:model.sex?:[NSString stringWithFormat:@"%d",[YCDataCenter sharedData].sex]] ;

    self.age.optionModel = [YCOptionModel modelWithName:[[YCEnumSwap sharedInstancetype] getEnumSwapWithKey:@"age" swapStr:model.age] code:model.age] ;
    
    self.level.optionModel = [YCOptionModel modelWithName:[[YCEnumSwap sharedInstancetype] getEnumSwapWithKey:@"level" swapStr:model.education] code:model.education] ;
    
    self.type.optionModel = [YCOptionModel modelWithName:[[YCEnumSwap sharedInstancetype] getEnumSwapWithKey:@"type" swapStr:model.workType] code:model.workType] ;
    
    self.income.optionModel = [YCOptionModel modelWithName:[[YCEnumSwap sharedInstancetype] getEnumSwapWithKey:@"income" swapStr:model.income] code:model.income] ; ;
    
    self.address.optionModel = [YCOptionModel modelWithName:[self getNameWithCode:model.orgId] code:model.orgId];

     [self.manager reloadData];
}

- (YCGeneralAPI *)setUserInfoAPI {
    if (!_setUserInfoAPI) {
        _setUserInfoAPI = [SMAppUserAPI userWithType:SetUserInfo success:^(__kindof YCBaseAPI * _Nonnull api, id  _Nonnull response) {
            [self showCustomMessage_SM:@"修改成功"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
                [[NSNotificationCenter defaultCenter] postNotificationName:kUpdatePersonInfoNotification object:nil];
            });
        }];
    }
    return _setUserInfoAPI;
}

- (YCGeneralAPI *)origanizsAPI {
    if (!_origanizsAPI) {
        _origanizsAPI = [SMAppUserAPI userWithType:GetOrganizations success:^(__kindof YCBaseAPI * _Nonnull api, id  _Nonnull response) {
            
            NSDictionary *resBizMap = response[@"resBizMap"];
            NSArray *origaniseModels = [NSArray yy_modelArrayWithClass:[SMOriganiseModel class] json:resBizMap[@"data"]];

            [self.origanizs removeAllObjects];
            [origaniseModels enumerateObjectsUsingBlock:^(SMOriganiseModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                YCOptionModel *model = [[YCOptionModel alloc] init];
                model.name = obj.name;
                model.code = [NSString stringWithFormat:@"%d",obj.id] ;
                [self.origanizs addObject:model];
            }];
            
            SMOptionPickerStrategy *addressStrategy = [SMOptionPickerStrategy stragetyWithBlock:^(YCOptionModel *model) {
                self.address.optionModel = model ;
                [self.manager reloadData];
            }];
            
            addressStrategy.options = self.origanizs;
            self.address.strategy = addressStrategy;
            
            [self.userInfoAPI start];

        }];
    }
    return _origanizsAPI;
}

- (NSMutableArray<YCOptionModel *> *)origanizs {
    if (!_origanizs) {
        _origanizs = [ NSMutableArray new];
    }
    return _origanizs;
}

- (NSString *)getNameWithCode:(NSString *)code {
    __block NSString *name = nil;
    [self.origanizs enumerateObjectsUsingBlock:^(YCOptionModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.code isEqualToString:code]) {
            *stop = YES;
            name = obj.name;
        }
    }];
    return  name;
}

@end
