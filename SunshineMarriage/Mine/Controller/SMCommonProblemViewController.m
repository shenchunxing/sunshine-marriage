//
//  SMCommonProblemViewController.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/24.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMCommonProblemViewController.h"
#import "YCTableViewKit.h"
#import "SMProblemItem.h"
#import "SMDefine.h"
#import "UIColor+Style.h"
#import "UIFont+Style.h"
#import <Masonry/Masonry.h>
#import "SMFoldSection.h"

@interface SMCommonProblemViewController ()
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) YCTableViewManager *manager;
@end

@implementation SMCommonProblemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationBar.title = @"常见问题";
}

- (void)initView {
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kNavigationHeight);
        make.height.mas_equalTo(self.view).mas_offset(-kNavigationHeight);
        make.width.left.mas_equalTo(self.view);
    }];
  
}

- (void)initData {
    SMProblemItem *introduce = [SMProblemItem item];
    introduce.hide = YES;
    introduce.text = @"用户登录后可以进行情感测评，通过专业的量表分析感情中遇到的问题，并给出专业的意见。";
    @weakify(self);
    SMFoldSection *introduceSection = [SMFoldSection sectionWithTitle:@"功能介绍" foldBlock:^(BOOL fold) {
        @strongify(self);
        introduce.hide = !fold;
        [self.manager reloadData];
    }];
    
    [introduceSection addItem:introduce];
    
    SMProblemItem *message = [SMProblemItem item];
    message.hide = YES;
    message.text = @"1.请您核实手机是否开启屏蔽系统短信，或安装了一些拦截短信的软件，建议您换一部手机安装手机卡再试，或者打开屏蔽短信的软件，查看屏蔽短信。\n2.手机停机、欠费状态下是不能够收到短信的，若该手机还会使用，建议充值手机话费后操作。";
    SMFoldSection *messageSection = [SMFoldSection sectionWithTitle:@"收不到验证码怎么办？" foldBlock:^(BOOL fold) {
        @strongify(self);
        message.hide = !fold;
         [self.manager reloadData];
    }];
    
    [messageSection addItem:message];
    
    [self.manager addSection:introduceSection];
    [self.manager addSection:messageSection];
    [self.manager reloadData];
}

- (YCTableViewManager *)manager {
    if (!_manager) {
        _manager = [[YCTableViewManager alloc] initWithTableView:self.tableView];
        [_manager registerItems:@[@"SMProblemItem"]];
    }
    return _manager;
}

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] init];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor yc_hex_F5F6FA];
    }
    return _tableView;
}


@end
