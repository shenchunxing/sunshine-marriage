//
//  SMMineViewController.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/21.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMMineViewController.h"
#import "SMDefine.h"
#import "YCCategoryModule.h"
#import "UIColor+Style.h"
#import "UIFont+Style.h"
#import <Masonry/Masonry.h>
#import "SMMineFunctionView.h"
#import "SMAboutUsViewController.h"
#import "SMPersonalInfoViewController.h"
#import "SMSettingViewController.h"
#import "SMCommonProblemViewController.h"
#import "SMLoginAPI.h"
#import "YCDataCenter.h"
#import <UIImageView+WebCache.h>
#import "SMAppUserAPI.h"
#import "SMRequestModel.h"
#import "SMPersonInfoModel.h"
#import "NSString+Extension.h"

@interface SMMineViewController ()
@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) UILabel *nameLabel;
//@property (nonatomic, strong) UILabel *phoneLabel;
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UIView *functionView;
@property (nonatomic, strong) YCGeneralAPI *userInfoAPI;
@end

@implementation SMMineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBar.hidden = YES;
    // Do any additional setup after loading the view.
    [self.view addSubview:self.topView];
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(160+kStatusBarHeight+28);
    }];
    
    [self.topView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(58+kStatusBarHeight);
        make.left.mas_equalTo(24);
        make.right.mas_lessThanOrEqualTo(self.topView).mas_offset(-120);
    }];
    
//    [self.topView addSubview:self.phoneLabel];
//    [self.phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(11);
//        make.left.mas_equalTo(24);
//    }];
//
    [self.topView addSubview:self.iconImageView];
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(40+kStatusBarHeight);
        make.right.mas_equalTo(-30);
        make.width.height.mas_equalTo(80);
    }];
    
    [self.view addSubview:self.functionView];
//    [self.functionView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(kStatusBarHeight+160);
//        make.width.left.mas_equalTo(self.view);
//        make.bottom.mas_equalTo(self.view.mas_bottom).mas_offset(-kTabBarHeight);
//    }];
    
    SMMineFunctionView *account = [SMMineFunctionView functionWithIconImageName:@"icon-wode-anquan.png" name:@"账户信息" tapClick:^{
        SMPersonalInfoViewController *account = [[SMPersonalInfoViewController alloc] init];
        [self.navigationController pushViewController:account animated:YES];
    }];
    [self.functionView addSubview:account];
    [account mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(5);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(60);
    }];
    
    SMMineFunctionView *question = [SMMineFunctionView functionWithIconImageName:@"icon-wode-wenti.png" name:@"常见问题" tapClick:^{
        SMCommonProblemViewController *problem = [[SMCommonProblemViewController alloc] init];
        [self.navigationController pushViewController:problem animated:YES];
    }];
    [self.functionView addSubview:question];
    [question mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(account.mas_bottom);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(60);
    }];
    
    SMMineFunctionView *aboutUs = [SMMineFunctionView functionWithIconImageName:@"icon-wode-woen.png" name:@"关于我们" tapClick:^{
        SMAboutUsViewController *aboutUs = [[SMAboutUsViewController alloc] init];
        [self.navigationController pushViewController:aboutUs animated:YES];
    }];
    [self.functionView addSubview:aboutUs];
    [aboutUs mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(question.mas_bottom);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(60);
    }];
    
    SMMineFunctionView *setting = [SMMineFunctionView functionWithIconImageName:@"icon-wode-shezi.png" name:@"设置" tapClick:^{
        SMSettingViewController *setting = [[SMSettingViewController alloc] init];
        [self.navigationController pushViewController:setting animated:YES];
    }];
    [self.functionView addSubview:setting];
    [setting mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(aboutUs.mas_bottom);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(60);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadData) name:kUpdatePersonInfoNotification object:nil];
}

- (void)loadData {
    [self.userInfoAPI start];
}

- (UIView *)topView {
    if (!_topView) {
        _topView = [[UIView alloc] init];
        _topView.backgroundColor = [UIColor yc_hex_5360FF];
    }
    return _topView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel yc_labelWithText:[YCDataCenter sharedData].nickname textFont:[UIFont yc_24_bold] textColor:[UIColor whiteColor]];
        _nameLabel.numberOfLines = 2;
    }
    return _nameLabel;
}

//- (UILabel *)phoneLabel {
//    if (!_phoneLabel) {
//        _phoneLabel = [UILabel yc_labelWithText:[YCDataCenter sharedData].phone textFont:[UIFont yc_12] textColor:[UIColor whiteColor]];
//    }
//    return _phoneLabel;
//}

- (UIImageView *)iconImageView {
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] init];
        [_iconImageView yc_radiusWithRadius:40];
        _iconImageView.backgroundColor = [UIColor yc_hex_ECECEC] ;
         [_iconImageView sd_setImageWithURL:[NSURL URLWithString:[YCDataCenter sharedData].headimgurl] placeholderImage:[UIImage imageNamed:[YCDataCenter sharedData].sex == 1 ? @"nan":@"nv"]];
    }
    return _iconImageView;
}

- (UIView *)functionView {
    if (!_functionView) {
        _functionView = [[UIView alloc] initWithFrame:CGRectMake(0, kStatusBarHeight+160, kWIDTH, kScreenHEIGHT - kStatusBarHeight -160-kTabBarHeight)];
        _functionView.backgroundColor = [UIColor whiteColor];
        [_functionView addRoundedCorners:UIRectCornerTopLeft | UIRectCornerTopRight withRadii:CGSizeMake(25, 25)];
    }
    return _functionView;
}

- (YCGeneralAPI *)userInfoAPI {
    if (!_userInfoAPI) {
        _userInfoAPI = [SMAppUserAPI userWithType:GetUserInfoByShareId success:^(__kindof YCBaseAPI * _Nonnull api, id  _Nonnull response) {
            NSDictionary *resBizMap = response[@"resBizMap"];
            SMPersonInfoModel *model = [SMPersonInfoModel yy_modelWithJSON:resBizMap[@"data"]];
            self.nameLabel.text = model.nickName?:[YCDataCenter sharedData].nickname;
//            self.phoneLabel.text = [model.phone stringByReplacingOccurrencesOfStringWithBeginLocation:3 length:4 commmonString:@"*"];
            [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:[YCDataCenter sharedData].headimgurl] placeholderImage:[UIImage imageNamed:[model.sex isEqualToString:@"1"] ? @"nan":@"nv"]];
        }];
        _userInfoAPI.requestModel.actionPath = [NSString stringWithFormat:@"getUserInfoByShareId/?shareId=%@",[YCDataCenter sharedData].shareId];
    }
    return _userInfoAPI;
}

@end
