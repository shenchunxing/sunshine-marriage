//
//  SMSettingViewController.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/24.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMSettingViewController.h"
#import "SMSettingItem.h"
#import "YCTableViewKit.h"
#import "UIColor+Style.h"
#import "SMDefine.h"
#import "UIButton+Extension.h"
#import "UIFont+Style.h"
#import "YCDataCenter.h"
#import "UIViewController+CustomToast.h"
#import "YCAlertView.h"
#import "UIView+Extension.h"
#import "SMTooL.h"
#import "YCMediator+SMLogin.h"
#import "SMWebViewController.h"
#import "YCNetworking.h"
#import "LoginViewViewController.h"
#import "NSString+Extension.h"

static NSString *const webUrlString = @"http://static.yangguanghy.com/agreementForApp";

@interface SMSettingViewController ()
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) YCTableViewManager *manager;
@property (nonatomic, strong) YCTableViewSection *section;
@property (nonatomic, strong) UIButton *loginOutBtn;
@property (nonatomic, strong) YCGeneralAPI *appVerisonAPI;
@property (nonatomic, assign) BOOL needUpdate;
@end

@implementation SMSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationBar.title = @"设置";
}

- (void)initView {
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kNavigationHeight);
        make.width.left.bottom.mas_equalTo(self.view);
    }];
    
    self.tableView.tableFooterView = self.loginOutBtn;
}

- (void)initData {
    SMSettingItem *verison = [SMSettingItem item];
    verison.name = @"版本信息";
    verison.detailColor = [UIColor yc_hex_999999];
    verison.detailName = [YCDataCenter versionCode];
    @weakify(self);
    verison.selectItemHandler = ^(NSIndexPath *indexPath) {
        @strongify(self);
        if (self.needUpdate) {
            [SMTooL findNewVerisonWithTitle:@"检查更新" content:@"发现新版本，是否立即更新？" jumpBlock:^{
                NSString *urlStr = @"itms-apps://itunes.apple.com/cn/app/id1501274338?mt=8";
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
            }];
        }else {
           [self showCustomMessage_SM:@"已是最新版本"];
        }
    };
    
    SMSettingItem *cache = [SMSettingItem item];
    cache.name = @"清除缓存";
    cache.detailName = @"立即清除";
    cache.selectItemHandler = ^(NSIndexPath *indexPath) {
        [SMTooL clearCacheTitle:@"确定删除所有缓存？" content:@"已缓存的图片等信息将会被删除"];
    };
    
    SMSettingItem *protocol = [SMSettingItem item];
    protocol.name = @"用户许可使用协议";
    protocol.selectItemHandler = ^(NSIndexPath *indexPath) {
        SMWebViewController *web = [[SMWebViewController alloc] initWithTitle:@"用户协议" webURLString:webUrlString];
        [self.navigationController pushViewController:web animated:YES];
    };

    [self.section addItem:verison];
    [self.section addItem:cache];
    [self.section addItem:protocol];
    [self.manager reloadData];
}

- (void)loadData {
    self.appVerisonAPI.params[@"devType"] = @"2";
    [self.appVerisonAPI start];
}

- (void)loginOut {
    [SMTooL loginOutWithTitle:@"退出登录" content:@"确定退出登录？" loginOutBlock:^{
        [[YCDataCenter sharedData] removeValueWithKey:@"authorization"] ;
        [[YCDataCenter sharedData] removeValueWithKey:@"appusercode"] ;
        [[YCDataCenter sharedData] removeValueWithKey:@"insertTime"] ;
        [[YCDataCenter sharedData] removeValueWithKey:@"shareId"] ;
        [[YCDataCenter sharedData] removeValueWithKey:@"thirdCode"] ;
        [[YCDataCenter sharedData] removeValueWithKey:@"thirdType"] ;
        [[YCDataCenter sharedData] removeValueWithKey:@"headimgurl"] ;
        [[YCDataCenter sharedData] removeValueWithKey:@"nickname"] ;
        [[YCDataCenter sharedData] removeValueWithKey:@"sex"] ;
//        [[YCDataCenter sharedData] removeValueWithKey:@"firstLogin"] ;
         
        if ([[NSDate date] timeIntervalSince1970] <1589155239) {
            SMNavigationController *nav = [[SMNavigationController alloc] initWithRootViewController:[[LoginViewViewController alloc] init]];
            [UIApplication sharedApplication].delegate.window.rootViewController = nav;
            return;
        }
        SMNavigationController *nav = [[YCMediator sharedInstance] loginViewControllerWithParmas:nil];
         [UIApplication sharedApplication].delegate.window.rootViewController = nav;
        [nav showCustomMessage_SM:@"退出成功"];
    }];
    
}

- (YCTableViewManager *)manager {
    if (!_manager) {
        _manager = [[YCTableViewManager alloc] initWithTableView:self.tableView];
        [_manager addSection:self.section];
        [_manager registerItems:@[@"SMSettingItem"]];
    }
    return _manager;
}

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] init];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor yc_hex_F5F6FA];
    }
    return _tableView;
}

- (YCTableViewSection *)section {
    if (!_section) {
        _section = [YCTableViewSection section];
        _section.footerView = [UIView new];
        _section.footerHeight = 10.5;
    }
    return _section;
}

- (UIButton *)loginOutBtn {
    if (!_loginOutBtn) {
        _loginOutBtn = [UIButton yc_customTextWithTitle:@"退出登录" titleColor:[UIColor yc_hex_5360FF] fontSize:[UIFont yc_18] target:self action:@selector(loginOut)];
        _loginOutBtn.backgroundColor = [UIColor whiteColor];
        _loginOutBtn.width = kWIDTH;
        _loginOutBtn.height = 60;
    }
    return _loginOutBtn;
}

- (YCGeneralAPI *)appVerisonAPI {
    if (!_appVerisonAPI) {
        _appVerisonAPI = [[YCGeneralAPI alloc] init];
        _appVerisonAPI.requestModel = [[YCRequestModel alloc] init];
        _appVerisonAPI.requestModel.serviceName = @"appVersion";
        _appVerisonAPI.requestModel.actionPath = @"getLatest";
        @weakify(self);
        _appVerisonAPI.apiSuccessHandler = ^(__kindof YCBaseAPI * _Nonnull api, id  _Nonnull response) {
            @strongify(self);
            NSDictionary *resBizMap = response[@"resBizMap"];
            self.needUpdate = [NSString compareVesionWithServerVersion:resBizMap[@"data"]] ;
        };
        _appVerisonAPI.apiFailureHandler = ^(__kindof YCBaseAPI * _Nonnull api, NSError * _Nonnull error) {
            
        };
    }
    return _appVerisonAPI;
}



@end
