//
//  SMAppUserAPI.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/29.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "YCGeneralAPI.h"

typedef NS_ENUM(NSUInteger,SMAppUserAPIType){
    GetOrganizations,                     //获取机构列表
    GetSubmitReport,                    //获取用户已经提交的报告
    GetUserInfoByShareId,                    //根据邀请码获取用户信息
    SetUserInfo, //设置用户信息
};

NS_ASSUME_NONNULL_BEGIN

@interface SMAppUserAPI : YCGeneralAPI
@property (nonatomic, assign) SMAppUserAPIType userAPIType;

+ (YCGeneralAPI *)userWithType:(SMAppUserAPIType)type success:(YCApiSuccessHandler)successHandle ;

+ (YCGeneralAPI *)userWithType:(SMAppUserAPIType)type success:(YCApiSuccessHandler)successHandle failure:(YCApiFailureHandler _Nullable)failureHandle;

+ (YCGeneralAPI *)reportWithType:(SMAppUserAPIType)type success:(YCApiSuccessHandler)successHandle failure:(YCApiFailureHandler _Nullable)failureHandle progress:(YCApiProgressBlock _Nullable)progressHandle ;

+ (YCGeneralAPI *)userAPIWithParams:(NSDictionary *)params type:(SMAppUserAPIType)type success:(YCApiSuccessHandler)successHandle ;

+ (YCGeneralAPI *)userAPIWithParams:(NSDictionary *)params type:(SMAppUserAPIType)type success:(YCApiSuccessHandler)successHandle failure:(YCApiFailureHandler)failureHandle ;

@end

NS_ASSUME_NONNULL_END
