//
//  SMReportAPI.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/29.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMAppUserAPI.h"
#import "YCNetworking.h"

@implementation SMAppUserAPI


+ (YCGeneralAPI *)userWithType:(SMAppUserAPIType)type success:(YCApiSuccessHandler)successHandle {
    return [self userAPIWithParams:nil type:type success:successHandle failure:nil progress:nil];
}

+ (YCGeneralAPI *)userWithType:(SMAppUserAPIType)type success:(YCApiSuccessHandler)successHandle failure:(YCApiFailureHandler _Nullable)failureHandle {
    return [self userAPIWithParams:nil type:type success:successHandle failure:failureHandle progress:nil];
}

+ (YCGeneralAPI *)userWithType:(SMAppUserAPIType)type success:(YCApiSuccessHandler)successHandle failure:(YCApiFailureHandler _Nullable)failureHandle progress:(YCApiProgressBlock _Nullable)progressHandle {
    return [self userAPIWithParams:nil type:type success:successHandle failure:failureHandle progress:progressHandle];
}

+ (YCGeneralAPI *)userAPIWithParams:(NSDictionary *)params type:(SMAppUserAPIType)type success:(YCApiSuccessHandler)successHandle {
    return [self userAPIWithParams:params type:type success:successHandle failure:nil progress:nil];
}

+ (YCGeneralAPI *)userAPIWithParams:(NSDictionary *)params type:(SMAppUserAPIType)type success:(YCApiSuccessHandler)successHandle failure:(YCApiFailureHandler)failureHandle {
    return [self userAPIWithParams:params type:type success:successHandle failure:failureHandle progress:nil];
}

+ (YCGeneralAPI *)userAPIWithParams:(NSDictionary *)params type:(SMAppUserAPIType)type success:(YCApiSuccessHandler)successHandle failure:(YCApiFailureHandler)failureHandle progress:(YCApiProgressBlock)progressHandle{
    YCGeneralAPI * api = [[YCGeneralAPI alloc]init];
    api.requestModel = [self getRequestModel];
    api.requestModel.portName = [self portName:type];
    api.requestModel.actionPath = [self actionPath:type];
    api.requestModel.serviceName = [self serverName:type];
    api.requestModel.requestType = [self requestType:type];
    api.params = [params mutableCopy];
    api.apiSuccessHandler = successHandle;
    api.apiFailureHandler = failureHandle;
    api.apiProgressBlock = progressHandle;
    return api;
}

+ (YCRequestModel *)getRequestModel{
    YCRequestModel *model = [[YCRequestModel alloc] init];
    model.apiVersion = nil;
    return model;
}

+ (NSString *)portName:(SMAppUserAPIType)type {
    return @"";
}

+ (NSString *)apiVersion:(SMAppUserAPIType)type {
    return @"";
}

+ (NSString *)actionPath:(SMAppUserAPIType)type {
    NSString *actionPath = nil;
    switch (type) {
        case GetOrganizations:
            actionPath = @"getOrganizations";
            break;
        case GetSubmitReport:
            actionPath = @"getSubmitReport";
            break;
        case GetUserInfoByShareId:
            actionPath = @"getUserInfoByShareId";
            break;
        case SetUserInfo:
            actionPath = @"setUserInfo";
            break;
    }
    return actionPath;
}

+ (NSString *)serverName:(SMAppUserAPIType)type {
    return @"appUser";
}

+ (YCHttpRequestType)requestType:(SMAppUserAPIType)type {
    if (type == GetOrganizations) {
        return YCHttpRequestTypeGet;
    }
    return YCHttpRequestTypePost;
}

@end
