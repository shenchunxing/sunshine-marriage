//
//  YCLoginOutProxy.m
//  YCHomeModule
//
//  Created by haima on 2019/6/15.
//

#import "YCLoginOutProxy.h"
#import "SMDefine.h"
#import "YCAlertView.h"
#import "SMTooL.h"
#import "UIViewController+CustomToast.h"
#import "NSObject+Helper.h"
@implementation YCLoginOutProxy
+ (instancetype)proxy {

    YCLoginOutProxy *proxy = [[YCLoginOutProxy alloc] init];
    return proxy;
}

- (BOOL)shouldContinueResponse:(YCResponseModel *)responseModel withResponseObject:(id)response {
    
    if ([responseModel isSuccess]) {
        return YES;
    }else{
        //token失效，退出登录
        if ([response[@"code"] isEqualToString:@"Unauthorized"]) {
            [self.yc_getCurrentViewController showCustomMessage_SM:response[@"msg"]];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationName_loginOut object:nil];
            });
            return NO;
        }
    }
    return YES;
}
@end
