//
//  YCLaunchServiceManager.h
//  YCHomeModule
//
//  Created by haima on 2019/3/28.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YCLaunchServiceManager : NSObject

/**
 登陆服务管理类，单例

 @return 唯一对象
 */
+ (instancetype)sharedInstance;

/**
 服务

 @return 服务列表
 */
- (NSMutableArray*)services;
@end

NS_ASSUME_NONNULL_END
