//
//  YCLoginOutProxy.h
//  YCHomeModule
//
//  Created by haima on 2019/6/15.
//

#import <Foundation/Foundation.h>
#import "YCNetworking.h"
NS_ASSUME_NONNULL_BEGIN

@interface YCLoginOutProxy : NSObject<YCHTTPProxy>

+ (instancetype)proxy;

@end

NS_ASSUME_NONNULL_END
