//
//  YCLaunchWeChatService.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/29.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import <WechatOpenSDK/WXApi.h>
NS_ASSUME_NONNULL_BEGIN

@interface YCLaunchWeChatService : NSObject<UIApplicationDelegate,WXApiLogDelegate,WXApiDelegate>

@end

NS_ASSUME_NONNULL_END
