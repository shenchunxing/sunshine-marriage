//
//  YCLaunchServiceManager.m
//  YCHomeModule
//
//  Created by haima on 2019/3/28.
//

#import "YCLaunchServiceManager.h"
#import "YCLaunchSystemService.h"
//#import "YCLaunchAssistiveService.h"
//#import "YCLaunchJpushService.h"
#import "YCLaunchWeChatService.h"
#import "YCLaunchQQService.h"

@interface YCLaunchServiceManager ()

@property (nonatomic, copy) NSMutableArray<id<UIApplicationDelegate>>* allServices;

@end

static YCLaunchServiceManager *instance = nil;

@implementation YCLaunchServiceManager

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

#pragma mark - public method
- (NSMutableArray *)services {
    if (_allServices == nil) {
        _allServices = [[NSMutableArray alloc] init];
        [self registerServices];
    }
    return _allServices;
}


#pragma mark - private

/**
 注册服务
 */
- (void)registerServices {
    [self registerService:[[YCLaunchSystemService alloc] init]];
    [self registerService:[[YCLaunchWeChatService alloc] init]];
    [self registerService:[[YCLaunchQQService alloc] init]];
}

- (void)registerService:(id<UIApplicationDelegate>)service {
    if (![self.allServices containsObject:service]) {
        [self.allServices addObject:service];
    }
}


@end
