//
//  YCLaunchSystemService.h
//  YCHomeModule
//
//  Created by haima on 2019/3/28.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
//系统配置服务类

typedef NS_ENUM(NSUInteger,YCLaunchSystemServiceType){
    
    YCLaunchTypeMain,                     //首页
    YCLaunchTypeLogin,                    //登录
    YCLaunchTypeGuide,                    //引导页
    YCLaunchTypeGestureLogin,             //手势登录
    YCLaunchTypeGestureSetting
};

NS_ASSUME_NONNULL_BEGIN

@interface YCLaunchSystemService : NSObject<UIApplicationDelegate>

+ (instancetype)launchService;

/**
 应用启动时加载主界面
 
 @param delegate AppDelegate
 */
- (void)launchWindowWithDelegate:(AppDelegate *)delegate;


/**
 应用启动后根据制定type跳入相应界面
 
 @param type LcLaunchType
 */
- (void)launchWindowWithType:(YCLaunchSystemServiceType)type;

@end

NS_ASSUME_NONNULL_END
