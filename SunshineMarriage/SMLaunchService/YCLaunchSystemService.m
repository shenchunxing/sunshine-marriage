//
//  YCLaunchSystemService.m
//  YCHomeModule
//
//  Created by haima on 2019/3/28.
//

#import "YCLaunchSystemService.h"
#import "YCBaseUI.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "YCHTTPClient.h"
//#import <YCCategoryModule/NSObject+Helper.h>
#import "SMDefine.h"
#import "YCDataCenter.h"
#import "YCLoginOutProxy.h"
//#import "YCRequestParamSignatureConstructor.h"

#import "YCMediator+SMLogin.h"
#import "YCMediator+SMTabBar.h"
#import "LoginViewViewController.h"


@interface YCLaunchSystemService()
@end
@implementation YCLaunchSystemService

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(nullable NSDictionary *)launchOptions {

    [self initIQKeyboardManager];
    [self initHttpClient];

    [self initRootViewController];
    [self adaptiveNewSystemForIOS11];
    [self checkNetwork];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginOut:) name:kNotificationName_loginOut object:nil];
    return YES;
}

+ (instancetype)launchService{
    static YCLaunchSystemService *service = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [[YCLaunchSystemService alloc]init];
    });
    return service;
}

#pragma mark - init http client
- (void)initHttpClient {
//    NSString *url = [[NSUserDefaults standardUserDefaults] valueForKey:@"BaseServiceHead"];
////    if (url.length==0) {
//        [[NSUserDefaults standardUserDefaults] setValue:BankSaasServiceHead forKey:@"BaseServiceHead"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
////    }
//    //监听cookie失效,代理设计
    [YCHTTPClient shareInstance].proxy = [YCLoginOutProxy proxy];
//    //参数构造器
//    [YCHTTPClient shareInstance].paramConstructor = [YCRequestParamSignatureConstructor constructor];
}

/**
 路由一个控制器作为根视图控制器
 */
- (void)initRootViewController {
    //登陆
    if([YCDataCenter sharedData].authorization){
        [self launchWindowWithType:YCLaunchTypeMain];
    }else{
        [self launchWindowWithType:YCLaunchTypeLogin];
    }
}

//适配ios11 tableView内容位置下移问题
- (void)adaptiveNewSystemForIOS11 {
    //iOS11 解决SafeArea的问题，同时能解决pop时上级页面scrollView抖动的问题
    if (@available(iOS 11, *)) {
        //iOS11 解决SafeArea的问题，同时能解决pop时上级页面scrollView抖动的问题
        [UIScrollView appearance].contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        [UITableView appearance].estimatedRowHeight = 0;
        [UITableView appearance].estimatedSectionHeaderHeight = 0;
        [UITableView appearance].estimatedSectionFooterHeight = 0;
    }
}


#pragma mark ---- IQKeyboardManager
- (void)initIQKeyboardManager {
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
    [IQKeyboardManager sharedManager].toolbarTintColor = [UIColor yc_hex_9B9EA8];
}


//网络检测
- (void)checkNetwork {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[YCHTTPClient shareInstance] checkNetworkAvailable:^(BOOL isAvailable) {
            NSLog(@"当前网络状态-----------%@",isAvailable? @"可用":@"不可用");
        }];
    });
}

#pragma mark - kNotificationName_loginOut
- (void)loginOut:(NSNotification *)notification {
    [[YCDataCenter sharedData] removeValueWithKey:@"authorization"] ;
    [[YCDataCenter sharedData] removeValueWithKey:@"appusercode"] ;
    [[YCDataCenter sharedData] removeValueWithKey:@"insertTime"] ;
    [[YCDataCenter sharedData] removeValueWithKey:@"shareId"] ;
    [[YCDataCenter sharedData] removeValueWithKey:@"thirdCode"] ;
    [[YCDataCenter sharedData] removeValueWithKey:@"thirdType"] ;
    [[YCDataCenter sharedData] removeValueWithKey:@"headimgurl"] ;
    [[YCDataCenter sharedData] removeValueWithKey:@"nickname"] ;
    [[YCDataCenter sharedData] removeValueWithKey:@"sex"] ;
    
    [self pushLogin];
}

- (void)launchWindowWithType:(YCLaunchSystemServiceType)type{
    switch (type) {
        case YCLaunchTypeMain://主页Tabbarcontroller
            [self pushMain];
            break;
        case YCLaunchTypeLogin://YCLoginViewController
            [self pushLogin];
            break;
        case YCLaunchTypeGuide://YCGuideViewController
//            [self pushGuide];
            break;
        case YCLaunchTypeGestureLogin://YCGestureViewController
//            [self pushGestureLogin];
            break;
        case YCLaunchTypeGestureSetting://YCGestureViewController
//            [self gestureSetting];
            break;
        default:
            break;
    }
    
}

//TabbarController
- (void)pushMain{
    UIViewController *vc = [[YCMediator sharedInstance] tabBarControllerWithParmas:nil];
    [UIApplication sharedApplication].delegate.window.rootViewController = vc;
}

//登录
- (void)pushLogin{
    if ([[NSDate date] timeIntervalSince1970] <1589155239) {
        SMNavigationController *nav = [[SMNavigationController alloc] initWithRootViewController:[[LoginViewViewController alloc] init]];
        [UIApplication sharedApplication].delegate.window.rootViewController = nav;
        return;
    }
    SMNavigationController *nav = [[YCMediator sharedInstance] loginViewControllerWithParmas:nil];
    [UIApplication sharedApplication].delegate.window.rootViewController = nav;
}


@end
