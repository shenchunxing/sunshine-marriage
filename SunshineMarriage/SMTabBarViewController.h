//
//  SMTabBarViewController.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/21.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SMTabBarViewController : UITabBarController

@end

NS_ASSUME_NONNULL_END
