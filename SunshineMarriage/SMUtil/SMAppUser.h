//
//  SMAppUser.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/31.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AppUser;
NS_ASSUME_NONNULL_BEGIN

@interface SMAppUser : NSObject

@property (nonatomic, strong) AppUser *appUser;
@property (nonatomic, copy) NSString *authorization;
@property (nonatomic, copy) NSString *orgName;

@end

@interface AppUser : NSObject

@property (nonatomic, assign) int age;
@property (nonatomic, assign) int education;
@property (nonatomic, copy) NSString *id;
@property (nonatomic, assign) double insertTime;
@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, assign) int orgId;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, assign) int sex;
@property (nonatomic, copy) NSString *shareId;
@property (nonatomic, copy) NSString *thirdCode;
@property (nonatomic, copy) NSString *thirdType;
@property (nonatomic, assign) int workType;

@end

NS_ASSUME_NONNULL_END
