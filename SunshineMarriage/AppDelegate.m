//
//  AppDelegate.m
//  YCBankSaasApp
//
//  Created by shenweihang on 2019/11/14.
//  Copyright © 2019 云车. All rights reserved.
//

#import "AppDelegate.h"
#import "YCLaunchServiceManager.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    for (id<UIApplicationDelegate> service in [YCLaunchServiceManager sharedInstance].services) {
        if ([service respondsToSelector:@selector(application:didFinishLaunchingWithOptions:)]) {
            [service application:application didFinishLaunchingWithOptions:launchOptions];
        }
    }
    [self.window makeKeyAndVisible];
    return YES;
}


#pragma mark -- 注册远程通知
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    for (id<UIApplicationDelegate> service in [YCLaunchServiceManager sharedInstance].services) {
        if ([service respondsToSelector:@selector(application:didRegisterForRemoteNotificationsWithDeviceToken:)]) {
            [service application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
        }
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    for (id<UIApplicationDelegate> service in [YCLaunchServiceManager sharedInstance].services) {
        if ([service respondsToSelector:@selector(application:didReceiveRemoteNotification:)]) {
            [service application:application didReceiveRemoteNotification:userInfo];
        }
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    for (id<UIApplicationDelegate> service in [YCLaunchServiceManager sharedInstance].services) {
        if ([service respondsToSelector:@selector(application:didReceiveRemoteNotification:)]) {
            [service application:application didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
        }
    }
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    for (id<UIApplicationDelegate> service in [YCLaunchServiceManager sharedInstance].services) {
        if ([service respondsToSelector:@selector(application:didFailToRegisterForRemoteNotificationsWithError:)]) {
            [service application:application didFailToRegisterForRemoteNotificationsWithError:error];
        }
    }
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
   for (id<UIApplicationDelegate> service in [YCLaunchServiceManager sharedInstance].services) {
        if ([service respondsToSelector:@selector(application:handleOpenURL:)]) {
            [service application:application handleOpenURL:url];
        }
    }
    return NO;
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    NSLog(@"[YCLaunchServiceManager sharedInstance].services = %@",[YCLaunchServiceManager sharedInstance].services);
    for (id<UIApplicationDelegate> service in [YCLaunchServiceManager sharedInstance].services) {
        if ([service respondsToSelector:@selector(application:openURL:options:)]) {
            [service application:app openURL:url options:options];
        }
    }
    return NO;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

#pragma mark --清除appBadge
- (void)applicationWillEnterForeground:(UIApplication *)application {
    for (id<UIApplicationDelegate> service in [YCLaunchServiceManager sharedInstance].services) {
        if ([service respondsToSelector:@selector(applicationWillEnterForeground:)]) {
            [service applicationWillEnterForeground:application];
        }
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    for (id<UIApplicationDelegate> service in [YCLaunchServiceManager sharedInstance].services) {
        if ([service respondsToSelector:@selector(applicationDidBecomeActive:)]) {
            [service applicationDidBecomeActive:application];
        }
    }
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



@end
