//
//  NSArray+LCProtect.m
//  YCSaas
//
//  Created by 刘成 on 2019/1/7.
//  Copyright © 2019年 刘成. All rights reserved.
//

#import "NSArray+LCProtect.h"
#import <objc/runtime.h>

@implementation NSArray (LCProtect)

+ (void)load{
    [super load];
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{  //方法交换只要一次就好
//        Method oldObjectAtIndex = class_getInstanceMethod(objc_getClass("__NSArrayI"), @selector(objectAtIndex:));
//        Method newObjectAtIndex = class_getInstanceMethod(objc_getClass("__NSArrayI"), @selector(objectAtIndexProtect:));
//        method_exchangeImplementations(oldObjectAtIndex, newObjectAtIndex);
//
//        Method old0ObjectAtIndex = class_getInstanceMethod(objc_getClass("__NSArray0"), @selector(objectAtIndex:));
//        Method new0ObjectAtIndex = class_getInstanceMethod(objc_getClass("__NSArray0"), @selector(emptyObjectAtIndexProtect:));
//        method_exchangeImplementations(old0ObjectAtIndex, new0ObjectAtIndex);
//    });
}
- (id)objectAtIndexProtect:(NSUInteger)index{
    if (index > self.count - 1 || !self.count){
        @try {
            return [self objectAtIndexProtect:index];
        } @catch (NSException *exception) {
            //__throwOutException  抛出异常
            return nil;
        } @finally {
            
        }
    }
    else{
        return [self objectAtIndexProtect:index];
    }
}

- (id)emptyObjectAtIndexProtect:(NSUInteger)index{
    if (index > self.count - 1 || !self.count){
        @try {
            return [self emptyObjectAtIndexProtect:index];
        } @catch (NSException *exception) {
            //__throwOutException  抛出异常
            return nil;
        } @finally {
            
        }
    }
    else{
        return [self emptyObjectAtIndexProtect:index];
    }
}
@end



//mutableArray的对象也需要做方法交换
@interface NSMutableArray (LCProtect)

@end

@implementation NSMutableArray (LCProtect)

+ (void)load{
    [super load];
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{  //方法交换只要一次就好
//        Method oldObjectAtIndex = class_getInstanceMethod(objc_getClass("__NSArrayM"), @selector(objectAtIndex:));
//        Method newObjectAtIndex = class_getInstanceMethod(objc_getClass("__NSArrayM"), @selector(objectAtIndexProtect:));
//        method_exchangeImplementations(oldObjectAtIndex, newObjectAtIndex);
//
//        Method oldInsertObjectAtIndex = class_getInstanceMethod(objc_getClass("__NSArrayM"), @selector(insertObject:atIndex:));
//        Method newInsertObjectAtIndex = class_getInstanceMethod(objc_getClass("__NSArrayM"), @selector(insertObjectProtect:atIndex:));
//        method_exchangeImplementations(oldInsertObjectAtIndex, newInsertObjectAtIndex);
//
//    });
}
- (id)objectAtIndexProtect:(NSUInteger)index{
    if (index > self.count - 1 || !self.count){
        @try {
            return [self objectAtIndexProtect:index];
        } @catch (NSException *exception) {
            //__throwOutException  抛出异常
            return nil;
        } @finally {
            
        }
    }
    else{
        return [self objectAtIndexProtect:index];
    }
}

- (void)insertObjectProtect:(id)anObject atIndex:(NSUInteger)index{
    
    if (anObject==nil) {
        return;
    }
    [self insertObjectProtect:anObject atIndex:index];
    
}

@end
