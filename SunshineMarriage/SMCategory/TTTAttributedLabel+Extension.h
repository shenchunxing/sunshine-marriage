//
//  TTTAttributedLabel+Extension.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/31.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "TTTAttributedLabel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TTTAttributedLabel (Extension)

+ (TTTAttributedLabel *)attributeLabelWithText:(NSString *)text linkAttribute:(NSString *)linkAttribute delegate:(id)delegate ;

@end

NS_ASSUME_NONNULL_END
