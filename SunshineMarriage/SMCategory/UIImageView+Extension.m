//
//  UIImageView+Extension.m
//  YCCategoryModule
//
//  Created by 沈春兴 on 2019/12/9.
//

#import "UIImageView+Extension.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImage+Extension.h"
#import "UIImage+YCBundle.h"
@implementation UIImageView (Extension)
- (void)setCircleHeader:(NSString *)url {
    UIImage *placeholder = [UIImage yc_resourceModuleImgWithName:@"touxiang"];
    [self sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image == nil) return;
        self.image = [image circleImage];
    }];
}

@end
