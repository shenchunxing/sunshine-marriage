//
//  UIImageView+Extension.h
//  YCCategoryModule
//
//  Created by 沈春兴 on 2019/12/9.
//

#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface UIImageView (Extension)
- (void)setCircleHeader:(NSString *)url;
@end

NS_ASSUME_NONNULL_END
