//
//  UIImage+Extension.m
//  YCCategoryModule
//
//  Created by haima on 2019/4/8.
//

#import "UIImage+Extension.h"
#import <AVFoundation/AVFoundation.h>
#import "UIImage+YCBundle.h"
@implementation UIImage (Extension)


/**
 * 加文字水印
 @param logoImage 需要加文字的图片
 @param watemarkText 文字描述
 @returns 加好文字的图片
 */
+ (UIImage *)addWatemarkTextAfteriOS7_WithLogoImage:(UIImage *)logoImage watemarkText:(NSString *)watemarkText{
    int w = logoImage.size.width;
    int h = logoImage.size.height;
    NSInteger fontSize = ceil(MIN(w, h)*0.02);
    
    UIGraphicsBeginImageContext(logoImage.size);
    [[UIColor whiteColor] set];
    [logoImage drawInRect:CGRectMake(0, 0, w, h)];
    UIFont * font = [UIFont systemFontOfSize:fontSize];
    //    [watemarkText drawInRect:CGRectMake(10, 10, 600, fontSize*10) withAttributes:@{NSFontAttributeName:font,NSForegroundColorAttributeName:kUIColorFromRGB(0x2435EF)}];
    
    NSShadow *shadow = [[NSShadow alloc]init];
    shadow.shadowBlurRadius = 6.0;
    shadow.shadowOffset = CGSizeMake(0, 0);
    shadow.shadowColor = [UIColor blackColor];
    [watemarkText drawInRect:CGRectMake(10, 10, 600, fontSize*10) withAttributes:@{NSFontAttributeName:font,NSForegroundColorAttributeName:[UIColor whiteColor],NSShadowAttributeName:shadow}];
    
    //    [mark drawInRect:CGRectMake(20,30 ,w ,fontSize*10) withAttributes:attr];         //左上角
    //    [mark drawInRect:CGRectMake(w - 10,20 ,100 ,100 ) withAttributes:attr];      //右上角
    //    [mark drawInRect:CGRectMake(w - 12, h -12 ,100 , 100) withAttributes:attr];  //右下角
    //    [mark drawInRect:CGRectMake(10, h - 12, 21, 12) withAttributes:attr];    //左下角
    UIImage * newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

/**
 * 加图片水印
 @param logoImage 需要加水印的logo图片
 @param watemarkImage 水印图片
 @returns 加好水印的图片
 */
+ (UIImage *)addWatemarkImageWithLogoImage:(UIImage *)logoImage watemarkImage:(UIImage *)watemarkImage logoImageRect:(CGRect)logoImageRect watemarkImageRect:(CGRect)watemarkImageRect{
    // 创建一个graphics context来画我们的东西
    UIGraphicsBeginImageContext(logoImageRect.size);
    // graphics context就像一张能让我们画上任何东西的纸。我们要做的第一件事就是把person画上去
    [logoImage drawInRect:CGRectMake(0, 0, logoImageRect.size.width, logoImageRect.size.height)];
    // 然后在把hat画在合适的位置
    [watemarkImage drawInRect:CGRectMake(watemarkImageRect.origin.x, watemarkImageRect.origin.y, watemarkImageRect.size.width, watemarkImageRect.size.height)];
    // 通过下面的语句创建新的UIImage
    UIImage * newImage = UIGraphicsGetImageFromCurrentImageContext();
    // 最后，我们必须得清理并关闭这个再也不需要的context
    UIGraphicsEndImageContext();
    return newImage;
}

/**
 * 加半透明水印
 @param logoImage 需要加水印的图片
 @param translucentWatemarkImage 水印
 @returns 加好水印的图片
 */
+ (UIImage *)addWatemarkImageWithLogoImage:(UIImage *)logoImage translucentWatemarkImage:(UIImage *)translucentWatemarkImage logoImageRect:(CGRect)logoImageRect translucentWatemarkImageRect:(CGRect)translucentWatemarkImageRect{
    UIGraphicsBeginImageContext(logoImage.size);    [logoImage drawInRect:CGRectMake(0, 0, logoImage.size.width, logoImage.size.height)];
    // 四个参数为水印的位置
    [translucentWatemarkImage drawInRect:CGRectMake(logoImage.size.width - 110, logoImage.size.height - 25, 100, 25)];
    UIImage * resultingImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resultingImage;
}

+ (void)compressImage:(UIImage *)image toByte:(NSUInteger)maxLength withBlock:(void (^)(NSData *imageData))block{
    if (maxLength <= 0 || [self isKindOfClass:[NSNull class]] || self == nil){
        block(nil);
        return;
    }
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        UIImage *newImage = [image copy];
        {
        CGFloat clipScale = 0.9;
//        NSData *pngData = UIImagePNGRepresentation(image);
//        NSLog(@"Original pnglength %zd", pngData.length);
        NSData *jpgData = UIImageJPEGRepresentation(image, 1.0);
        NSLog(@"Original jpglength %zd", jpgData.length);
        while (jpgData.length > maxLength) {
            NSData *newImageData = UIImageJPEGRepresentation(newImage, 0.0);
            if (newImageData.length < maxLength) {
                CGFloat scale = 1.0;
                newImageData = UIImageJPEGRepresentation(newImage, scale);
                while (newImageData.length > maxLength) {
                    scale -= 0.1;
                    newImageData = UIImageJPEGRepresentation(newImage, scale);
                }
                jpgData = newImageData;
                break;
            } else {
                newImage = [self compressWithImage:newImage width:newImage.size.width * clipScale];
                jpgData = UIImageJPEGRepresentation(newImage, 1.0);
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            block(jpgData);
        });
        }
    });
}

+ (UIImage *)compressWithImage:(UIImage *)image width:(CGFloat)width {
    if (width <= 0 || [image isKindOfClass:[NSNull class]] || image == nil) return nil;
    CGSize newSize = CGSizeMake(width, width * (image.size.height / image.size.width));
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return theImage;
}

//获取视频的第一帧截图, 返回UIImage
+ (UIImage*)getVideoPreViewImageWithPath:(NSURL *)videoPath {
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoPath options:nil];
    AVAssetImageGenerator *gen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    gen.appliesPreferredTrackTransform = YES;
    CMTime time  = CMTimeMakeWithSeconds(0.0, 600);
    NSError *error   = nil;
    CMTime actualTime;
    CGImageRef image = [gen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage *img = [[UIImage alloc] initWithCGImage:image];
    
    return img;
}

- (instancetype)circleImage
{
    // 开启图形上下文
    UIGraphicsBeginImageContext(self.size);
    
    // 上下文
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    // 添加一个圆
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    CGContextAddEllipseInRect(ctx, rect);
    
    // 裁剪
    CGContextClip(ctx);
    
    // 绘制图片
    [self drawInRect:rect];
    
    // 获得图片
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    // 关闭图形上下文
    UIGraphicsEndImageContext();
    
    return image;
}

+ (instancetype)circleImage:(NSString *)name {
    return [[self yc_resourceModuleImgWithName:name] circleImage];
}

@end
