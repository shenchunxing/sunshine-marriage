//
//  UIViewController+modalPresentationStyle.m
//  YCCategoryModule
//
//  Created by 沈春兴 on 2019/9/23.
//

#import "UIViewController+modalPresentationStyle.h"

@implementation UIViewController (modalPresentationStyle)

- (UIModalPresentationStyle)modalPresentationStyle{
    return UIModalPresentationFullScreen;
}


@end
