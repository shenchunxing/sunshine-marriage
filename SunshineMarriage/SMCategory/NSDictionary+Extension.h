//
//  NSDictionary+Extension.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/2/4.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//


#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDictionary (Extension)

+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString ;

@end

NS_ASSUME_NONNULL_END
