//
//  MBProgressHUD+Custom.m
//  YCCategoryModule
//
//  Created by haima on 2019/4/11.
//

#import "MBProgressHUD+Custom.h"
#import "NSObject+Helper.h"
#import "YCCustomHUD.h"

@implementation MBProgressHUD (Custom)

+ (void)showOnView:(UIView *)view{
    [self showMessage:@"加载中" onView:view];
}

+ (void)showMessage:(NSString *)message {
    [self showMessage:message onView:nil];
}

+ (void)showMessage:(NSString *)message delay:(CGFloat)delay{
    [self showMessage:message onView:nil];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [MBProgressHUD hideHUD];
    });
}

+ (MBProgressHUD *)showMessage:(NSString *)message onView:(UIView *)view {
    if (view == nil) {
        view = [self yc_getCurrentViewController].view;
    }
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeCustomView;
    hud.customView = [MBProgressHUD getCustomHudWithText:message];
    hud.bezelView.color = [UIColor clearColor];
    hud.margin = 0.f;
    hud.offset = CGPointMake(0, -100);

    //隐藏后移除
    hud.removeFromSuperViewOnHide = YES;
    return hud;
}

+ (void)hideHUD {
    [self hideHUDForView:nil];
}
+ (void)hideHUDForView:(UIView *)view {
    if (view == nil) {
        view = [self yc_getCurrentViewController].view;
    }
    [self hideHUDForView:view animated:YES];
}

+ (YCCustomHUD *)getCustomHudWithText:(NSString *)text{
    YCCustomHUD *hud = [[YCCustomHUD alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
    if (text.length>0) {
        hud.titleLbl.text = text;
    }
    return hud;
}

@end
