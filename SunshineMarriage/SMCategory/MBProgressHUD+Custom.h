//
//  MBProgressHUD+Custom.h
//  YCCategoryModule
//
//  Created by haima on 2019/4/11.
//

#import "MBProgressHUD.h"

@interface MBProgressHUD (Custom)

+ (void)showOnView:(UIView *)view;

/**
 展示hud，默认window

 @param message 信息
 */
+ (void)showMessage:(NSString *)message;
+ (void)showMessage:(NSString *)message delay:(CGFloat)delay;
/**
 展示指定视图上的HUD

 @param message hud显示信息
 @param view 视图
 @return hud
 */
+ (MBProgressHUD *)showMessage:(NSString *)message onView:(UIView *)view;

/**
 隐藏HUD，默认当前视图
 */
+ (void)hideHUD;

/**
 隐藏指定视图上的HUD

 @param view 视图
 */
+ (void)hideHUDForView:(UIView *)view;
@end
