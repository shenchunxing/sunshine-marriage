//
//  TTTAttributedLabel+Extension.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/31.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "TTTAttributedLabel+Extension.h"
#import "YCCategoryModule.h"

@implementation TTTAttributedLabel (Extension)

+ (TTTAttributedLabel *)attributeLabelWithText:(NSString *)text linkAttribute:(NSString *)linkAttribute delegate:(id)delegate{
   TTTAttributedLabel *_protocolLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectZero];
   _protocolLabel.font = [UIFont yc_11_bold];
   _protocolLabel.textColor = [UIColor yc_hex_333333];
   _protocolLabel.lineBreakMode = NSLineBreakByCharWrapping;
   _protocolLabel.numberOfLines = 0;
   _protocolLabel.userInteractionEnabled = YES;
   _protocolLabel.enabledTextCheckingTypes = NSTextCheckingTypeLink;
   _protocolLabel.verticalAlignment = TTTAttributedLabelVerticalAlignmentCenter;
   _protocolLabel.delegate = delegate; // Delegate
    
    NSMutableDictionary *linkAttributes = [NSMutableDictionary dictionary];
   [linkAttributes setValue:[NSNumber numberWithBool:NO] forKey:(NSString *)kCTUnderlineStyleAttributeName];
    [linkAttributes setValue:(__bridge id)[UIColor yc_hex_5360FF].CGColor forKey:(NSString *)kCTForegroundColorAttributeName];
    _protocolLabel.linkAttributes = linkAttributes;
   _protocolLabel.activeLinkAttributes = @{NSForegroundColorAttributeName:[UIColor yc_hex_5360FF]};
          
   [_protocolLabel setText:text afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
      NSRange boldRange = [[mutableAttributedString string] rangeOfString:linkAttribute options:NSCaseInsensitiveSearch];
       UIFont *boldSystemFont = [UIFont yc_11_bold];
       CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
       if (font) {
           [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:boldRange];
           [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[[UIColor yc_hex_5360FF] CGColor] range:boldRange];
           CFRelease(font);
       }
      return mutableAttributedString;

   }];
   
   //添加点击链接
   NSRange boldRange1 = [text rangeOfString:linkAttribute options:NSCaseInsensitiveSearch];
   [_protocolLabel addLinkToURL: nil withRange:boldRange1]; 
    
    return _protocolLabel;
}

@end
