//
//  SMReportAPI.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/29.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "YCGeneralAPI.h"

typedef NS_ENUM(NSUInteger,SMReportAPIType){
    GetAssmentReport,                     //获取某个具体评测表
    GetMarryReport,                    //获取用户评测表
    InitUnionReport,                    //创建联合报告
    SubmitAssmentReportAnswers //提交某个评测表答案
};

NS_ASSUME_NONNULL_BEGIN

@interface SMReportAPI : YCGeneralAPI
@property (nonatomic, assign) SMReportAPIType reportType;

+ (YCGeneralAPI *)reportWithType:(SMReportAPIType)type success:(YCApiSuccessHandler)successHandle ;

+ (YCGeneralAPI *)reportWithType:(SMReportAPIType)type success:(YCApiSuccessHandler)successHandle failure:(YCApiFailureHandler _Nullable)failureHandle;

+ (YCGeneralAPI *)reportWithType:(SMReportAPIType)type success:(YCApiSuccessHandler)successHandle failure:(YCApiFailureHandler _Nullable)failureHandle progress:(YCApiProgressBlock _Nullable)progressHandle ;

+ (YCGeneralAPI *)reportAPIWithParams:(NSDictionary *)params type:(SMReportAPIType)type success:(YCApiSuccessHandler)successHandle ;

+ (YCGeneralAPI *)reportAPIWithParams:(NSDictionary *)params type:(SMReportAPIType)type success:(YCApiSuccessHandler)successHandle failure:(YCApiFailureHandler)failureHandle ;

@end

NS_ASSUME_NONNULL_END
