//
//  SMEvaluatingViewController.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/29.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMEvaluatingViewController.h"

#import "SMDefine.h"
#import "SMAnswerCell.h"
#import "YCCategoryModule.h"
#import "SMDefine.h"

#import "GKCycleScrollView.h"
#import "SMReportAPI.h"
#import "YCNetworking.h"
#import "SMRequestModel.h"
#import "SMAssessmentReportQuestionsModel.h"
#import "YCAPIHUDPresenter.h"
#import "NSString+YCAttriButedCreate.h"
#import "SMTooL.h"
#import "UIViewController+CustomToast.h"
#import "SMAssessmentReportModel.h"

@interface SMEvaluatingViewController ()<GKCycleScrollViewDataSource, GKCycleScrollViewDelegate>
@property (nonatomic, strong) NSMutableArray               *dataArr;
@property (nonatomic, strong) GKCycleScrollView     *cycleScrollView;
@property (nonatomic, strong) YCGeneralAPI *assmentReportAPI;
@property (nonatomic, strong) UILabel *pageLabel ;
@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, strong) YCGeneralAPI *submitAPI;
@property (nonatomic, strong) NSMutableArray *jsonData;
@property (nonatomic, strong) SMAssessmentReportModel *reportModel;

@end

@implementation SMEvaluatingViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationBar.title = self.navTitle ;
    self.view.backgroundColor = [UIColor yc_hex_F5F6FA];
    self.currentIndex = 1;

    self.dataArr = [NSMutableArray new] ;
    self.jsonData = [NSMutableArray new];
    
    [self.view addSubview:self.cycleScrollView];
    [self.cycleScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.right.equalTo(self.view);
         make.top.mas_equalTo(kNavigationHeight);
         make.height.mas_equalTo(kHEIGHT - kNavigationHeight -100);
    }];
    
    [self.cycleScrollView reloadData];
    
    
    [self.view addSubview:self.pageLabel];
    [self.pageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view);
        make.top.mas_equalTo(self.cycleScrollView.mas_bottom).mas_offset(24.5);
        make.height.mas_equalTo(30);
    }];
    
}

- (void)yc_viewControllerDidLeftClick:(UIViewController *)viewController {
    [SMTooL toolWithTitle:@"温馨提示" content:@"退出后将失去答题进度，确认要退出吗？" confirmHandle:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)loadData {
    [self.assmentReportAPI start];
}

#pragma mark - GKCycleScrollViewDataSource
- (NSInteger)numberOfCellsInCycleScrollView:(GKCycleScrollView *)cycleScrollView {
    return self.dataArr.count;
}

- (GKCycleScrollViewCell *)cycleScrollView:(GKCycleScrollView *)cycleScrollView cellForViewAtIndex:(NSInteger)index {
//    static NSString *cellID = @"GKCycleScrollViewCell";
//    GKCycleScrollViewCell *cell = [cycleScrollView dequeueReusableCell];
//    if (!cell) {
      GKCycleScrollViewCell  *cell = [GKCycleScrollViewCell new];
//    }

    [cell setModel:self.dataArr[index] isFirstModel:index == 0 isLastModel:index == self.dataArr.count - 1 ];
    
    //下一个题目
    cell.forwardToNextBlock = ^(NSString *answer){
        if (self.currentIndex < self.dataArr.count) {
             self.currentIndex++;
        }
        [self setPage];
        [self.cycleScrollView scrollToCellAtIndex:index+1 animated:YES];
        
        /// 组装答案字符串
        if ([self.jsonData safeObjectAtIndex:index]) {
            [self.jsonData replaceObjectAtIndex:index withObject:@{@"a":answer,@"n":[NSString stringWithFormat:@"%ld",index+1]}];
        }else {
           [self.jsonData addObject:@{@"a":answer,@"n":[NSString stringWithFormat:@"%ld",index+1]}];
        }
        
    };
    
    //返回上一个题目
    cell.returnToPreviousBlock = ^{
        self.currentIndex--;
        [self setPage];
        [self.cycleScrollView scrollToCellAtIndex:index -1 animated:YES];
    };
    
    //提交
    cell.submitToServiceBlock = ^{
//        NSString *answersJson = [self yc_jsonStringCompactFormatForNSArray:self.jsonData] ;
//        self.submitAPI.params[@"reportId"] = self.reportId;
//        self.submitAPI.params[@"marryReportId"] = self.marryReportId;
//        self.submitAPI.params[@"answersJson"] = answersJson;
        [self.submitAPI start];
    };
    return cell;
}

- (void)submit {

}

- (void)setPage {
    NSString *text = [NSString stringWithFormat:@"%ld/%lu",(long)self.currentIndex ,(unsigned long)self.dataArr.count] ;
    NSRange range = [text rangeOfString:@"/"] ;
    if (range.location) {
        NSMutableAttributedString *str = [text attributedWithTextFont:[UIFont yc_30] textColor:[UIColor yc_hex_5360FF] range:NSMakeRange(0, range.location)];
        self.pageLabel.attributedText = str ;
    }
}

#pragma mark - GKCycleScrollViewDelegate
- (CGSize)sizeForCellInCycleScrollView:(GKCycleScrollView *)cycleScrollView {
    return CGSizeMake(kWIDTH - 85, kHEIGHT - kNavigationHeight -100);
}


- (GKCycleScrollView *)cycleScrollView {
    if (!_cycleScrollView) {
        _cycleScrollView = [GKCycleScrollView new];
        _cycleScrollView.dataSource = self;
        _cycleScrollView.delegate = self;
        _cycleScrollView.isAutoScroll = NO;
        _cycleScrollView.isInfiniteLoop = NO;
        _cycleScrollView.isChangeAlpha = NO;
        _cycleScrollView.leftRightMargin = 20;
        _cycleScrollView.topBottomMargin = 30;
        _cycleScrollView.scrollView.scrollEnabled = NO;
    }
    return _cycleScrollView;
}

- (YCGeneralAPI *)assmentReportAPI {
    if (!_assmentReportAPI) {
        _assmentReportAPI = [SMReportAPI reportWithType:GetAssmentReport success:^(__kindof YCBaseAPI * _Nonnull api, id  _Nonnull response) {
            NSDictionary *resBizMap = response[@"resBizMap"];
            
            self.reportModel = [SMAssessmentReportModel yy_modelWithJSON:resBizMap[@"assessmentReport"]];
            
            NSArray *models = [NSArray yy_modelArrayWithClass:[SMAssessmentReportQuestionsModel class] json:resBizMap[@"assessmentReportQuestions"]];
            [self.dataArr addObjectsFromArray:models];
            
            [self setPage];
            
            [self.cycleScrollView reloadData];
        }];
        _assmentReportAPI.requestModel.actionPath = [NSString stringWithFormat:@"getAssmentReport/?reportId=%@",self.reportId] ;
        _assmentReportAPI.presenter = [YCAlertHUDPresnter HUDWithView:self.view];
    }
    return _assmentReportAPI;
}

- (YCGeneralAPI *)submitAPI {
    if (!_submitAPI) {
        @weakify(self);
        _submitAPI = [SMReportAPI reportWithType:SubmitAssmentReportAnswers success:^(__kindof YCBaseAPI * _Nonnull api, id  _Nonnull response) {
            @strongify(self);
            [self showCustomMessage_SM:@"提交成功"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popToRootViewControllerAnimated:YES];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:kReportSubmitSuccessNotification object:nil];
                
            });
        } failure:^(__kindof YCBaseAPI * _Nonnull api, NSError * _Nonnull error) {
            @strongify(self);
            [self showCustomMessage_SM:error.userInfo[@"NSLocalizedDescription"]];
        }];
       NSString *answersJson = [self yc_jsonStringCompactFormatForNSArray:self.jsonData] ;
       _submitAPI.requestModel.actionPath = [NSString stringWithFormat:@"submitAssmentReportAnswers/?reportId=%@&marryReportId=%@&answersJson=%@",self.reportId,self.marryReportId,answersJson];
        _submitAPI.presenter = [YCAlertHUDPresnter HUDWithView:self.view];
    }
    return _submitAPI;
}

- (UILabel *)pageLabel {
    if(!_pageLabel) {
        _pageLabel = [UILabel yc_labelWithText:nil textFont:[UIFont yc_13] textColor:[UIColor yc_hex_999999]];
    }
    return _pageLabel;
}

- (NSString *)yc_jsonStringCompactFormatForNSArray:(NSArray *)arrJson {
    if (![arrJson isKindOfClass:[NSArray class]] || ![NSJSONSerialization isValidJSONObject:arrJson]) {
        return nil;
    }
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrJson options:0 error:nil];
    NSString *strJson = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return strJson;
}

@end
