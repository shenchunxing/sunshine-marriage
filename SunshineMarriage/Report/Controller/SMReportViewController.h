//
//  SMReportViewController.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/21.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "YCBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SMReportViewController : YCBaseViewController

@property (nonatomic, copy) NSString *type;

@end

NS_ASSUME_NONNULL_END
