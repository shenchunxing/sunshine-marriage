//
//  SMReportMainViewController.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/23.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMReportMainViewController.h"
#import "SMReportViewController.h"
@interface SMReportMainViewController ()

@end

@implementation SMReportMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBar.leftBarButton = nil;
    [self yc_setupTitleViewWithTitle:@"报告列表"];
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    SMReportViewController *report = [[SMReportViewController alloc] init];
    report.type = [NSString stringWithFormat:@"%d",index+1] ;
    return report;
}

@end
