//
//  SMEvaluatingViewController.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/29.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "YCBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SMEvaluatingViewController : YCBaseViewController

@property (nonatomic, copy) NSString *marryReportId;
@property (nonatomic, copy) NSString *reportId;
@property (nonatomic, copy) NSString *navTitle;

@end

NS_ASSUME_NONNULL_END
