//
//  SMReportViewController.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/21.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMReportViewController.h"
#import "YCTableViewKit.h"
#import "SMReportItem.h"
#import "UIColor+Style.h"
#import "UIFont+Style.h"
#import <Masonry/Masonry.h>
#import "SMDefine.h"
#import "UIScrollView+EmptyData.h"
#import "SMAppUserAPI.h"
#import "SMRequestModel.h"
#import "SMSubmitedReportModel.h"
#import <MJRefresh/MJRefresh.h>

@interface SMReportViewController ()<YCEmptyViewDataSource>
@property (nonatomic, strong) YCTableViewManager *manager;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) YCTableViewSection *section;
@property (nonatomic, strong) YCGeneralAPI *reportAPI;
@end

@implementation SMReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor yc_hex_F5F6FA];
    self.navigationBar.hidden = YES;
    // Do any additional setup after loading the view.
    [self generatePullRefreshTableHeaderView:self.tableView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadData) name:kReportSubmitSuccessNotification object:nil];
}

- (void)initView {
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(50);
        make.bottom.mas_equalTo(self.view);
        make.width.left.mas_equalTo(self.view);
    }];

}

- (void)headerRefreshAction {
    [self loadData];
}

- (void)loadData {
    [self.reportAPI start];
}

- (YCTableViewManager *)manager {
    if (!_manager) {
        _manager = [[YCTableViewManager alloc] initWithTableView:self.tableView];
        [_manager addSection:self.section];
        [_manager registerItems:@[@"SMReportItem"]];
    }
    return _manager;
}

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] init];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor yc_hex_F5F6FA];
        _tableView.emptyViewDataSource = self;
    }
    return _tableView;
}

- (YCTableViewSection *)section {
    if (!_section) {
        _section = [YCTableViewSection section];
        _section.headerHeight = 30;
        _section.headerView = [UIView new];
    }
    return _section;
}

- (YCEmptyStyle *)styleForEmptyView:(UIScrollView *)scrollView {
    return [YCEmptyStyle reportStyle];
}

- (YCGeneralAPI *)reportAPI {
    if (!_reportAPI) {
        _reportAPI = [SMAppUserAPI userWithType:GetSubmitReport success:^(__kindof YCBaseAPI * _Nonnull api, id  _Nonnull response) {
            [self.tableView.mj_header endRefreshing];
            NSDictionary *resBizMap = response[@"resBizMap"];
            NSArray *models = [NSArray yy_modelArrayWithClass:[SMReportItem class] json:resBizMap[@"data"]];
            [self.section removeAllItems];
            [self.section addItems:models];
            [self.manager reloadData];
        } failure:^(__kindof YCBaseAPI * _Nonnull api, NSError * _Nonnull error) {
            [self.tableView.mj_header endRefreshing];
        }];
        _reportAPI.requestModel.actionPath = [NSString stringWithFormat:@"getSubmitReport/?type=%@",self.type] ;
    }
    return _reportAPI;
}

@end
