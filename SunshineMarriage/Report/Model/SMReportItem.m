//
//  SMReportItem.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/23.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMReportItem.h"
#import "UIFont+Style.h"
#import "NSDictionary+Extension.h"
#import <YYModel/YYModel.h>

@implementation SMReportItem

- (CGFloat)itemHeight {
    NSDictionary *dict =  [NSDictionary dictionaryWithJsonString:self.sysAssmentJson];
    self.json = [SysAssmentJson yy_modelWithJSON:dict];
    
    
//    self.json.sumScoreDes  = @"借记卡可敬可嘉健康口口声声说你能解决黄金时间回家回家回家时回家回家弄不明白你们呢哈哈哈哈";
//
    NSString *text = self.json.sumScoreDes ;
    
    

    if (!self.json.sumScoreDes) {
        text = [NSString stringWithFormat:@"平均分:%@",dict[@"avgScore"]];
    }
    
    //人格问卷
    if ([self.reportId isEqualToString:@"11"]) {
        text = [NSString stringWithFormat:@"%@:%@,\n%@:%@,\n%@:%@,\n%@:%@",self.json.titleE,self.json.desE,self.json.titleN,self.json.desN,self.json.titleP,self.json.desP,self.json.titleL,self.json.desL] ;
    }
    
    //新婚物语
    if ([self.reportId isEqualToString:@"10"]) {
        text = self.json.des ;
    }
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:7];
    paragraphStyle.firstLineHeadIndent = 82;
//    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    NSDictionary * attributes = @{NSFontAttributeName:[UIFont yc_14], NSParagraphStyleAttributeName: paragraphStyle };
    CGSize size = [text boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width -91, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:attributes context:nil].size ;
    return size.height+108+40+15+20+20;
}


@end

@implementation SysAssmentJson



@end
