//
//  SMAnswerItem.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/29.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "YCTableViewItem.h"
NS_ASSUME_NONNULL_BEGIN

@interface SMAnswerItem : YCTableViewItem

@property (nonatomic, copy) NSString *des;
@property (nonatomic, copy) NSString *letter;
@property (nonatomic, assign) int order;
@property (nonatomic, assign) BOOL selected;

@end

NS_ASSUME_NONNULL_END
