//
//  SMAssessmentReportQuestionsModel.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/31.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMAssessmentReportQuestionsModel.h"
#import <YYModel/YYModel.h>
#import "YCCategoryModule.h"

@implementation SMAssessmentReportQuestionsModel

- (NSArray<SMAnswerItem *> *)items {
    if (self.options) {
        id tmp = [NSJSONSerialization JSONObjectWithData:[self.options dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments | NSJSONReadingMutableLeaves | NSJSONReadingMutableContainers error:nil] ;
        if ([tmp isKindOfClass:[NSArray class]]) {
            NSArray *items = [NSArray yy_modelArrayWithClass:[SMAnswerItem class] json:tmp];
            return items;
        }
    }
    return nil;
}

- (CGFloat)totalHeight {
    CGFloat titleHeight =  [self.name boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width -68*2, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont yc_15]} context:nil].size.height;
    return titleHeight+26+self.cellHeights + 120+35+30;
}

- (CGFloat)cellHeights {
  __block CGFloat height = 0;
    [self.items enumerateObjectsUsingBlock:^(SMAnswerItem * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
       NSDictionary *attributes = @{NSFontAttributeName : [UIFont yc_15]};
        CGSize size = [obj.des boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width - 2*68 - 30 - 20, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:attributes context:nil].size;
        size.height = ceil(size.height);
        height += (size.height > 15 ? size.height + 30: 45);
    }];
    return height;
}

@end

