//
//  SMAssessmentReportModel.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/2/2.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SMAssessmentReportModel : NSObject
@property (nonatomic, copy) NSString *desUrl;
@property (nonatomic, copy) NSString *description;
@property (nonatomic, assign) int id;
@property (nonatomic, copy) NSString *name ;

@end

NS_ASSUME_NONNULL_END
