//
//  SMReportItem.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/23.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "YCTableViewItem.h"
@class SysAssmentJson ;
NS_ASSUME_NONNULL_BEGIN

@interface SMReportItem : YCTableViewItem

- (CGFloat)itemHeight;

@property (nonatomic, copy) NSString *answersJson;
@property (nonatomic, copy) NSString *desUrl;
@property (nonatomic, copy) NSString *id;
@property (nonatomic, assign) double insertTime;
@property (nonatomic, copy) NSString *marryReportId;
@property (nonatomic, copy) NSString *reportId;
@property (nonatomic, copy) NSString *reportName;
@property (nonatomic, assign) int state;
@property (nonatomic, copy) NSString *sysAssmentJson;
@property (nonatomic, strong) SysAssmentJson *json;

//answersJson = "[{\"a\":\"C\",\"n\":1},{\"a\":\"C\",\"n\":2},{\"a\":\"C\",\"n\":3},{\"a\":\"D\",\"n\":4},{\"a\":\"C\",\"n\":5},{\"a\":\"C\",\"n\":6},{\"a\":\"D\",\"n\":7},{\"a\":\"D\",\"n\":8},{\"a\":\"D\",\"n\":9},{\"a\":\"D\",\"n\":10},{\"a\":\"D\",\"n\":11},{\"a\":\"D\",\"n\":12},{\"a\":\"D\",\"n\":13},{\"a\":\"D\",\"n\":14},{\"a\":\"D\",\"n\":15},{\"a\":\"D\",\"n\":16},{\"a\":\"E\",\"n\":17},{\"a\":\"D\",\"n\":18},{\"a\":\"D\",\"n\":19},{\"a\":\"D\",\"n\":20},{\"a\":\"D\",\"n\":21},{\"a\":\"D\",\"n\":22},{\"a\":\"D\",\"n\":23},{\"a\":\"D\",\"n\":24},{\"a\":\"D\",\"n\":25},{\"a\":\"D\",\"n\":26},{\"a\":\"D\",\"n\":27},{\"a\":\"D\",\"n\":28},{\"a\":\"D\",\"n\":29},{\"a\":\"D\",\"n\":30},{\"a\":\"D\",\"n\":31},{\"a\":\"D\",\"n\":32},{\"a\":\"D\",\"n\":33},{\"a\":\"C\",\"n\":34},{\"a\":\"D\",\"n\":35}]";
//desUrl = "http://192.168.1.70:7082/img/sccs_img.png";
//id = 63;
//insertTime = 1579754581000;
//marryReportId = 82;
//reportId = 2;
//reportName = "\U81ea\U6211\U548c\U8c10\U91cf\U8868";
//state = 1;
//sysAssmentJson = "{\"sumDes\":\"\U603b\U5206\",\"sumScore\":117,\"sumScoreDes\":\"\U5f97\U5206\U8d8a\U9ad8\U81ea\U6211\U548c\U8c10\U7a0b\U5ea6\U8d8a\U9ad8\",\"expirementDes\":\"\U81ea\U6211\U4e0e\U7ecf\U9a8c\U7684\U4e0d\U4e00\U81f4\",\"expirementScore\":64,\"expirementScoreDes\":\"\U53cd\U6620\U7684\U662f\U81ea\U6211\U4e0e\U7ecf\U9a8c\U4e4b\U95f4\U7684\U5173\U7cfb\Uff0c\U5305\U542b\U5bf9\U80fd\U529b\U548c\U60c5\U611f\U7684\U81ea\U6211\U8bc4\U4ef7\Uff0c\U81ea\U6211\U4e00\U81f4\U6027\U3001\U65e0\U52a9\U611f\U7b49\Uff0c\U5b83\U6240\U4ea7\U751f\U7684\U75c7\U72b6\U66f4\U591a\U5730\U53cd\U6620\U4e86\U5bf9\U7ecf\U9a8c\U7684\U4e0d\U5408\U7406\U671f\U671b\U3002\",\"flexDes\":\"\U81ea\U6211\U7075\U62ec\U6027\",\"flexScore\":27,\"flexScoreDes\":\"\U4e0e\U654c\U5bf9\U4e0e\U6050\U6016\U7684\U76f8\U5173\U663e\U8457\Uff0c\U53ef\U4ee5\U9884\U793a\U81ea\U6211\U6982\U5ff5\U7684\U523b\U677f\U548c\U50f5\U5316\U3002\",\"stereotypeDes\":\"\U81ea\U6211\U523b\U677f\U6027\",\"stereotypeScore\":26,\"stereotypeScoreDes\":\"\U4e0d\U4ec5\U540c\U8d28\U6027\U4fe1\U5ea6\U8f83\U4f4e\Uff0c\U800c\U4e14\U4e0e\U504f\U6267\U6709\U663e\U8457\U76f8\U5173\"}";

@end

@interface SysAssmentJson : NSObject
@property (nonatomic, copy) NSString *sumDes;
@property (nonatomic, copy) NSString *sumScore;
@property (nonatomic, copy) NSString *sumScoreDes;
@property (nonatomic, copy) NSString *expirementDes;
@property (nonatomic, copy) NSString *expirementScore;
@property (nonatomic, copy) NSString *expirementScoreDes;
@property (nonatomic, copy) NSString *flexDes;
@property (nonatomic, copy) NSString *flexScore;
@property (nonatomic, copy) NSString *stereotypeDes;
@property (nonatomic, copy) NSString *stereotypeScoreDes;


@property (nonatomic, copy) NSString *titleE;
@property (nonatomic, copy) NSString *desE;
@property (nonatomic, copy) NSString *scoreE;
@property (nonatomic, copy) NSString *titleN;
@property (nonatomic, copy) NSString *desN;
@property (nonatomic, copy) NSString *scoreN;
@property (nonatomic, copy) NSString *titleP;
@property (nonatomic, copy) NSString *desP;
@property (nonatomic, copy) NSString *scoreP;
@property (nonatomic, copy) NSString *titleL;
@property (nonatomic, copy) NSString *desL;
@property (nonatomic, copy) NSString *scoreL;


@property (nonatomic, copy) NSString *avgScore;



@property (nonatomic, copy) NSString *des;
@property (nonatomic, copy) NSString *state;

/*
 {"titleE":"E（内向-外向）","desE":"分数高表示人格外向，可能是好交际，渴望刺激和冒险，情感易于冲动。分数低表示人格内向，可能是好静，富于内省，除了亲密的朋友之外，对一般人缄默冷淡，不喜欢刺激，喜欢有秩序的生活方式，情绪比较稳定。","scoreE":7,"titleN":"N（神经质）","desN":"反映的是正常行为，并非指神经症。分数高者常常焦虑、担忧、郁郁不乐忧心忡忡，遇到刺激有强烈的情绪反应，以至出现不够理智的行为；分数低者情绪反应缓慢且轻缓，很容易恢复平静，稳重、性情温和、善于自我控制。","scoreN":4,"titleP":"P（精神质）","desP":"并非暗指精神病，它在所有人身上都存在，只是程度不同。高分者可能是孤独、不关心他人，难以适应外部环境，不近人情、感觉迟钝、与他人不友好、喜欢寻衅搅扰、喜欢干奇特的事情，并且不顾危险；低分者能与人相处，能较好地适应环境，态度温和、不粗暴、善从人意。 ","scoreP":8,"titleL":"L（掩饰性）","desL":"成人随年龄而升高；儿童随年龄而减低。","scoreL":12}
 */

@end

NS_ASSUME_NONNULL_END
