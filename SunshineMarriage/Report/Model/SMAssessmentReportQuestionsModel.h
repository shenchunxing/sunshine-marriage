//
//  SMAssessmentReportQuestionsModel.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/31.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SMAnswerItem.h"

@interface SMAssessmentReportQuestionsModel : NSObject

@property (nonatomic, assign) int   id;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *options;
@property (nonatomic, assign) int orderNo;
@property (nonatomic, assign) int reportId;
@property (nonatomic, copy) NSArray <SMAnswerItem *> *items;

- (CGFloat)totalHeight;
- (CGFloat)cellHeights;

@end

