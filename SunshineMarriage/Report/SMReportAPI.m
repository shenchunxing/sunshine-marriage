//
//  SMReportAPI.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/29.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMReportAPI.h"
#import "YCNetworking.h"
#import "SMRequestModel.h"

@implementation SMReportAPI


+ (YCGeneralAPI *)reportWithType:(SMReportAPIType)type success:(YCApiSuccessHandler)successHandle {
    return [self reportAPIWithParams:nil type:type success:successHandle failure:nil progress:nil];
}

+ (YCGeneralAPI *)reportWithType:(SMReportAPIType)type success:(YCApiSuccessHandler)successHandle failure:(YCApiFailureHandler _Nullable)failureHandle {
    return [self reportAPIWithParams:nil type:type success:successHandle failure:failureHandle progress:nil];
}

+ (YCGeneralAPI *)reportWithType:(SMReportAPIType)type success:(YCApiSuccessHandler)successHandle failure:(YCApiFailureHandler _Nullable)failureHandle progress:(YCApiProgressBlock _Nullable)progressHandle {
    return [self reportAPIWithParams:nil type:type success:successHandle failure:failureHandle progress:progressHandle];
}

+ (YCGeneralAPI *)reportAPIWithParams:(NSDictionary *)params type:(SMReportAPIType)type success:(YCApiSuccessHandler)successHandle {
    return [self reportAPIWithParams:params type:type success:successHandle failure:nil progress:nil];
}

+ (YCGeneralAPI *)reportAPIWithParams:(NSDictionary *)params type:(SMReportAPIType)type success:(YCApiSuccessHandler)successHandle failure:(YCApiFailureHandler)failureHandle {
    return [self reportAPIWithParams:params type:type success:successHandle failure:failureHandle progress:nil];
}

+ (YCGeneralAPI *)reportAPIWithParams:(NSDictionary *)params type:(SMReportAPIType)type success:(YCApiSuccessHandler)successHandle failure:(YCApiFailureHandler)failureHandle progress:(YCApiProgressBlock)progressHandle{
    YCGeneralAPI * api = [[YCGeneralAPI alloc]init];
    api.requestModel = [[SMRequestModel alloc] init];
    api.requestModel.actionPath = [self actionPath:type];
    api.requestModel.serviceName = [self serverName:type];
    api.requestModel.requestType = [self requestType:type];
    api.params = [params mutableCopy];
    api.apiSuccessHandler = successHandle;
    api.apiFailureHandler = failureHandle;
    api.apiProgressBlock = progressHandle;
    return api;
}

+ (NSString *)actionPath:(SMReportAPIType)type {
    NSString *actionPath = nil;
    switch (type) {
        case GetAssmentReport:
            actionPath = @"getAssmentReport";
            break;
        case GetMarryReport:
            actionPath = @"getMarryReport";
            break;
        case InitUnionReport:
            actionPath = @"initUnionReport";
            break;
        case SubmitAssmentReportAnswers:
            actionPath = @"submitAssmentReportAnswers";
            break;
    }
    return actionPath;
}

+ (NSString *)serverName:(SMReportAPIType)type {
    return @"ReportController";
}

+ (YCHttpRequestType)requestType:(SMReportAPIType)type {
    return YCHttpRequestTypePost;
}

@end
