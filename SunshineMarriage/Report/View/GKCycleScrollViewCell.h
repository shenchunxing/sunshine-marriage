//
//  GKCycleScrollViewCell.h
//  GKCycleScrollViewDemo
//
//  Created by QuintGao on 2019/9/15.
//  Copyright © 2019 QuintGao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SMAssessmentReportQuestionsModel.h"
NS_ASSUME_NONNULL_BEGIN

typedef void(^ForwardToNextBlock)(NSString *answer);
typedef void(^ReturnToPreviousBlock)(void);
typedef void(^SubmitToServiceBlock)(void);

@interface GKCycleScrollViewCell : UICollectionViewCell

@property (nonatomic, copy) ForwardToNextBlock forwardToNextBlock;
@property (nonatomic, copy) ReturnToPreviousBlock returnToPreviousBlock;
@property (nonatomic, copy) SubmitToServiceBlock submitToServiceBlock;
@property (nonatomic, strong) SMAssessmentReportQuestionsModel *model;

- (void)setModel:(SMAssessmentReportQuestionsModel * _Nonnull)model isFirstModel:(BOOL)isFirstModel isLastModel:(BOOL)isLastModel ;


@end

NS_ASSUME_NONNULL_END
