//
//  GKCycleScrollViewCell.m
//  GKCycleScrollViewDemo
//
//  Created by QuintGao on 2019/9/15.
//  Copyright © 2019 QuintGao. All rights reserved.
//

#import "GKCycleScrollViewCell.h"
#import "SMDefine.h"
#import "YCCategoryModule.h"
#import <Masonry/Masonry.h>
#import "SMAnswerItem.h"
#import "SMAnswerCell.h"
#import "YCTableViewKit.h"

@interface GKCycleScrollViewCell ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *frontButton;
@property (nonatomic, strong) UIButton *lastButton;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) SMAnswerItem *lastItem;
@property (nonatomic, strong) YCTableViewManager *manager;
@property (nonatomic, strong) YCTableViewSection *section;

@end

@implementation GKCycleScrollViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
        self.layer.shadowColor = [UIColor colorWithRed:200/255.0 green:200/255.0 blue:200/255.0 alpha:0.17].CGColor;
        self.layer.shadowOffset = CGSizeMake(0,5);
        self.layer.shadowOpacity = 1;
        self.layer.shadowRadius = 9;
        self.layer.cornerRadius = 5;
        
        [self.contentView addSubview:self.titleLabel];
        
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(40);
            make.left.mas_equalTo(26);
            make.right.mas_equalTo(-26);
        }];

        [self.contentView addSubview:self.tableView];
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(27.5);
            make.height.mas_equalTo(0);
            make.left.mas_equalTo(26);
            make.right.mas_equalTo(-26);
        }];
        
        [self.contentView addSubview:self.frontButton];
           [self.frontButton mas_makeConstraints:^(MASConstraintMaker *make) {
               make.bottom.mas_equalTo(self.contentView.mas_bottom).mas_offset(- 65);
               make.height.mas_equalTo(35);
               make.left.mas_equalTo(25);
               make.right.mas_equalTo(-25);
           }];
    }
    return self;
}

- (void)setModel:(SMAssessmentReportQuestionsModel *)model isFirstModel:(BOOL)isFirstModel isLastModel:(BOOL)isLastModel{
    _model = model ;

    self.titleLabel.text = model.name;
    
    [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
       make.height.mas_equalTo(model.totalHeight);
    }];
    
    self.frontButton.hidden = isFirstModel ;
    
    //最后一页
    if (isLastModel) {
        [self addFrontButtonAndSubmitButton];
    }
    
    //页面选项内容
    [self addAnswerItemWithModel:model];
}


/// 根据model创建items
/// @param model model
- (void)addAnswerItemWithModel:(SMAssessmentReportQuestionsModel *)model{
    [self.section removeAllItems];
    
    __block SMAnswerItem *lastItem = nil;
    for (SMAnswerItem *item in model.items) {
        item.selected = NO;
        [self.section addItem:item];

        __weak typeof(item) weakItem = item;
        item.selectItemHandler = ^(NSIndexPath *indexPath) {
            if (weakItem == lastItem) {
                !self.forwardToNextBlock ?:self.forwardToNextBlock(weakItem.letter) ;
                return ;
            }

            lastItem.selected = NO;
            weakItem.selected = YES;
            [self.manager reloadData];

            lastItem = weakItem ;

            !self.forwardToNextBlock ?:self.forwardToNextBlock(weakItem.letter) ;
        };
    }

   [self.manager reloadData];

}

- (void)front {
    !self.returnToPreviousBlock ?:self.returnToPreviousBlock() ;
}

- (void)submit {
   !self.submitToServiceBlock ?:self.submitToServiceBlock() ;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel yc_labelWithText:nil textFont:[UIFont yc_15] textColor:[UIColor yc_hex_333333]];
        _titleLabel.numberOfLines = 0;
    }
    return _titleLabel;
}

- (UIButton *)frontButton {
    if (!_frontButton) {
        _frontButton = [UIButton yc_customTextWithTitle:@"上一题" titleColor:[UIColor whiteColor] fontSize:[UIFont yc_15] target:self action:@selector(front)];
        [_frontButton yc_radiusWithRadius:2];
        _frontButton.backgroundColor = [UIColor yc_hex_5360FF];
    }
    return _frontButton;
}

- (UIButton *)lastButton {
    if (!_lastButton) {
        _lastButton = [UIButton yc_customTextWithTitle:@"提交" titleColor:[UIColor whiteColor] fontSize:[UIFont yc_15] target:self action:@selector(submit)];
        [_lastButton yc_radiusWithRadius:2];
        _lastButton.backgroundColor = [UIColor yc_hex_5360FF];
    }
    return _lastButton;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.scrollEnabled = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone ;
    }
    return _tableView;
}
    
- (void)addFrontButtonAndSubmitButton {
    [self.contentView addSubview:self.frontButton];
    [self.frontButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(105);
         make.bottom.mas_equalTo(self.contentView.mas_bottom).mas_offset(- 65);
        make.height.mas_equalTo(35);
        make.left.mas_equalTo(25);
    }];

    [self.contentView addSubview:self.lastButton];
    [self.lastButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.contentView.mas_bottom).mas_offset(- 65);
        make.height.mas_equalTo(35);
        make.right.mas_equalTo(-25);
        make.width.mas_equalTo(105);
    }];
}
    
- (YCTableViewManager *)manager {
    if (!_manager) {
        _manager = [[YCTableViewManager alloc] initWithTableView:self.tableView];
        [_manager addSection:self.section];
        [_manager registerItems:@[@"SMAnswerItem"]];
    }
    return _manager;
}

- (YCTableViewSection *)section {
    if (!_section) {
        _section = [YCTableViewSection section];
    }
    return _section;
}


@end
