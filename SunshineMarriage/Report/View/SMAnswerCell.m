//
//  SMAnswerCell.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/29.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMAnswerCell.h"
#import "SMDefine.h"
#import "YCCategoryModule.h"
#import <Masonry/Masonry.h>
#import "SMAnswerItem.h"

@interface SMAnswerCell ()
@property (nonatomic, strong) UILabel *chooseTitle;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIImageView *chooseImageView;
@end

@implementation SMAnswerCell

- (void)cellDidLoad {
   self.selectionStyle = UITableViewCellSelectionStyleNone ;
   UILabel *chooseTitle = [UILabel yc_labelWithText:nil textFont:[UIFont yc_15] textColor:[UIColor yc_hex_333333]];
   [self.contentView addSubview:chooseTitle];
   [chooseTitle mas_makeConstraints:^(MASConstraintMaker *make) {
       make.left.mas_equalTo(0);
       make.top.mas_equalTo(self.contentView);
   }];
   self.chooseTitle = chooseTitle;
   
   UIImageView *chooseImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_unselected"]];
   [self.contentView addSubview:chooseImageView];
   [chooseImageView mas_makeConstraints:^(MASConstraintMaker *make) {
       make.right.mas_equalTo(0);
       make.centerY.mas_equalTo(self.contentView);
       make.width.height.mas_equalTo(20);
   }];
   self.chooseImageView = chooseImageView;
    
    UILabel *contentLabel = [UILabel yc_labelWithText:nil textFont:[UIFont yc_15] textColor:[UIColor yc_hex_333333]];
    contentLabel.numberOfLines = 0;
    [self.contentView addSubview:contentLabel];
    [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(chooseTitle);
        make.right.mas_equalTo(chooseImageView.mas_left);
    }];
    self.contentLabel = contentLabel;
}

+ (CGFloat)heightForCellWithItem:(SMAnswerItem *)item {
//    item.des = @"我周围的人往往觉得我对自己的看法有些矛盾";
    NSDictionary *attributes = @{NSFontAttributeName : [UIFont yc_15]};
    CGSize size = [item.des boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width - 2*68 - 30 - 20, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:attributes context:nil].size;
    size.height = ceil(size.height);
    return  (size.height > 15 ? size.height : 15)+30;
}

- (void)configCellWithItem:(SMAnswerItem *)item {
    self.chooseTitle.text = [NSString stringWithFormat:@"%@、",item.letter] ;
    self.contentLabel.text = item.des ;
    self.chooseImageView.image = [UIImage imageNamed: item.selected ? @"icon_selected" : @"icon_unselected"] ;
    [self.chooseImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView).mas_offset(-15);
    }];
}

@end
