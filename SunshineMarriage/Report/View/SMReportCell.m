//
//  SMReportCell.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/23.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMReportCell.h"
#import "SMReportItem.h"
#import <TTTAttributedLabel/TTTAttributedLabel.h>
#import "UIColor+Style.h"
#import "UIFont+Style.h"
#import "SMDefine.h"
#import <Masonry/Masonry.h>
#import "YCCategoryModule.h"
#import "NSDictionary+Extension.h"
#import <YYModel/YYModel.h>

@interface SMReportCell ()
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *resultLabel;
@property (nonatomic, strong) UILabel *analyzeLabel;
@property (nonatomic, strong) UILabel *detailLab;
@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UILabel *consultLabel;
@end

@implementation SMReportCell

- (void)cellDidLoad {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = [UIColor yc_hex_F5F6FA];
    
    UIView *backView = [[UIView alloc] init];
    backView.backgroundColor = [UIColor whiteColor];
    backView.layer.shadowColor = [UIColor colorWithRed:209/255.0 green:209/255.0 blue:209/255.0 alpha:0.36].CGColor;
    backView.layer.shadowOffset = CGSizeMake(0,3);
    backView.layer.shadowOpacity = 1;
    backView.layer.shadowRadius = 11;
    backView.layer.cornerRadius = 10;
    [self.contentView addSubview:backView];
    self.backView = backView;
    
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(27.5);
        make.right.mas_equalTo(-27.5);
        make.top.mas_equalTo(0);
        make.bottom.mas_equalTo(self.contentView.mas_bottom).mas_offset(-20);
    }];
    
    [backView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20);
        make.left.mas_equalTo(17.5);
    }];
    
    [backView addSubview:self.timeLabel];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(24);
        make.right.mas_equalTo(-15);
    }];
    
    [backView addSubview:self.resultLabel];
    [self.resultLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(71);
        make.left.mas_equalTo(17.5);
    }];
    
    [backView addSubview:self.analyzeLabel];
    [self.analyzeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(103);
        make.left.mas_equalTo(17.5);
        make.height.mas_equalTo(26);
        make.width.mas_equalTo(77);
    }];
    
    [backView addSubview:self.detailLab];
    [self.detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(105);
        make.left.mas_equalTo(17.5);
        make.right.lessThanOrEqualTo(backView.mas_right).mas_offset(-23);
//        make.bottom.mas_equalTo(self.backView).mas_offset(-22);
    }];
    
    UIView *line = [[UIView alloc] init];
    line.backgroundColor = [UIColor yc_hex_CCCCCC];
    [backView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(0.5);
        make.top.mas_equalTo(55);
    }];
    
    [backView addSubview:self.consultLabel];
    [self.consultLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.detailLab.mas_bottom).mas_offset(40);
        make.right.mas_equalTo(-17.5);
    }];
}

+ (CGFloat)heightForCellWithItem:(SMReportItem *)item {
//    item.json.sumScoreDes  = @"借记卡可敬可嘉健康口口声声说你能解决黄金时间回家回家回家时回家回家弄不明白你们呢哈哈哈哈";
  return item.itemHeight;
}

- (void)configCellWithItem:(SMReportItem *)item {
//item.json.sumScoreDes  = @"借记卡可敬可嘉健康口口声声说你能解决黄金时间回家回家回家时回家回家弄不明白你们呢哈哈哈哈";
    self.nameLabel.text = item.reportName ;
    self.timeLabel.text = [NSDate yc_getTimeString:item.insertTime format:YCDateFormatterTypeYYYYMMDD];
    //默认
    self.resultLabel.text = [NSString stringWithFormat:@"%@:%@",item.json.sumDes,item.json.sumScore];
       self.detailLab.text = item.json.sumScoreDes ;
    
    if(!item.json.sumScoreDes) {
        self.resultLabel.text = [NSString stringWithFormat:@"总分:%@",item.json.sumScore];
        self.detailLab.text = [NSString stringWithFormat:@"平均分:%@",item.json.avgScore]; ;
    }
    
    //人格问卷
    if ([item.reportId isEqualToString:@"11"]) {
         self.resultLabel.text = nil;
         self.detailLab.text = [NSString stringWithFormat:@"%@:%@,\n%@:%@,\n%@:%@,\n%@:%@",item.json.titleE,item.json.desE,item.json.titleN,item.json.desN,item.json.titleP,item.json.desP,item.json.titleL,item.json.desL] ;
               
    }
    //新婚物语
    if ([item.reportId isEqualToString:@"10"]) {
        self.resultLabel.text = nil;
        self.detailLab.text = item.json.des;
    }
   
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.detailLab.text];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:7];
    paragraphStyle.firstLineHeadIndent = 82;
//    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, self.detailLab.text.length)];
    [self.detailLab sizeToFit];
    self.detailLab.attributedText = attributedString;
}



- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel yc_labelWithText:nil textFont:[UIFont yc_17_bold] textColor:[UIColor yc_hex_333333]];
    }
    return _nameLabel;
}

- (UILabel *)consultLabel {
    if (!_consultLabel) {
        _consultLabel = [UILabel yc_labelWithText:@"具体报告请向所辖地专家咨询" textFont:[UIFont yc_14] textColor:[UIColor yc_hex_5360FF]];
    }
    return _consultLabel;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [UILabel yc_labelWithText:nil textFont:[UIFont yc_12] textColor:[UIColor yc_hex_999999]];
    }
    return _timeLabel;
}

- (UILabel *)resultLabel {
    if (!_resultLabel) {
        _resultLabel = [UILabel yc_labelWithText:nil textFont:[UIFont yc_14] textColor:[UIColor yc_hex_333333]];
    }
    return _resultLabel;
}

- (UILabel *)analyzeLabel {
    if (!_analyzeLabel) {
        _analyzeLabel = [UILabel yc_labelWithText:@"专家分析:" textFont:[UIFont yc_14] textColor:[UIColor yc_hex_5360FF]];
        _analyzeLabel.backgroundColor = [UIColor yc_hex_EFEFFF];
        _analyzeLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _analyzeLabel;
}

- (UILabel *)detailLab {
    if (!_detailLab) {
        _detailLab = [UILabel yc_labelWithText:nil textFont:[UIFont yc_14] textColor:[UIColor yc_hex_333333]];
        _detailLab.numberOfLines = 0;
    }
    return _detailLab;
}


@end
