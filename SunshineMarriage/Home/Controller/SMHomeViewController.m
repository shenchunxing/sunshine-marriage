//
//  HomeViewController.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/21.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMHomeViewController.h"
#import "YCTableViewKit.h"
#import "SMAppraisalItem.h"
#import "UIColor+Style.h"
#import "UIFont+Style.h"
#import "YCCategoryModule.h"
#import "SMDefine.h"
#import "SMAppUserAPI.h"
#import "SMOrganizationsModel.h"
#import "YCDataCenter.h"
#import "SMPersonInfoModel.h"
#import "SMRequestModel.h"

@interface SMHomeViewController ()
@property (nonatomic, strong) UIImageView *topImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *tipsLabel;
@property (nonatomic, strong) UIImageView *advertisingImageView;
@property (nonatomic, strong) YCTableViewManager *manager;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) YCTableViewSection *section;
@property (nonatomic, strong) YCGeneralAPI *getOrganizationsAPI;
@property (nonatomic, strong) YCGeneralAPI *userInfoAPI;
@end

@implementation SMHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBar.hidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadData) name:kUpdatePersonInfoNotification object:nil];
}

- (void)initView {
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(self.view).mas_offset(-kTabBarHeight);
        make.width.left.mas_equalTo(self.view);
    }];
    
    self.tableView.tableHeaderView = [self setHeader];
}

- (UIView *)setHeader {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kWIDTH, 282+kStatusBarHeight)];
    
    [header addSubview:self.topImageView];
    [self.topImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(0);
        make.height.mas_equalTo(174*kScaleFit);
        make.right.mas_equalTo(header);
    }];
    
    [header addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(11.5);
        make.top.mas_equalTo(kStatusBarHeight+40);
    }];
    
    [header addSubview:self.tipsLabel];
    [self.tipsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12.5);
        make.top.mas_equalTo(kStatusBarHeight+83);
    }];
    
    [header addSubview:self.advertisingImageView];
    [self.advertisingImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.tipsLabel.mas_bottom).mas_offset(23.5);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(header).mas_offset(-12);
        make.height.mas_equalTo(140);
    }];
    return header;
}

- (void)loadData {
    [self.userInfoAPI start];
}

- (void)initData {
    SMAppraisalItem *before = [SMAppraisalItem item];
    before.title = @"婚前情感评测";
    before.content = @"针对婚前男女双方的感情评测";
    before.imageName = @"home_hunqian";
    before.type = @"1";
    
    SMAppraisalItem *inner = [SMAppraisalItem item];
    inner.title = @"婚内情感评测";
    inner.content = @"通过量表了解自身不足与对方融洽相处";
    inner.imageName = @"home_hunnei";
    inner.type = @"2";
    
    SMAppraisalItem *after = [SMAppraisalItem item];
    after.title = @"婚后情感评测";
    after.content = @"认识自身缺点如何开始新的感情";
    after.imageName = @"home_hunhou";
    after.type = @"3";
    
    [self.section addItem:before];
    [self.section addItem:inner];
    [self.section addItem:after];
    [self.manager reloadData];
}

- (YCTableViewManager *)manager {
    if (!_manager) {
        _manager = [[YCTableViewManager alloc] initWithTableView:self.tableView];
        [_manager addSection:self.section];
        [_manager registerItems:@[@"SMAppraisalItem"]];
    }
    return _manager;
}

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] init];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor yc_hex_F5F6FA];
    }
    return _tableView;
}

- (YCTableViewSection *)section {
    if (!_section) {
        _section = [YCTableViewSection section];
        _section.headerView = [UIView new];
        _section.headerHeight = 25;
    }
    return _section;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel yc_labelWithText:[YCDataCenter sharedData].nickname textFont:[UIFont yc_24_bold] textColor:[UIColor yc_hex_373737]];
    }
    return _nameLabel;
}

- (UILabel *)tipsLabel {
    if (!_tipsLabel) {
        _tipsLabel = [UILabel yc_labelWithText:@"欢迎您进入阳光婚姻评估系统，请开始您的测评" textFont:[UIFont yc_14] textColor:[UIColor yc_hex_373737]];
    }
    return _tipsLabel;
}

- (UIImageView *)advertisingImageView {
    if (!_advertisingImageView) {
        _advertisingImageView = [[UIImageView alloc] init];
        _advertisingImageView.image = [UIImage imageNamed:@"banner"];
        [_advertisingImageView yc_radiusWithRadius:10];
    }
    return _advertisingImageView;
}

- (UIImageView *)topImageView {
    if (!_topImageView) {
        _topImageView = [[UIImageView alloc] init];
        _topImageView.image = [UIImage imageNamed:@"-s-bg.png"];
    }
    return _topImageView;
}


- (YCGeneralAPI *)userInfoAPI {
    if (!_userInfoAPI) {
        _userInfoAPI = [SMAppUserAPI userWithType:GetUserInfoByShareId success:^(__kindof YCBaseAPI * _Nonnull api, id  _Nonnull response) {
            NSDictionary *resBizMap = response[@"resBizMap"];
            SMPersonInfoModel *model = [SMPersonInfoModel yy_modelWithJSON:resBizMap[@"data"]];
            self.nameLabel.text = model.nickName?:[YCDataCenter sharedData].nickname;
        }];
        _userInfoAPI.requestModel.actionPath = [NSString stringWithFormat:@"getUserInfoByShareId/?shareId=%@",[YCDataCenter sharedData].shareId];
    }
    return _userInfoAPI;
}

@end
