//
//  SMEvaluatingListViewController.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/30.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMEvaluatingListViewController.h"
#import <Masonry/Masonry.h>
#import "YCCategoryModule.h"
#import "SMDefine.h"
#import "SMEvaluatingListItem.h"
#import "YCTableViewKit.h"
#import "SMReportAPI.h"
#import "SMRequestModel.h"
#import "SMMarryReportModel.h"

@interface SMEvaluatingListViewController ()
@property (nonatomic, strong) YCTableViewManager *manager;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) YCTableViewSection *section;
@property (nonatomic, copy) NSString *navTitle;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, strong) YCGeneralAPI *marryReportAPI;
@property (nonatomic, strong) SMMarryReportModel *model;
@end

@implementation SMEvaluatingListViewController

- (instancetype)initWithNavTitle:(NSString *)navtitle type:(nonnull NSString *)type {
    if (self = [super init]) {
        self.navTitle = navtitle;
        self.type = type;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationBar.title = self.navTitle ;
}

- (void)initView {
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kNavigationHeight);
        make.height.mas_equalTo(self.view).mas_offset(-kNavigationHeight);
        make.width.left.mas_equalTo(self.view);
    }];

}

- (void)loadData {
    [self.marryReportAPI start];
}


- (YCTableViewManager *)manager {
    if (!_manager) {
        _manager = [[YCTableViewManager alloc] initWithTableView:self.tableView];
        [_manager addSection:self.section];
        [_manager registerItems:@[@"SMEvaluatingListItem"]];
    }
    return _manager;
}

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] init];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor yc_hex_F5F6FA];
    }
    return _tableView;
}

- (YCTableViewSection *)section {
    if (!_section) {
        _section = [YCTableViewSection section];
    }
    return _section;
}

- (YCGeneralAPI *)marryReportAPI {
    if (!_marryReportAPI) {
        _marryReportAPI = [SMReportAPI reportWithType:GetMarryReport success:^(__kindof YCBaseAPI * _Nonnull api, id  _Nonnull response) {
            NSDictionary *resBizMap = response[@"resBizMap"];
            SMMarryReportModel *model = [SMMarryReportModel yy_modelWithJSON:resBizMap[@"marryReport"]];
            [model.items enumerateObjectsUsingBlock:^(SMEvaluatingListItem * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                obj.marryReportId = model.id;
                [self.section addItem:obj];
            }];
            self.model = model;
            [self.manager reloadData];
        } failure:^(__kindof YCBaseAPI * _Nonnull api, NSError * _Nonnull error) {
            
        }];
        _marryReportAPI.requestModel.actionPath = [NSString stringWithFormat:@"getMarryReport/?type=%@",self.type] ;
    }
    return _marryReportAPI;
}

@end
