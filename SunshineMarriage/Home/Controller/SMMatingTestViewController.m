//
//  SMMatingTestViewController.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/30.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMMatingTestViewController.h"
#import "YCCategoryModule.h"
#import <Masonry/Masonry.h>
#import "SMDefine.h"
#import "CursorPositionTextField.h"
#import "SMEvaluatingViewController.h"
#import "SMReportAPI.h"
#import "SMRequestModel.h"
#import "YCDataCenter.h"
#import "SMAppUserAPI.h"
#import "SMPersonInfoModel.h"
#import "UIViewController+CustomToast.h"
#import "SMTooL.h"

static NSString *const ExplainText = @"本套测评为配套测试，需要输入对方ID系统才能根据测评答案给出相应的感情建议";

@interface SMMatingTestViewController ()
@property (nonatomic, strong) UILabel *idLabel;
@property (nonatomic, strong) UILabel *explainLabel;
@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) CursorPositionTextField *textField;
@property (nonatomic, strong) UILabel *tipsLabel;
@property (nonatomic, strong) UIButton *beginTestBtn;
@property (nonatomic, strong) YCGeneralAPI *unionReport;
@property (nonatomic, strong) YCGeneralAPI *userInfoAPI;
@end

@implementation SMMatingTestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationBar.title = self.navTitle ;
    
    [self.view addSubview:self.idLabel];
    [self.idLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(43+kNavigationHeight);
        make.centerX.mas_equalTo(self.view);
    }];
    
    [self.view addSubview:self.explainLabel];
    [self.explainLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.idLabel.mas_bottom).mas_offset(28);
        make.left.mas_equalTo(65);
        make.right.mas_equalTo(-65);
    }];
//
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:ExplainText];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:7];
    paragraphStyle.firstLineHeadIndent = 15;
    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, ExplainText.length)];
    self.explainLabel.attributedText = attributedString;
    [self.explainLabel sizeToFit];
    self.explainLabel.numberOfLines = 0;
    
//    [self.view addSubview:self.containerView];
//    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(self.explainLabel.mas_bottom).mas_offset(70);
//        make.left.mas_equalTo(72);
//        make.right.mas_equalTo(-72);
//        make.height.mas_equalTo(42);
//    }];
    
    [self.view addSubview:self.textField];
    self.textField.frame = CGRectMake(72, 193+kNavigationHeight, kWIDTH - 144, 42);
//    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
//         make.top.mas_equalTo(self.explainLabel.mas_bottom).mas_offset(70);
//         make.left.mas_equalTo(72);
//         make.right.mas_equalTo(-72);
//         make.height.mas_equalTo(42);
//    }];
    
    [self.view addSubview:self.tipsLabel];
    [self.tipsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.textField.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(72);
    }];
    self.tipsLabel.hidden = YES;
    
    [self.view addSubview:self.beginTestBtn];
    [self.beginTestBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.textField.mas_bottom).mas_offset(38);
        make.left.mas_equalTo(72);
        make.right.mas_equalTo(-72);
        make.height.mas_equalTo(40);
    }];
}

- (void)loadData {
//    self.unionReport.params[@"reportId"] = self.reportId;
//    self.unionReport.params[@"shareCode"] = [YCDataCenter sharedData].shareId;
   
//    [self.userInfoAPI start];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.textField becomeFirstResponder];
}

- (void)beginTest {
    [self.userInfoAPI start];
//     [self.unionReport start];
//    SMEvaluatingViewController *vc = [[SMEvaluatingViewController alloc] init];
//    [self.yc_getCurrentViewController.navigationController pushViewController:vc animated:YES];
}

- (UILabel *)idLabel {
    if (!_idLabel) {
        _idLabel = [UILabel yc_labelWithText:[NSString stringWithFormat:@"我的ID：%@",[YCDataCenter sharedData].shareId] textFont:[UIFont yc_18] textColor:[UIColor yc_hex_333333] textAlignment:NSTextAlignmentCenter];
    }
    return _idLabel;
}

- (UILabel *)explainLabel {
    if (!_explainLabel) {
        _explainLabel = [UILabel yc_labelWithText:@"本套测评为配套测试，需要输入对方ID系统才能根据测评答案给出相应的感情建议" textFont:[UIFont yc_13] textColor:[UIColor yc_hex_333333] textAlignment:NSTextAlignmentCenter];
        _explainLabel.numberOfLines = 0;
    }
    return _explainLabel;
}

- (UIView *)containerView {
    if(!_containerView){
        _containerView = [[UIView alloc] init];
        _containerView.backgroundColor = [UIColor yc_hex_F0F0F0];
        [_containerView yc_radiusWithRadius:3];
    }
    return _containerView ;
}

- (CursorPositionTextField *)textField {
    if (!_textField) {
        _textField = [[CursorPositionTextField alloc] init];
        _textField.font = [UIFont yc_16_bold];
        _textField.textColor = [UIColor yc_hex_333333];
        _textField.backgroundColor = [UIColor yc_hex_F0F0F0];
        _textField.textAlignment = NSTextAlignmentCenter;
        [_textField yc_radiusWithRadius:3];
        [_textField setPlaceHolder:@"请输入对方ID" color:[UIColor yc_hex_333333] font:[UIFont yc_13]];
    }
    return _textField;
}

- (UILabel *)tipsLabel {
    if (!_tipsLabel) {
        _tipsLabel = [UILabel yc_labelWithText:@"对方姓名：李一" textFont:[UIFont yc_13] textColor:[UIColor yc_hex_333333]];
    }
    return _tipsLabel;
}

- (UIButton *)beginTestBtn {
    if (!_beginTestBtn) {
        _beginTestBtn = [UIButton yc_customTextWithTitle:@"开始评测" titleColor:[UIColor whiteColor] fontSize:[UIFont yc_16] target:self action:@selector(beginTest)];
        [_beginTestBtn yc_radiusWithRadius:4];
        _beginTestBtn.backgroundColor = [UIColor yc_hex_5360FF];
    }
    return _beginTestBtn;
}

- (YCGeneralAPI *)unionReport {
    if (!_unionReport) {
        @weakify(self);
        _unionReport = [SMReportAPI reportWithType:InitUnionReport success:^(__kindof YCBaseAPI * _Nonnull api, id  _Nonnull response) {
            @strongify(self);
            SMEvaluatingViewController *vc = [[SMEvaluatingViewController alloc] init];
            vc.reportId = self.reportId ;
            vc.marryReportId = self.marryReportId;
            vc.navTitle = self.navTitle;
            [self.yc_getCurrentViewController.navigationController pushViewController:vc animated:YES];
        }];
        _unionReport.apiFailureHandler = ^(__kindof YCBaseAPI * _Nonnull api, NSError * _Nonnull error) {
            @strongify(self);
            [self showCustomMessage_SM:error.userInfo[@"NSLocalizedDescription"]];
        };
 _unionReport.requestModel.actionPath = [NSString stringWithFormat:@"initUnionReport/?shareCode=%@&reportId=%@",self.textField.text,self.reportId];
    }
    return _unionReport;
}

- (YCGeneralAPI *)userInfoAPI {
    if (!_userInfoAPI) {
        _userInfoAPI = [SMAppUserAPI userWithType:GetUserInfoByShareId success:^(__kindof YCBaseAPI * _Nonnull api, id  _Nonnull response) {
            NSDictionary *resBizMap = response[@"resBizMap"];
            SMPersonInfoModel *model = [SMPersonInfoModel yy_modelWithJSON:resBizMap[@"data"]];
            [SMTooL toolWithTitle:@"温馨提示" content:[NSString stringWithFormat:@"该用户姓名为%@",model.nickName] confirmHandle:^{
                [self.unionReport start];
            }];
        } failure:^(__kindof YCBaseAPI * _Nonnull api, NSError * _Nonnull error) {
            [self showCustomMessage_SM:error.userInfo[@"NSLocalizedDescription"]];
        }];
        _userInfoAPI.requestModel.actionPath = [NSString stringWithFormat:@"getUserInfoByShareId/?shareId=%@",self.textField.text];
    }
    return _userInfoAPI;
}

@end
