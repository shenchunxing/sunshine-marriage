//
//  SMEvaluatingListViewController.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/30.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "YCBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SMEvaluatingListViewController : YCBaseViewController

- (instancetype)initWithNavTitle:(NSString *)navtitle type:(NSString *)type;

@end

NS_ASSUME_NONNULL_END
