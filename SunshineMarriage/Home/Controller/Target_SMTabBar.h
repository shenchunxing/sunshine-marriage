//
//  Target_SMTabBar.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/22.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import <Foundation/Foundation.h>
@class SMTabBarViewController;

NS_ASSUME_NONNULL_BEGIN

@interface Target_SMTabBar : NSObject

- (SMTabBarViewController *)Action_tabBarController:(NSDictionary *)params ;

@end

NS_ASSUME_NONNULL_END
