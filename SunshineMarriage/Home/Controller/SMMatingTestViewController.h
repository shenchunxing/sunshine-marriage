//
//  SMMatingTestViewController.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/30.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "YCBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SMMatingTestViewController : YCBaseViewController

@property (nonatomic, copy) NSString *reportId;
@property (nonatomic, copy) NSString *navTitle;
@property (nonatomic, copy) NSString *marryReportId;

@end

NS_ASSUME_NONNULL_END
