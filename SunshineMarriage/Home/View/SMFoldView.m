//
//  YCTitleView.m
//  YCBaseUI
//
//  Created by haima on 2019/4/3.
//

#import "SMFoldView.h"
#import <Masonry/Masonry.h>
#import "UIColor+Style.h"
#import "UIFont+Style.h"
#import "YCCategoryModule.h"

CGFloat const kSMTitleViewHeight = 60;

@interface SMFoldView ()

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic,strong) UIImageView *arrowImageView;
@property (nonatomic, assign) BOOL directionisUp;
@end

@implementation SMFoldView

- (instancetype)initWithTitle:(NSString *)title {
    
    if (self = [super init]) {
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.titleLabel];

        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).offset(24);
            make.left.equalTo(@25);
        }];
        
        [self.titleLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
        self.titleLabel.text = title;
        
        [self addSubview:self.arrowImageView];
        [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(12);
            make.height.mas_equalTo(6.5);
            make.right.mas_equalTo(-25);
            make.centerY.mas_equalTo(self);
        }];
        
        UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)];
        [self addGestureRecognizer:tap];

    }
    return self;
}

- (void)tap {
    self.directionisUp = !self.directionisUp;
    self.arrowImageView.image = [UIImage imageNamed:self.directionisUp ? @"jiantou_up":@"jiantou_down"];
    !self.tapBlock?:self.tapBlock(self.directionisUp);
}

- (void)setTitleView:(NSString *)title {
    self.titleLabel.text = title;
}

- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor yc_hex_333333];
        _titleLabel.font = [UIFont yc_15];
        _titleLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _titleLabel;
}

- (UIImageView *)arrowImageView {
    if (!_arrowImageView) {
        _arrowImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"jiantou_down"]];
    }
    return _arrowImageView;
}

@end
