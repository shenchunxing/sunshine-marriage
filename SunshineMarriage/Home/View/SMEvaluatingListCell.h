//
//  SMEvaluatingListCell.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/30.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YCTableViewKit.h"
NS_ASSUME_NONNULL_BEGIN

@interface SMEvaluatingListCell : UITableViewCell<YCTableViewCellProtocol>

@end

NS_ASSUME_NONNULL_END
