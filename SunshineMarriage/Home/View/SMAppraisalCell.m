//
//  SMAppraisalCell.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/23.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMAppraisalCell.h"
#import "SMAppraisalItem.h"
#import "UIColor+Style.h"
#import "UIFont+Style.h"
#import "SMDefine.h"
#import "UILabel+Extension.h"
#import "YCCategoryModule.h"
#import <Masonry/Masonry.h>
#import "SMEvaluatingListViewController.h"


@interface SMAppraisalCell ()
@property (nonatomic, strong) UILabel *titLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIImageView *detailImageView;
@property (nonatomic, strong) UIButton *enterBtn;
@property (nonatomic, strong) SMAppraisalItem *item;
@end

@implementation SMAppraisalCell

- (void)cellDidLoad {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = [UIColor yc_hex_F5F6FA];
    UIView *backView = [[UIView alloc] init];
    backView.backgroundColor = [UIColor whiteColor];
    [backView yc_radiusWithRadius:10];
    [self.contentView addSubview:backView];
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(120);
        make.top.mas_equalTo(0);
    }];
    
    [backView addSubview:self.titLabel];
    [self.titLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(15.5);
        make.left.mas_equalTo(15);
    }];
    
    [backView addSubview:self.contentLabel];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(43.5);
        make.left.mas_equalTo(14.5);
    }];
    
    [backView addSubview:self.detailImageView];
    [self.detailImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(backView).mas_offset(-8);
        make.width.mas_equalTo(123.65);
        make.top.mas_equalTo(15);
        make.bottom.mas_equalTo(backView).mas_offset(-15);
    }];
    
    [backView addSubview:self.enterBtn];
    [self.enterBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(68);
        make.width.mas_equalTo(88);
        make.height.mas_equalTo(32);
    }];
}

+ (CGFloat)heightForCellWithItem:(SMAppraisalItem *)item {
    return 135;
}

- (void)configCellWithItem:(SMAppraisalItem *)item {
    self.item = item;
    self.titLabel.text = item.title;
    self.contentLabel.text = item.content;
    self.detailImageView.image = [UIImage imageNamed:item.imageName];
}


/// 点击进入
- (void)enter {
    SMEvaluatingListViewController * vc = [[SMEvaluatingListViewController alloc] initWithNavTitle:self.item.title type:self.item.type];
    [self.yc_getCurrentViewController.navigationController pushViewController:vc animated:YES];
}

- (UILabel *)titLabel {
    if (!_titLabel) {
        _titLabel = [UILabel yc_labelWithText:nil textFont:[UIFont yc_19_bold] textColor:[UIColor yc_hex_333333]];
    }
    return _titLabel;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [UILabel yc_labelWithText:nil textFont:[UIFont yc_12] textColor:[UIColor yc_hex_999999]];
    }
    return _contentLabel;
}

- (UIImageView *)detailImageView {
    if (!_detailImageView) {
        _detailImageView = [[UIImageView alloc] init];
    }
    return _detailImageView;
}

- (UIButton *)enterBtn {
    if (!_enterBtn) {
        _enterBtn = [UIButton yc_customTextWithTitle:@"点击进入" titleColor:[UIColor whiteColor] fontSize:[UIFont yc_14] target:self action:@selector(enter)];
        _enterBtn.backgroundColor = [UIColor colorWithRed:83/255.0 green:96/255.0 blue:255/255.0 alpha:1.0];
        _enterBtn.layer.shadowColor = [UIColor colorWithRed:83/255.0 green:96/255.0 blue:255/255.0 alpha:0.36].CGColor;
        _enterBtn.layer.shadowOffset = CGSizeMake(0,4);
        _enterBtn.layer.shadowOpacity = 1;
        _enterBtn.layer.shadowRadius = 11;
        _enterBtn.layer.cornerRadius = 3;
    }
    return _enterBtn;
}

@end
