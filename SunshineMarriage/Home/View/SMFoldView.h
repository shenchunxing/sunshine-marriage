//
//  YCTitleView.h
//  YCBaseUI
//
//  Created by haima on 2019/4/3.
//

#import <UIKit/UIKit.h>

extern CGFloat const kSMTitleViewHeight;

@interface SMFoldView : UIView

@property (nonatomic,copy) void(^tapBlock)(BOOL fold);

- (instancetype)initWithTitle:(NSString *)title;

@end

