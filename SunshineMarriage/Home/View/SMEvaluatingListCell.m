//
//  SMEvaluatingListCell.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/30.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMEvaluatingListCell.h"
#import <Masonry/Masonry.h>
#import "YCCategoryModule.h"
#import "SMDefine.h"
#import "SMEvaluatingListItem.h"
#import "SMEvaluatingViewController.h"
#import "SMMatingTestViewController.h"

@interface SMEvaluatingListCell ()
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *gotoAnswerBtn;
@property (nonatomic, strong) SMEvaluatingListItem *item;
@end

@implementation SMEvaluatingListCell

- (void)cellDidLoad {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView);
        make.left.mas_equalTo(32);
    }];
    
    [self.contentView addSubview:self.gotoAnswerBtn];
    [self.gotoAnswerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-32);
        make.height.mas_equalTo(35);
        make.width.mas_equalTo(74);
        make.centerY.mas_equalTo(self.contentView);
    }];
    
    UIView *line = [[UIView alloc] init];
    line.backgroundColor = [UIColor yc_hex_EFEFEF];
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.contentView);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(kLineHeight);
    }];
}

+ (CGFloat)heightForCellWithItem:(SMEvaluatingListItem *)item {
    return 80;
}

- (void)configCellWithItem:(SMEvaluatingListItem *)item {
    self.item = item;
    self.titleLabel.text = item.reportName ;
    [self.gotoAnswerBtn setTitle:item.state == 0 ? @"去完成":@"已完成" forState:UIControlStateNormal];
    self.gotoAnswerBtn.backgroundColor = item.state == 0 ? [UIColor yc_hex_5360FF]:[UIColor yc_hex_CCCCCC];
    self.gotoAnswerBtn.userInteractionEnabled = item.state == 0 ;
}

- (void)gotoAnswer {
    if ([self.item.reportName isEqualToString:@"新婚物语"]) {
        SMMatingTestViewController *test = [[SMMatingTestViewController alloc] init];
        test.reportId = self.item.reportId;
        test.navTitle = self.item.reportName  ;
        test.marryReportId = self.item.marryReportId ;
        [self.yc_getCurrentViewController.navigationController pushViewController:test animated:YES];
        return;
    }
    SMEvaluatingViewController *vc = [[SMEvaluatingViewController alloc] init];
    vc.reportId = self.item.reportId;
    vc.navTitle = self.item.reportName ;
    vc.marryReportId = self.item.marryReportId ;
    [self.yc_getCurrentViewController.navigationController pushViewController:vc animated:YES];
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel yc_labelWithText:@"自我和谐量表" textFont:[UIFont yc_16] textColor:[UIColor yc_hex_333333]];
    }
    return _titleLabel;
}

- (UIButton *)gotoAnswerBtn {
    if (!_gotoAnswerBtn) {
        _gotoAnswerBtn = [UIButton yc_customTextWithTitle:@"去完成" titleColor:[UIColor whiteColor] fontSize:[UIFont yc_15] target:self action:@selector(gotoAnswer)];
        [_gotoAnswerBtn yc_radiusWithRadius:2];
        _gotoAnswerBtn.backgroundColor = [UIColor yc_hex_5360FF];
    }
    return _gotoAnswerBtn;
}

@end
