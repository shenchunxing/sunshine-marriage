//
//  SMAppraisalItem.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/23.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "YCTableViewItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface SMAppraisalItem : YCTableViewItem
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *imageName;
@property (nonatomic, copy) NSString *type;

@end

NS_ASSUME_NONNULL_END
