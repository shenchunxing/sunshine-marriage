//
//  SMEvaluatingListItem.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/30.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "YCTableViewItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface SMEvaluatingListItem : YCTableViewItem

@property (nonatomic, copy) NSString *reportId;
@property (nonatomic, copy) NSString *reportName;
@property (nonatomic, assign) int state;
@property (nonatomic, copy) NSString *marryReportId;

@end

NS_ASSUME_NONNULL_END
