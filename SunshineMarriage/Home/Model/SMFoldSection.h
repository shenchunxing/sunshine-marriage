//
//  SMFoldSection.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/24.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "YCTableViewSection.h"

NS_ASSUME_NONNULL_BEGIN

@interface SMFoldSection : YCTableViewSection

@property (nonatomic, copy) NSString *title;
+ (instancetype)sectionWithTitle:(NSString *)title foldBlock:(void(^)(BOOL))foldBlock; 

@end

NS_ASSUME_NONNULL_END
