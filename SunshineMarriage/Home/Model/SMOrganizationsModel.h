//
//  SMOrganizationsModel.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/31.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SMOrganizationsModel : NSObject

@property (nonatomic, copy) NSString *description;
@property (nonatomic, assign) int id;
@property (nonatomic, assign) int level;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) int *parentId;

@end

NS_ASSUME_NONNULL_END
