//
//  SMMarryReportModel.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/31.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMMarryReportModel.h"
#import <YYModel/YYModel.h>

@implementation SMMarryReportModel

- (NSArray<SMEvaluatingListItem *> *)items {
    if (self.reportStateJson) {
        id tmp = [NSJSONSerialization JSONObjectWithData:[self.reportStateJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments | NSJSONReadingMutableLeaves | NSJSONReadingMutableContainers error:nil] ;
        if ([tmp isKindOfClass:[NSArray class]]) {
            NSArray *items = [NSArray yy_modelArrayWithClass:[SMEvaluatingListItem class] json:tmp];
            return items;
        }
    }
    return nil;
}

@end
