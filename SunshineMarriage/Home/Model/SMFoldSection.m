//
//  SMFoldSection.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/24.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMFoldSection.h"
#import "SMFoldView.h"
@implementation SMFoldSection

+ (instancetype)sectionWithTitle:(NSString *)title foldBlock:(nonnull void (^)(BOOL))foldBlock {
    SMFoldSection *section = [[self alloc] init];
    section.title = title;
    SMFoldView *titleView = [[SMFoldView alloc] initWithTitle:title];
    titleView.tapBlock = foldBlock;
    section.headerView = titleView;
    section.headerHeight = kSMTitleViewHeight;

    if (!title) {
        section.headerView = nil;
        section.headerHeight = 0;
    }
    
    return section;
}

@end
