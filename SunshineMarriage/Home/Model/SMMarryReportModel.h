//
//  SMMarryReportModel.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/31.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SMEvaluatingListItem.h"
NS_ASSUME_NONNULL_BEGIN

@interface SMMarryReportModel : NSObject

@property (nonatomic, copy) NSString *id;
@property (nonatomic, assign) double insertTime;
@property (nonatomic, copy) NSString *reportStateJson;
@property (nonatomic, copy) NSArray <SMEvaluatingListItem *> *items;
@property (nonatomic, assign) int  state;
@property (nonatomic, assign) int type;
@property (nonatomic, assign) int userId;


@end

NS_ASSUME_NONNULL_END
