//
//  SMLoginAPI.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/22.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMLoginAPI.h"
#import "SMRequestModel.h"
#import "YCDataCenter.h"

@interface SMLoginAPI ()
@property (nonatomic, strong) SMRequestModel *model;
@end
@implementation SMLoginAPI

- (YCRequestModel *)apiRequestModel {
    return self.model;
}

- (SMRequestModel *)model {
    if (!_model) {
        _model = [SMRequestModel modelWithActionPath:[NSString stringWithFormat:@"?thirdCode=%@&thirdType=%@",[YCDataCenter sharedData].appusercode,[YCDataCenter sharedData].thirdType]];
        _model.serviceName = @"thirdLogin";
        _model.requestType = YCHttpRequestTypePost;
    }
    return _model;
}

@end
