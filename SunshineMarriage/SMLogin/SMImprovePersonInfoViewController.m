//
//  SMImprovePersonInfoViewController.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/3/3.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMImprovePersonInfoViewController.h"
#import "UIColor+Style.h"
#import "UIFont+Style.h"
#import "SMDefine.h"
#import <Masonry/Masonry.h>
#import "UILabel+Extension.h"
#import "YCTableViewKit.h"
#import "SMPickerItem.h"
#import "YCDataCenter.h"
#import <UIImageView+WebCache.h>
#import "UIButton+Extension.h"
#import "UIView+Extension.h"
#import "SMPickerItem.h"
#import "SMNamePickerStrategy.h"
#import "SMOptionPickerStrategy.h"
#import "SMAppUserAPI.h"
#import "SMRequestModel.h"
#import "SMOriganiseModel.h"
#import <YYModel/YYModel.h>
#import "SMPersonInfoModel.h"
#import "YCTextViewItem.h"
#import "YCEditViewHelper.h"
#import "UIViewController+CustomAlert.h"
#import "UIViewController+CustomToast.h"
#import "YCMediator+SMTabBar.h"
#import "SMTooL.h"

@interface SMImprovePersonInfoViewController ()

@property (nonatomic, strong) UIButton *finishBtn;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) YCTableViewManager *manager;
@property (nonatomic, strong) YCGeneralAPI *userInfoAPI;
@property (nonatomic, strong) YCGeneralAPI *setUserInfoAPI;
@property (nonatomic, strong) YCGeneralAPI *origanizsAPI;
@property (nonatomic, strong) NSMutableArray <YCOptionModel *>*origanizs;

@property (nonatomic, strong) SMPickerItem *name;
@property (nonatomic, strong) SMPickerItem *sex;
@property (nonatomic, strong) SMPickerItem *age;
@property (nonatomic, strong) YCTextViewItem *phone;
@property (nonatomic, strong) SMPickerItem *level;
@property (nonatomic, strong) SMPickerItem *type;
@property (nonatomic, strong) SMPickerItem *income;
@property (nonatomic, strong) SMPickerItem *address;

@property (nonatomic, strong) UIButton *arrowBtn;
@end

@implementation SMImprovePersonInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.navigationBar.hidden = YES;
    self.navigationBar.backgroundColor = [UIColor whiteColor];
    UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_arrow"]];
    arrow.frame = CGRectMake(0, 12, 20, 20);
    [self.navigationBar.leftBarButton addSubview:arrow];
}

- (void)yc_viewControllerDidLeftClick:(UIViewController *)viewController {
    [[YCDataCenter sharedData] clearLoginInfo];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)initView {
    UILabel *improve = [UILabel yc_labelWithText:@"完善您的个人信息" textFont:[UIFont yc_24_bold] textColor:[UIColor yc_hex_333333]];
    [self.view addSubview:improve];
    [improve mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(13);
        make.top.mas_equalTo(kNavigationHeight+26);
    }];
    
    UILabel *detailLabel = [UILabel yc_labelWithText:@"所有信息仅用于系统管理，未经授权绝不外传" textFont:[UIFont yc_12] textColor:[UIColor yc_hex_999999]];
    [self.view addSubview:detailLabel];
    [detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(13);
        make.top.mas_equalTo(improve.mas_bottom).mas_offset(19);
    }];
    
    UIImageView *iconImageView = [[UIImageView alloc] init];
    [iconImageView yc_radiusWithRadius:35];
    [self.view addSubview:iconImageView];
    [iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view);
        make.width.height.mas_equalTo(70);
        make.top.mas_equalTo(detailLabel.mas_bottom).mas_offset(49);
    }];
    [iconImageView sd_setImageWithURL:[NSURL URLWithString:[YCDataCenter sharedData].headimgurl] placeholderImage:[UIImage imageNamed:[YCDataCenter sharedData].sex == 1 ? @"nan":@"nv"]];
  
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(iconImageView.mas_bottom).mas_offset(10);
        make.bottom.mas_equalTo(self.view).mas_offset(-45);
        make.left.mas_equalTo(42.5);
        make.right.mas_equalTo(-42.5);
    }];
    self.tableView.tableFooterView = [self footerView];
}

- (void)loadData {
    [self.origanizsAPI start];
}

- (void)initData {
    YCTableViewSection *baseSection = [YCTableViewSection section];
    [self.manager addSection:baseSection];
    
    SMPickerItem *name = [SMPickerItem item];
    name.title = @"姓名";
    name.key = @"nickName";
    name.cellDefaultHeight = 70;
    name.titleFont = [UIFont yc_16_bold];
    name.titleColor = [UIColor yc_hex_333333];
    name.text = [YCDataCenter sharedData].nickname;
    [baseSection addItem:name];
    SMNamePickerStrategy *nameStrategy = [SMNamePickerStrategy stragetyWithNamePickerStrategyBlock:^(NSString *text) {
        name.text = text;
        [self.manager reloadData];
    }];
    name.strategy = nameStrategy ;
    self.name = name;
    
    SMPickerItem *sex = [SMPickerItem item];
    sex.title = @"性别";
    sex.key = @"sex";
    sex.cellDefaultHeight = 70;
    sex.titleFont = [UIFont yc_16_bold];
    sex.titleColor = [UIColor yc_hex_333333];
    [baseSection addItem:sex];
    SMOptionPickerStrategy *sexStrategy = [SMOptionPickerStrategy stragetyWithBlock:^(YCOptionModel *model) {
        sex.optionModel = model;
        [self.manager reloadData];
    }];
    sexStrategy.options = [[YCEnumSwap sharedInstancetype] getDataSourceWithKey:@"sex"];
    sex.strategy = sexStrategy;
    self.sex = sex;
    
    SMPickerItem *age = [SMPickerItem item];
    age.title = @"年龄";
    age.key = @"age";
    age.cellDefaultHeight = 70;
    age.titleFont = [UIFont yc_16_bold];
    age.titleColor = [UIColor yc_hex_333333];
    [baseSection addItem:age];
    SMOptionPickerStrategy *ageStrategy = [SMOptionPickerStrategy stragetyWithBlock:^(YCOptionModel *model) {
        age.optionModel = model ;
        [self.manager reloadData];
    }];
    ageStrategy.options = [[YCEnumSwap sharedInstancetype] getDataSourceWithKey:@"age"];
    age.strategy = ageStrategy;
    self.age = age;
    
    
    YCTextViewItem *phone = [YCTextViewItem textViewItem];
    phone.alignedRight = YES;
    phone.title = @"手机号";
    phone.key = @"phone";
//    phone.cellDefaultHeight = 70;
    phone.titleFont = [UIFont yc_16_bold];
    phone.titleColor = [UIColor yc_hex_333333];
    phone.textColor = [UIColor yc_hex_999999];
    phone.hideClearBtn = YES;
    phone.keyBoardType = UIKeyboardTypeNumberPad ;
    phone.limitLength = 11;
    [baseSection addItem:phone];
    self.phone = phone;

    SMPickerItem *level = [SMPickerItem item];
    level.title = @"文化程度";
    level.key = @"education";
    level.cellDefaultHeight = 70;
    level.titleFont = [UIFont yc_16_bold];
    level.titleColor = [UIColor yc_hex_333333];
    [baseSection addItem:level];
    SMOptionPickerStrategy *levelStrategy = [SMOptionPickerStrategy stragetyWithBlock:^(YCOptionModel *model) {
        level.optionModel = model ;
        [self.manager reloadData];
    }];
    levelStrategy.options = [[YCEnumSwap sharedInstancetype] getDataSourceWithKey:@"level"];
    level.strategy = levelStrategy;
    self.level = level;
    
    SMPickerItem *type = [SMPickerItem item];
    type.title = @"工作类型";
    type.key = @"workType";
    type.cellDefaultHeight = 70;
    type.titleFont = [UIFont yc_16_bold];
    type.titleColor = [UIColor yc_hex_333333];
    [baseSection addItem:type];
    SMOptionPickerStrategy *typeStrategy = [SMOptionPickerStrategy stragetyWithBlock:^(YCOptionModel *model) {
        type.optionModel = model ;
        [self.manager reloadData];
    }];
    typeStrategy.options = [[YCEnumSwap sharedInstancetype] getDataSourceWithKey:@"type"];
    type.strategy = typeStrategy;
    self.type = type;
    
    SMPickerItem *income = [SMPickerItem item];
    income.title = @"收入（选填）";
    income.key = @"income";
    income.titleFont = [UIFont yc_16_bold];
    income.titleColor = [UIColor yc_hex_333333];
    income.cellDefaultHeight = 70;
    [baseSection addItem:income];
    SMOptionPickerStrategy *incomeStrategy = [SMOptionPickerStrategy stragetyWithBlock:^(YCOptionModel *model) {
        income.optionModel = model ;
        [self.manager reloadData];
    }];
    incomeStrategy.options = [[YCEnumSwap sharedInstancetype] getDataSourceWithKey:@"income"];;
    income.strategy = incomeStrategy;
    self.income = income;
    
    SMPickerItem *address = [SMPickerItem item];
    address.title = @"所属婚姻登记处";
    address.key = @"orgId";
    address.titleFont = [UIFont yc_16_bold];
    address.titleColor = [UIColor yc_hex_333333];
    address.cellDefaultHeight = 70;
    [baseSection addItem:address];
    self.address = address;
    
    [self.manager reloadData];
}

- (void)finish {
    if (![YCEditViewHelper sm_isAnyItemValiableInSections:self.manager.sections showMessage:YES]) {
        [self showCustomMessage_SM:@"请填写完所有信息"];
        return ;
    }
    self.setUserInfoAPI.requestModel.actionPath = [NSString stringWithFormat:@"setUserInfo/?phone=%@&nickName=%@&sex=%@&age=%@&education=%@&workType=%@&income=%@&orgId=%@",self.phone.text,self.name.text,self.sex.optionModel.code,self.age.optionModel.code,self.level.optionModel.code,self.type.optionModel.code,self.income.optionModel.code,self.address.optionModel.code];
    [self.setUserInfoAPI start];
}

- (YCTableViewManager *)manager {
    if (!_manager) {
        _manager = [[YCTableViewManager alloc] initWithTableView:self.tableView];
        [_manager registerItems:@[@"SMPickerItem",@"YCTextViewItem"]];
    }
    return _manager;
}

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] init];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor whiteColor];
    }
    return _tableView;
}

- (UIButton *)finishBtn {
    if (!_finishBtn) {
        _finishBtn = [UIButton yc_customTextWithTitle:@"完成" titleColor:[UIColor whiteColor] fontSize:[UIFont yc_18] target:self action:@selector(finish)];
        [_finishBtn yc_radiusWithRadius:24];
        _finishBtn.backgroundColor = [UIColor yc_hex_5360FF];
    }
    return _finishBtn;
}

- (UIView *)footerView {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kWIDTH - 42.5*2, 48+36)];
    self.finishBtn.frame = CGRectMake(0, 36.5, kWIDTH - 42.5*2, 48);
    [view addSubview:self.finishBtn];
    return view;
}

- (YCGeneralAPI *)userInfoAPI {
    if (!_userInfoAPI) {
        _userInfoAPI = [SMAppUserAPI userWithType:GetUserInfoByShareId success:^(__kindof YCBaseAPI * _Nonnull api, id  _Nonnull response) {
            NSDictionary *resBizMap = response[@"resBizMap"];
            SMPersonInfoModel *model = [SMPersonInfoModel yy_modelWithJSON:resBizMap[@"data"]];
            [self configItemWithModel:model];
        }];
        _userInfoAPI.requestModel.actionPath = [NSString stringWithFormat:@"getUserInfoByShareId/?shareId=%@",[YCDataCenter sharedData].shareId];
    }
    return _userInfoAPI;
}

- (void)configItemWithModel:(SMPersonInfoModel *)model {

    self.name.text = model.nickName?:[YCDataCenter sharedData].nickname ;
    self.phone.text = model.phone ;
    
    self.sex.optionModel = [YCOptionModel modelWithName:[[YCEnumSwap sharedInstancetype] getEnumSwapWithKey:@"sex" swapStr:model.sex?:[NSString stringWithFormat:@"%d",[YCDataCenter sharedData].sex]] code:model.sex?:[NSString stringWithFormat:@"%d",[YCDataCenter sharedData].sex]] ;

    self.age.optionModel = [YCOptionModel modelWithName:[[YCEnumSwap sharedInstancetype] getEnumSwapWithKey:@"age" swapStr:model.age] code:model.age] ;
    
    self.level.optionModel = [YCOptionModel modelWithName:[[YCEnumSwap sharedInstancetype] getEnumSwapWithKey:@"level" swapStr:model.education] code:model.education] ;
    
    self.type.optionModel = [YCOptionModel modelWithName:[[YCEnumSwap sharedInstancetype] getEnumSwapWithKey:@"type" swapStr:model.workType] code:model.workType] ;
    
    self.income.optionModel = [YCOptionModel modelWithName:[[YCEnumSwap sharedInstancetype] getEnumSwapWithKey:@"income" swapStr:model.income] code:model.income] ; ;
    
    self.address.optionModel = [YCOptionModel modelWithName:[self getNameWithCode:model.orgId] code:model.orgId];

     [self.manager reloadData];
}

- (YCGeneralAPI *)setUserInfoAPI {
    if (!_setUserInfoAPI) {
        @weakify(self);
        _setUserInfoAPI = [SMAppUserAPI userWithType:SetUserInfo success:^(__kindof YCBaseAPI * _Nonnull api, id  _Nonnull response) {
            @strongify(self);
            [self jumpToTabBarController];
        }];
    }
    return _setUserInfoAPI;
}

- (void)jumpToTabBarController {
    UIViewController *vc = [[YCMediator sharedInstance] tabBarControllerWithParmas:nil];
    [UIApplication sharedApplication].delegate.window.rootViewController = vc;
}

- (YCGeneralAPI *)origanizsAPI {
    if (!_origanizsAPI) {
        _origanizsAPI = [SMAppUserAPI userWithType:GetOrganizations success:^(__kindof YCBaseAPI * _Nonnull api, id  _Nonnull response) {
            
            NSDictionary *resBizMap = response[@"resBizMap"];
            NSArray *origaniseModels = [NSArray yy_modelArrayWithClass:[SMOriganiseModel class] json:resBizMap[@"data"]];

            [self.origanizs removeAllObjects];
            [origaniseModels enumerateObjectsUsingBlock:^(SMOriganiseModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                YCOptionModel *model = [[YCOptionModel alloc] init];
                model.name = obj.name;
                model.code = [NSString stringWithFormat:@"%d",obj.id] ;
                [self.origanizs addObject:model];
            }];
            
            SMOptionPickerStrategy *addressStrategy = [SMOptionPickerStrategy stragetyWithBlock:^(YCOptionModel *model) {
                self.address.optionModel = model ;
                [self.manager reloadData];
            }];
            
            addressStrategy.options = self.origanizs;
            self.address.strategy = addressStrategy;
            
            [self.userInfoAPI start];

        }];
    }
    return _origanizsAPI;
}

- (NSMutableArray<YCOptionModel *> *)origanizs {
    if (!_origanizs) {
        _origanizs = [ NSMutableArray new];
    }
    return _origanizs;
}

- (NSString *)getNameWithCode:(NSString *)code {
    __block NSString *name = nil;
    [self.origanizs enumerateObjectsUsingBlock:^(YCOptionModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.code isEqualToString:code]) {
            *stop = YES;
            name = obj.name;
        }
    }];
    return  name;
}

- (UIButton *)arrowBtn {
    if (!_arrowBtn) {
        _arrowBtn = [[UIButton alloc] init];
        _arrowBtn.imageView.size = CGSizeMake(20, 20);
        [_arrowBtn setImage:[UIImage imageNamed:@"icon_arrow"] forState:UIControlStateNormal];
    }
    return _arrowBtn;
}

@end
