//
//  SMWebViewController.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/30.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "YCBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SMWebViewController : YCBaseViewController

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *webURLString;

- (instancetype)initWithTitle:(NSString *)title webURLString:(NSString *)webURLString ;

@end

NS_ASSUME_NONNULL_END
