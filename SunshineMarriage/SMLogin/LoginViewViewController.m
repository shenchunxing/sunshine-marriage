//
//  LoginViewViewController.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/3/4.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "LoginViewViewController.h"
#import <Masonry/Masonry.h>
#import "YCPhoneNumberTextField.h"
#import "YCEncryptionTextField.h"
#import "UILabel+Extension.h"
#import "UIButton+Extension.h"
#import "UIColor+Style.h"
#import "UIFont+Style.h"
#import "UIView+Extension.h"
#import "YCTextFieldEditManager.h"
#import "SMLoginAPI.h"
#import "YCMediator+SMTabBar.h"
#import "UIViewController+CustomToast.h"
#import "YCDataCenter.h"
#import "SMAppUser.h"

@interface LoginViewViewController ()
@property (strong, nonatomic) UIButton *loginBtn;//登录按钮
@property (strong, nonatomic) YCPhoneNumberTextField *nameTextField; //手机号
@property (strong, nonatomic) YCEncryptionTextField *passwordTextField; //密码
@property (nonatomic, strong) SMLoginAPI *loginAPI;
@end

@implementation LoginViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationBar.hidden = YES;
    [self.view addSubview:self.nameTextField];
    [self.nameTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.height.mas_equalTo(57);
        make.top.mas_equalTo(200);
        make.right.mas_equalTo(self.view).offset(-30);
    }];
    
    [self.view addSubview:self.passwordTextField];
    [self.passwordTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.height.mas_equalTo(57);
        make.top.mas_equalTo(self.nameTextField.mas_bottom);
        make.right.mas_equalTo(self.view).offset(-30);
    }];

    [self.view addSubview:self.loginBtn];
    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.passwordTextField.mas_bottom).offset(60);
        make.left.mas_equalTo(30);
        make.height.mas_equalTo(48);
        make.right.mas_equalTo(self.view).offset(-30);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldChanged:) name:UITextFieldTextDidChangeNotification object:nil];
}

- (void)textFieldChanged:(NSNotification *)noti{
    self.loginBtn.userInteractionEnabled = [YCTextFieldEditManager verifyTextfieldsInSuperview:self.view];
    self.loginBtn.backgroundColor = [YCTextFieldEditManager verifyTextfieldsInSuperview:self.view] ? [UIColor yc_hex_108EE9]:[UIColor yc_hex_CACCD4];
}

- (void)loginBtnClicked {
    if (![YCTextFieldEditManager verifyTextfields:@[self.nameTextField,self.passwordTextField]])
        return;
    if ([self.nameTextField.text isEqualToString:@"187 6819 2045"] && [self.passwordTextField.text isEqualToString:@"123456"]) {
        [YCDataCenter sharedData].appusercode = @"62B940048532BAC07D771098E0C7EF7E" ;
        [YCDataCenter sharedData].thirdType = @"qq";
        [self.loginAPI start];
    }else{
        [self showToast:@"账号密码不匹配"];
    }
    
}

- (void)jumpToTabBarController {
    UIViewController *vc = [[YCMediator sharedInstance] tabBarControllerWithParmas:nil];
    [UIApplication sharedApplication].delegate.window.rootViewController = vc;
    [vc showCustomMessage_SM:@"登录成功"];
}

- (void)saveloginInfo:(id)response {
    NSDictionary *resBizMap = response[@"resBizMap"];
    SMAppUser *user = [SMAppUser yy_modelWithJSON:resBizMap];
    [YCDataCenter sharedData].authorization = user.authorization;
    [YCDataCenter sharedData].nickname = user.appUser.nickName;
    [YCDataCenter sharedData].insertTime = user.appUser.insertTime;
    [YCDataCenter sharedData].shareId = user.appUser.shareId;
}

- (SMLoginAPI *)loginAPI {
    if (!_loginAPI) {
        _loginAPI = [[SMLoginAPI alloc] init];
        __weak typeof(self) weakSelf = self;
        _loginAPI.apiSuccessHandler = ^(__kindof YCBaseAPI * _Nonnull api, id  _Nonnull response) {
            [weakSelf saveloginInfo:response] ;
            [weakSelf jumpToTabBarController];
        };
        _loginAPI.presenter = [YCAlertHUDPresnter HUDWithView:self.view];
    }
    return _loginAPI;
}

- (YCPhoneNumberTextField *)nameTextField{
    if (!_nameTextField) {
        _nameTextField = [[YCPhoneNumberTextField alloc] initWithName:@"手机号码"];
        _nameTextField.imageName = @"icon_user";
        _nameTextField.maxLength = 13;
    }
    return _nameTextField;
}

- (YCEncryptionTextField *)passwordTextField{
    if (!_passwordTextField) {
        _passwordTextField = [[YCEncryptionTextField alloc] initWithName:@"密码"];
        _passwordTextField.textFieldType = YCPasswordTextFieldType;
        _passwordTextField.imageName = @"icon_password";
        _passwordTextField.minLength = 6;
        _passwordTextField.maxLength = 16;
//        _passwordTextField.returnKeyType = UIReturnKeyGo;
        _passwordTextField.delegate = self;
    }
    return _passwordTextField;
}

- (UIButton *)loginBtn{
    if (!_loginBtn) {
        _loginBtn = [UIButton yc_customTextWithTitle:@"登录" titleColor:[UIColor whiteColor] fontSize:[UIFont yc_18] target:self action:@selector(loginBtnClicked)];
        _loginBtn.backgroundColor = [UIColor yc_hex_CACCD4];
        _loginBtn.userInteractionEnabled = NO;
        [_loginBtn yc_radiusWithRadius:2];

    }
    return _loginBtn;
}

@end
