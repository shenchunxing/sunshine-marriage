//
//  Target_SMLogin.h
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/22.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SMNavigationController.h"

NS_ASSUME_NONNULL_BEGIN

@interface Target_SMLogin : NSObject

- (SMNavigationController *)Action_nativeLoginViewController:(NSDictionary *)params ;

@end

NS_ASSUME_NONNULL_END
