//
//  SMWebViewController.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/30.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMWebViewController.h"
#import <Masonry/Masonry.h>
#import "SMDefine.h"
#import <WebKit/WebKit.h>
#import "YCCategoryModule.h"

@interface SMWebViewController ()<WKNavigationDelegate,WKUIDelegate>
@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UIProgressView *progressView;
@end

@implementation SMWebViewController

- (instancetype)initWithTitle:(NSString *)title webURLString:(NSString *)webURLString {
    if (self = [super init]) {
        self.title = title;
        self.webURLString = webURLString;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self yc_setupTitleViewWithTitle:self.title];
    [self addWebview];
    [self.webView addObserver:self forKeyPath:@"estimatedProgress" options:0 context:nil];
    [self.webView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:NSStringFromSelector(@selector(estimatedProgress))]
        && object == _webView) {
        self.progressView.progress = _webView.estimatedProgress;
        if (_webView.estimatedProgress >= 1.0f) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.progressView.progress = 0;
            });
        }
    }else if([keyPath isEqualToString:@"title"]
             && object == _webView){
        self.navigationBar.title = _webView.title;
            }else{
                [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
            }
        }

- (void)addWebview {
    WKWebViewConfiguration *webConfiguration = [WKWebViewConfiguration new];
    self.webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, kNavigationHeight, kWIDTH, kHEIGHT - kNavigationHeight) configuration:webConfiguration];
    self.webView.navigationDelegate = self;
    [self.view addSubview:self.webView];
    
    self.progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
    self.progressView.frame = CGRectMake(0, kNavigationHeight, kWIDTH, 10);
    [self.view addSubview:self.progressView];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    CGAffineTransform transform = CGAffineTransformMakeScale(2, 2);
    self.activityIndicator.transform = transform;
    [self.view addSubview:self.activityIndicator];
    self.activityIndicator.size = CGSizeMake(50, 50);
    self.activityIndicator.center = self.webView.center;
    [self.activityIndicator startAnimating];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:self.webURLString]];
    [self.webView loadRequest:request];

}

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    [self.activityIndicator stopAnimating];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [self.activityIndicator stopAnimating];
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
   
}

- (void)dealloc {
    [_webView removeObserver:self forKeyPath:NSStringFromSelector(@selector(estimatedProgress))];
    [_webView removeObserver:self forKeyPath:NSStringFromSelector(@selector(title))];
}

@end
