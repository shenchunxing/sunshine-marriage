//
//  SMLoginViewController.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/22.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "SMLoginViewController.h"
#import "UIColor+Style.h"
#import "UIFont+Style.h"
#import <Masonry/Masonry.h>
#import "UILabel+Extension.h"
#import "UIButton+Extension.h"
#import "UIButton+ImagePosition.h"
#import "SMDefine.h"
#import <TTTAttributedLabel/TTTAttributedLabel.h>
#import <WXApi.h>
#import "SMWebViewController.h"
#import "SMLoginAPI.h"
#import "YCMediator+SMTabBar.h"
#import "UIViewController+CustomToast.h"
#import "YCDataCenter.h"
#import "YCRequestModel.h"
#import "TTTAttributedLabel+Extension.h"

#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import "SMAppUser.h"
#import "SMImprovePersonInfoViewController.h"

static NSString *const webUrl = @"http://static.yangguanghy.com/agreementForApp/";

@interface SMLoginViewController ()<TTTAttributedLabelDelegate,TencentSessionDelegate>
/// 微信/qq快速登录
@property (nonatomic, strong) UILabel *loginNameLabel;
/// 微信按钮
@property (nonatomic, strong) UIButton *weChatButton;
/// qq按钮
@property (nonatomic, strong) UIButton *qqButton;
/// 协议
@property (nonatomic, strong) TTTAttributedLabel *protocolLabel;
@property (nonatomic, strong) SMLoginAPI *loginAPI;

@property (nonatomic,strong) TencentOAuth *tencentOAuth;
@end

@implementation SMLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationBar.hidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(weChatRequest) name:@"weiChatOK" object:nil];
}

- (void)initView {
    [self.view addSubview:self.loginNameLabel];
    [self.loginNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(77.5+kNavigationHeight);
        make.left.mas_equalTo(42.5);
    }];
    
    [self.view addSubview:self.weChatButton];
    [self.weChatButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.loginNameLabel.mas_bottom).mas_offset(108.5);
        make.left.mas_equalTo(43);
        make.height.mas_equalTo(45);
        make.right.mas_offset(-43);
    }];
    
    [self.view addSubview:self.qqButton];
    [self.qqButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.weChatButton.mas_bottom).mas_offset(25);
        make.left.mas_equalTo(43);
        make.height.mas_equalTo(45);
        make.right.mas_offset(-43);
    }];
    
    [self.view addSubview:self.protocolLabel];
    [self.protocolLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_bottom).mas_offset(-50 -11-kIphoneXBottom);
        make.centerX.mas_equalTo(self.view);
    }];

}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)weChatRequest {;
    [self.loginAPI start];
}

/// 点击微信登录
- (void)weChat {
    if([WXApi isWXAppInstalled]){//判断用户是否已安装微信App
        SendAuthReq *req = [[SendAuthReq alloc] init];
        req.state = @"wx_oauth_authorization_state";//用于保持请求和回调的状态，授权请求或原样带回
        req.scope = @"snsapi_userinfo";//授权作用域：获取用户个人信息
        //唤起微信
        [WXApi sendReq: req completion:nil];
    }else{
        [self showCustomMessage_SM:@"未安装微信应用或版本过低"];
    }
}

- (UILabel *)loginNameLabel {
    if (!_loginNameLabel) {
        _loginNameLabel = [UILabel yc_labelWithText:@"微信/QQ登录" textFont:[UIFont yc_27_bold] textColor:[UIColor yc_hex_333333]];
    }
    return _loginNameLabel;
}

- (UIButton *)weChatButton {
    if (!_weChatButton) {
        _weChatButton = [UIButton yc_buttonWithType:UIButtonTypeCustom title:@"微信快速登录" titleColor:[UIColor whiteColor] fontSize:[UIFont yc_15_bold] bgImage:nil image:[UIImage imageNamed:@""] target:self action:@selector(weChat)];
        _weChatButton.layer.cornerRadius = 4;
        _weChatButton.backgroundColor = [UIColor yc_hex_28B836];
//        _weChatButton.imageEdgeInsets = UIEdgeInsetsMake(11, 0, 11, 0);
//        [_weChatButton yc_imagePosition:YCImagePositionLeft space:9.5];
    }
    return _weChatButton;
}

- (UIButton *)qqButton {
    if (!_qqButton) {
        _qqButton = [UIButton yc_buttonWithType:UIButtonTypeCustom title:@"QQ快速登录" titleColor:[UIColor whiteColor] fontSize:[UIFont yc_15_bold] bgImage:nil image:[UIImage imageNamed:@""] target:self action:@selector(qqLoginBtnAction)];
        _qqButton.layer.cornerRadius = 4;
        _qqButton.backgroundColor = [UIColor yc_hex_008CFF];
//        _qqButton.imageEdgeInsets = UIEdgeInsetsMake(11, 0, 11, 0);
//        [_qqButton yc_imagePosition:YCImagePositionLeft space:9.5];
    }
    return _qqButton;
}

- (TTTAttributedLabel *)protocolLabel {
    if (!_protocolLabel) {
        _protocolLabel = [TTTAttributedLabel attributeLabelWithText:@"登录代表您已同意《用户协议》" linkAttribute:@"《用户协议》" delegate:self];
    }
    return _protocolLabel;
}

- (SMLoginAPI *)loginAPI {
    if (!_loginAPI) {
        _loginAPI = [[SMLoginAPI alloc] init];
        @weakify(self);
        _loginAPI.apiSuccessHandler = ^(__kindof YCBaseAPI * _Nonnull api, id  _Nonnull response) {
            @strongify(self);
            [self saveloginInfo:response] ;
            if ([YCDataCenter sharedData].nickname && ![[YCDataCenter sharedData].nickname isEqualToString:@""])  [self jumpToTabBarController];
            else [self jumpToImprove];
           
        };
        _loginAPI.presenter = [YCAlertHUDPresnter HUDWithView:self.view];
    }
    return _loginAPI;
}

- (void)jumpToImprove {
    SMImprovePersonInfoViewController *improve = [[SMImprovePersonInfoViewController alloc] init];
    [self.navigationController pushViewController:improve animated:YES];
    [improve showCustomMessage_SM:@"登录成功"];
}

#pragma mark - TTTAttributedLabelDelegate
- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    SMWebViewController *web = [[SMWebViewController alloc] initWithTitle:@"用户协议" webURLString:webUrl];
    [self.navigationController pushViewController:web animated:YES];
}

- (void)jumpToTabBarController {
    UIViewController *vc = [[YCMediator sharedInstance] tabBarControllerWithParmas:nil];
    [UIApplication sharedApplication].delegate.window.rootViewController = vc;
    [vc showCustomMessage_SM:@"登录成功"];
}

- (void)saveloginInfo:(id)response {
    NSDictionary *resBizMap = response[@"resBizMap"];
    SMAppUser *user = [SMAppUser yy_modelWithJSON:resBizMap];
    [YCDataCenter sharedData].authorization = user.authorization;
    [YCDataCenter sharedData].nickname = user.appUser.nickName;
    [YCDataCenter sharedData].insertTime = user.appUser.insertTime;
    [YCDataCenter sharedData].shareId = user.appUser.shareId;
}


/// QQ登录
- (void)qqLoginBtnAction{
    if ([TencentOAuth iphoneQQInstalled]) {
        self.tencentOAuth = [[TencentOAuth alloc]initWithAppId:@"101801428" andDelegate:self];
        NSMutableArray *permission = [@[] mutableCopy];
        permission = [NSMutableArray arrayWithObjects:@"get_user_info",@"get_simple_userinfo",nil];
        [self.tencentOAuth authorize:permission inSafari:NO];
    }else{
        [self showCustomMessage_SM:@"未安装QQ应用或版本过低"];
    }
}

#pragma mark --------- qq登录状态回调  TencentSessionDelegate------
- (void)tencentDidLogin {
    if (_tencentOAuth.accessToken) {
        [YCDataCenter sharedData].appusercode = _tencentOAuth.openId ;//2F0975AF89950B58687AECA51F1AC802
        [YCDataCenter sharedData].thirdType = @"qq";
        
        [self.tencentOAuth getUserInfo];
    }else{
        [self showCustomMessage_SM:@"登录失败！没有获取到accessToken"];
    }
}

/// 登录失败后的回调
- (void)tencentDidNotLogin:(BOOL)cancelled{

}

/// 登录时网络有问题的回调
- (void)tencentDidNotNetWork {
    
}

/// 取得用户信息的回调
- (void)getUserInfoResponse:(APIResponse *)response {
    //保存头像
    [YCDataCenter sharedData].headimgurl = response.jsonResponse[@"figureurl_qq_2"] ;
    //昵称
//    [YCDataCenter sharedData].nickname = response.jsonResponse[@"nickname"];
    //性别
    [YCDataCenter sharedData].sex = [response.jsonResponse[@"gender"] isEqualToString:@"男"] ? 1:2;

    [self.loginAPI start];
}

@end
