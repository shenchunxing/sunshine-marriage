//
//  Target_SMLogin.m
//  SunshineMarriage
//
//  Created by 沈春兴 on 2020/1/22.
//  Copyright © 2020 ShenChunXing. All rights reserved.
//

#import "Target_SMLogin.h"
#import "SMLoginViewController.h"

@implementation Target_SMLogin

- (SMNavigationController *)Action_nativeLoginViewController:(NSDictionary *)params {
    SMLoginViewController *vc = [[SMLoginViewController alloc] init];
    return [[SMNavigationController alloc] initWithRootViewController:vc];
}

@end
